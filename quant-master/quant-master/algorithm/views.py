from django.shortcuts import render
from django.http import HttpResponse
from algorithm.models import *

import numpy as np

from sklearn import metrics
from sklearn.linear_model import LinearRegression

stock = [] #股票型
stock_net_value = []
grade_stock=[]
bond = [] #债券型
bond_net_value = []
grade_bond=[]

year = '2019'#所选年份
start = year+'-04-19'
end = year+'-04-29'
startY = year+'-01-01'
endY = year+'-12-31'

#四个权重,目前设为定值
Weight1 = 0.5
Weight2 = 1-Weight1
Weight3 = 0.5
Weight4 = 1-Weight3

index_return_rateY = 0.0397#国债年收益率
index_return_rate = index_return_rateY/250#指数收益率(天）,国债年收益率/250=3.97%/250=0.0001588
index_return_rateW = index_return_rateY/50#指数收益率(周）,国债年收益率/250*5=3.97%/50=0.000794

class Rate:
    def _init_(self, code, rate,excess_rate,dimension1):
        self.code = code
        self.rate = rate#收益率
        self.excess_rate = excess_rate#超额收益率
        self.dimension1 = dimension1#维度一

class R_squared:
    def _init_(self, code, r_squared, dimension2):
        self.code = code
        self.r_squared = r_squared#解释度稳定性
        self.dimension2 = dimension2#维度二

class Parameter:
    def _init_(self, code, standard_deviation, max_drawdown, dimension3, annualized_returns):
        self.code = code
        self.standard_deviation=standard_deviation#标准差(每日收益的年化标准差）
        self.max_drawdown = max_drawdown#最大回撤率
        self.dimension3 = dimension3#维度三
        self.annualized_returns = annualized_returns#年化收益率

class Sharpe_ratio:
    def _init_(self, code, sharpe_ratio, dimension4):
        self.code = code
        self.sharpe_ratio = sharpe_ratio#夏普比率
        self.dimension4 = dimension4#维度四

class GradeQH:
    def _init_(self, code, sharpe_ratio, total_tna, grade1, grade2, grade):
        self.code = code
        self.sharpe_ratio = sharpe_ratio#夏普比率
        self.total_tna = total_tna#期末基金资产净值
        self.grade1 = grade1
        self.grade2 = grade2
        self.grade =grade#清华算法分值

class Grade:
    def _init_(self, code, grade, grade1, grade2, grade3, grade4):
        self.code = code
        self.grade = grade
        self.grade1 = grade1
        self.grade2 = grade2
        self.grade3 = grade3
        self.grade4 = grade4

#选择相应种类的基金
fund_info = fund_info.objects.all()
for info in fund_info:
    if info.operate_mode_id != 401003 and (not bool(info.end_date)):
        if info.underlying_asset_type_id == 402001:
            stock.append(info)
        elif info.underlying_asset_type_id == 402003:
            bond.append(info)
'''
#测试
count_stock = 0;
count_bond = 0;
for info in stock:
    print("code:",info.main_code,",name:",info.name,"end_date:",info.end_date,"分类:",info.operate_mode,info.underlying_asset_type)
    count_stock=count_stock + 1;
for info in bond:
    print("code:",info.main_code,",name:",info.name,"end_date:",info.end_date,",分类:",info.operate_mode,info.underlying_asset_type)
    count_bond = count_bond + 1;
print("股票型基金个数:",count_stock,",债券型基金个数:",count_bond,",基金总数:",count_stock+count_bond)
'''
#获取净值数据
for info in bond:
    dict = fund_net_value.objects.filter(code=info.main_code).order_by('-day')
    if dict == None:
        pass  # 588090
    else:
        bond_net_value.append(dict)

for info in stock:
    dict = fund_net_value.objects.filter(code=info.main_code).order_by('-day')
    if dict == None:
        pass  # 588090
    else:
        stock_net_value.append(dict)
'''
#测试
print("股票型:")
for i in stock_net_value:
    for info in i:
        print("code:", info.code, ",day:", info.day, ",net_value:", info.net_value)
print("债券型:")
for i in bond_net_value:
    for info in i:
        print("code:", info.code, ",day:", info.day, ",net_value:", info.net_value)
'''
#获取测试数据
test1_net_value = []
test2_net_value = []
test3_net_value = []
test4_net_value = []
for i in stock_net_value:
    test = [];test1=[];test3 = [];n=[];rate3 = [];downside_risk3=[]
    num = -1;past = 0.0
    for info in i:
        if num==-1:
            test3.append(info.code)#test3[0]是这个基金的编号code
            num=num+1
        if(info.day >= startY and info.day <= endY):
            test.append(info.net_value)
            num = num + 1
            if num == 1:
                end_net_value = info.net_value#期末净值
            else:
                day_rate = (end_net_value - info.net_value)/info.net_value #info.day这一天的收益率
                if(day_rate >= index_return_rate):
                    risk = 0.0
                else:
                    risk = day_rate - index_return_rate
                rate3.append(day_rate)
                downside_risk3.append(risk)
                end_net_value = info.net_value
                if (info.day >= start and info.day < end):
                    test1.append(info)
            n.append(num)
    if num>=230:#判断是否得到数据库中一年的数据
        test1_net_value.append(test1)#获取测试数据(一周的基金净值）
        test3.append(test)#test3[1]是这个基金这一年的净值数组
        test3.append(n)#test3[2]是这个基金这一年净值的编号数组
        test3.append(rate3)#test3[3]是这个基金这一年的收益率数组（每天）
        test3.append(downside_risk3)#test3[4]是这个基金这一年的下行风险数组（每天）
        test3_net_value.append(test3)

for i in bond_net_value:
    test = [];test2=[];test4 = [];n=[];rate4 = [];downside_risk4=[]
    num = -1;past = 0.0
    for info in i:
        if num==-1:
            test4.append(info.code)
            num=num+1
        if(info.day >= startY and info.day <= endY):
            test.append(info.net_value)
            num = num + 1
            if num == 1:
                end_net_value = info.net_value#期末净值
            else:
                day_rate = (end_net_value - info.net_value)/info.net_value #info.day这一天的收益率
                if(day_rate >= index_return_rate):
                    risk = 0.0
                else:
                    risk = day_rate - index_return_rate
                rate4.append(day_rate)
                downside_risk4.append(risk)
                end_net_value = info.net_value
                if (info.day >= start and info.day < end):
                    test2.append(info)
            n.append(num)
    if num>=230:#判断是否得到数据库中一年的数据
        test2_net_value.append(test2)#获取测试数据(一周的基金净值）
        test4.append(test)#test4[1]是这个基金这一年的净值数组
        test4.append(n)#test4[2]是这个基金这一年净值的编号数组
        test4.append(rate4)#test4[3]是这个基金这一年的收益率数组（每天）
        test4.append(downside_risk4)#test4[4]是这个基金这一年的下行风险数组（每天)
        test4_net_value.append(test4)
'''
#测试(一周的基金净值）
count_stock = 0;
count_bond = 0;
print("股票型:")
for i in test1_net_value:
    for info in i:
        print("code:", info.code, ",day:", info.day, ",net_value:", info.net_value)
        break
    count_stock=count_stock + 1;
print("债券型:")
for i in test2_net_value:
    for info in i:
        print("code:", info.code, ",day:", info.day, ",net_value:", info.net_value)
        break
    count_bond=count_bond + 1;
print("股票型基金个数:",count_stock,",债券型基金个数:",count_bond,",基金总数:",count_stock+count_bond)
#测试(一年的相关数据）
print("股票型:")
for info in test3_net_value:
    print("code:", info[0], ",总数:",len(info[1]))
print("债券型:")
for info in test4_net_value:
    print("code:", info[0], ",总数:",len(info[1]))
print("股票型:")
for info in test3_net_value:
    print("code:", info[0], ",net_value:", info[1], ",编号数组:", info[2], "收益率数组:",info[3], "下行风险数组:",info[4])
print("债券型:")
for info in test4_net_value:
    print("code:", info[0], ",net_value:", info[1], ",编号数组:", info[2], "收益率数组:",info[3], "下行风险数组:",info[4])
'''
#计算维度一
Dimension1_stock = []#（股票型）维度一列表
Dimension1_bond = []#(债券型)维度一列表
for test in test1_net_value:
    r=Rate()
    i=1
    for info in test:
        r.code = info.code
        if(i==1):
            a = info.net_value
        if(i==len(test)):
            b = info.net_value
        i = i + 1
    r.rate = (a-b)/b
    r.excess_rate = r.rate - index_return_rateW
    r.dimension1=Weight1*r.rate+Weight2*r.excess_rate#维度1 = w1 * 收益率 + w2 * 超额收益率
    Dimension1_stock.append(r)

for test in test2_net_value:
    r=Rate()
    i=1
    for info in test:
        r.code = info.code
        if(i==1):
            a = info.net_value
        if(i==len(test)):
            b = info.net_value
        i = i + 1
    r.rate = (a-b)/b
    r.excess_rate = r.rate - index_return_rateW
    r.dimension1=Weight1*r.rate+Weight2*r.excess_rate#维度1 = w1 * 收益率 + w2 * 超额收益率
    Dimension1_bond.append(r)
'''
#测试
print("股票型:")
for r in Dimension1_stock:
    print("code:", r.code, ",rate:", r.rate, ",excess_rate:", r.excess_rate,",维度1:", r.dimension1)
print("债券型:")
for r in Dimension1_bond:
    print("code:", r.code, ",rate:", r.rate, ",excess_rate:", r.excess_rate,",维度1:", r.dimension1)
'''
Dimension1_stock.sort(key=lambda Rate: Rate.dimension1, reverse=True)
Dimension1_bond.sort(key=lambda Rate: Rate.dimension1, reverse=True)
#按照比例值分配分数
G = 10.0;#维度一总分数
count = len(Dimension1_stock)
for r in Dimension1_stock:
    g=Grade()
    g.code=r.code
    g.grade1=count*G/len(Dimension1_stock)
    count = count -1
    grade_stock.append(g)

count = len(Dimension1_bond)
for r in Dimension1_bond:
    g=Grade()
    g.code=r.code
    g.grade1=count*G/len(Dimension1_bond)
    count = count -1
    grade_bond.append(g)
'''
#测试
print("维度一:\n股票型:")
for g in grade_stock:
    print("code:", g.code, ",grade1:", g.grade1)
print("债券型:")
for g in grade_bond:
    print("code:", g.code, ",grade1:", g.grade1)
'''
#求维度二（解释度稳定性R-squared）
Dimension2_stock = []#（股票型）维度二列表
Dimension2_bond = []#(债券型)维度二列表
for info in test3_net_value:
    xtrain = np.array(info[2])
    ytrain = np.array(info[1])
    model = LinearRegression()
    model.fit(xtrain[:,np.newaxis],ytrain)
    ytest = model.predict(xtrain[:,np.newaxis])#yteset=根据X值评估出来的Y值
    mse = metrics.mean_absolute_error(ytrain,ytest)#均方差
    rmse = np.sqrt(mse) #均方根
    #print(mse)#print(rmse)
    ssr = ((ytest-ytrain.mean())**2).sum()
    sst = ((ytrain-ytrain.mean())**2).sum()
    r=R_squared()
    r.code=info[0]
    r.r_squared = ssr/sst
    r.dimension2=r.r_squared#维度二只考虑解释度稳定性
    Dimension2_stock.append(r)

for info in test4_net_value:
    xtrain = np.array(info[2])
    ytrain = np.array(info[1])
    model = LinearRegression()
    model.fit(xtrain[:,np.newaxis],ytrain)
    ytest = model.predict(xtrain[:,np.newaxis])#yteset=根据X值评估出来的Y值
    mse = metrics.mean_absolute_error(ytrain,ytest)#均方差
    rmse = np.sqrt(mse) #均方根
    #print(mse)#print(rmse)
    ssr = ((ytest-ytrain.mean())**2).sum()
    sst = ((ytrain-ytrain.mean())**2).sum()
    r=R_squared()
    r.code = info[0]
    r.r_squared = ssr/sst
    r.dimension2=r.r_squared#维度二只考虑解释度稳定性
    Dimension2_bond.append(r)
'''
#测试
print("股票型:")
for r in Dimension2_stock:
    print("code:", r.code, ",维度2:", r.dimension2)
print("债券型:")
for r in Dimension2_bond:
    print("code:", r.code, ",维度2:", r.dimension2)
'''
Dimension2_stock.sort(key=lambda R_squared: R_squared.dimension2, reverse=True)
Dimension2_bond.sort(key=lambda R_squared: R_squared.dimension2, reverse=True)
#按照比例值分配分数
G = 5.0;#维度二总分数
count = len(Dimension2_stock)
for r in Dimension2_stock:
    for g in grade_stock:
        if(g.code == r.code):
            g.grade2 = count*G/len(Dimension2_stock)
            count = count - 1

count = len(Dimension2_bond)
for r in Dimension2_bond:
    for g in grade_bond:
        if(g.code == r.code):
            g.grade2 = count*G/len(Dimension2_bond)
            count = count -1
'''
#测试
grade_stock.sort(key=lambda Grade: Grade.grade2, reverse=True)
grade_bond.sort(key=lambda Grade: Grade.grade2, reverse=True)
print("维度二:\n股票型:")
for g in grade_stock:
    print("code:", g.code, ",grage2:", g.grade2)
print("债券型:")
for g in grade_bond:
    print("code:", g.code, ",grage2:", g.grade2)
'''
#求维度三
Dimension3_stock = []#（股票型）维度三列表
Dimension3_bond = []#(债券型)维度三列表
for info in test3_net_value:
    p=Parameter()
    p.code=info[0]
    #求最大回撤率
    list=info[1]
    maxac=np.zeros(len(list))
    b=list[0]
    for i in range(0,len(list)): #遍历数组，当其后一项大于前一项时，赋值给b
        if list[i]>b:
            b=list[i]
        maxac[i]=b
    #print(maxac)
    i = np.argmax((maxac-list)/maxac) #结束位置
    if i != 0:
        j = np.argmax(list[:i])  # 开始位置
        max=(list[j] - list[i]) / (list[j])
    else:
        max=0
    p.max_drawdown=max
    #print("code:",p.code,"max_drawdown:",max)
    #求年化标准差
    rate_standard_deviation=np.std(np.array(info[3]))#收益率标准差
    down_standard_deviation=np.std(np.array(info[4]))#年化下行标准差
    #print("收益率标准差:",rate_standard_deviation,"年化下行标准差:",down_standard_deviation)
    p.standard_deviation = rate_standard_deviation/down_standard_deviation
    p.dimension3=Weight3*p.max_drawdown+Weight4*p.standard_deviation
    #print("年化标准差:", p.standard_deviation, "最大回撤:", p.max_drawdown)
    startY_net_value=info[1][len(info[1]) - 1]
    endY_net_value=info[1][0]
    p.annualized_returns = (endY_net_value / startY_net_value - 1)*250/len(info[1])
    #print("这一年的期初净值:", startY_net_value, ",这一年的期末净值:", endY_net_value,",年化收益率:", p.annualized_returns)
    Dimension3_stock.append(p)

for info in test4_net_value:
    p=Parameter()
    p.code=info[0]
    # 求最大回撤率
    list=info[1]
    maxac=np.zeros(len(list))
    b=list[0]
    for i in range(0,len(list)): #遍历数组，当其后一项大于前一项时，赋值给b
        if list[i]>b:
            b=list[i]
        maxac[i]=b
    #print(maxac)
    i = np.argmax((maxac-list)/maxac) #结束位置
    if i != 0:
        j = np.argmax(list[:i])  # 开始位置
        max=(list[j] - list[i]) / (list[j])
    else:
        max=0
    p.max_drawdown=max
    #print("code:",p.code,"max_drawdown:",max)
    #求年化标准差
    rate_standard_deviation=np.std(np.array(info[3]))#收益率标准差
    down_standard_deviation=np.std(np.array(info[4]))#年化下行标准差
    #print("收益率标准差:",rate_standard_deviation,"年化下行标准差:",down_standard_deviation)
    p.standard_deviation = rate_standard_deviation/down_standard_deviation
    p.dimension3=Weight3*p.max_drawdown+Weight4*p.standard_deviation
    #print("年化标准差:", p.standard_deviation, "最大回撤:", p.max_drawdown)
    startY_net_value = info[1][len(info[1]) - 1]
    endY_net_value = info[1][0]
    p.annualized_returns = (endY_net_value / startY_net_value - 1) * 250 / len(info[1])
    #print("这一年的期初净值:", startY_net_value, ",这一年的期末净值:", endY_net_value,",年化收益率:", p.annualized_returns)
    Dimension3_bond.append(p)
'''
#测试
print("股票型:")
for p in Dimension3_stock:
    #print("code:",p.code,"年化标准差:", p.standard_deviation, "最大回撤:", p.max_drawdown,",年化收益率:", p.annualized_returns,"维度三:", p.dimension3)
print("债券型:")
for p in Dimension3_bond:
    #print("code:",p.code,"年化标准差:", p.standard_deviation, "最大回撤:", p.max_drawdown,",年化收益率:", p.annualized_returns,"维度三:", p.dimension3)
'''
Dimension3_stock.sort(key=lambda Parameter: Parameter.dimension3)
Dimension3_bond.sort(key=lambda Parameter: Parameter.dimension3)
#按照比例值分配分数
G=10.0;#维度三总分数
count=len(Dimension3_stock)
for p in Dimension3_stock:
    for g in grade_stock:
        if(g.code == p.code):
            g.grade3 = count*G/len(Dimension3_stock)
            count = count - 1

count=len(Dimension3_bond)
for p in Dimension3_bond:
    for g in grade_bond:
        if(g.code == p.code):
            g.grade3 = count*G/len(Dimension3_bond)
            count = count -1
'''
#测试
grade_stock.sort(key=lambda Grade: Grade.grade3, reverse=True)
grade_bond.sort(key=lambda Grade: Grade.grade3, reverse=True)
print("维度三:\n股票型:")
for g in grade_stock:
    print("code:", g.code, ",grade3:", g.grade3)
print("债券型:")
for g in grade_bond:
    print("code:", g.code, ",grade3:", g.grade3)
'''
#求维度四（夏普比率）
Dimension4_stock = []#（股票型）维度四列表
Dimension4_bond = []#(债券型)维度四列表
for p in Dimension3_stock:
    s = Sharpe_ratio()
    s.code = p.code
    #夏普比率=(策略年化收益率 - 回测起始交易日的无风险利率) / 策略收益波动率(用策略每日收益的年化标准差代替)
    s.sharpe_ratio = (p.annualized_returns - index_return_rateY)/p.standard_deviation
    s.dimension4 = s.sharpe_ratio#维度四只考虑夏普比率
    Dimension4_stock.append(s)
for p in Dimension3_bond:
    s = Sharpe_ratio()
    s.code = p.code
    #夏普比率=(策略年化收益率 - 回测起始交易日的无风险利率) / 策略收益波动率(用策略每日收益的年化标准差代替)
    s.sharpe_ratio = (p.annualized_returns - index_return_rateY)/p.standard_deviation
    s.dimension4 = s.sharpe_ratio#维度四只考虑夏普比率
    Dimension4_bond.append(s)
'''
#测试
print("股票型:")
for s in Dimension4_stock:
    print("code:", s.code, ",维度四:", s.dimension4)
print("债券型:")
for s in Dimension4_bond:
    print("code:", s.code, ",维度四:", s.dimension4)
'''
Dimension4_stock.sort(key=lambda Sharpe_ratio: Sharpe_ratio.dimension4, reverse=True)
Dimension4_bond.sort(key=lambda Sharpe_ratio: Sharpe_ratio.dimension4, reverse=True)
#按照比例值分配分数
G=10.0;#维度四总分数
count=len(Dimension4_stock)
for s in Dimension4_stock:
    for g in grade_stock:
        if(g.code == s.code):
            g.grade4 = count*G/len(Dimension4_stock)
            count = count - 1

count=len(Dimension4_bond)
for s in Dimension4_bond:
    for g in grade_bond:
        if(g.code == s.code):
            g.grade4 = count*G/len(Dimension4_bond)
            count = count -1
'''
#测试
grade_stock.sort(key=lambda Grade: Grade.grade4, reverse=True)
grade_bond.sort(key=lambda Grade: Grade.grade4, reverse=True)
print("维度四:\n股票型:")
for g in grade_stock:
    print("code:", g.code, ",grade4:", g.grade4)
print("债券型:")
for g in grade_bond:
    print("code:", g.code, ",grade4:", g.grade4)
'''
#获取年末基金规模数据
bond_fin_indicator=[]
stock_fin_indicator=[]
test5 = []
test6 = []
for info in Dimension4_stock:
    dict = fund_fin_indicator.objects.filter(code=info.code).order_by('-id')
    if dict == None:
        pass  # 588090
    else:
        stock_fin_indicator.append(dict)

for info in Dimension4_bond:
    dict = fund_fin_indicator.objects.filter(code=info.code).order_by('-id')
    if dict == None:
        pass  # 588090
    else:
        bond_fin_indicator.append(dict)

for i in stock_fin_indicator:
    flag = 0
    for info in i:
        if(info.report_type_id == 403006 and info.period_start == startY and flag==0):
            test5.append(info)
            flag = 1
        elif(info.report_type_id == 403004 and info.period_end == endY and flag==0):
            test5.append(info)
            flag = 1

for i in bond_fin_indicator:
    flag = 0
    for info in i:
        if (info.report_type_id == 403006 and info.period_start == startY and flag==0):
            test6.append(info)
            flag = 1
        elif (info.report_type_id == 403004 and info.period_end == endY and flag == 0):
            test6.append(info)
            flag = 1
'''
#测试
print("股票型:")
for info in test5:
    print("code:", info.code, ",report_type:", info.report_type,",period_start",info.period_start,",total_tna:", info.total_tna)
print("债券型:")
for info in test6:
    print("code:", info.code, ",report_type:", info.report_type,",period_start",info.period_start,",total_tna:", info.total_tna)
'''
gradeQH_stock=[]
gradeQH_bond=[]
for s in Dimension4_stock:
    g = GradeQH()
    g.code = s.code
    g.sharpe_ratio = s.sharpe_ratio
    for info in test5:
        if info.code == g.code:
            g.total_tna = info.total_tna
            gradeQH_stock.append(g)
            break;

for s in Dimension4_bond:
    g = GradeQH()
    g.code = s.code
    g.sharpe_ratio = s.sharpe_ratio
    for info in test6:
        if info.code == g.code:
            g.total_tna = info.total_tna
            gradeQH_bond.append(g)
            break;
'''
#测试
print("股票型:")
for g in gradeQH_stock:
    print("code:", g.code, ",夏普比率:", g.sharpe_ratio, ",期末基金资产净值:", g.total_tna)
print("债券型:")
for g in gradeQH_bond:
    print("code:", g.code, ",夏普比率:", g.sharpe_ratio, ",期末基金资产净值:", g.total_tna)
'''
gradeQH_stock.sort(key=lambda GradeQH: GradeQH.total_tna, reverse=True)
gradeQH_bond.sort(key=lambda GradeQH: GradeQH.total_tna, reverse=True)
#求出清华算法分值并排序
G = 10.0;#总分数
count = len(gradeQH_stock)
for s in gradeQH_stock:
    for g in grade_stock:
        if(g.code == s.code):
            s.grade1 = g.grade4
            s.grade2 = count*G/len(gradeQH_stock)
            #print(s.grade2)
            s.grade = 0.2 * s.grade1 + 0.8 * s.grade2
            count = count - 1

count = len(gradeQH_bond)
for s in gradeQH_bond:
    for g in grade_bond:
        if(g.code == s.code):
            s.grade1 = g.grade4
            s.grade2 = count*G/len(gradeQH_bond)
            #print(s.grade2)
            s.grade = 0.2 * s.grade1 + 0.8 * s.grade2
            count = count - 1
gradeQH_stock.sort(key=lambda GradeQH: GradeQH.grade, reverse=True)
gradeQH_bond.sort(key=lambda GradeQH: GradeQH.grade, reverse=True)
'''
#测试
print("清华算法排序表:\n股票型:")
for g in gradeQH_stock:
    print("code:", g.code, ",grade:", g.grade)
print("债券型:")
for g in gradeQH_bond:
    print("code:", g.code, ",grade:", g.grade)
'''
#求出总分并排序，得到股票型/债券型基金排序表
for g in grade_stock:
    g.grade = g.grade1+g.grade2+g.grade3+g.grade4
for g in grade_bond:
    g.grade = g.grade1+g.grade2+g.grade3+g.grade4
grade_stock.sort(key=lambda Grade: Grade.grade, reverse=True)
grade_bond.sort(key=lambda Grade: Grade.grade, reverse=True)
'''
#测试
print("电科算法排序表:（总分）\n股票型:")
for g in grade_stock:
    print("code:", g.code, ",grade:", g.grade)
print("债券型:")
for g in grade_bond:
    print("code:", g.code, ",grade:", g.grade)
'''
def update_data():
    for g in grade_stock:
        stock0 = stock_grade(code=g.code, grade=g.grade)
        stock0.save()
    for g in grade_bond:
        bond0 = bond_grade(code=g.code, grade=g.grade)
        bond0.save()
    return 0

def update_data2():
    for g in gradeQH_stock:
        stock0 = stock_gradeQH(code=g.code, grade=g.grade)
        stock0.save()
    for g in gradeQH_bond:
        bond0 = bond_gradeQH(code=g.code, grade=g.grade)
        bond0.save()
    return 0

# update_data()
# update_data2()