from django.db import models
from mongoengine import *
# Create your models here.
connect('data', host='47.108.25.113', port=29999, username='test', password='8035test')

class fund_info(Document):
    _id = StringField()
    id = IntField()
    main_code = StringField()
    name = StringField()
    advisor = StringField()
    trustee = StringField()
    operate_mode_id = IntField()
    operate_mode = StringField()
    underlying_asset_type_id = IntField()
    underlying_asset_type = StringField()
    start_date = IntField()
    end_date = IntField()
    meta = {'collection': 'fund_info', 'managed': 'False'}

class fund_net_value(Document):
    _id = StringField()
    id = IntField()
    code = StringField()
    day = StringField()
    net_value = FloatField()
    sum_value = FloatField()
    factor = FloatField()
    acc_factor = FloatField()
    refactor_net_value = FloatField()
    meta = {'collection': 'fund_net_value', 'managed': 'False'}

class fund_fin_indicator(Document):
    _id = StringField()
    id = IntField()
    code = StringField()
    name = StringField()
    period_start = StringField()
    period_end = StringField()
    pub_date = StringField()
    report_type_id = IntField()
    report_type = StringField()
    profit = FloatField()#本期利润
    adjust_profit = FloatField()#本期利润扣减本期公允价值变动损益后的净值
    avg_profit = FloatField()#加权平均份额平均利润
    avg_roe = FloatField()
    profit_available = FloatField()
    profit_avaialbe_per_share = FloatField()
    total_tna = FloatField()#期末基金资产净值
    nav = FloatField()#期末基金份额净值
    adjust_nav = FloatField()
    nav_growth = FloatField()
    acc_nav_growth = FloatField()
    adjust_nav_growth = FloatField()
    total_asset = FloatField()
meta = {'collection': 'fund_fin_indicator', 'managed': 'False'}


class bond_grade(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'bond_grade', 'managed': 'False'}

class stock_grade(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'stock_grade', 'managed': 'False'}

# class bond_grade(Document):
#     _id = ObjectIdField()
#     code = StringField()
#     grade = FloatField()
#     meta = {'collection': 'bond_grade', 'managed': 'False'}

class stock_gradeQH(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'stock_gradeQH', 'managed': 'False'}

class bond_gradeQH(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'bond_gradeQH', 'managed': 'False'}