from . import views
from django.urls import path
app_name = 'algorithm'
urlpatterns = [
    path('update_data/', views.update_data, name='update_data'),
    path('update_data2/', views.update_data2, name='update_data2'),
]