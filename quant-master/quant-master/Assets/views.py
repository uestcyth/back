import asyncio
import base64
import calendar
import datetime
import decimal
import json
from operator import itemgetter

import aiohttp
import pymysql
from dateutil.relativedelta import relativedelta
from django.db.models import Q, Sum
from django.db.models.functions import math
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.timezone import now

#from SCCProgram.settings import CRM_URLS, DATABASES
from datamodels.models import Account, Responsible, Productofficial, Order, Asset, Accountbank, \
    Orderpayoutdetail, Ordersharechangeinfo, Ordertransfer, Orderredemption, Networth, Productbank


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        super(CustomEncoder, self).default(o)


def CustomResponse(string):
    response = HttpResponse(string)
    response.status_code = 500
    return response


# 解码
def base64_decode(string):
    if string:
        string = base64.b64decode(string).decode("utf-8")
        return string
    else:
        return None


# 缺少近一年涨幅
def get_assets(request):
    data = json.loads(request.body)
    customer_cid = data['customer_cid']
    assets = Asset.objects.filter(account_cid=customer_cid)

    product_list = []
    total_assets = 0
    buyamount = 0
    position_value = 0
    floatingprofit = 0
    interest = 0
    total_solid_assets = 0
    total_securities_assets = 0
    total_stock_assets = 0

    for a in assets:
        if a.holdshare is None:
            a.holdshare = 0
        if a.newasset is None:
            a.newasset = 0
        if a.floatmoney is None:
            a.floatmoney = 0
        if a.interest is None:
            a.interest = 0
        if a.buyamount is None:
            a.buyamount = 0
        if a.redeemedearnings is None:
            a.redeemedearnings = 0
        p = Productofficial.objects.get(crm_id=a.product_cid)
        flag = 0  # 控制append
        if p.recordtype == '类固收':
            for r in product_list:  # 计算同产品认购金额的总值
                if p.id == r['id']:
                    r['money'] = r['money'] + a.buyamount
                    flag = 1
                else:
                    pass
            if flag == 1:
                pass
            else:
                product_list.append(
                    {'name': p.name, 'abbr': p.productabbreviation, 'recordtype': p.recordtype, 'id': p.id,
                     'crmid': p.crm_id, 'comparison': p.comparison, 'comparisonabbreviation': p.comparisonabbreviation,
                     'monthlimit': p.monthlimit, 'origin': p.origin, 'stage': p.stage,
                     'money': a.buyamount})
            position_value = position_value + a.newasset
            total_assets = total_assets + a.buyamount
            total_solid_assets += a.buyamount
        elif p.recordtype == '二级市场':
            for r in product_list:
                if p == r:
                    r.money = r.money + a.buyamount
                    flag = 1
                else:
                    pass
            if flag == 1:
                pass
            else:
                product_list.append(
                    {'name': p.name, 'abbr': p.productabbreviation, 'recordtype': p.recordtype, 'id': p.id,
                     'crmid': p.crm_id, 'direction': p.direction,
                     'origin': p.origin, 'stage': p.stage, 'renewalperiod': p.renewalperiod,
                     'money': a.buyamount})
            position_value = position_value + a.newasset
            total_assets = total_assets + a.newasset
            total_securities_assets += a.newasset
        elif p.recordtype == '股权':
            for r in product_list:
                if p == r:
                    r.money = r.money + a.buyamount
                    flag = 1
                else:
                    pass
            if flag == 1:
                pass
            else:
                product_list.append(
                    {'name': p.name, 'abbr': p.productabbreviation, 'recordtype': p.recordtype, 'id': p.id,
                     'crmid': p.crm_id, 'value': p.value,
                     'origin': p.origin, 'stage': p.stage, 'money': a.buyamount})  # 无近一年涨幅
            position_value = position_value + a.newasset
            total_assets = total_assets + a.buyamount
            total_stock_assets += a.buyamount
        buyamount = buyamount + a.buyamount
        # 持仓市值position_value是三类产品的newasset之和
        floatingprofit = floatingprofit + a.floatmoney
        # 已收利息是类固收产品已收利息+已赎回二级的到账收益
        interest = interest + a.interest + a.redeemedearnings
        # 总资产
    sumNewAsset = Asset.objects.filter(account_cid=customer_cid).aggregate(sum=Sum('newasset'))
    # 持仓收益
    sumFloatmoney = Asset.objects.filter(account_cid=customer_cid).aggregate(sum=Sum('floatmoney'))
    inventory_earnings = "-"
    if sumFloatmoney['sum']:
        inventory_earnings = format(sumFloatmoney['sum'], ',')
    total_assets_a = "-"
    if sumNewAsset['sum']:
        total_assets_a = format(sumNewAsset['sum'], ',')
    results = ({'total_assets': total_assets, 'total_solid_assets': total_solid_assets,
                'total_securities_assets': total_securities_assets, 'total_stock_assets': total_stock_assets,
                'buyamount': buyamount, 'position_value': position_value,
                'floatingprofit': floatingprofit, 'interest': interest,
                'product': product_list, 'inventory_earnings':inventory_earnings ,'total_assets_a' : total_assets_a})
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 起息日 到期日 从资产表获取
def get_assets_list(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    account_cid = data['account_cid']
    assets = Asset.objects.filter(account_cid=account_cid)
    results = []
    for a in assets:
        if a.redeemedearnings is None:
            a.redeemedearnings = 0
        try:
            product = Productofficial.objects.get(crm_id=a.product_cid)
            order = Order.objects.filter(Q(account_cid=account_cid) & Q(product_cid=product.crm_id) & ~Q(ostatus="已清算")).first()
            if not order:
                date = None
                startearningday = None
                maturitydate = None
            else:
                date = order.date
                startearningday = order.startearningday
                maturitydate = order.maturitydate
            if product.recordtype == "类固收":
                results.append({
                    'id': a.id,
                    'crm_id': a.crm_id,
                    'recordtype': '类固收',
                    'name': a.name,
                    'buyamount': a.buyamount,
                    'floatmoney': a.floatmoney,
                    'interest': a.interest,
                    'comparison': product.comparison,
                    'comparisonabbreviation': product.comparisonabbreviation,
                    'startearningday': startearningday,
                    'maturitydate': maturitydate,
                })
            elif product.recordtype == "二级市场":
                results.append({
                    'id': a.id,
                    'crm_id': a.crm_id,
                    'recordtype': '二级市场',
                    'name': a.name,
                    'buyamount': a.buyamount,
                    'floatmoney': a.floatmoney,
                    'interest': a.interest + a.redeemedearnings,
                    'holdshare': a.holdshare,
                    'newcleanvaluedate': a.newcleanvaluedate,
                    'newasset': a.newasset,
                    'date': date,
                })
            elif product.recordtype == "股权":
                results.append({
                    'id': a.id,
                    'crm_id': a.crm_id,
                    'recordtype': '股权',
                    'name': a.name,
                    'buyamount': a.buyamount,
                    'floatmoney': a.floatmoney,
                    'interest': a.interest,
                    'productperiod': product.productperiod,
                    'date': date,
                    'allocatedamount': a.allocatedamount,
                })
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + a.product_cid)

    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def detail_asset(request):
    data = json.loads(request.body)
    asset_id = data['asset_id']
    try:
        asset = Asset.objects.get(id=asset_id)
    except Asset.DoesNotExist:
        return CustomResponse('资产不存在:' + asset_id)
    try:
        product = Productofficial.objects.get(crm_id=asset.product_cid)
    except Productofficial.DoesNotExist:
        return CustomResponse('产品不存在:' + asset.product_cid)
    result = []
    order = Order.objects.filter(account_cid=asset.account_cid, product_cid=asset.product_cid).first()
    if not order:
        date = None
        startearningday = None
        maturitydate = None
        nownet = None
    else:
        date = order.date
        startearningday = order.startearningday
        maturitydate = order.maturitydate
        nownet = order.nownet
    if product.recordtype == "类固收":
        result = ({
            'recordtype': '类固收',
            'account_cid': asset.account_cid,
            'asset_name': asset.name,
            'product_name': product.name,
            'product_abbreviation': product.productabbreviation,
            'product_id': product.id,
            'product_cid': product.crm_id,
            'buyamount': asset.buyamount,
            'comparison': product.comparison,
            'comparisonabbreviation': product.comparisonabbreviation,
            'startearningday': startearningday,
            'maturitydate': maturitydate,
            'interest': asset.interest,
            # 已分配收益
        })
    elif product.recordtype == "二级市场":
        result = ({
            'recordtype': '二级市场',
            'account_cid': asset.account_cid,
            'asset_name': asset.name,
            'product_name': product.name,
            'product_abbreviation': product.productabbreviation,
            'product_id': product.id,
            'product_cid': product.crm_id,
            'buyamount': asset.buyamount,
            'holdshare': asset.holdshare,
            'nownet': nownet,
            'date': date,
            'newcleanvalue': asset.newcleanvalue,
            'newcleanvaluedate': asset.newcleanvaluedate,
            'newasset': asset.newasset,
            'floatmoney': asset.floatmoney,
        })
    elif product.recordtype == "股权":
        result = ({
            'recordtype': '股权',
            'account_cid': asset.account_cid,
            'asset_name': asset.name,
            'product_name': product.name,
            'product_abbreviation': product.productabbreviation,
            'product_id': product.id,
            'product_cid': product.crm_id,
            'buyamount': asset.buyamount,
            'date': date,
            'productperiod': product.productperiod,
            'allocatedamount': asset.allocatedamount,
        })
    return HttpResponse(json.dumps(result, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 未用 添加订单 并且给客户添加产品对应的顾问 添在了顾问客户表中
def order_product_by_customer(request):
    data = json.loads(request.body)
    product_cid = data['product_cid']
    customer_cid = data['customer_cid']
    money = data['money']
    product = Productofficial.objects.get(crm_id=product_cid)
    account = Account.objects.get(crm_id=customer_cid)
    responsible_res = Responsible.objects.get(crm_id=product.responsible_cid)
    bank = Accountbank.objects.get(owneraccount_cid=customer_cid)

    new_order = Order()
    new_order.product = product
    new_order.account = account
    new_order.responsible = responsible_res
    new_order.name = product.name
    new_order.accountbank = bank
    new_order.bookmoney = money
    if product.recordtype == "类固收":
        recordtype = "0126F0000020pRrQAI"
    elif product.recordtype == "二级市场":
        recordtype = "0126F0000020pRqQAI"
        new_order.nownet = product.value
    elif product.recordtype == "股权":
        recordtype = "0126F0000020pRsQAI"
    else:
        recordtype = None
    new_order.recordtypeid = recordtype
    new_order.ostatus = '预约成功'
    new_order.save()
    try:
        ResponsibleAccount.objects.get(responsible_id=responsible_res.id, account_id=account.id)
    except ResponsibleAccount.DoesNotExist:
        new_res_acc = ResponsibleAccount()
        new_res_acc.responsible_id = responsible_res.id
        new_res_acc.account_id = account.id
        new_res_acc.save()
    return HttpResponse('预约成功')


# 认购记录
def get_my_order(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    orders = Order.objects.filter(account_cid=account_cid, ostatus__in=valid_status)
    results = []
    for o in orders:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        try:
            responsible = Responsible.objects.get(crm_id=o.responsible_cid)
        except Responsible.DoesNotExist:
            return CustomResponse('投顾不存在:' + o.responsible_cid)
        if o.recordtypeid == '0126F0000020pRrQAI':
            results.append(
                {'name': o.name, 'product': product.name, 'account': account.name, 'responsible': responsible.name,
                 'type': '类固收', 'bookmoney': o.bookmoney, 'money_4': o.money_4, 'ostatus': o.ostatus, 'term': o.term,
                 'defendrate': o.defendrate, 'startearningday': o.startearningday, 'maturitydate': o.maturitydate,
                 'date': o.date})
        elif o.recordtypeid == '0126F0000020pRqQAI':
            results.append(
                {'name': o.name, 'product': product.name, 'account': account.name, 'responsible': responsible.name,
                 'type': '二级市场', 'bookmoney': o.bookmoney, 'money_4': o.money_4, 'ostatus': o.ostatus, 'num': o.num,
                 'nownet': o.nownet, 'date': o.date})
        elif o.recordtypeid == '0126F0000020pRsQAI':
            results.append(
                {'name': o.name, 'product': product.name, 'account': account.name, 'responsible': responsible.name,
                 'type': '股权', 'bookmoney': o.bookmoney, 'money_4': o.money_4, 'ostatus': o.ostatus, 'date': o.date})
    # 按认购日期排序
    results = sorted(results, key=itemgetter('date'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_my_orderredemption(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    valid_status = ['已批准']
    orders = Orderredemption.objects.filter(account_cid=account_cid, approvalstatus__in=valid_status)
    results = []
    for o in orders:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        results.append({'product': product.name, 'account': account.name, 'number': o.number,
                        'redemptiondate': o.redemptiondate, 'net': o.net,
                        'redeemamount': o.redeemamount, 'deduction': o.deduction, 'transfermoney': o.transfermoney,
                        'approvalstatus': o.approvalstatus, })
    # 按赎回日期排序
    results = sorted(results, key=itemgetter('redemptiondate'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_my_ordertransfer(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    valid_status = ['已批准']
    # 转让出
    orders1 = Ordertransfer.objects.filter(transferaccount_cid=account_cid, approvalstatus__in=valid_status)
    # 转让入
    orders2 = Ordertransfer.objects.filter(receiver_cid=account_cid, approvalstatus__in=valid_status)
    results = []
    for o in orders1:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            receiver = Account.objects.get(crm_id=o.receiver_cid)
        except Account.DoesNotExist:
            return CustomResponse('接收用户不存在:' + o.receiver_cid)
        try:
            transferaccount = Account.objects.get(crm_id=o.transferaccount_cid)
        except Account.DoesNotExist:
            return CustomResponse('转让用户不存在:' + o.transferaccount_cid)
        results.append({'product': product.name, 'receiver': receiver.name, 'transferaccount': transferaccount.name,
                        'transferdate': o.transferdate,
                        'transferamount': o.transferamount, 'transfernet': o.transfernet,
                        'transferamount2': o.transferamount2, 'remarks': o.remarks, 'approvalstatus': o.approvalstatus})
    for o in orders2:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            receiver = Account.objects.get(crm_id=o.receiver_cid)
        except Account.DoesNotExist:
            return CustomResponse('接收用户不存在:' + o.receiver_cid)
        try:
            transferaccount = Account.objects.get(crm_id=o.transferaccount_cid)
        except Account.DoesNotExist:
            return CustomResponse('转让用户不存在:' + o.transferaccount_cid)

        results.append({'product': product.name, 'receiver': receiver.name, 'transferaccount': transferaccount.name,
                        'transferdate': o.transferdate,
                        'transferamount': o.transferamount, 'transfernet': o.transfernet,
                        'transferamount2': o.transferamount2, 'remarks': o.remarks, 'approvalstatus': o.approvalstatus})
    # 按转让日期排序
    results = sorted(results, key=itemgetter('transferdate'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_my_ordersharechangeinfo(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    orders = Ordersharechangeinfo.objects.filter(account_cid=account_cid)
    results = []
    for o in orders:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        results.append({'product': product.name, 'account': account.name, 'netdate': o.netdate,
                        'changeamount': o.changeamount, 'name': o.name})
    # 按 分配日期排序
    results = sorted(results, key=itemgetter('netdate'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_my_orderpayoutdetail(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    orders = Orderpayoutdetail.objects.filter(account_cid=account_cid)
    results = []
    for o in orders:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        results.append(
            {'product': product.name, 'account': account.name, 'name': o.name, 'startearningday': o.startearningday,
             'dividenddate': o.dividenddate,
             'valuedays': o.valuedays, 'defendrate': o.defendrate, 'defendamount': o.defendamount,
             'companysubsidy': o.companysubsidy, 'calccapital': o.calccapital, 'paybackamount': o.paybackamount,
             'totalamount': o.totalamount})
    # 按分配日期排序
    results = sorted(results, key=itemgetter('dividenddate'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_my_booked_order(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    orders = Order.objects.filter(account_cid=account_cid)
    results = []
    for o in orders:
        if o.createdate:
            sort_date = o.createdate
        else:
            sort_date = datetime.date(1900, 1, 1)

        if o.ostatus in ['认购撤销','预约成功', '待打款', '打款中', '打款审批完成', '资料审核中', '资料审批驳回', '认购成功']:
            try:
                product = Productofficial.objects.get(crm_id=o.product_cid)
            except Productofficial.DoesNotExist:
                return CustomResponse('产品不存在:' + o.product_cid)
            results.append({'id': o.id,
                            'productabbreviation': product.productabbreviation,
                            'product': product.name,
                            'bookmoney': o.bookmoney,
                            'ostatus': o.ostatus,
                            'responsible_cid': o.responsible_cid,
                            'sort_date': sort_date,
                            'onlinesign': product.onlinesign,
                            'product_id': product.id})
    results = sorted(results, key=itemgetter('sort_date', 'id'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def detail_booked_order(request):
    data = json.loads(request.body)
    order_id = data['order_id']

    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return CustomResponse('订单不存在:' + order_id)
    try:
        product = Productofficial.objects.get(crm_id=order.product_cid)
    except Productofficial.DoesNotExist:
        return CustomResponse('产品不存在:' + order.product_cid)
    try:
        account = Account.objects.get(crm_id=order.account_cid)
        if account.recordtypeid == '0126F0000020pRJQAY':
            recordtype = '个人'
        else:
            recordtype = '机构'
    except Account.DoesNotExist:
        return CustomResponse('用户不存在:' + order.account_cid)
    bankid = '-'
    bankName = '-'
    bankno = '-'
    try:
        if order.accountbank_cid is not None:
            bank = Accountbank.objects.get(crm_id=order.accountbank_cid)
            bankid = bank.bankno
            bankName = bank.bankname
            bankno = bank.bankno
    except Accountbank.DoesNotExist:
        bankid = '-'
        bankName = '-'
    result = {
        'certificatenumber': base64_decode(account.certificatenumber),
        'product': product.name,
        'productabbreviation': product.productabbreviation,
        'product_id': product.id,
        'product_risklevel': product.risklevel,
        'account_recordtype': recordtype,
        'responsible_cid': order.responsible_cid,
        'order_no': order.orderno,
        'account': account.name,
        'account_id': account.id,
        'ostatus': order.ostatus,
        'bookmoney': order.bookmoney,
        'createdate': order.createdate,
        'money_4': order.money_4,
        'date': order.date,
        'order_type': product.recordtype,
        'bankid': bankid,
        'bankName': bankName,
        'bankno': bankno,
        'subscription_fee': order.subscription_fee,
        'openday': order.openday,
        'confirmdate': order.confirmdate,
        'nownet': order.nownet,
        'num': order.num,
        'task_id': order.taskid,
        'origin': product.origin,
        'addmin': product.addmin,
        'remainingamountcanbookedf':product.remainingamountcanbookedf,
        'id':order.id,
        'agreementTaskId': order.agreementtaskid,
        'onlinesign': product.onlinesign,
        'agreementSign': product.agreementsign
    }
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_all_order(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    valid_status2 = ['已批准']
    order1 = Order.objects.filter(account_cid=account_cid, ostatus__in=valid_status1)
    results = []
    for o in order1:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        try:
            responsible = Responsible.objects.get(crm_id=o.responsible_cid)
        except Responsible.DoesNotExist:
            return CustomResponse('投顾不存在:' + o.responsible_cid)
        if o.recordtypeid == '0126F0000020pRrQAI':
            results.append(
                {'name': o.name, 'product': product.name, 'account': account.name, 'responsible': responsible.name,
                 'type': '类固收', 'bookmoney': o.bookmoney, 'money_4': o.money_4, 'ostatus': o.ostatus, 'term': o.term,
                 'defendrate': o.defendrate, 'startearningday': o.startearningday, 'maturitydate': o.maturitydate,
                 'date': o.date})
        elif o.recordtypeid == '0126F0000020pRqQAI':
            results.append(
                {'name': o.name, 'product': product.name, 'account': account.name, 'responsible': responsible.name,
                 'type': '二级市场', 'bookmoney': o.bookmoney, 'money_4': o.money_4, 'ostatus': o.ostatus, 'num': o.num,
                 'nownet': o.nownet, 'date': o.date})
        elif o.recordtypeid == '0126F0000020pRsQAI':
            results.append(
                {'name': o.name, 'product': product.name, 'account': account.name, 'responsible': responsible.name,
                 'type': '股权', 'bookmoney': o.bookmoney, 'money_4': o.money_4, 'ostatus': o.ostatus, 'date': o.date})

    order2 = Ordersharechangeinfo.objects.filter(account_cid=account_cid)
    for o in order2:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        results.append({'product': product.name, 'account': account.name, 'netdate': o.netdate,
                        'changeamount': o.changeamount, 'name': o.name, 'date': o.netdate, })

    order3 = Orderredemption.objects.filter(account_cid=account_cid, approvalstatus__in=valid_status2)
    for o in order3:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        results.append({'product': product.name, 'account': account.name, 'number': o.number,
                        'redemptiondate': o.redemptiondate, 'net': o.net,
                        'redeemamount': o.redeemamount, 'deduction': o.deduction, 'transfermoney': o.transfermoney,
                        'approvalstatus': o.approvalstatus, 'date': o.redemptiondate, })

    order4 = Ordertransfer.objects.filter(transferaccount_cid=account_cid, approvalstatus__in=valid_status2)
    for o in order4:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            receiver = Account.objects.get(crm_id=o.receiver_cid)
        except Account.DoesNotExist:
            return CustomResponse('接收用户不存在:' + o.receiver_cid)
        try:
            transferaccount = Account.objects.get(crm_id=o.transferaccount_cid)
        except Account.DoesNotExist:
            return CustomResponse('转让用户不存在:' + o.transferaccount_cid)
        results.append(
            {'product': product.name, 'reciever': receiver.name, 'transferaccount': transferaccount.name,
             'transferdate': o.transferdate,
             'transferamount': o.transferamount, 'transfernet': o.transfernet, 'transferamount2': o.transferamount2,
             'remarks': o.remarks, 'approvalstatus': o.approvalstatus, 'date': o.transferdate, })

    order5 = Ordertransfer.objects.filter(receiver_cid=account_cid, approvalstatus__in=valid_status2)
    for o in order5:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            receiver = Account.objects.get(crm_id=o.receiver_cid)
        except Account.DoesNotExist:
            return CustomResponse('接收用户不存在:' + o.receiver_cid)
        try:
            transferaccount = Account.objects.get(crm_id=o.transferaccount_cid)
        except Account.DoesNotExist:
            return CustomResponse('转让用户不存在:' + o.transferaccount_cid)
        results.append(
            {'product': product.name, 'reciever': receiver.name, 'transferaccount': transferaccount.name,
             'transferdate': o.transferdate,
             'transferamount': o.transferamount, 'transfernet': o.transfernet, 'transferamount2': o.transferamount2,
             'remarks': o.remarks, 'approvalstatus': o.approvalstatus, 'date': o.transferdate, })

    order6 = Orderpayoutdetail.objects.filter(account_cid=account_cid)
    for o in order6:
        try:
            product = Productofficial.objects.get(crm_id=o.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
        try:
            account = Account.objects.get(crm_id=account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在:' + account_cid)
        results.append(
            {'product': product.name, 'account': account.name, 'name': o.name, 'startearningday': o.startearningday,
             'dividenddate': o.dividenddate,
             'valuedays': o.valuedays, 'defendrate': o.defendrate, 'defendamount': o.defendamount,
             'companysubsidy': o.companysubsidy, 'calccapital': o.calccapital, 'paybackamount': o.paybackamount,
             'totalamount': o.totalamount, 'date': o.dividenddate, })
    # 按认购-赎回-派息-调减-转让合并
    results = sorted(results, key=itemgetter('date'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用 对二级市场和股权的某个某持情况进行查询
def get_security_detail(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    asset_info = Asset.objects.get(crm_id=account_id)
    product = Productofficial.objects.get(crm_id=asset_info.product_cid)
    results = ({
        'buyamount': asset_info.buyamount, 'floatmoney': asset_info.floatmoney, 'holdshare': asset_info.holdshare,
        'interest': asset_info.interest, 'newasset': asset_info.newasset, 'newcleanvalue': asset_info.newcleanvalue,
        'productabbreviation': product.productabbreviation, 'name': product.name, 'id': product.id,
        'value': product.value, 'origin': product.origin, 'valuedate': product.valuedate,  # 认购起点=起投资金
        # 基金详情页
        'setupdate': product.setupdate, 'adminacus': product.adminacus, 'adminbcus': product.adminbcus,
        'risklevel': product.risklevel,
        # 交易须知页
        'addmin': product.addmin,
        'ratesub': product.ratesub, 'redemptionrate': product.redemptionrate
    })
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def get_solid_detail(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    asset_info = Asset.objects.get(crm_id=account_id)
    product = Productofficial.objects.get(crm_id=asset_info.product_cid)
    result = ({
        'buyamount': asset_info.buyamount, 'floatmoney': asset_info.floatmoney, 'holdshare': asset_info.holdshare,
        'interest': asset_info.interest, 'newasset': asset_info.newasset, 'newcleanvalue': asset_info.newcleanvalue,
        'productabbreviation': product.productabbreviation, 'name': product.name, 'id': product.id,
        'mouthlimit': product.monthlimit, 'renewalperiod': product.renewalperiod,
        # 产品详情
        'setupdate': product.setupdate, 'adminacus': product.adminacus, 'adminbcus': product.adminbcus,
        'risklevel': product.risklevel,
        # 投资信息
        'investmentobj': product.investmentobj,
        # 交易规则
        'origin': product.origin, 'addmin': product.addmin,
        'ratesub': product.ratesub, 'redemptionrate': product.redemptionrate
    })
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def search_by_date(request):
    data = json.loads(request.body)
    year = int(data['year'])
    month = int(data['month'])
    account_cid = data['account_cid']
    orders = Order.objects.filter(account_cid=account_cid, date__month=month, date__year=year)
    results = []
    for i in orders:
        product = Productofficial.objects.get(crm_id=i.product_cid)
        account = Account.objects.get(crm_id=account_cid)
        responsible = Responsible.objects.get(crm_id=i.responsible_cid)
        if i.recordtypeid == '0126F0000020pRrQAI':
            results.append({'name': i.name, 'product': product.name, 'account': account.name,
                            'responsible': responsible.name, 'type': '类固收',
                            'bookmoney': i.bookmoney, 'money_4': i.money_4, 'ostatus': i.ostatus, 'term': i.term,
                            'defendrate': i.defendrate, 'startearningday': i.startearningday,
                            'maturitydate': i.maturitydate})
        elif i.recordtypeid == '0126F0000020pRqQAI':
            results.append({'name': i.name, 'product': product.name, 'account': account.name,
                            'responsible': responsible.name,
                            'type': '二级市场',
                            'bookmoney': i.bookmoney, 'money_4': i.money_4, 'ostatus': i.ostatus, 'num': i.num,
                            'nownet': i.nownet})
        elif i.recordtypeid == '0126F0000020pRsQAI':
            results.append({'name': i.name, 'product': product.name, 'account': account.name,
                            'responsible': responsible.name,
                            'type': '股权',
                            'bookmoney': i.bookmoney, 'money_4': i.money_4, 'ostatus': i.ostatus})
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_profit_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    order_cid = data['order_cid']
    number = data['number']
    year = data['year']
    month = data['month']
    results = []
    order = Order.objects.get(crm_id=order_cid)
    account = Account.objects.get(crm_id=order.account_cid)
    product = Productofficial.objects.get(crm_id=order.product_cid)
    if order.accountbank_cid:
        accountbank = Accountbank.objects.get(crm_id=order.accountbank_cid)
    elif account.crm_id:
        accountbank = Accountbank.objects.get(owneraccount_cid=account.crm_id)
    else:
        return HttpResponseBadRequest("查无此人银行账户")
    # 计算状态
    if not order.maturitydate:
        status = "存续中"
    elif order.maturitydate < datetime.date(year, month + 1, 1):
        status = "已到期"
    elif order.maturitydate >= datetime.date(year, month + 1, 1):
        status = "存续中"
    else:
        status = ""
    # 计算存续天数
    if order.maturitydate is None:
        holdday = 0
    elif order.startearningday is None:
        holdday = 0
    else:
        holdday = (order.maturitydate - order.startearningday).days
    # 通过存续天数判断存续期限
    renewal_period = round(holdday / 30)
    # 将收益计算日期转换成date格式
    date = datetime.date(year, month, 1)
    if status == "已到期":
        if product.recordtype == "类固收":
            results = ({  # 注意没有获取到的数据
                "No.": number, "customer": account.name, "recordtype": product.recordtype, "status": status,
                "productname": product.name, "underlying": "-", "bookmoney": order.bookmoney,
                "subscription_fee": order.subscription_fee,
                "bankname": accountbank.bankname, "bankno": accountbank.bankno[-4:], "predictinterest": "-",
                "renewal_period": renewal_period, "buydate": order.date, "startearningday": order.startearningday,
                "maturityday": order.maturitydate, "holdday": holdday, "payday": "-", "nownet": order.nownet,
                "quitnet": "-",
                "profit": get_solid_profit(account.crm_id, product.crm_id, order.id, order.money_4, date),
            })
        elif product.recordtype == "二级市场":
            results = ({
                "No.": number, "customer": account.name, "recordtype": product.recordtype, "status": status,
                "productname": product.name, "underlying": "-", "bookmoney": order.bookmoney,
                "subscription_fee": order.subscription_fee,
                "bankname": accountbank.bankname, "bankno": accountbank.bankno[-4:], "predictinterest": "-",
                "buydate": order.date, "nownet": order.nownet, "newestnet": product.value,
                "valuedate": product.valuedate,
                "profit": get_second_profit(date, product.crm_id, account.crm_id, order.num, product.value,
                                            order.money_4)
            })
        elif product.recordtype == "股权":
            # 如果没有购买时净值信息，显示-
            if not order.nownet:
                earningrate = "-"
            else:
                earningrate = str((product.value / order.nownet - 1) * 100) + "%"
            results = ({
                "No.": number, "customer": account.name, "recordtype": product.recordtype, "status": status,
                "productname": product.name, "underlying": "-", "bookmoney": order.bookmoney,
                "subscription_fee": order.subscription_fee,
                "bankname": accountbank.bankname, "bankno": accountbank.bankno[-4:], "predictinterest": "-",
                "buydate": order.date, "nownet": order.nownet, "newestnet": product.value,
                "valuedate": product.valuedate,
                "earningrate": earningrate,
            })
    elif status == "存续中":
        if product.recordtype == "类固收":
            results = ({
                "No.": number, "customer": account.name, "recordtype": product.recordtype, "status": status,
                "productname": product.name, "underlying": "-", "bookmoney": order.bookmoney,
                "subscription_fee": order.subscription_fee,
                "bankname": accountbank.bankname, "bankno": accountbank.bankno[-4:], "predictinterest": "-",
                "renewal_period": renewal_period, "buydate": order.date, "startearningday": order.startearningday,
                "maturityday": order.maturitydate, "holdday": holdday, "payday": "-", "nownet": order.nownet,
                "quitnet": "-",
                "profit": get_solid_profit(account.crm_id, product.crm_id, order.id, order.money_4, date),
            })
        elif product.recordtype == "二级市场":
            # 如果没有购买时净值信息，显示-
            if not order.nownet:
                earningrate = "-"
            else:
                earningrate = str((product.value / order.nownet - 1) * 100) + "%"
            results = ({
                "No.": number, "customer": account.name, "recordtype": product.recordtype, "status": status,
                "productname": product.name, "underlying": "-", "bookmoney": order.bookmoney,
                "subscription_fee": order.subscription_fee,
                "bankname": accountbank.bankname, "bankno": accountbank.bankno[-4:], "predictinterest": "-",
                "buydate": order.date, "nownet": order.nownet, "newestnet": product.value,
                "valuedate": product.valuedate,
                "earningrate": earningrate,
            })
        elif product.recordtype == "股权":
            # 如果没有购买时净值信息，显示-
            if not order.nownet:
                earningrate = "-"
            else:
                earningrate = str((product.value / order.nownet - 1) * 100) + "%"
            results = ({
                "No.": number, "customer": account.name, "recordtype": product.recordtype, "status": status,
                "productname": product.name, "underlying": "-", "bookmoney": order.bookmoney,
                "subscription_fee": order.subscription_fee,
                "bankname": accountbank.bankname, "bankno": accountbank.bankno[-4:], "predictinterest": "-",
                "buydate": order.date, "nownet": order.nownet, "newestnet": product.value,
                "valuedate": product.valuedate,
                "earningrate": earningrate,
            })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 未用?
def order_list_info(request):
    data = json.load(request)
    todaydate = datetime.datetime.now()
    order_list = data['order_list']
    order_info_list = []
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    for i in order_list:
        order_info = Order.objects.get(id=i, ostatus__in=valid_status)
        if_exist = 0
        profit = 0
        for k in order_info_list:
            if order_info.account_cid == k['account_cid'] and order_info.product_cid == k['product_cid']:
                if_exist = 1
                profit = 0
                break
            else:
                pass
        if if_exist == 0:
            if order_info.recordtypeid == "0126F0000020pRrQAI":
                # 类固收
                profit = get_solid_profit(order_info.account_cid, order_info.product_cid, order_info.id,
                                          order_info.money_4,
                                          todaydate)
            elif order_info.recordtypeid == "0126F0000020pRqQAI":
                # 二级市场
                order_red = Orderredemption.objects.filter(product_cid=order_info.product_cid,
                                                           account_cid=order_info.account_cid,
                                                           redemptiondate__month=todaydate.month,
                                                           redemptiondate__year=todaydate.year)
                order_red_list = list(order_red)
                if len(order_red_list) == 0:
                    profit = 0
                else:
                    profit = get_second_profit(todaydate, order_info.product_cid, order_info.account_cid,
                                               order_info.order_num,
                                               "product.value", order_info.money_4)
            elif order_info.recordtypeid == "0126F0000020pRsQAI":
                # 股权
                profit = get_share_profit(order_info.id)
        if_exist = 0
        order_info_list.append({
            'profit': profit,
            'bookMoney': order_info.money_4,
            'startEarningMoney': order_info.startearningday,
            'maturityDate': order_info.maturitydate,
            'account_cid': order_info.account_cid,
            'product_cid': order_info.product_cid
        })
    result = {'order_info_list': order_info_list}
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_second_profit(today, product_cid, account_cid, order_num, product_value, order_money4):
    # 已赎回二级市场收益
    profit = 0
    try:
        order_red = Orderredemption.objects.filter(product_cid=product_cid, account_cid=account_cid,
                                                   redemptiondate__month=today.month, redemptiondate__year=today.year)
        for o in order_red:
            transfermoney = o.transfermoney
            if not transfermoney:
                profit = 0
            # profit += order_num * product_value + transfermoney - order_money4
            else:
                profit += transfermoney - order_money4
    except Orderredemption.DoesNotExist:
        profit = 0
    return profit


def get_solid_profit(account_cid, product_cid, order_id, order_money4, date):
    todaydate = date
    profit = 0
    try:
        order_pay = Orderpayoutdetail.objects.filter(account_cid=account_cid, product_cid=product_cid,
                                                     dividenddate__year=todaydate.year,
                                                     dividenddate__month=todaydate.month)
        for o in order_pay:
            # 如果派息比本金少，说明不是最后一次派息
            if o.totalamount < order_money4:
                profit += o.totalamount
            # 派息比本金多，说明是最后一次派息，计算收益需要减去本金
            else:
                profit += o.totalamount - order_money4
    except Orderpayoutdetail.DoesNotExist:
        profit = 0
    return profit


def get_share_profit(order_id):
    order = Order.objects.get(id=order_id)
    product = Productofficial.objects.get(crm_id=order.product_cid)
    if not order.nownet:
        earningrate = "-"
    else:
        if not product.value:
            earningrate = '-'
        else:
            earningrate = str((product.value / order.nownet - 1) * 100) + "%"
    return earningrate


def get_profit(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    if not data:
        return HttpResponseBadRequest("Request Error")
    account_crmid = Account.objects.get(id=account_id).crm_id
    if not account_crmid:
        return HttpResponseBadRequest("Request Error")
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    order_query_info = Order.objects.filter(account_cid=account_crmid, ostatus__in=valid_status)
    order_query_info = list(order_query_info)
    year = data['year']
    month = data['month']
    day = calendar.monthrange(year, month)[1]
    date_str = str(year) + '-' + str(month) + '-' + str(day)
    search_date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    for j in order_query_info[:]:
        if not j.maturitydate:
            pass
        else:
            maturitydate = datetime.datetime.strftime(j.maturitydate, '%Y/%m/%d')
            todaydate = datetime.datetime.strftime(search_date, '%Y/%m/%d')
            if maturitydate < todaydate:  # 到期
                if j.maturitydate.year == search_date.year and j.maturitydate.month == search_date.month:

                    pass
                else:
                    order_query_info.remove(j)
    order_unexpired_list = order_query_info
    solid_profit = 0
    solid_list = []
    solid_id_list = []
    solid_amount = 0
    second_profit = 0
    second_list = []
    second_id_list = []
    second_amount = 0
    share_profit = 0
    share_list = []
    share_id_list = []
    share_amount = 0
    for i in order_query_info:
        account_cid = i.account_cid
        product_cid = i.product_cid
        if i.recordtypeid == "0126F0000020pRrQAI":
            # 类固收
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in solid_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                solid_profit = solid_profit + get_solid_profit(i.account_cid, i.product_cid,
                                                               i.id, i.money_4,
                                                               search_date)
                solid_amount = solid_amount + 1
            solid_list.append(order_info)
            solid_id_list.append(i.id)

        elif i.recordtypeid == "0126F0000020pRqQAI":
            # 二级市场
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in second_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                second_profit = second_profit + get_second_profit(search_date, i.product_cid,
                                                                  i.account_cid, i.num,
                                                                  "product.value", i.money_4)
                second_amount = second_amount + 1
            second_list.append(order_info)
            second_id_list.append(i.id)
        elif i.recordtypeid == "0126F0000020pRsQAI":
            # 股权
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in share_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                share_amount = share_amount + 1
            share_list.append(order_info)
            share_id_list.append(i.id)

    result = [
        {
            'type': '类固收',
            'profit': solid_profit,
            'amount': solid_amount,
            'order_id_list': solid_id_list
        }, {
            'type': '二级市场',
            'profit': second_profit,
            'amount': second_amount,
            'order_id_list': second_id_list
        }, {
            'type': '股权',
            'profit': share_profit,
            'amount': share_amount,
            'order_id_list': share_id_list
        }
    ]
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_profit_by_type(request):
    data = json.load(request)
    order_list = data['order_list']
    result_list = []

    year = data['year']
    month = data['month']
    day = calendar.monthrange(year, month)[1]
    date_str = str(year) + '-' + str(month) + '-' + str(day)
    search_date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    for i in order_list:
        order = Order.objects.get(id=i, ostatus__in=valid_status)
        account_cid = order.account_cid
        product_cid = order.product_cid
        if order.recordtypeid == "0126F0000020pRrQAI":
            # 类固收account_cid, product_cid, order_id, order_money4, date
            profit = get_solid_profit(order.account_cid, order.product_cid, order.id, order.money_4, search_date)

        elif order.recordtypeid == "0126F0000020pRqQAI":
            # 二级市场

            profit = get_second_profit(search_date, order.product_cid, order.account_cid, order.num,
                                       "product.value", order.money_4)

        elif order.recordtypeid == "0126F0000020pRsQAI":
            # 股权
            profit = get_share_profit(order.id)
        bookmoney = order.money_4
        startdate = order.startearningday
        maturityDate = order.maturitydate
        account_cid = order.account_cid
        product_cid = order.product_cid
        order_cid = order.crm_id
        product = Productofficial.objects.get(crm_id=product_cid)
        order_info = {
            'profit': profit,
            'bookMoney': bookmoney,
            'startEarningDay': startdate,
            'maturityDate': maturityDate,
            'account_cid': account_cid,
            'product_cid': product_cid,
            'product_name': product.name,
            'order_id': i,
            'order_cid': order_cid
        }
        result_list.append(order_info)
    result = {'order_info': result_list}
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_asset_info(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    if not data:
        return HttpResponseBadRequest("Request Error")
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    user_account = Account.objects.get(id=account_id)
    account_crmid = Account.objects.get(id=account_id).crm_id
    if not account_crmid:
        return HttpResponseBadRequest("Request Error")
    order_query_info = Order.objects.filter(account_cid=account_crmid, ostatus__in=valid_status)
    order_query_info = list(order_query_info)
    responsible = Responsible.objects.get(crm_id=user_account.responsible_id)
    responsible_name = responsible.name
    expired_list = []
    year = data['year']
    month = data['month']
    day = calendar.monthrange(year, month)[1]
    date_str = str(year) + '-' + str(month) + '-' + str(day)
    search_date = datetime.datetime.strptime(date_str, '%Y-%m-%d')

    for j in order_query_info[:]:
        if not j.maturitydate:
            pass
        else:
            maturitydate = datetime.datetime.strftime(j.maturitydate, '%Y/%m/%d')
            todaydate = datetime.datetime.strftime(search_date, '%Y/%m/%d')
            if maturitydate < todaydate:  # 到期的
                if j.maturitydate.year == search_date.year and j.maturitydate.month == search_date.month:
                    print(j.id)
                    expired_list.append(j)
                order_query_info.remove(j)
            else:  # 未到期
                pass
    order_unexpired_list = order_query_info
    solid_profit = 0
    solid_list = []
    solid_id_list = []
    solid_amount = 0
    second_profit = 0
    second_list = []
    second_id_list = []
    second_amount = 0
    share_profit = 0
    share_list = []
    share_id_list = []
    share_amount = 0
    for i in order_query_info:
        account_cid = i.account_cid
        product_cid = i.product_cid
        if i.recordtypeid == "0126F0000020pRrQAI":
            # 类固收
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in solid_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                solid_profit = solid_profit + get_solid_profit(i.account_cid, i.product_cid,
                                                               i.id,
                                                               i.money_4,
                                                               search_date)
                solid_amount = solid_amount + 1
            solid_list.append(order_info)
            solid_id_list.append(i.id)

        elif i.recordtypeid == "0126F0000020pRqQAI":
            # 二级市场
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in second_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                second_profit = second_profit + get_second_profit(search_date, i.product_cid,
                                                                  i.account_cid, i.num,
                                                                  "product.value", i.money_4)
                second_amount = second_amount + 1
            second_list.append(order_info)
            second_id_list.append(i.id)
        elif i.recordtypeid == "0126F0000020pRsQAI":
            # 股权
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in share_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                share_amount = share_amount + 1
            share_list.append(order_info)
            share_id_list.append(i.id)

    result = [
        {
            'type': '类固收',
            'profit': solid_profit,
            'amount': solid_amount,
            'order_id_list': solid_id_list
        }, {
            'type': '二级市场',
            'profit': second_profit,
            'amount': second_amount,
            'order_id_list': second_id_list
        }, {
            'type': '股权',
            'profit': share_profit,
            'amount': share_amount,
            'order_id_list': share_id_list
        }
    ]

    # 这里是关于已到期的
    solid_profit2 = 0
    solid_list2 = []
    solid_id_list2 = []
    solid_amount2 = 0
    second_profit2 = 0
    second_list2 = []
    second_id_list2 = []
    second_amount2 = 0
    share_profit2 = 0
    share_list2 = []
    share_id_list2 = []
    share_amount2 = 0
    for i in expired_list:
        account_cid = i.account_cid
        product_cid = i.product_cid
        if i.recordtypeid == "0126F0000020pRrQAI":
            # 类固收
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in solid_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                solid_profit2 = solid_profit2 + get_solid_profit(i.account_cid, i.product_cid,
                                                                 i.id, i.money_4,
                                                                 search_date)
                solid_amount2 = solid_amount2 + 1
            solid_list2.append(order_info)
            solid_id_list2.append(i.id)

        elif i.recordtypeid == "0126F0000020pRqQAI":
            # 二级市场
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in second_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                second_profit2 = second_profit2 + get_second_profit(search_date, i.product_cid,
                                                                    i.account_cid, i.num,
                                                                    "product.value", i.money_4)
                second_amount2 = second_amount2 + 1
            second_list2.append(order_info)
            second_id_list2.append(i.id)
        elif i.recordtypeid == "0126F0000020pRsQAI":
            # 股权
            if_exist = 0
            order_info = {'account_cid': i.account_cid, 'product_cid': i.product_cid, 'order_id': i.id}
            for k in share_list:
                if k['account_cid'] == i.account_cid and k['product_cid'] == i.product_cid:
                    if_exist = 1
                    break
                else:
                    pass
            if if_exist == 0:
                share_amount2 = share_amount2 + 1
            share_list2.append(order_info)
            share_id_list2.append(i.id)

    result_expired = [
        {
            'type': '类固收',
            'profit': solid_profit2,
            'amount': solid_amount2,
            'order_id_list': solid_id_list2
        }, {
            'type': '二级市场',
            'profit': second_profit2,
            'amount': second_amount2,
            'order_id_list': second_id_list2
        }, {
            'type': '股权',
            'profit': share_profit2,
            'amount': share_amount2,
            'order_id_list': share_id_list2
        }
    ]
    res_result = {'expired': result_expired, 'unexpired': result, 'responsible_name': responsible_name,
                  'account_name': user_account.name, 'salutation': user_account.salutation}
    return HttpResponse(json.dumps(res_result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_monthly_bill(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    year = data['year']
    month = data['month']
    if not data:
        return HttpResponse('Request Error')
    start_date = datetime.date(year, month, 1)
    next_month = start_date + relativedelta(months=1)
    end_date = next_month - datetime.timedelta(days=1)
    solid_results = []
    securities_results = []
    stock_results = []
    solid_transactions = []
    securities_transactions = []
    stock_transactions = []
    solid_products = []
    securities_products = []
    stock_products = []
    transactions = []
    bills = []
    solid_result = None
    securities_result = None
    stock_result = None
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    valid_status2 = ['已批准']

    # 类固收
    order_solid = Order.objects.filter(account_cid=account_cid, recordtypeid='0126F0000020pRrQAI',
                                       maturitydate__gte=end_date, startearningday__lte=start_date,
                                       ostatus__in=valid_status1)
    for o in order_solid:
        # 获取此产品订单对应的派息订单
        order_payout = Orderpayoutdetail.objects.filter(account_cid=account_cid, product_cid=o.product_cid).order_by(
            'dividenddate')
        product = Productofficial.objects.get(crm_id=o.product_cid)
        # 初始化
        profit = 0
        paybackamount = 0
        start_asset = 0
        end_asset = o.money_4
        # 从订单起息日对应的年月开始
        date_year = o.startearningday.year
        date_month = o.startearningday.month
        date = datetime.date(date_year, date_month, 1)
        # 进行资产累积计算
        while date < end_date:
            # is_changed控制资产计算路径
            is_changed = 0
            # 检查是否有派息
            if order_payout:
                profit = 0
                paybackamount = 0
                # 计算每个派息情况
                for op in order_payout:
                    # 如果年月匹配,修改期初期末资产
                    if op.dividenddate.month == date.month and op.dividenddate.year == date.year and (
                            product not in solid_products):
                        if not op.defendamount:
                            op.defendamount = 0
                        if not op.paybackamount:
                            op.paybackamount = 0
                        profit += op.defendamount
                        paybackamount += op.paybackamount
                        end_asset -= op.paybackamount
                        # 保存至月交易记录
                        solid_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": op.dividenddate, "type": "派息", "totalamount": op.totalamount, "num": "--",
                            "value": "--"
                        })
                        is_changed = 1
                # start_asset = end_asset
            # 如果已经进行了派息的修改操作，则不执行以下语句
            if is_changed == 0:
                profit = 0
                paybackamount = 0
                start_asset = end_asset
                end_asset -= paybackamount
            date += relativedelta(months=1)
        # 判断是否存在同种产品
        if product not in solid_products:
            solid_result = ({
                "productname": product.name, "productabbr": product.productabbreviation,
                "start_asset": start_asset, "end_asset": end_asset, "profit": profit,
                "paybackamount": paybackamount,
            })
            solid_products.append(product)
            total_start_asset = start_asset
            total_end_asset = end_asset
            total_profit = profit
            total_paybackamount = paybackamount
        else:
            solid_result = ({
                "productname": product.name, "productabbr": product.productabbreviation,
                "start_asset": total_start_asset + start_asset, "end_asset": total_end_asset + end_asset,
                "profit": total_profit + profit, "paybackamount": total_paybackamount + paybackamount,
            })
            total_start_asset += start_asset
            total_end_asset += end_asset
            total_profit += profit
            total_paybackamount += paybackamount
    solid_results.append(solid_result)

    # 二级市场
    order_securities = Order.objects.filter(account_cid=account_cid, recordtypeid='0126F0000020pRqQAI',
                                            openday__lte=start_date, ostatus__in=valid_status1)
    for o in order_securities:
        # 获取此产品订单对应的赎回/调减/转让订单
        order_red = Orderredemption.objects.filter(account_cid=account_cid, product_cid=o.product_cid,
                                                   transfermoney__isnull=False,
                                                   approvalstatus__in=valid_status2).order_by('redemptiondate')
        order_share = Ordersharechangeinfo.objects.filter(account_cid=account_cid, product_cid=o.product_cid).order_by(
            'netdate')
        order_trans_out = Ordertransfer.objects.filter(transferaccount_cid=account_cid,
                                                       product_cid=o.product_cid,
                                                       approvalstatus__in=valid_status2).order_by('transferdate')
        order_trans_in = Ordertransfer.objects.filter(receiver_cid=account_cid,
                                                      product_cid=o.product_cid,
                                                      approvalstatus__in=valid_status2).order_by('transferdate')
        product = Productofficial.objects.get(crm_id=o.product_cid)
        # 初始化
        start_asset = 0
        end_asset = o.money_4
        if not o.num:
            o.num = o.money_4
        end_num = o.num
        end_value = "--"
        end_value_date = "--"
        # 从订单开放日对应的年月开始
        date_year = o.openday.year
        date_month = o.openday.month
        date = datetime.date(date_year, date_month, 1)
        # 进行资产累积计算
        while date < end_date:
            networth = Networth.objects.filter(product_cid=o.product_cid, updatedate__year=date.year,
                                               updatedate__month=date.month).order_by('-updatedate').first()
            # 净值表里没有信息的无法计算,抛弃此产品
            if not networth:
                break
            # is_changed控制资产计算路径
            is_changed = 0
            # 检查是否有赎回
            if order_red:
                # 计算每个赎回情况
                for od in order_red:
                    # 如果年月匹配,修改期初期末资产
                    if od.redemptiondate.month == date.month and od.redemptiondate.year == date.year and (
                            product not in securities_products):
                        end_num -= od.number
                        end_value = networth.value
                        end_value_date = networth.updatedate
                        # start_asset = end_asset
                        end_asset -= od.transfermoney
                        # 保存至月交易记录
                        securities_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": od.redemptiondate, "type": "赎回", "totalamount": od.transfermoney,
                            "num": od.number, "value": od.net,
                        })
                        is_changed = 1
            # 计算是否有调减
            if order_share:
                # 计算每个调减情况
                for os in order_share:
                    # 如果年月匹配,修改期初期末资产
                    if os.netdate.month == date.month and os.netdate.year == date.year and (
                            product not in securities_products):
                        end_num -= os.changeamount  # 注意changeamount可能出现负值
                        end_value = networth.value
                        end_value_date = networth.updatedate
                        # start_asset = end_asset
                        end_asset -= end_value * end_num  # 注意!精度计算可能有误差
                        # 保存至月交易记录
                        securities_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": os.netdate, "type": "调减", "totalamount": end_value * end_num,
                            "num": os.changeamount, "value": networth.value,
                        })
                        is_changed = 1
            # 检查是否有转让出
            if order_trans_out:
                # 计算每个转让出情况
                for oo in order_trans_out:
                    # 如果年月匹配,修改期初期末资产
                    if oo.transferdate.month == date.month and oo.transferdate.year == date.year and (
                            product not in securities_products):
                        end_num -= oo.transferamount2
                        end_value = networth.value
                        end_value_date = networth.updatedate
                        # start_asset = end_asset
                        end_asset -= oo.transferamount
                        # 保存至月交易记录
                        securities_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": oo.transferdate, "type": "转让出", "totalamount": oo.transferamount,
                            "num": oo.transferamount2, "value": oo.transfernet,
                        })
                        is_changed = 1
            # 检查是否有转让入
            if order_trans_in:
                # 计算每个转让出情况
                for oi in order_trans_in:
                    # 如果年月匹配,修改期初期末资产
                    if oi.transferdate.month == date.month and oi.transferdate.year == date.year and (
                            product not in securities_products):
                        end_num += oi.transferamount2
                        end_value = networth.value
                        end_value_date = networth.updatedate
                        # start_asset = end_asset
                        end_asset += oi.transferamount
                        # 保存至月交易记录
                        securities_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": oi.transferdate, "type": "转让入", "totalamount": oi.transferamount,
                            "num": oi.transferamount2, "value": oi.transfernet,
                        })
                        is_changed = 1
            if is_changed == 0:
                end_num = end_num
                end_value = networth.value
                end_value_date = networth.updatedate
                start_asset = end_asset
                end_asset = end_asset
            date += relativedelta(months=1)

        # 判断是否存在同种产品
        if product not in securities_products:
            securities_result = ({
                "productname": product.name, "productabbr": product.productabbreviation,
                "start_asset": start_asset, "end_asset": end_asset, "end_num": end_num,
                "end_value": end_value, "end_value_date": end_value_date,
            })
            securities_products.append(product)
            total_end_num = end_num
            total_start_asset = start_asset
            total_end_asset = end_asset
        else:
            securities_result = ({
                "productname": product.name, "productabbr": product.productabbreviation,
                "start_asset": total_start_asset + start_asset, "end_asset": total_end_asset + end_asset,
                "end_num": total_end_num + end_num,
                "end_value": end_value, "end_value_date": end_value_date,
            })
            total_end_num += end_num
            total_start_asset += start_asset
            total_end_asset += end_asset
    securities_results.append(securities_result)

    # 股权
    order_stock = Order.objects.filter(recordtypeid="0126F0000020pRsQAI", account_cid=account_cid,
                                       ostatus__in=valid_status1)
    for o in order_stock:
        # 获取此产品订单对应的转让订单
        order_trans_out = Ordertransfer.objects.filter(transferaccount_cid=account_cid,
                                                       product_cid=o.product_cid,
                                                       approvalstatus__in=valid_status2).order_by('transferdate')
        order_trans_in = Ordertransfer.objects.filter(receiver_cid=account_cid,
                                                      product_cid=o.product_cid,
                                                      approvalstatus__in=valid_status2).order_by('transferdate')
        product = Productofficial.objects.get(crm_id=o.product_cid)
        # 初始化
        start_asset = o.money_4
        end_asset = o.money_4
        if not o.num:
            o.num = o.money_4
        end_num = o.num
        end_value = "--"
        end_value_date = "--"
        if not o.date:
            continue
        # 从下单日对应的年月开始
        date_year = o.date.year
        date_month = o.date.month
        date = datetime.date(date_year, date_month, 1)
        # 进行资产累积计算
        while date < end_date:
            # is_changed控制资产计算路径
            is_changed = 0
            # 检查是否有转让出
            if order_trans_out:
                # 计算每个转让出情况
                for oo in order_trans_out:
                    # 如果年月匹配,修改期初期末资产
                    if oo.transferdate.month == date.month and oo.transferdate.year == date.year and (
                            product not in stock_products):
                        end_num -= oo.transferamount2
                        # 净值表里只有二级市场信息,作为占位符暂不删除,默认"--"
                        end_value = "--"
                        end_value_date = "--"
                        # start_asset = end_asset
                        end_asset -= oo.transferamount
                        # 保存至月交易记录
                        securities_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": oo.transferdate, "type": "转让出", "totalamount": oo.transferamount,
                            "num": oo.transferamount2, "value": oo.transfernet,
                        })
                        is_changed = 1
            # 检查是否有转让入
            if order_trans_in:
                # 计算每个转让出情况
                for oi in order_trans_in:
                    # 如果年月匹配,修改期初期末资产
                    if oi.transferdate.month == date.month and oi.transferdate.year == date.year and (
                            product not in stock_products):
                        end_num += oi.transferamount2
                        end_value = "--"
                        end_value_date = "--"
                        # start_asset = end_asset
                        end_asset += oi.transferamount
                        # 保存至月交易记录
                        securities_transactions.append({
                            "productname": product.name, "productabbr": product.productabbreviation,
                            "date": oi.transferdate, "type": "转让入", "totalamount": oi.transferamount,
                            "num": oi.transferamount2, "value": oi.transfernet,
                        })
                        is_changed = 1
            if is_changed == 0:
                end_num = end_num
                end_value = "--"
                end_value_date = "--"
                start_asset = end_asset
                end_asset = end_asset
            date += relativedelta(months=1)

        # 判断是否存在同种产品
        if product not in stock_products:
            stock_result = ({
                "productname": product.name, "productabbr": product.productabbreviation,
                "start_asset": start_asset, "end_asset": end_asset, "end_num": end_num,
                "end_value": end_value, "end_value_date": end_value_date,
            })
            stock_products.append(product)
            total_end_num = end_num
            total_start_asset = start_asset
            total_end_asset = end_asset
        else:
            stock_result = ({
                "productname": product.name, "productabbr": product.productabbreviation,
                "start_asset": total_start_asset + start_asset, "end_asset": total_end_asset + end_asset,
                "end_num": total_end_num + end_num,
                "end_value": end_value, "end_value_date": end_value_date,
            })
            total_end_num += end_num
            total_start_asset += start_asset
            total_end_asset += end_asset
    stock_results.append(stock_result)

    # 清理交易记录
    for so in solid_transactions[:]:
        if start_date > so["date"]:
            solid_transactions.remove(so)
    for se in securities_transactions[:]:
        if start_date > se["date"]:
            securities_transactions.remove(se)
    for st in stock_transactions[:]:
        if start_date > st["date"]:
            stock_transactions.remove(st)
    transactions.extend(solid_transactions)
    transactions.extend(securities_transactions)
    transactions.extend(stock_transactions)
    bills.append({
        "solid_results": solid_results, "securities_results": securities_results, "stock_results": stock_results,
        "transactions": transactions,
    })
    return HttpResponse(json.dumps(bills, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_earning(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    start_year = data['start_year']
    start_month = data['start_month']
    if not data:
        return HttpResponse('Request Error')
    start_date = datetime.date(start_year, start_month, 1)
    end_date = datetime.datetime.now().date()
    results = []
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    valid_status2 = ['已批准']

    # 获取各类型订单
    order_solid = Order.objects.filter(account_cid=account_cid, recordtypeid='0126F0000020pRrQAI',
                                       ostatus__in=valid_status1)
    order_securities = Order.objects.filter(account_cid=account_cid, recordtypeid='0126F0000020pRqQAI',
                                            ostatus__in=valid_status1)
    # order_stock = Order.objects.filter(recordtypeid="0126F0000020pRsQAI", account_cid=account_cid,
    #                                    ostatus__in=valid_status1)

    date = start_date
    while date < end_date:
        # 初始化
        profit = 0
        # 类固收
        for o in order_solid:
            # 获取此产品订单对应的派息订单
            order_payout = Orderpayoutdetail.objects.filter(account_cid=account_cid,
                                                            product_cid=o.product_cid).order_by('dividenddate')
            # 检查是否有派息
            if order_payout:
                # 计算每个派息情况
                for op in order_payout:
                    # 如果年月匹配,修改期初期末资产
                    if op.dividenddate.month == date.month and op.dividenddate.year == date.year:
                        if not op.defendamount:
                            op.defendamount = 0
                        profit += op.defendamount
        # 二级市场
        for o in order_securities:
            # 获取此产品订单对应的赎回/调减/转让订单
            order_red = Orderredemption.objects.filter(account_cid=account_cid, product_cid=o.product_cid,
                                                       transfermoney__isnull=False,
                                                       approvalstatus__in=valid_status2).order_by('redemptiondate')
            if order_red:
                # 计算每个赎回情况
                for od in order_red:
                    # 如果年月匹配,修改期初期末资产
                    if od.redemptiondate.month == date.month and od.redemptiondate.year == date.year:
                        if not od.transfermoney:
                            od.transfermoney = 0
                        profit += od.transfermoney - o.money_4

        # 股权
        # 暂无

        results.append({
            "date": datetime.datetime.strftime(date, '%Y-%m'), "profit": profit,
        })
        date += relativedelta(months=1)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 同步函数，维护Asset表中二级收益数据
def calculate_all_redeemedearnings(request):
    today = datetime.datetime.now().date()
    # 注意！部分赎回无法计算
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回']
    valid_status2 = ['已批准']
    assets_securities = Asset.objects.filter(name__contains='二级市场')
    for i, a in enumerate(assets_securities):
        print(i)
        profit = 0
        redeemed = 0
        money_4 = 0
        # 记录赎回数据，防止累加
        redemption = []
        order_securities = Order.objects.filter(account_cid=a.account_cid, product_cid=a.product_cid,
                                                recordtypeid='0126F0000020pRqQAI',
                                                ostatus__in=valid_status1)

        for o in order_securities:
            order_red = Orderredemption.objects.filter(product_cid=o.product_cid, account_cid=o.account_cid,
                                                       transfermoney__isnull=False,
                                                       approvalstatus__in=valid_status2).order_by('redemptiondate')
            for od in order_red:
                if od not in redemption:
                    # 计算每个赎回情况
                    if od.redemptiondate <= today:
                        redeemed += od.transfermoney
                        redemption.append(od)
            money_4 += o.money_4
        # 计算相对收益
        # if redeemed != 0:
        #     profit = redeemed - money_4
        Asset.objects.filter(product_cid=a.product_cid, account_cid=a.account_cid).update(redeemedearnings=redeemed)
    return HttpResponse("计算所有二级收益完成")


# 将获取的crmid同步回数据库
def sync_order_2_mysql(order_id, crm_id):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()

    sql = "UPDATE %s.Order SET CRM_ID = %s WHERE ID = %s" % (DATABASES['default']['NAME'], crm_id, order_id)
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
    except:
        # 如果发生错误则回滚
        db.rollback()
    '''
    sql = "SELECT * FROM scc_5.Order where ID = %s" % order_id
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 获取所有记录列表
        results = cursor.fetchall()
        for row in results:
            print(row)
    except:
        print("Error: unable to fetch data")
    '''
    # 关闭数据库连接
    db.close()


# 同步订单表到CRM，未添加日志
async def sync_order_2_crm(url, data, order_id):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data) as response:
            text = await response.text()
            sync_order_2_mysql(order_id, text)
            return text


# 预约订单，主动同步
def order_product_by_responsible(request):
    from SCCProgram.wsgi import thread_loop_4_order

    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    product_cid = data['product_cid']
    customer_id = data['customer_id']
    bookmoney = data['bookmoney']
    try:
        product = Productofficial.objects.get(crm_id=product_cid)
        if product.origin > bookmoney:
            return HttpResponse('认购金额不足认购起点')
        if product.remainingamountcanbookedf < bookmoney:
            return HttpResponse('剩余可预约额度不足')
    except Productofficial.DoesNotExist:
        return CustomResponse('未找到产品')
    try:
        account = Account.objects.get(id=customer_id)
        if not account.crm_id:
            return CustomResponse('未找到用户')
    except Account.DoesNotExist:
        return CustomResponse('未找到用户')

    # 写入数据库
    today = datetime.datetime.now()
    new_order = Order.objects.create(product_cid=product_cid, account_cid=account.crm_id,
                                     responsible_cid=responsible_cid, bookmoney=bookmoney, ostatus='预约成功',
                                     createdate=today)

    # 维护剩余可预约额度
    Productofficial.objects.filter(crm_id=product_cid).update(
        remainingamountcanbookedf=product.remainingamountcanbookedf - bookmoney)

    # 异步执行CRM同步
    url = CRM_URLS['ORDER']
    crm_data = {
        'Product__c': product_cid,
        'Account__c': account.crm_id,
        'Responsible__c': responsible_cid,
        'BookMoney__c': bookmoney,
    }
    # 产生coroutine任务sync_order交给子线程thread_loop_4_order执行
    future = asyncio.run_coroutine_threadsafe(sync_order_2_crm(url, json.dumps(crm_data), new_order.id),
                                              thread_loop_4_order)
    # response = future.result()

    return HttpResponse(new_order.id)


# 暂不使用
def get_orderlist_by_responsible(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    # 加入订单按创建时间排序
    data = Order.objects.filter(responsible_cid=responsible_cid).order_by('-date')
    result = []
    for item in data:
        try:
            account = Account.objects.get(crm_id=item.account_cid)
        except Account.DoesNotExist:
            return CustomResponse('用户不存在' + item.account_cid)
        try:
            product = Productofficial.objects.get(crm_id=item.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在' + item.product_cid)
        info = {
            "product_name": product.name,
            "product_cid": product.crm_id,
            "account_name": account.name,
            "ostatus": item.ostatus,
            "bookmoney": item.bookmoney,
            "createdate": item.createdate,
            "money_4": item.money_4,
            "received_date": item.date,
            "order_id": item.id,
        }
        result.append(info)
    return HttpResponse(json.dumps(result, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 暂不使用
def get_orderinfo_by_responsible(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return CustomResponse('订单不存在:' + order_id)
    try:
        product = Productofficial.objects.get(crm_id=order.product_cid)
    except Productofficial.DoesNotExist:
        return CustomResponse('产品不存在' + order.product_cid)
    try:
        account = Account.objects.get(crm_id=order.account_cid)
    except Account.DoesNotExist:
        return CustomResponse('用户不存在' + order.account_cid)
    try:
        bank = Accountbank.objects.get(crm_id=order.accountbank_cid)
        bankid = bank.bankno
    except Accountbank.DoesNotExist:
        bankid = '-'
    solid_info = []
    security_info = []
    if product.recordtype == '类固收':
        solid_info = ({
            'term': order.term,
            'defendrate': order.defendrate,
            'startearningday': order.startearningday,
            'maturitydate': order.maturitydate,
        })
    elif product.recordtype == '二级市场':
        security_info = ({
            'openday': order.openday,
            'confirmdate': order.confirmdate,
            'nownet': order.nownet,
            'num': order.num,
        })
    result = ({
        'product_name': product.name,
        'account_name': account.name,
        'ostatus': order.ostatus,
        'bookmoney': order.bookmoney,
        'createdate': order.createdate,
        'money_4': order.money_4,
        'received_date': order.date,
        'order_type': product.recordtype,
        'account_bankid': bankid,
        'subscription_fee': order.subscription_fee,
        'solid_info': solid_info,
        'security_info': security_info,
    })
    return HttpResponse(json.dumps(result, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')

def get_order_by_responsible_new(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    if responsible_cid == '':
        return CustomResponse('投顾ID不能为空')
    try:
        responsible = Responsible.objects.get(crm_id=responsible_cid)
    except Responsible.DoesNotExist:
        return CustomResponse('投顾不存在:' + responsible_cid)
    #分别根据投顾id 查询order表的DependentsResCrmId和Responsible_CID的数据  然后进行去重合并
    orders = Order.objects.filter(Q(responsible_cid=responsible_cid) | Q(dependentsrescrmid=responsible_cid)).order_by('-date')
    results = []
    for order in orders:
        try:
            product = Productofficial.objects.get(crm_id=order.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在' + order.product_cid)
        order_responsible_name = responsible.name
        #订单表所属投顾查询
        if order.responsible_cid:
            if order.responsible_cid != responsible_cid:
                res = Responsible.objects.get(crm_id=order.responsible_cid)
                order_responsible_name = res.name
        try:
            print(order.account_cid)
            account = Account.objects.get(crm_id=order.account_cid)
            if account.recordtypeid == '0126F0000020pRJQAY':
                recordtype = '个人'
            else:
                recordtype = '机构'
        except Account.DoesNotExist:
            return CustomResponse('用户不存在' + order.account_cid)
        bankid = '-'
        bankno = '-'
        bankName = '-'
        try:
            bank = Accountbank.objects.get(crm_id=order.accountbank_cid)
            bankid = bank.bankno
            bankno = bank.bankno
            bankName = bank.bankname
        except Accountbank.DoesNotExist:
            bankid = '-'

        solid_info = None
        security_info = None
        if order.createdate:
            sort_date = order.createdate
        else:
            sort_date = datetime.date(1900, 1, 1)

        if product.recordtype == '类固收':
            solid_info = ({
                'term': order.term,
                'defendrate': order.defendrate,
                'startearningday': order.startearningday,
                'maturitydate': order.maturitydate,
            })
        elif product.recordtype == '二级市场':
            security_info = ({
                'openday': order.openday,
                'confirmdate': order.confirmdate,
                'nownet': order.nownet,
                'num': order.num,
            })
        result = ({
            'responsible_name': order_responsible_name,
            'product_name': product.name,
            'product_risklevel': product.risklevel,
            'product_id': product.id,
            'account_name': account.name,
            'account': account.name,
            'account_id': account.id,
            'account_recordtype': recordtype,
            'order_id': order.id,
            'order_no': order.orderno,
            'ostatus': order.ostatus,
            'bookmoney': order.bookmoney,
            'createdate': order.createdate,
            'money_4': order.money_4,
            'received_date': order.date,
            'order_type': product.recordtype,
            'account_bankid': bankid,
            'bankno': bankno,
            'bankName': bankName,
            'subscription_fee': order.subscription_fee,
            'task_id': order.taskid,
            'solid_info': solid_info,
            'security_info': security_info,
            'sort_date': sort_date,
            'origin': product.origin,
            'addmin': product.addmin,
        })
        results.append(result)
    results = sorted(results, key=itemgetter('sort_date', 'order_id'), reverse=True)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')

def get_order_by_responsible(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    if responsible_cid == '':
        return CustomResponse('投顾ID不能为空')
    orders = Order.objects.filter(responsible_cid=responsible_cid).order_by('-date')
    try:
        responsible = Responsible.objects.get(crm_id=responsible_cid)
    except Responsible.DoesNotExist:
        return CustomResponse('投顾不存在:' + responsible_cid)
    results = []
    for order in orders:
        try:
            product = Productofficial.objects.get(crm_id=order.product_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在' + order.product_cid)

        try:
            account = Account.objects.get(crm_id=order.account_cid)
            if account.recordtypeid == '0126F0000020pRJQAY':
                recordtype = '个人'
            else:
                recordtype = '机构'
        except Account.DoesNotExist:
            return CustomResponse('用户不存在' + order.account_cid)
        bankid = '-'
        bankno = '-'
        bankName = '-'
        try:
            bank = Accountbank.objects.get(crm_id=order.accountbank_cid)
            bankid = bank.bankno
            bankno = bank.bankno
            bankName = bank.bankname
        except Accountbank.DoesNotExist:
            bankid = '-'

        solid_info = None
        security_info = None
        if order.createdate:
            sort_date = order.createdate
        else:
            sort_date = datetime.date(1900, 1, 1)

        if product.recordtype == '类固收':
            solid_info = ({
                'term': order.term,
                'defendrate': order.defendrate,
                'startearningday': order.startearningday,
                'maturitydate': order.maturitydate,
            })
        elif product.recordtype == '二级市场':
            security_info = ({
                'openday': order.openday,
                'confirmdate': order.confirmdate,
                'nownet': order.nownet,
                'num': order.num,
            })
        result = ({
            'responsible_name': responsible.name,
            'product_name': product.name,
            'product_risklevel': product.risklevel,
            'product_id': product.id,
            'account_name': account.name,
            'account': account.name,
            'account_id': account.id,
            'account_recordtype': recordtype,
            'order_id': order.id,
            'order_no': order.orderno,
            'ostatus': order.ostatus,
            'bookmoney': order.bookmoney,
            'createdate': order.createdate,
            'money_4': order.money_4,
            'received_date': order.date,
            'order_type': product.recordtype,
            'account_bankid': bankid,
            'bankno': bankno,
            'bankName': bankName,
            'subscription_fee': order.subscription_fee,
            'task_id': order.taskid,
            'solid_info': solid_info,
            'security_info': security_info,
            'sort_date': sort_date,
            'origin': product.origin,
            'addmin': product.addmin,
        })
        results.append(result)
    results = sorted(results, key=itemgetter('sort_date', 'order_id'), reverse=True)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def save_order_cache(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    try:
        order = Order.objects.get(id=order_id)
        order.ordercache = json.dumps(data)
        order.save()
        return HttpResponse('订单缓存保存成功')
    except Order.DoesNotExist:
        return CustomResponse('订单不存在:' + order_id)

def save_order_cacheBySign(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    accountBankCId = data['accountBankCId']
    try:
        order = Order.objects.get(id=order_id)
        order.ordercache = json.dumps(data)
        order.accountbank_cid = accountBankCId
        order.save()
        return HttpResponse('订单缓存保存成功')
    except Order.DoesNotExist:
        return CustomResponse('订单不存在:' + order_id)

def load_order_cache(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    try:
        order = Order.objects.get(id=order_id)
        return HttpResponse(order.ordercache)
    except Order.DoesNotExist:
        return CustomResponse('订单不存在:' + order_id)


def get_contract_info(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    try:
        order = Order.objects.get(id=order_id)
        try:
            product = Productofficial.objects.get(crm_id=order.product_cid)
            try:
                account = Account.objects.get(crm_id=order.account_cid)
                try:
                    customer_banks = Accountbank.objects.filter(owneraccount_cid=order.account_cid)
                    banks = []
                    try:
                        productbank = Productbank.objects.get(product_cid=order.product_cid, type='募集账户')
                        for bank in customer_banks:
                            banks.append({
                                'customer_bankname': bank.bankname,
                                'customer_bankno': bank.bankno,
                                'customer_bankCID': bank.crm_id,
                            })
                        result = ({
                            'product_name': product.name,
                            'bookmoney': order.bookmoney,
                            'origin': product.origin,
                            'addmin': product.addmin,
                            'product_fundername': productbank.accountname,
                            'product_bankname': productbank.bankname,
                            'product_bankno': productbank.bankno,
                            'paperstype': account.paperstype,
                            'certificatenumber': base64_decode(account.certificatenumber),
                            'banks': banks,
                        })
                        return HttpResponse(json.dumps(result, ensure_ascii=False, cls=CustomEncoder),
                                            content_type="application/json",
                                            charset='utf-8')
                    except Productbank.DoesNotExist:
                        return CustomResponse('募集账户不存在:' + order.product_cid)
                except Accountbank.DoesNotExist:
                    return CustomResponse('客户账户不存在:' + order.accountbank_cid)
            except Account.DoesNotExist:
                return CustomResponse('客户不存在:' + order.account_cid)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + order.product_cid)
    except Order.DoesNotExist:
        return CustomResponse('订单不存在:' + order_id)

# 计算 类固收 私募证券昨日收益
def get_user_earnings(account_cid):
    sql = " select p.RecordType as type,o.BuyAmount,o.rate,p.crm_id,o.HoldShare,o.nownet,p.ProductAbbreviation " \
          " from Asset o " \
           " left join ProductOfficial p on p.CRM_ID = o.Product_CID " \
           " where o.Account_CID = '%s'  " % (account_cid)
    sqlresult = executeSql(sql)
    earnings = 0
    if sqlresult != "":
        for i in sqlresult:
            # 产品类别
            type = i[0]
            # 认购本金
            buyAmount = i[1]
            # 业绩比较基准
            rate = i[2]
            # 产品crmid
            p_crm_id = i[3]
            # 持有份额
            holdshare = i[4]
            # 买入净值
            nownet = i[5]
            if type == "类固收" and buyAmount and rate:
                #rate 数据库显示格式为7.5即7.5%
                earnings += (buyAmount * rate / 100) / 365
            if type == "二级市场":
                # 获取现在时间
                nowDate_1 = (datetime.datetime.now()+datetime.timedelta(days=-1)).strftime("%Y-%m-%d")
                # 获取该产品最新净值
                print(p_crm_id ,"a0U6F00000lM5MRUA0" )
                networths = Networth.objects.filter(product_cid=p_crm_id).order_by("-updatedate")
                # 如果最新净值时间 等于上一天时间 就显示最新净值，如果不等于上一天时间 就设为0
                # 昨日单位净值-昨日前一天非空单位净值
                if len(networths) >= 2:
                    yesterday = networths[0]
                    the_day_before = networths[1]
                    if yesterday.updatedate and nowDate_1 == str(yesterday.updatedate):
                        value = 0
                        if yesterday.value:
                            value = yesterday.value
                        if the_day_before.value and value != 0:
                            value = value - the_day_before.value
                        earnings += (holdshare * value)
                if len(networths) == 1:
                    yesterday = networths[0]
                    if yesterday.updatedate and nowDate_1 == str(yesterday.updatedate):
                        if yesterday.value:
                            earnings += (holdshare * (yesterday.value - nownet))
    return format(round(earnings, 2) , ",")

# 资产
def get_user_asset(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    if account_cid == "":
        return resultCodeAndData(400, "account_cid不能为空", "")
    try:
        account = Account.objects.get(crm_id=account_cid)
    except Account.DoesNotExist:
        return resultCodeAndData(400, "用户不存在", "")
    sumNewAsset = Asset.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('newasset'))
    sumFloatmoney = Asset.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('floatmoney'))
    # 已派息金额 = 基准收益+补贴金额
    sumDefendamount = Orderpayoutdetail.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('defendamount')) # 基准收益
    sumCompanysubsidy = Orderpayoutdetail.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('companysubsidy'))# 补贴金额
    sql = " select p.RecordType as type,SUM(o.BuyAmount) as buymoney,SUM(o.NewAsset)  from Asset o " \
           " left join ProductOfficial p on p.CRM_ID = o.Product_CID " \
           " where o.Account_CID = '%s' and o.BuyAmount is not null group by p.RecordType " % (account.crm_id)
    sqlresult = executeSql(sql)
    results = [{
            "name" : "类固收",
            "category": "定融计划",  # 产品类别
            "assets_lv": 0,  # 资产占比
            "money": 0,  # 资产金额
            "money_str": 0,  # 资产金额
        }, {
            "name": "股权",
            "category": "私募股权",  # 产品类别
            "assets_lv": 0,  # 资产占比
            "money": 0,  # 资产金额
            "money_str": 0,  # 资产金额
        }, {
            "name": "二级市场",
            "category": "私募证券",  # 产品类别
            "assets_lv": 0,  # 资产占比
            "money": 0,  # 资产金额
            "money_str": 0,  # 资产金额
        }]
    if sqlresult != "":
        for i in sqlresult:
            money = i[1] if i[1] else 0 #认购本金
            newAsset = i[2] if i[2] else 0 #最新市值
            name = i[0]
            for x in results:
                if name == x["name"]:
                    x["money"] = newAsset
                    x["money_str"] = format(newAsset, ',')
                    if sumNewAsset['sum'] != 0 :
                        x["assets_lv"] = str(round(newAsset / sumNewAsset['sum'] * 100, 2))
    inventory_earnings = "-"
    grand_total_earnings = "-"
    if sumFloatmoney['sum']:
        inventory_earnings = format(sumFloatmoney['sum'], ',')
        grand_total_earnings = sumFloatmoney['sum']
    total_assets = "-"
    if sumNewAsset['sum']:
        total_assets = format(sumNewAsset['sum'], ',')
    # 累计收益
    if sumDefendamount['sum']:
        grand_total_earnings += sumDefendamount['sum']
    if sumCompanysubsidy['sum']:
        grand_total_earnings += sumCompanysubsidy['sum']
    if grand_total_earnings != "-":
        grand_total_earnings = format(grand_total_earnings, ',')
    result = {
        "total_assets": total_assets, # 总资产
        "yesterda_earnings": get_user_earnings(account_cid), # 昨日收益(元)
        "inventory_earnings": inventory_earnings , # 持仓收益(元)
        "grand_total_earnings": grand_total_earnings, # 累计收益(元) + or -
        "results": results
    }
    return resultCodeAndData(200 , "请求成功" , result)

# 资产详情
def get_user_asset_info(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    if account_cid == "":
        return resultCodeAndData(400, "account_cid不能为空", "")
    try:
        account = Account.objects.get(crm_id=account_cid)
    except Account.DoesNotExist:
        return resultCodeAndData(400, "用户不存在", "")
    # 总资产
    sumNewAsset = Asset.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('newasset'))
    # 持仓收益
    sumFloatmoney = Asset.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('floatmoney'))
    # 已派息金额 = 基准收益+补贴金额
    sumDefendamount = Orderpayoutdetail.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('defendamount')) # 基准收益
    sumCompanysubsidy = Orderpayoutdetail.objects.filter(account_cid=account.crm_id).aggregate(sum=Sum('companysubsidy'))# 补贴金额
    #已分配收益+赎回收益 TODO
    sql = " select p.RecordType as type,o.BuyAmount,p.name,p.id,o.NewAsset,o.StartEarningDay,o.MaturityDate,p.productabbreviation," \
          " o.Rate,o.Term,o. Date ,o. Profit,o.NewAsset,o.NewCleanValueDate,o.FloatMoney" \
          " from Asset o " \
           " left join ProductOfficial p on p.CRM_ID = o.Product_CID " \
           " where o.Account_CID = '%s' and o.BuyAmount is not null  " % (account.crm_id)
    sqlresult = executeSql(sql)
    lgs = []
    gq = []
    zq = []
    if sqlresult != "":
        for i in sqlresult:
            name = i[0] #类别
            buyamount = i[1] #认购本金
            product_name = i[2] #产品名称
            product_id = i[3] #产品id
            newasset = i[4] #最新市值
            startearningday = i[5] #起息日
            maturitydate = i[6] #到息日
            product_productabbreviation = i[7] #产品简称
            asset_rate = i[8] #业绩比较基准
            asset_term = i[9] #投资期限
            asset_date = i[10] #到账日期
            asset_profit = i[11] #已分配收益
            asset_newasset = i[12] #参考市值
            asset_newcleanvaluedate = i[13] #参考市值日期
            asset_floatmoney = i[14] #持仓收益
            common ={
                "buyamount" : format(buyamount, ",") if buyamount else "", # 认购本金
                "product_id" : product_id, #产品id
                "product_name" : product_name, # 产品名称
                "product_productabbreviation" : product_productabbreviation, # 产品名称
                "assets_lv" :  str(round(newasset / sumNewAsset['sum'] * 100, 2)), # 资产占比
            }
            if name == "类固收":
                common.update({
                    "type": "定融计划", # 产品类别
                    "assets_jz": str(asset_rate) + "%" if asset_rate else "", # 业绩比较基准
                    "assets_startearningday": startearningday, #起息日
                    "assets_maturitydate": maturitydate,# 到息日
                })
                lgs.append(common)
            if name == "股权":
               common.update({
                   "type": "私募股权",  # 产品类别
                   "assets_investment_horizon": asset_term,  # 投资期限
                   "assets_account_date": asset_date,  # 到账日期
                   "assets_assigned_earnings": asset_profit,  # 已分配收益
                })
               gq.append(common)
            if name == "二级市场":
                common.update({
                    "type": "私募证券",  # 产品类别
                    "reference_value": format(asset_newasset , ",") if asset_newasset else "" ,  # 参考市值
                    "reference_value_date": format(asset_newcleanvaluedate, ",") if asset_newcleanvaluedate else "",  # 参考市值日期
                    "assets_account_date": asset_date,  # 到账日期
                    "assets_position_gains": format(asset_floatmoney, ",") if asset_floatmoney else "",  # 持仓收益
                })
                zq.append(common)
    if len(lgs) > 0:
        lgs = sorted(lgs, key=itemgetter('product_name', 'assets_maturitydate'), reverse=True)
    if len(gq) > 0:
        gq = sorted(gq, key=itemgetter('product_name', 'assets_account_date'), reverse=True)
    if len(zq) > 0:
        zq = sorted(zq, key=itemgetter('product_name', 'assets_account_date'), reverse=True)
    results = {
        "lgs": lgs,
        "gq": gq,
        "zq": zq,
    }
    inventory_earnings =  "-"
    grand_total_earnings = "-"
    if sumFloatmoney['sum']:
        inventory_earnings = format(sumFloatmoney['sum'], ',')
        grand_total_earnings = sumFloatmoney['sum']
    total_assets = "-"
    if sumNewAsset['sum']:
        total_assets = format(sumNewAsset['sum'], ',')
    # 累计收益
    if sumDefendamount['sum']:
        grand_total_earnings += sumDefendamount['sum']
    if sumCompanysubsidy['sum']:
        grand_total_earnings += sumCompanysubsidy['sum']
    if grand_total_earnings != "-":
        grand_total_earnings = format(grand_total_earnings, ',')
    result = {
        "total_assets": total_assets, # 总资产
        "yesterda_earnings": get_user_earnings(account_cid), # 昨日收益(元)
        "inventory_earnings": inventory_earnings, # 持仓收益(元)
        "grand_total_earnings": grand_total_earnings, # 累计收益(元) + or -
        "results": results
    }
    return resultCodeAndData(200, "请求成功", result)


def resultCodeAndData(code, msg, results):
    results = {'code': code,
               'msg': msg,
               "data": results
               }
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json,charset=utf-8")


def executeSql(sql):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()
    results = ""
    try:
        # 执行sql语句
        cursor.execute(sql)
        results = cursor.fetchall()
        # 提交到数据库执行
        db.commit()
    except:
        # 如果发生错误则回滚
        db.rollback()
    '''
    sql = "SELECT * FROM scc_5.Order where ID = %s" % order_id
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 获取所有记录列表
        results = cursor.fetchall()
        for row in results:
            print(row)
    except:
        print("Error: unable to fetch data")
    '''
    # 关闭数据库连接
    db.close()
    return results