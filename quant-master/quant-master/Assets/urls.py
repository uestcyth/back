from django.urls import path

from . import views

app_name = 'Assets'
urlpatterns = [
    # 未使用
    path('order_product_by_customer/', views.order_product_by_customer, name='order_product_by_customer'),
    path('get_security_detail/', views.get_security_detail, name='get_security_detail'),
    path('get_solid_detail/', views.get_solid_detail, name='get_solid_detail'),
    path('search_by_date/', views.search_by_date, name='search_by_date'),
    # 资产详情
    path('get_assets/', views.get_assets, name='get_assets'),
    path('get_assets_list/', views.get_assets_list, name='get_assets_list'),
    path('detail_asset/', views.detail_asset, name='detail_asset'),
    # 交易记录
    path('get_my_order/', views.get_my_order, name='get_my_order'),
    path('get_my_orderredemption/', views.get_my_orderredemption, name='get_my_orderredemption'),
    path('get_my_ordersharechangeinfo/', views.get_my_ordersharechangeinfo, name='get_my_ordersharechangeinfo'),
    path('get_my_orderpayoutdetail/', views.get_my_orderpayoutdetail, name='get_my_orderpayoutdetail'),
    path('get_my_booked_order/', views.get_my_booked_order, name='get_my_booked_order'),
    path('get_my_ordertransfer/', views.get_my_ordertransfer, name='get_my_ordertransfer'),
    path('get_all_order/', views.get_all_order, name='get_all_order'),
    # 收益
    path('get_profit_details/', views.get_profit_details, name='get_profit_details'),
    path('get_asset_info/', views.get_asset_info, name='get_asset_info'),
    path('order_list_info/', views.order_list_info, name='order_list_info'),
    path('get_profit/', views.get_profit, name='get_profit'),
    path('get_profit_by_type/', views.get_profit_by_type, name='get_profit_by_type'),
    # 对账单
    path('get_monthly_bill/', views.get_monthly_bill, name='get_monthly_bill'),
    # 盈利图
    path('get_earning/', views.get_earning, name='get_earning'),
    # 维护二级收益数据
    path('calculate_all_redeemedearnings/', views.calculate_all_redeemedearnings,
         name='calculate_all_redeemedearnings'),
    # 预约订单
    path('order_product_by_responsible/', views.order_product_by_responsible, name='order_product_by_responsible'),
    path('get_orderlist_by_responsible/', views.get_orderlist_by_responsible, name='get_orderlist_by_responsible'),
    path('get_orderinfo_by_responsible/', views.get_orderinfo_by_responsible, name='get_orderinfo_by_responsible'),
    path('get_order_by_responsible/', views.get_order_by_responsible, name='get_order_by_responsible'),
    path('get_order_by_responsible_new/', views.get_order_by_responsible_new, name='get_order_by_responsible_new'),
    path('detail_booked_order/', views.detail_booked_order, name='detail_booked_order'),
    path('save_order_cache/', views.save_order_cache, name='save_order_cache'),
    path('load_order_cache/', views.load_order_cache, name='load_order_cache'),
    path('save_order_cacheBySign/', views.save_order_cacheBySign, name='save_order_cacheBySign'),
    path('get_contract_info/', views.get_contract_info, name='get_contract_info'),
    # 资产
    path('get_user_asset/', views.get_user_asset, name='get_user_asset'),
    # 资产详情
    path('get_user_asset_info/', views.get_user_asset_info, name='get_user_asset_info'),

]
