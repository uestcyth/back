# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Account(models.Model):
    recordtypeid = models.CharField(db_column='RecordTypeId', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=45, blank=True, null=True)  # Field name made lowercase.
    num = models.CharField(db_column='Num', max_length=45, blank=True, null=True)  # Field name made lowercase.
    elephone = models.CharField(db_column='Elephone', max_length=45, blank=True, null=True)  # Field name made lowercase.
    paperstype = models.CharField(db_column='Paperstype', max_length=45, blank=True, null=True)  # Field name made lowercase.
    certificatenumber = models.CharField(db_column='CertificateNumber', max_length=45, blank=True, null=True)  # Field name made lowercase.
    salutation = models.CharField(db_column='Salutation', max_length=20, blank=True, null=True)  # Field name made lowercase.
    createddate = models.DateField(db_column='CreatedDate', blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=45, blank=True, null=True)  # Field name made lowercase.
    branch = models.CharField(db_column='Branch', max_length=45, blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=45, blank=True, null=True)  # Field name made lowercase.
    expire = models.DateField(db_column='Expire', blank=True, null=True)  # Field name made lowercase.
    responsible_id = models.CharField(db_column='Responsible_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    risklevel = models.CharField(db_column='Risklevel', max_length=45, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    user_id = models.IntegerField(db_column='User_ID', blank=True, null=True)  # Field name made lowercase.
    unionid = models.CharField(db_column='UnionId', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remarkname = models.CharField(db_column='RemarkName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remark = models.CharField(db_column='Remark', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    sex = models.IntegerField(db_column='Sex', blank=True, null=True)  # Field name made lowercase.
    res_phone = models.CharField(db_column='Res_Phone', max_length=255, blank=True, null=True)  # Field name made lowercase.
    birthday = models.CharField(db_column='Birthday', max_length=255, blank=True, null=True)  # Field name made lowercase.
    frxm = models.CharField(db_column='Frxm', max_length=255, blank=True, null=True)  # Field name made lowercase.
    currentasset = models.DecimalField(db_column='CurrentAsset', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nowasset = models.DecimalField(db_column='NowAsset', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Account'


class Accountbank(models.Model):
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    owneraccount_cid = models.CharField(db_column='OwnerAccount_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bankname = models.CharField(db_column='BankName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accountname = models.CharField(db_column='AccountName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    bankno = models.CharField(db_column='BankNO', max_length=45, blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=45, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AccountBank'


class Agencyinfo(models.Model):
    code = models.CharField(db_column='Code', max_length=45)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=45)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AgencyInfo'


class Asset(models.Model):
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255)  # Field name made lowercase.
    account_cid = models.CharField(db_column='Account_CID', max_length=255)  # Field name made lowercase.
    buyamount = models.DecimalField(db_column='BuyAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    allshare = models.DecimalField(db_column='AllShare', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    costvalue = models.DecimalField(db_column='CostValue', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    holdshare = models.DecimalField(db_column='HoldShare', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    newcleanvalue = models.DecimalField(db_column='NewCleanValue', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    newcleanvaluedate = models.DateField(db_column='NewCleanValueDate', blank=True, null=True)  # Field name made lowercase.
    newasset = models.DecimalField(db_column='NewAsset', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    floatmoney = models.DecimalField(db_column='FloatMoney', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    redeemedearnings = models.DecimalField(db_column='RedeemedEarnings', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    interest = models.DecimalField(db_column='Interest', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    allocatedamount = models.DecimalField(db_column='AllocatedAmount', max_digits=20, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    arrivaldate = models.DateField(db_column='ArrivalDate', blank=True, null=True)  # Field name made lowercase.
    nownet = models.DecimalField(db_column='NowNet', max_digits=20, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    startearningday = models.DateField(db_column='StartEarningDay', blank=True, null=True)  # Field name made lowercase.
    maturitydate = models.DateField(db_column='MaturityDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Asset'


class Channelmanagement(models.Model):
    rmcode = models.CharField(db_column='RMCode', max_length=255, blank=True, null=True)  # Field name made lowercase.
    department = models.CharField(db_column='Department', max_length=7, blank=True, null=True)  # Field name made lowercase.
    team = models.CharField(db_column='Team', max_length=45, blank=True, null=True)  # Field name made lowercase.
    branch = models.CharField(db_column='Branch', max_length=5, blank=True, null=True)  # Field name made lowercase.
    positioni = models.CharField(db_column='PositionI', max_length=45, blank=True, null=True)  # Field name made lowercase.
    is_incumbent = models.IntegerField(db_column='IS_Incumbent', blank=True, null=True)  # Field name made lowercase.
    channel = models.CharField(db_column='Channel', max_length=2, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=45, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(db_column='User', max_length=45, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='Phone', max_length=45, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    user_id = models.IntegerField(db_column='User_ID', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ChannelManagement'


class ChannelmanagementResponsible(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    channelmanagement_id = models.IntegerField(db_column='ChannelManagement_ID', blank=True, null=True)  # Field name made lowercase.
    responsible_id = models.IntegerField(db_column='Responsible_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ChannelManagement_Responsible'


class Combinationinfo(models.Model):
    name = models.CharField(db_column='Name', max_length=45, blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=45)  # Field name made lowercase.
    expectprofits = models.DecimalField(db_column='ExpectProfits', max_digits=10, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    createdate = models.DateField(db_column='CreateDate', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CombinationInfo'


class Combinationposition(models.Model):
    combinationinfo = models.ForeignKey(Combinationinfo, models.DO_NOTHING, db_column='CombinationInfo_ID')  # Field name made lowercase.
    responsible_cid = models.CharField(db_column='Responsible_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    account_cid = models.CharField(db_column='Account_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CombinationPosition'


class Combinationproduct(models.Model):
    combinationproportion = models.ForeignKey('Combinationproportion', models.DO_NOTHING, db_column='CombinationProportion_ID')  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CombinationProduct'


class Combinationproportion(models.Model):
    combination = models.ForeignKey(Combinationinfo, models.DO_NOTHING, db_column='Combination_ID')  # Field name made lowercase.
    producttype = models.CharField(db_column='ProductType', max_length=45, blank=True, null=True)  # Field name made lowercase.
    proportion = models.CharField(db_column='Proportion', max_length=45, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CombinationProportion'


class Commissiondetail(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rate = models.DecimalField(db_column='Rate', max_digits=18, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    productname = models.CharField(db_column='ProductName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    unit_cid = models.CharField(db_column='Unit__CID', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed because it contained more than one '_' in a row.
    maturitydate = models.DateField(db_column='MaturityDate', blank=True, null=True)  # Field name made lowercase.
    rgfee = models.DecimalField(db_column='RGFee', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=255, blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=100, blank=True, null=True)  # Field name made lowercase.
    calculationrule = models.CharField(db_column='CalculationRule', max_length=255, blank=True, null=True)  # Field name made lowercase.
    order_cid = models.CharField(db_column='Order_CID', max_length=100, blank=True, null=True)  # Field name made lowercase.
    rgmoney = models.DecimalField(db_column='RGMoney', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    responsible_cid = models.CharField(db_column='Responsible_CID', max_length=100, blank=True, null=True)  # Field name made lowercase.
    startearningday = models.DateField(db_column='StartEarningDay', blank=True, null=True)  # Field name made lowercase.
    amount = models.DecimalField(db_column='Amount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CommissionDetail'


class Commissionunit(models.Model):
    name = models.CharField(db_column='Name', max_length=45, blank=True, null=True)  # Field name made lowercase.
    responsible_cid = models.CharField(db_column='Responsible_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CommissionUnit'


class Communicateboard(models.Model):
    from_cid = models.CharField(db_column='From_CID', max_length=255)  # Field name made lowercase.
    to_cid = models.CharField(db_column='To_CID', max_length=255)  # Field name made lowercase.
    role_from = models.IntegerField(db_column='Role_from')  # Field name made lowercase.
    role_to = models.IntegerField(db_column='Role_to')  # Field name made lowercase.
    content = models.TextField(db_column='Content', blank=True, null=True)  # Field name made lowercase.
    time = models.DateTimeField(db_column='Time', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CommunicateBoard'


class Contractno(models.Model):
    order = models.ForeignKey('Order', models.DO_NOTHING, db_column='Order_ID', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='Status')  # Field name made lowercase.
    no = models.CharField(db_column='No', max_length=255)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    type = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ContractNo'


class Favoriteproduct(models.Model):
    user = models.ForeignKey('User', models.DO_NOTHING, db_column='User_ID')  # Field name made lowercase.
    product = models.ForeignKey('Productofficial', models.DO_NOTHING, db_column='Product_ID')  # Field name made lowercase.
    adddate = models.DateField(db_column='AddDate')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FavoriteProduct'


class Messageboard(models.Model):
    crm_id = models.CharField(db_column='CRM_ID', max_length=255)  # Field name made lowercase.
    content = models.CharField(db_column='Content', max_length=800, blank=True, null=True)  # Field name made lowercase.
    reply = models.CharField(db_column='Reply', max_length=255, blank=True, null=True)  # Field name made lowercase.
    messagetime = models.DateTimeField(db_column='MessageTime', blank=True, null=True)  # Field name made lowercase.
    replytime = models.DateTimeField(db_column='ReplyTime', blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MessageBoard'


class Networth(models.Model):
    product_cid = models.CharField(db_column='Product_CID', max_length=255)  # Field name made lowercase.
    value = models.DecimalField(db_column='Value', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    updatedate = models.DateField(db_column='UpDateDate', blank=True, null=True)  # Field name made lowercase.
    conquire = models.IntegerField(db_column='Conquire', blank=True, null=True)  # Field name made lowercase.
    cumulativenetworth = models.DecimalField(db_column='CumulativeNetWorth', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NetWorth'


class Order(models.Model):
    recordtypeid = models.CharField(db_column='RecordTypeId', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ostatus = models.CharField(db_column='Ostatus', max_length=45, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    account_cid = models.CharField(db_column='Account_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    responsible_cid = models.CharField(db_column='Responsible_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    am_find_user_cid = models.CharField(db_column='AM_Find_User_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bookmoney = models.DecimalField(db_column='BookMoney', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    money_4 = models.DecimalField(db_column='Money_4', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    money5 = models.DecimalField(db_column='Money5', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    reliefamount = models.DecimalField(db_column='ReliefAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    subscription_fee = models.DecimalField(db_column='Subscription_Fee', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    buymoney = models.DecimalField(db_column='BuyMoney', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    term = models.CharField(db_column='Term', max_length=45, blank=True, null=True)  # Field name made lowercase.
    defendrate = models.DecimalField(db_column='DefendRate', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    nownet = models.DecimalField(db_column='NowNet', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    num = models.DecimalField(db_column='Num', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    rmlocation = models.CharField(db_column='RMLocation', max_length=45, blank=True, null=True)  # Field name made lowercase.
    team = models.CharField(db_column='Team', max_length=5, blank=True, null=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    openday = models.DateField(db_column='Openday', blank=True, null=True)  # Field name made lowercase.
    startearningday = models.DateField(db_column='StartEarningDay', blank=True, null=True)  # Field name made lowercase.
    maturitydate = models.DateField(db_column='MaturityDate', blank=True, null=True)  # Field name made lowercase.
    paymentvoucher = models.TextField(db_column='PaymentVoucher', blank=True, null=True)  # Field name made lowercase.
    accountbank_cid = models.CharField(db_column='AccountBank_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cancelorderop = models.IntegerField(db_column='CancelOrderOp', blank=True, null=True)  # Field name made lowercase.
    cancelreason = models.CharField(db_column='CancelReason', max_length=255, blank=True, null=True)  # Field name made lowercase.
    holdday = models.IntegerField(db_column='HoldDay', blank=True, null=True)  # Field name made lowercase.
    commissionformula = models.DecimalField(db_column='CommissionFormula', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    createdate = models.DateField(db_column='CreateDate', blank=True, null=True)  # Field name made lowercase.
    confirmdate = models.DateField(db_column='ConfirmDate', blank=True, null=True)  # Field name made lowercase.
    orderno = models.CharField(db_column='OrderNo', max_length=45, blank=True, null=True)  # Field name made lowercase.
    ordercache = models.TextField(db_column='OrderCache', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    taskid = models.CharField(db_column='TaskId', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agreementtaskid = models.CharField(db_column='agreementTaskId', max_length=50, blank=True, null=True)  # Field name made lowercase.
    issignagreement = models.IntegerField(db_column='isSignAgreement', blank=True, null=True)  # Field name made lowercase.
    dependentsrescrmid = models.CharField(db_column='DependentsResCrmId', max_length=100, blank=True, null=True)  # Field name made lowercase.
    commissionrate = models.DecimalField(db_column='CommissionRate', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Order'


class Orderpayoutdetail(models.Model):
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    account_cid = models.CharField(db_column='Account_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    startearningday = models.DateField(db_column='StartEarningDay', blank=True, null=True)  # Field name made lowercase.
    dividenddate = models.DateField(db_column='DividendDate', blank=True, null=True)  # Field name made lowercase.
    valuedays = models.DecimalField(db_column='ValueDays', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    defendrate = models.DecimalField(db_column='DefendRate', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    defendamount = models.DecimalField(db_column='DefendAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    companysubsidy = models.DecimalField(db_column='CompanySubsidy', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    calccapital = models.DecimalField(db_column='CalcCapital', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    paybackamount = models.DecimalField(db_column='PaybackAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    totalamount = models.DecimalField(db_column='TotalAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    comments = models.CharField(db_column='Comments', max_length=45, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OrderPayoutDetail'


class Orderredemption(models.Model):
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    account_cid = models.CharField(db_column='Account_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    number = models.DecimalField(db_column='Number', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    redemptiondate = models.DateField(db_column='RedemptionDate', blank=True, null=True)  # Field name made lowercase.
    net = models.DecimalField(db_column='Net', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    redemptionapplication = models.TextField(db_column='RedemptionApplication', blank=True, null=True)  # Field name made lowercase.
    redeemamount = models.DecimalField(db_column='RedeemAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    deduction = models.DecimalField(db_column='Deduction', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    transfermoney = models.DecimalField(db_column='Transfermoney', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    deductioninfo = models.CharField(db_column='Deductioninfo', max_length=45, blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    approvalstatus = models.CharField(db_column='Approvalstatus', max_length=45, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OrderRedemption'


class Ordersharechangeinfo(models.Model):
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    account_cid = models.CharField(db_column='Account_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    changereason = models.CharField(db_column='ChangeReason', max_length=45, blank=True, null=True)  # Field name made lowercase.
    netdate = models.DateField(db_column='NetDate', blank=True, null=True)  # Field name made lowercase.
    changeamount = models.DecimalField(db_column='ChangeAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OrderShareChangeInfo'


class Ordertransfer(models.Model):
    receiver_cid = models.CharField(db_column='Receiver_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    transferaccount_cid = models.CharField(db_column='TransferAccount_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    transferdate = models.DateField(db_column='TransferDate', blank=True, null=True)  # Field name made lowercase.
    transferamount = models.DecimalField(db_column='TransferAmount', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    transfernet = models.DecimalField(db_column='TransferNet', max_digits=20, decimal_places=5, blank=True, null=True)  # Field name made lowercase.
    transferamount2 = models.DecimalField(db_column='TransferAmount2', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    remarks = models.TextField(db_column='Remarks', blank=True, null=True)  # Field name made lowercase.
    approvalstatus = models.CharField(db_column='ApprovalStatus', max_length=45, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OrderTransfer'


class Productbank(models.Model):
    type = models.CharField(db_column='Type', max_length=45, blank=True, null=True)  # Field name made lowercase.
    bankname = models.CharField(db_column='BankName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    accountname = models.CharField(db_column='AccountName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    bankno = models.CharField(db_column='BankNO', max_length=45, blank=True, null=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ProductBank'


class Productfile(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    product_cid = models.CharField(db_column='Product_CID', max_length=100, blank=True, null=True)  # Field name made lowercase.
    filename = models.CharField(db_column='FileName', max_length=100, blank=True, null=True)  # Field name made lowercase.
    fileurl = models.CharField(db_column='FileUrl', max_length=225, blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ProductFile'


class Productofficial(models.Model):
    name = models.CharField(db_column='Name', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    productabbreviation = models.CharField(db_column='ProductAbbreviation', max_length=45, blank=True, null=True)  # Field name made lowercase.
    recordtype = models.CharField(db_column='RecordType', max_length=255, blank=True, null=True)  # Field name made lowercase.
    productbackupcode = models.CharField(db_column='ProductBackupCode', max_length=45, blank=True, null=True)  # Field name made lowercase.
    deaddate = models.DateField(db_column='DeadDate', blank=True, null=True)  # Field name made lowercase.
    setupdate = models.DateField(db_column='SetUpDate', blank=True, null=True)  # Field name made lowercase.
    responsible_cid = models.CharField(db_column='Responsible_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    scale = models.DecimalField(db_column='Scale', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    subjectname = models.CharField(db_column='SubjectName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    stage = models.CharField(db_column='Stage', max_length=45, blank=True, null=True)  # Field name made lowercase.
    value = models.DecimalField(db_column='Value', max_digits=20, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    valuedate = models.DateField(db_column='ValueDate', blank=True, null=True)  # Field name made lowercase.
    comparison = models.CharField(db_column='Comparison', max_length=255, blank=True, null=True)  # Field name made lowercase.
    orgtype = models.CharField(db_column='OrgType', max_length=45, blank=True, null=True)  # Field name made lowercase.
    compareindex = models.CharField(db_column='CompareIndex', max_length=45, blank=True, null=True)  # Field name made lowercase.
    risklevel = models.CharField(db_column='RiskLevel', max_length=45, blank=True, null=True)  # Field name made lowercase.
    startdaterule = models.CharField(db_column='StartDateRule', max_length=255, blank=True, null=True)  # Field name made lowercase.
    opendaydes = models.CharField(db_column='OpenDayDes', max_length=255, blank=True, null=True)  # Field name made lowercase.
    direction = models.CharField(db_column='Direction', max_length=45, blank=True, null=True)  # Field name made lowercase.
    investmentway = models.TextField(db_column='InvestmentWay', blank=True, null=True)  # Field name made lowercase.
    investmentobj = models.TextField(db_column='InvestmentObj', blank=True, null=True)  # Field name made lowercase.
    limit = models.TextField(db_column='Limit', blank=True, null=True)  # Field name made lowercase.
    investmentstructure = models.TextField(db_column='InvestmentStructure', blank=True, null=True)  # Field name made lowercase.
    analyze = models.TextField(db_column='Analyze', blank=True, null=True)  # Field name made lowercase.
    ratesub = models.DecimalField(db_column='RateSub', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    origin = models.DecimalField(db_column='Origin', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    addmin = models.DecimalField(db_column='AddMin', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    redemptionrate = models.CharField(db_column='RedemptionRate', max_length=255, blank=True, null=True)  # Field name made lowercase.
    adminacus = models.CharField(db_column='AdminACus', max_length=45, blank=True, null=True)  # Field name made lowercase.
    adminbcus = models.CharField(db_column='AdminBCus', max_length=45, blank=True, null=True)  # Field name made lowercase.
    admina_admincost = models.DecimalField(db_column='AdminA_AdminCost', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    counterparty = models.TextField(db_column='Counterparty', blank=True, null=True)  # Field name made lowercase.
    details = models.TextField(db_column='Details', blank=True, null=True)  # Field name made lowercase.
    monthlimit = models.DecimalField(db_column='MonthLimit', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    currency = models.CharField(db_column='Currency', max_length=3, blank=True, null=True)  # Field name made lowercase.
    controlopenmoney = models.DecimalField(db_column='ControlOpenMoney', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    intermamount = models.CharField(db_column='InTermAmount', max_length=45, blank=True, null=True)  # Field name made lowercase.
    renewalperiod = models.CharField(db_column='RenewalPeriod', max_length=45, blank=True, null=True)  # Field name made lowercase.
    productperiod = models.CharField(db_column='ProductPeriod', max_length=45, blank=True, null=True)  # Field name made lowercase.
    lockday = models.CharField(db_column='LockDay', max_length=45, blank=True, null=True)  # Field name made lowercase.
    mainriskdanger = models.TextField(db_column='MainRiskDanger', blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    formulatype = models.CharField(db_column='FormulaType', max_length=45, blank=True, null=True)  # Field name made lowercase.
    commissonratio = models.DecimalField(db_column='CommissonRatio', max_digits=20, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    incentive = models.IntegerField(db_column='Incentive', blank=True, null=True)  # Field name made lowercase.
    remainingamountcanbookedf = models.DecimalField(db_column='RemainingAmountCanBookedf', max_digits=20, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    file = models.CharField(db_column='File', max_length=2550, blank=True, null=True)  # Field name made lowercase.
    comparisonabbreviation = models.CharField(db_column='ComparisonAbbreviation', max_length=45, blank=True, null=True)  # Field name made lowercase.
    channel = models.CharField(db_column='Channel', max_length=255, blank=True, null=True)  # Field name made lowercase.
    onlinesign = models.IntegerField(db_column='OnlineSign', blank=True, null=True)  # Field name made lowercase.
    agreementsign = models.IntegerField(db_column='AgreementSign', blank=True, null=True)  # Field name made lowercase.
    redrule = models.CharField(db_column='RedRule', max_length=100, blank=True, null=True)  # Field name made lowercase.
    producttag = models.CharField(db_column='ProductTag', max_length=100, blank=True, null=True)  # Field name made lowercase.
    highlights = models.CharField(db_column='HighLights', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tactics = models.CharField(db_column='Tactics', max_length=255, blank=True, null=True)  # Field name made lowercase.
    subrule = models.CharField(db_column='SubRule', max_length=100, blank=True, null=True)  # Field name made lowercase.
    rateoperation = models.DecimalField(db_column='RateOperation', max_digits=20, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
    underlyingasset = models.CharField(db_column='UnderlyingAsset', max_length=520, blank=True, null=True)  # Field name made lowercase.
    underlyingassetdes = models.CharField(db_column='UnderlyingAssetDes', max_length=255, blank=True, null=True)  # Field name made lowercase.
    distributionmode = models.CharField(db_column='DistributionMode', max_length=100, blank=True, null=True)  # Field name made lowercase.
    publisher = models.CharField(db_column='Publisher', max_length=100, blank=True, null=True)  # Field name made lowercase.
    listedorg = models.CharField(db_column='ListedOrg', max_length=100, blank=True, null=True)  # Field name made lowercase.
    riskcontrolsys = models.CharField(db_column='RiskControlSys', max_length=255, blank=True, null=True)  # Field name made lowercase.
    perforrewardcus = models.CharField(db_column='PerforRewardCus', max_length=255, blank=True, null=True)  # Field name made lowercase.
    qiutway = models.CharField(db_column='QiutWay', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ProductOfficial'


class Productqaoption(models.Model):
    topic = models.ForeignKey('Productqatopic', models.DO_NOTHING, db_column='Topic_ID')  # Field name made lowercase.
    content = models.CharField(db_column='Content', max_length=255)  # Field name made lowercase.
    score = models.IntegerField(db_column='Score')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ProductQAOption'


class Productqatopic(models.Model):
    accounttype = models.CharField(db_column='AccountType', max_length=45)  # Field name made lowercase.
    content = models.CharField(db_column='Content', max_length=255)  # Field name made lowercase.
    number = models.IntegerField(db_column='Number')  # Field name made lowercase.
    product = models.ForeignKey(Productofficial, models.DO_NOTHING, db_column='Product_ID')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ProductQATopic'


class Questionnaireoption(models.Model):
    topic = models.ForeignKey('Questionnairetopic', models.DO_NOTHING, db_column='Topic_ID')  # Field name made lowercase.
    content = models.CharField(db_column='Content', max_length=45)  # Field name made lowercase.
    score = models.CharField(db_column='Score', max_length=45)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'QuestionnaireOption'


class Questionnairetopic(models.Model):
    content = models.CharField(db_column='Content', max_length=255)  # Field name made lowercase.
    number = models.IntegerField(db_column='Number', unique=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'QuestionnaireTopic'


class QuestionnaireAccount(models.Model):
    user_id = models.IntegerField(db_column='User_ID')  # Field name made lowercase.
    topic_id = models.IntegerField(db_column='Topic_ID')  # Field name made lowercase.
    option_id = models.IntegerField(db_column='Option_ID')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Questionnaire_Account'


class Responsible(models.Model):
    rmcode = models.CharField(db_column='RMCode', max_length=255, blank=True, null=True)  # Field name made lowercase.
    department = models.CharField(db_column='Department', max_length=45, blank=True, null=True)  # Field name made lowercase.
    team = models.CharField(db_column='Team', max_length=45, blank=True, null=True)  # Field name made lowercase.
    branch = models.CharField(db_column='Branch', max_length=45, blank=True, null=True)  # Field name made lowercase.
    positioni = models.CharField(db_column='PositionI', max_length=45, blank=True, null=True)  # Field name made lowercase.
    is_incumbent = models.IntegerField(db_column='IS_Incumbent', blank=True, null=True)  # Field name made lowercase.
    channel = models.CharField(db_column='Channel', max_length=10, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=45, blank=True, null=True)  # Field name made lowercase.
    user = models.CharField(db_column='User', max_length=45, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(max_length=45, blank=True, null=True)
    certificatenumber = models.CharField(db_column='CertificateNumber', max_length=45, blank=True, null=True)  # Field name made lowercase.
    crm_id = models.CharField(db_column='CRM_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    user_id = models.IntegerField(db_column='User_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    photo = models.CharField(db_column='Photo', max_length=45, blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=255, blank=True, null=True)  # Field name made lowercase.
    identity = models.CharField(db_column='Identity', max_length=255, blank=True, null=True)  # Field name made lowercase.
    company = models.CharField(db_column='Company', max_length=255, blank=True, null=True)  # Field name made lowercase.
    superior_cid = models.CharField(db_column='Superior_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    agencycode = models.CharField(db_column='AgencyCode', max_length=45, blank=True, null=True)  # Field name made lowercase.
    recommendcode = models.CharField(db_column='RecommendCode', max_length=4, blank=True, null=True)  # Field name made lowercase.
    companyprofile = models.CharField(db_column='CompanyProfile', max_length=255, blank=True, null=True)  # Field name made lowercase.
    selfprofile = models.CharField(db_column='SelfProfile', max_length=255, blank=True, null=True)  # Field name made lowercase.
    shareremark = models.CharField(db_column='ShareRemark', max_length=255, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=45, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Responsible'


class Responsibleproduct(models.Model):
    responsible = models.ForeignKey(Responsible, models.DO_NOTHING, db_column='Responsible_ID')  # Field name made lowercase.
    product = models.ForeignKey(Productofficial, models.DO_NOTHING, db_column='Product_ID')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ResponsibleProduct'


class ResponsibleAccount(models.Model):
    responsible_id = models.IntegerField(db_column='Responsible_ID', blank=True, null=True)  # Field name made lowercase.
    account_id = models.IntegerField(db_column='Account_ID')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Responsible_Account'


class Signtask(models.Model):
    order = models.ForeignKey(Order, models.DO_NOTHING, db_column='Order_ID')  # Field name made lowercase.
    account = models.ForeignKey(Account, models.DO_NOTHING, db_column='Account_ID')  # Field name made lowercase.
    signstatus = models.IntegerField(db_column='SignStatus')  # Field name made lowercase.
    party = models.CharField(db_column='Party', max_length=45)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    type = models.IntegerField(db_column='Type', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SignTask'


class Teammember(models.Model):
    team_id = models.IntegerField(db_column='Team_ID', blank=True, null=True)  # Field name made lowercase.
    member_cid = models.CharField(db_column='Member_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TeamMember'


class UpLog(models.Model):
    oldcrm_id = models.CharField(max_length=255, blank=True, null=True)
    newnum = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Up_Log'


class Userbehavior(models.Model):
    user = models.ForeignKey('User', models.DO_NOTHING, db_column='User_ID')  # Field name made lowercase.
    product = models.ForeignKey(Productofficial, models.DO_NOTHING, db_column='Product_ID', blank=True, null=True)  # Field name made lowercase.
    behaviortype = models.CharField(db_column='BehaviorType', max_length=45)  # Field name made lowercase.
    time = models.DateTimeField(db_column='Time')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'UserBehavior'


class Role(models.Model):
    name = models.CharField(db_column='Name', max_length=45, blank=True, null=True)  # Field name made lowercase.
    authority = models.CharField(db_column='Authority', max_length=45, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'role'


class User(models.Model):
    username = models.CharField(db_column='Username', unique=True, max_length=45, blank=True, null=True)  # Field name made lowercase.
    wechat_openid = models.CharField(db_column='Wechat_openid', unique=True, max_length=45, blank=True, null=True)  # Field name made lowercase.
    wechat_avatar = models.CharField(db_column='Wechat_avatar', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wechat_nickname = models.CharField(db_column='Wechat_NickName', max_length=255, blank=True, null=True)  # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=45, blank=True, null=True)  # Field name made lowercase.
    risklevel = models.CharField(db_column='Risklevel', max_length=3, blank=True, null=True)  # Field name made lowercase.
    responsible_cid = models.CharField(db_column='Responsible_CID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    gender = models.IntegerField(db_column='Gender', blank=True, null=True)  # Field name made lowercase.
    country = models.CharField(db_column='Country', max_length=45, blank=True, null=True)  # Field name made lowercase.
    province = models.CharField(db_column='Province', max_length=45, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='City', max_length=45, blank=True, null=True)  # Field name made lowercase.
    createtime = models.DateTimeField(db_column='CreateTime', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'user'


class UserRole(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING, db_column='User_ID')  # Field name made lowercase.
    role = models.ForeignKey(Role, models.DO_NOTHING, db_column='Role_ID')  # Field name made lowercase.
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'user_role'
