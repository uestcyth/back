from django.shortcuts import render

# Create your views here.
import datetime
import decimal
import json

from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest

from datamodels.models import Account, Responsible, Productofficial, Order, Asset, Accountbank, \
    Orderpayoutdetail, Ordersharechangeinfo, Ordertransfer, Orderredemption


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        super(CustomEncoder, self).default(o)

def test1(request):
    return HttpResponse("sss")

