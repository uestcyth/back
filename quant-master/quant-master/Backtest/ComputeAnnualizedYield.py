from pymongo import MongoClient
import datetime
import numpy
MONGOHOST = '47.108.25.113' #linux下通过ifconfig命令，找ip
MONGODB = 'data' #mongodb下的一个db
MONGOCOLLECTION = 'stock_daily' #mongodb下的test下的一个collection
# 创建client连接mongodb
client = MongoClient("mongodb://47.108.25.113:29999/")
# 指定mongo下的data数据库
db = client.data
# 账号、密码
db.authenticate("test", "8035test")
# collection=stock_daily
col = db.stock_daily
# 查询日期时，是从begin_date的后一天开始返回
def QueryByCode(code, begin_day, end_day):
    result = []
    Query_list = col.find({"code": code,
              "date": {
                    "$gt": begin_day,
                    "$lte": end_day
                }
              })
    for i in Query_list:
        result.append(i)
    return result
def getData(codeList, begin_day, end_day):
    result = []
    for i in codeList:
        result.append({
            "code":i,
            "data":QueryByCode(i, begin_day, end_day)
        })
    return result

def compute_annualized_yield(codeList, start_date,end_date):
    # start_date = "2019-12-01"
    # end_date="2020-5-01"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    now_date = start_date
    begin_date = now_date - datetime.timedelta(days=365)                  #用来每次循环+1
    half_begin=now_date - datetime.timedelta(days=180)
    half_now=start_date

    data = getData(codeList, str(begin_date)[:10], str(end_date)[:10])
    ratelist=[]
    timelist=[]
    half_ratelist=[]
    half_timelist=[]
    half_flag=0

    while(begin_date<end_date-datetime.timedelta(days=365)):
        if begin_date>=start_date-datetime.timedelta(days=365):
            half_flag=1

        benjin = 0
        now = 0
        benjin_half=0
        now_half=0

        day1=0              #计算本金和now的日期差值
        day2=0
        half_nowDay=begin_date
        half_benjinDay=0

        for i in codeList:
            for k in data:
                if(k['code'] == i):
                    stock_data = k['data']
                    # stock_data 单只股票的全部数据
                    for index in range(len(stock_data)):
                        # 数据的日期在目标点前的即为无用，在此之后或者等于目标点的都可以取
                        if(stock_data[index]['date'] < str(begin_date)[:10]):
                            continue
                        else:
                            day1=datetime.datetime.strptime(stock_data[index]['date'], "%Y-%m-%d")#stock_data[index]['date']
                            benjin = benjin + stock_data[index]['avg']

                        for index_half in range(len(stock_data)):
                            if(stock_data[index_half]['date'] < str(half_begin)[:10]):
                                pass
                            else:
                                half_benjinDay=datetime.datetime.strptime(stock_data[index_half]['date'], "%Y-%m-%d")
                                benjin_half=benjin_half+stock_data[index_half]['avg']
                                # print(half_benjinDay)
                                break
                        break
        for i in codeList:
            for k in data:
                if (k['code'] == i):
                    stock_data = k['data']
                    # stock_data 单只股票的全部数据
                    for index in range(len(stock_data)):
                        # 数据的日期在目标点前的即为无用，在此之后或者等于目标点的都可以取
                        if (stock_data[index]['date'] < str(now_date)[:10]):
                            pass
                        else:
                            day2=datetime.datetime.strptime(stock_data[index]['date'], "%Y-%m-%d")#stock_data[index]['date']
                            now = now + stock_data[index]['avg']
                            if half_flag == 1:
                                now_half = now
                                half_nowDay = datetime.datetime.strptime(stock_data[index]['date'], "%Y-%m-%d")
                                # print(half_nowDay)
                            break

        if (benjin == 0):
            #print("benjin == 0,error")
            begin_date = begin_date + datetime.timedelta(days=1)
            now_date = now_date + datetime.timedelta(days=1)
            half_begin = half_begin + datetime.timedelta(days=1)
        else:
            difference=(day2-day1).days
            rate = now - benjin
            rate = rate * 365
            rate = rate / benjin
            rate = rate / difference
            #print(rate)
            ratelist.append(rate)
            timelist.append(str(now_date)[:10])

        if(half_begin==0 and half_flag==1):
            begin_date = begin_date + datetime.timedelta(days=1)
            now_date = now_date + datetime.timedelta(days=1)
            half_begin = half_begin + datetime.timedelta(days=1)
        elif half_flag==1:
            difference_half=(half_nowDay-half_benjinDay).days
            half_rate=now_half-benjin_half
            half_rate=half_rate*365
            half_rate=half_rate/benjin_half
            half_rate=half_rate/difference_half
            half_ratelist.append(half_rate)
            half_timelist.append(str(now_date)[:10])

        begin_date = begin_date + datetime.timedelta(days=1)
        now_date = now_date + datetime.timedelta(days=1)
        half_begin = half_begin + datetime.timedelta(days=1)
        half_now=half_now+datetime.timedelta(days=1)


    return {"timelist":timelist,
            "ratelist":ratelist,
            "half_timelist": half_timelist,
            "half_ratelist":half_ratelist
            }

compute_annualized_yield(['000002.XSHE'],'2019-12-01','2020-12-01')
