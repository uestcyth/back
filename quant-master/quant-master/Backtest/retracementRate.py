from pymongo import MongoClient
import pandas as pd
import numpy as np
import talib
import datetime

MONGOHOST = '47.108.25.113' #linux下通过ifconfig命令，找ip
MONGODB = 'data' #mongodb下的一个db
MONGOCOLLECTION = 'stock_daily' #mongodb下的test下的一个collection
# 创建client连接mongodb
client = MongoClient("mongodb://47.108.25.113:29999/")
# 指定mongo下的data数据库
db = client.data
# 账号、密码
db.authenticate("test", "8035test")
# collection=stock_daily
col = db.stock_daily
# 查询日期时，是从begin_date的后一天开始返回
def QueryByCode(code, begin_day, end_day):
    result = []
    Query_list = col.find({"code": code,
              "date": {
                    "$gt": begin_day,
                    "$lte": end_day
                }
              })
    for i in Query_list:
        result.append(i)
    return result

def getData(codeList, begin_day, end_day):
    result = []
    for i in codeList:
        result.append({
            "code":i,
            "data":QueryByCode(i, begin_day, end_day)
        })
    return result

def retracement(codeList, numsList, start_date, end_date):
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    now_date = start_date
    begin_date = now_date  # 这个用来每次循环+1
    data = getData(codeList, str(begin_date)[:10], str(end_date)[:10])
    for i in data:
        print(i['data'][0])
    valueList = []
    timeList = []
    maxList = []
    temp = 0
    nums_index = 0
    for i in data:
        if(temp == 0):
            for j in i['data']:
                valueList.append(j['avg'] * numsList[nums_index])
                timeList.append(j['date'])
        else:
            index = 0
            for j in i['data']:
                # 计算保留两位小数
                valueList[index] = round(valueList[index] + j['avg'] * numsList[nums_index], 2)
                index = index + 1
        temp = 1
        nums_index = nums_index + 1
    # print(len(valueList))
    print(valueList)
    for i in range(len(valueList)):
        maxRate = (valueList[i] - min(valueList[i:])) / valueList[i]
        maxList.append(maxRate)
    return {
        "timeList": timeList,
        "maxList": maxList
    }




print(retracement(['000002.XSHE','000001.XSHE'], [1,2], '2018-12-01','2019-12-01'))

# k = [6,4,3,2,1]
