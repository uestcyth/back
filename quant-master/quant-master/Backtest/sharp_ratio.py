import datetime

import numpy
import numpy as np
from Backtest.ComputeAnnualizedYield_fund import compute_annualized_yield_fund


#  ratelist通过年化函数返回，
def sharp_ratio(ratelist_market, ratelist_composition, timelist):
    # start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    # end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    #  算出市场和投资组合的方差、协方差，设置国债一年期盈利率为0.026
    bond_cn = 0.026
    sharplist=[]
    sharp_timelist=[]

    for i in range(len(timelist)):
        Rf=bond_cn
        beta=0
        expect_Rm=np.mean(ratelist_market)
        market_var=np.var(ratelist_market[:i+1])
        cov=np.cov(ratelist_market[:i+1],ratelist_composition[:i+1])
        cov=cov[0][1]
        #手动计算协方差cov=EXY-EX*EY
        # E_composition=np.mean(ratelist_composition[:i+1])
        # E_market=np.mean(ratelist_market[:i+1])
        # j=i
        # #EXY=∑ i*j*(Pij)
        # E_marketMclComposition=0
        # while j>=0:
        #     E_marketMclComposition=E_marketMclComposition+(ratelist_composition[j]*ratelist_market[j])
        #     j=j-1
        # E_marketMclComposition=E_marketMclComposition/(i+1)
        # cov=E_marketMclComposition-E_composition*E_market
        # print(cov)
        beta=cov/market_var
        composition_std=np.std(ratelist_composition[:i+1],ddof=1)
        sharprate=((expect_Rm-Rf)*beta)/composition_std
        if numpy.isnan(sharprate):
            sharprate=0
        sharplist.append(sharprate)
        sharp_timelist.append(timelist[i])
        # print(sharprate,expect_Rm,np.mean(ratelist_composition),ratelist_market[i],ratelist_composition[i])
    return {
        "sharplist":sharplist,
        "timelist":sharp_timelist
    }

# numlist = []
# numlist_market=[]
# i = 500
# while (i > 0):
#     numlist.append([1, 0])
#     numlist_market.append([1, 0])
#     i = i - 1
# # 之后numlist会替换为计算每天应购买基金数量的列表，以percentlist为参数。numlist最后一行为基金代码，方便后面的计算
# codelist_composition = ['150052', '150009']
# codelist_market = ['150051']  # 大盘取沪深300A
#
# numlist.append(codelist_composition)
# numlist_market.append(codelist_market)
#
# results_AnnualizedYield = compute_annualized_yield_fund(codelist_composition, numlist, '2019-01-01', '2020-01-01')
# results_AnnualizedYield_market = compute_annualized_yield_fund(codelist_market, numlist_market, '2019-01-01', '2020-01-01')
# #获得市场的收益率和投资组合的收益率和timelist
# sharp_ratio( results_AnnualizedYield_market['ratelist'],results_AnnualizedYield['ratelist'], results_AnnualizedYield['timelist'])
