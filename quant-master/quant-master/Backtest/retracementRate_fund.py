from pymongo import MongoClient
import datetime
from Backtest.GetNumlist import getNumlist_fund

MONGOHOST = '47.108.25.113'  # linux下通过ifconfig命令，找ip
MONGODB = 'data'  # mongodb下的一个db
MONGOCOLLECTION = 'fund_net_value'  # mongodb下的test下的一个collection
# 创建client连接mongodb
client = MongoClient("mongodb://47.108.25.113:29999/")
# 指定mongo下的data数据库
db = client.data
# 账号、密码
db.authenticate("test", "8035test")
# collection=stock_daily
col = db.fund_net_value


# 查询日期时，是从begin_date的后一天开始返回
def QueryByCode(code, begin_day, end_day):
    result = []
    Query_list = col.find({"code": code,
                           "day": {
                               "$gt": begin_day,
                               "$lte": end_day
                           }
                           })
    for i in Query_list:
        result.append(i)
    return result


def getData(codeList, begin_day, end_day):
    result = []
    for i in codeList:
        result.append({
            "code": i,
            "data": QueryByCode(i, begin_day, end_day)
        })
    return result


def retracement_fund(codeList, numsList, start_date, end_date):
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    num = numsList[len(numlist) - (len(numsList) - 365)]  # 取到start_day购买的基金数量
    now_date = start_date
    begin_date = now_date  # 这个用来每次循环+1
    data = getData(codeList, str(begin_date)[:10], str(end_date)[:10])

    valueList = []
    rateList = []
    timeList = []
    maxList = []
    while (begin_date < end_date):
        value = 0
        for i in codeList:
            for k in data:
                if (k['code'] == i):
                    stock_data = k['data']
                    for index in range(len(stock_data)):
                        # 数据的日期在目标点前的即为无用，在此之后或者等于目标点的都可以取
                        if (stock_data[index]['day'] < str(begin_date)[:10]):
                            continue
                        else:
                            j = 0
                            #  找到对应及基金应该购买的数量
                            while j < len(num):
                                if codeList[j] == stock_data[index]['code']:
                                    value = value + stock_data[index]['refactor_net_value'] * num[j]
                                    break
                                else:
                                    j = j + 1
                            break
        if value == 0:  # 出现这种情况，是因为end_date为非交易日，取不到数据
            valueList.append(valueList[-1])
            timeList.append(str(begin_date)[:10])
            begin_date = begin_date + datetime.timedelta(days=1)
            continue
            # valueList.append(valueList[-1])
            # timeList.append(timeList[-1])
            # begin_date = begin_date + datetime.timedelta(days=1)
        valueList.append(value)
        timeList.append(str(begin_date)[:10])
        begin_date = begin_date + datetime.timedelta(days=1)

    for i in range(len(valueList) - 1):
        maxRate = (valueList[i] - min(valueList[i:])) / valueList[i]
        maxList.append(maxRate)
    for i in range(len(valueList)):
        rate = valueList[i] - valueList[0]
        rate = rate / valueList[0]
        rateList.append(rate)

    return {
        "ratelist": rateList,
        "timelist": timeList,
        "maxlist": maxList
    }


numlist = getNumlist_fund(['150008', '150009'], [0.3, 0.7], '2018-11-30', '2019-11-30')
retracement_fund(['150008', '150009'], numlist, '2018-11-30', '2019-11-30')
# k = [6,4,3,2,1]
