import datetime
from pymongo import MongoClient

MONGOHOST = '47.108.25.113'  # linux下通过ifconfig命令，找ip
MONGODB = 'data'  # mongodb下的一个db
MONGOCOLLECTION = 'fund_net_value'  # mongodb下的test下的一个collection
# 创建client连接mongodb
client = MongoClient("mongodb://47.108.25.113:29999/")
# 指定mongo下的data数据库
db = client.data
# 账号、密码
db.authenticate("test", "8035test")
# collection=stock_daily
col = db.fund_net_value


def QueryByCode(code, begin_day, end_day):
    result = []
    Query_list = col.find({"code": code,
                           "day": {
                               "$gt": begin_day,
                               "$lte": end_day
                           }
                           })
    for i in Query_list:
        result.append(i)
    return result


def getData(codeList, begin_day, end_day):
    result = []
    for i in codeList:
        result.append({
            "code": i,
            "data": QueryByCode(i, begin_day, end_day)
        })
    return result


def getNumlist_fund(codelist, percentlist, start_date, end_date):
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    now_date = start_date
    begin_date = now_date - datetime.timedelta(days=365)  # 用来每次循环+1
    half_now = start_date

    data = getData(codelist, str(begin_date)[:10], str(end_date)[:10])

    numlist = []
    temp = 0
    index = 0
    cash = 100000
    for k in data:
        if temp == 0:
            for j in k['data']:
                while (1):
                    if begin_date < datetime.datetime.strptime(j['day'], "%Y-%m-%d"):
                        numlist.append([cash * percentlist[temp] / j['refactor_net_value']])
                        begin_date = begin_date + datetime.timedelta(days=1)
                    else:
                        numlist.append([cash * percentlist[temp] / j['refactor_net_value']])
                        begin_date = begin_date + datetime.timedelta(days=1)
                        break
        else:
            for y in k['data']:
                while (1):
                    if begin_date < datetime.datetime.strptime(y['day'], "%Y-%m-%d"):
                        numlist[index].append(cash * percentlist[temp] / y['refactor_net_value'])
                        begin_date = begin_date + datetime.timedelta(days=1)
                        index = index + 1
                    else:
                        numlist[index].append(cash * percentlist[temp] / y['refactor_net_value'])
                        begin_date = begin_date + datetime.timedelta(days=1)
                        index = index + 1
                        break
        begin_date = start_date - datetime.timedelta(days=365)
        temp = temp + 1
        index=0
    # print(numlist)
    return numlist


# 改计算年化和回撤的index
# getNumlist_fund(['150008','150009','150012'], [0.3,0.7,0.1], '2019-01-01', '2020-01-01')
