import datetime
import decimal
import json

from django.http import HttpResponse

from Backtest.ComputeAnnualizedYield import compute_annualized_yield
from Backtest.ComputeAnnualizedYield_fund import compute_annualized_yield_fund
from Backtest.MACD import MACD_strategy, context
from Backtest.backtest_fund import backtest_fund
from datamodels.models import Account, Responsible, Productofficial, Order, Asset, Accountbank, \
    Orderpayoutdetail, Ordersharechangeinfo, Ordertransfer, Orderredemption


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        super(CustomEncoder, self).default(o)


def test(request):
    return HttpResponse(0)


# 每日价值
# 用来描述在指定的某一天，持有的资产组合的价值
def daily_value(assets):
    dailyvalue = 0
    for a in range(len(assets)):
        dailyvalue = dailyvalue + a.price * a.quantity
    return dailyvalue


# 每日收益率
# 用来描述在t这一天里，持有的资产组合和前一天相比较的收益率
def daily_return(dailyvalue, last_dailyvalue):
    daily_return = dailyvalue / last_dailyvalue - 1
    return daily_return


# 累计收益率
# 用来描述在指定时间里，总资产组合的收益率
def total_return(start_dailyvalue, end_dailyvalue):
    total_return = start_dailyvalue / end_dailyvalue - 1
    return total_return


# 策略年化收益率
# 用来描述将策略在选定周期内的收益率，换算成年收益率(默认一年为**365**天)来计算的，是一种理论收益
def annualized_portfolio_return(start_portfoliovalue, end_portfoliovalue, days):
    apr = (end_portfoliovalue / start_portfoliovalue) ** (365 / days) - 1
    return apr


# 基准年化收益率
#  用来描述将基准在选定周期内的收益率，换算成年收益率(默认一年为**365**天)来计算的，是一种理论收益
def annualized_benchmark_return(start_benchmarkindex, end_benchmarkindex, days):
    abr = (end_benchmarkindex / start_benchmarkindex) ** (365 / days) - 1
    return abr


def AnnualizedYield(request):
    data = json.loads(request.body)
    # print(data['codelist'],type(data['codelist']))
    results = compute_annualized_yield(data['codelist'], data['begin_date'], data['end_date'])
    # print(results)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def MACD(request):
    data = json.loads(request.body)
    context1 = context(data['start_time'], data['end_time'], data['cash'])
    results = MACD_strategy(context1)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 计算基金的年化收益，参数为：基金代码列表、对应的交易日需要购买的基金数量，开始日期，结束日期
def AnnualizedYield_fund(request):
    data = json.loads(request.body)
    # print(data['codelist'],type(data['codelist']))

    num = []
    codelist = []
    i = 800
    while (i > 0):
        num.append([1, 1])
        i = i - 1
    num.append(['150008', '150009'])
    # print(num[-1][0])
    # print(data['start_time'],data['end_time'],data['codelist'],len(data['codelist']))
    codelist = data['codelist'].replace('\'', '').split(',')

    results = compute_annualized_yield_fund(codelist, num, data['start_time'], data['end_time'])
    # print(results)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def backtestFund(request):
    data = json.loads(request.body)
    codelist = data['codelist']
    print(codelist)
    print(len(codelist))
    results = backtest_fund(codelist, data['percentlist'], data['start_date'], data['end_date'])
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')
