from django.urls import path

from . import views

app_name = 'Backtest'
urlpatterns = [
    path('test/', views.test, name='test'),
    path('AnnualizedYield/', views.AnnualizedYield, name='AnnualizedYield'),
    path('MACD/', views.MACD, name='MACD'),
    path('AnnualizedYield_fund/', views.AnnualizedYield_fund, name='AnnualizedYield_fund'),
    path('backtestFund/', views.backtestFund, name='backtestFund')
]