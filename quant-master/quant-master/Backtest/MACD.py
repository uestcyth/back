from pymongo import MongoClient
import pandas as pd
import numpy as np
import talib
import datetime

MONGOHOST = '47.108.25.113' #linux下通过ifconfig命令，找ip
MONGODB = 'data' #mongodb下的一个db
MONGOCOLLECTION = 'stock_daily' #mongodb下的test下的一个collection
# 创建client连接mongodb
client = MongoClient("mongodb://47.108.25.113:29999/")
# 指定mongo下的data数据库
db = client.data
# 账号、密码
db.authenticate("test", "8035test")
# collection=stock_daily
col = db.stock_daily
# 查询日期时，是从begin_date的后一天开始返回
def QueryByCode(code, begin_day, end_day):
    result = []
    Query_list = col.find({"code": code,
              "date": {
                    "$gt": begin_day,
                    "$lte": end_day
                }
              })
    for i in Query_list:
        result.append(i)
    return result

def getData(codeList, begin_day, end_day):
    result = []
    for i in codeList:
        result.append({
            "code":i,
            "data":QueryByCode(i, begin_day, end_day)
        })
    return result

class context:
    def __init__(self, start_time, end_time,cash):
        self.start_time=start_time
        self.end_time=end_time
        self.now_time=start_time
        self.cash=cash
        self.hold=0

    def plus_one_day(self):
        self.now_time=datetime.datetime.strptime(self.now_time, "%Y-%m-%d")
        self.now_time=self.now_time + datetime.timedelta(days=1)

def MACD_strategy(context):
    start_time=context.start_time
    end_time=context.end_time
    now_time=datetime.datetime.strptime(start_time, "%Y-%m-%d")
    data = getData(['000006.XSHE'], str(now_time-datetime.timedelta(days=500))[0:10],end_time )

    close=[]
    datelist=[]
    buylist=[]
    selllist=[]
    dict1={}

    for k in data:
            stock_data = k['data']
            # stock_data 单只股票的全部数据
            for index in range(len(stock_data)):
                datelist.append(stock_data[index]['date'])
                close.append(stock_data[index]['close'])
                dict1.update({stock_data[index]['date']:stock_data[index]['close']})
            close=np.array(close)
            # print(list(dict1)[1])
            # print(dict1[list(dict1)[1]])

    while(now_time<datetime.datetime.strptime(end_time, "%Y-%m-%d")):
        buy=0
        sell=0
        while(1):
            if str(now_time)[:10] in dict1.keys():
                break
            else:
                now_time = now_time + datetime.timedelta(days=1)


        now=str(now_time)[:10]

        index=0
        for item in range(len(datelist)):
            if now==datelist[item]:
                index=item
            else:
                pass

        macd_tmp = talib.MACD(close[index-300:index], fastperiod=12, slowperiod=26, signalperiod=20)
        DIF = macd_tmp[0]
        DEA = macd_tmp[1]
        MACD = macd_tmp[2]

        if MACD[-1] > 0 and MACD[-4] < 0:
            sell=1
        elif MACD[-1] < 0 and MACD[-4] > 0:
            buy=1

        if buy==1 and context.cash>close[index] :
            context.hold=context.hold+1
            context.cash=context.cash-close[index]
            buylist.append(now_time)
            # print("在这时买了"+str(now_time)[:10])
        elif sell==1 and context.hold>0:
            context.hold=context.hold-1
            context.cash=context.cash+close[index]
            selllist.append(now_time)
            # print("在这里卖了"+str(now_time)[:10])
        now_time = now_time + datetime.timedelta(days=1)
    return {"买入时间":buylist,
            "卖出时间":selllist,
            "持有现金":context.cash,
            "持有股票数量":context.hold}
context1=context('2020-02-01','2021-02-01',100000)
MACD_strategy(context1)