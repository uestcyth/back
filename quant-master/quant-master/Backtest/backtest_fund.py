import datetime

from Backtest.ComputeAnnualizedYield_fund import compute_annualized_yield_fund
from Backtest.retracementRate_fund import retracement_fund
from Backtest.sharp_ratio import sharp_ratio
from Backtest.GetNumlist import getNumlist_fund

# 这个函数产生的warning暂时不管，是numpy计算协方差时产生的
def backtest_fund(codelist,percentlist,start_date,end_date):
    numlist = getNumlist_fund(codelist,percentlist, start_date, end_date)
    codelist_market = ['163407']  # 大盘取兴全沪深300
    numlist_market = getNumlist_fund(codelist_market,[1],start_date,end_date)
    # i=500
    # while (i > 0):
    #     numlist.append([1, 2])
    #     numlist_market.append([1, 2])
    #     i = i - 1
    # numlist.append(codelist)
    # numlist_market.append(codelist_market)
    # 之后numlist会替换为计算每天应购买基金数量的列表，以percentlist为参数。numlist最后一行为基金代码，方便后面的计算

    results_AnnualizedYield=compute_annualized_yield_fund(codelist,numlist,start_date,end_date)
    results_retracementRate=retracement_fund(codelist,numlist,start_date,end_date)
    results_AnnualizedYield_market = compute_annualized_yield_fund(codelist_market, numlist_market, start_date, end_date)
    # results_retracementRate_market=retracement_fund(codelist_market,numlist_market,start_date,end_date)
    results_sharpRatio=sharp_ratio( results_AnnualizedYield_market['ratelist'],results_AnnualizedYield['ratelist'], results_AnnualizedYield['timelist'])
    return {
        "AnnualizedYield_one_timelist":results_AnnualizedYield['timelist'],
        "AnnualizedYield_one_ratelist":results_AnnualizedYield['ratelist'],
        "AnnualizedYield_half_ratelist": results_AnnualizedYield['half_ratelist'],
        "retracementRate_ratelist": results_retracementRate['maxlist'],
        "sharp_ratio_ratelist":results_sharpRatio['sharplist']
    }

codelist=['161911', '164401']
backtest_fund(codelist,[0.5,0.5],'2012-04-10','2020-08-27')
