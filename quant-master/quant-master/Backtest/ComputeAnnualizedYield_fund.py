from pymongo import MongoClient
import datetime
from Backtest.GetNumlist import getNumlist_fund

MONGOHOST = '47.108.25.113'  # linux下通过ifconfig命令，找ip
MONGODB = 'data'  # mongodb下的一个db
MONGOCOLLECTION = 'fund_net_value'  # mongodb下的test下的一个collection
# 创建client连接mongodb
client = MongoClient("mongodb://47.108.25.113:29999/")
# 指定mongo下的data数据库
db = client.data
# 账号、密码
db.authenticate("test", "8035test")
# collection=stock_daily
col = db.fund_net_value


def QueryByCode(code, begin_day, end_day):
    result = []
    Query_list = col.find({"code": code,
                           "day": {
                               "$gt": begin_day,
                               "$lte": end_day
                           }
                           })
    for i in Query_list:
        result.append(i)
    return result


def getData(codeList, begin_day, end_day):
    result = []
    for i in codeList:
        result.append({
            "code": i,
            "data": QueryByCode(i, begin_day, end_day)
        })
    return result


def compute_annualized_yield_fund(codeList, num, start_date, end_date):
    # start_date = "2019-12-01"
    # end_date="2020-5-01"
    start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    now_date = start_date
    begin_date = now_date - datetime.timedelta(days=365)  # 用来每次循环+1
    half_begin = now_date - datetime.timedelta(days=180)
    half_now = start_date

    data = getData(codeList, str(begin_date)[:10], str(end_date)[:10])
    ratelist = []
    timelist = []
    half_ratelist = []
    half_timelist = []
    half_flag = 0
    num_index = 0

    while (begin_date < end_date - datetime.timedelta(days=365)):  # 这里需要对numlist的index做相应的修改，使其变为0~729，每次循环index+1
        if begin_date >= start_date - datetime.timedelta(days=365):
            half_flag = 1

        benjin = 0
        now = 0
        benjin_half = 0
        now_half = 0

        day1 = 0  # 计算本金和now的日期差值
        day2 = 0
        half_nowDay = begin_date
        half_benjinDay = 0

        for i in codeList:
            for k in data:
                if (k['code'] == i):
                    stock_data = k['data']
                    # stock_data 单只股票的全部数据
                    # j=0
                    # percent=0
                    # while(j<len(codeList)):
                    #     percent=codeList[1][j]
                    #     j+=1
                    # 获取对应基金代码的应该买的数量
                    for index in range(len(stock_data)):
                        # 数据的日期在目标点前的即为无用，在此之后或者等于目标点的都可以取
                        if (stock_data[index]['day'] < str(begin_date)[:10]):
                            continue
                        else:
                            j = 0
                            while (j < len(codeList)):
                                if codeList[j] == stock_data[index]['code']:
                                    break
                                else:
                                    j += 1
                            day1 = datetime.datetime.strptime(stock_data[index]['day'], "%Y-%m-%d")
                            benjin = benjin + stock_data[index]['refactor_net_value'] * num[num_index][j]

                        for index_half in range(len(stock_data)):
                            if (stock_data[index_half]['day'] < str(half_begin)[:10]):
                                pass
                            else:
                                j = 0
                                while (j < len(codeList)):
                                    if codeList[j] == stock_data[index_half]['code']:
                                        break
                                    else:
                                        j += 1
                                half_benjinDay = datetime.datetime.strptime(stock_data[index_half]['day'], "%Y-%m-%d")
                                # print(half_benjinDay)
                                benjin_half = benjin_half + (stock_data[index_half]['refactor_net_value']) * \
                                              num[num_index + 185][j]
                                break
                        break

        for i in codeList:
            for k in data:
                if (k['code'] == i):
                    stock_data = k['data']
                    # stock_data 单只股票的全部数据
                    # j = 0
                    # percent = 0
                    # while (j < len(codeList[0])):
                    #     percent = codeList[1][j]
                    #     j += 1
                    # 获取对应基金代码的所占比例
                    for index in range(len(stock_data)):
                        # 数据的日期在目标点前的即为无用，在此之后或者等于目标点的都可以取
                        if (stock_data[index]['day'] < str(now_date)[:10]):
                            pass
                        else:
                            j = 0
                            while (j < len(codeList)):
                                if codeList[j] == stock_data[index]['code']:
                                    break
                                else:
                                    j += 1
                            day2 = datetime.datetime.strptime(stock_data[index]['day'], "%Y-%m-%d")
                            now = now + stock_data[index]['refactor_net_value'] * float(num[num_index][j])
                            if half_flag == 1:
                                now_half = now_half + stock_data[index]['refactor_net_value'] * float(
                                    num[num_index + 185][j])
                                half_nowDay = datetime.datetime.strptime(stock_data[index]['day'], "%Y-%m-%d")
                                # print(half_nowDay)
                            break

        if (benjin == 0):
            # print("benjin == 0,error")
            begin_date = begin_date + datetime.timedelta(days=1)
            now_date = now_date + datetime.timedelta(days=1)
            half_begin = half_begin + datetime.timedelta(days=1)
        else:
            difference = (day2 - day1).days
            # print(str(day2)[:10],str(day1)[:10],difference)
            if difference == 0:  # 4/22改，如果起始日期超过数据有效日期，会出现difference==0的情况，后面计算时出现除0
                difference = 1
            rate = now - benjin
            rate = rate * 365
            rate = rate / benjin
            rate = rate / difference
            # print(rate)
            ratelist.append(rate)
            timelist.append(str(now_date)[:10])

        if (half_begin == 0 and half_flag == 1):
            begin_date = begin_date + datetime.timedelta(days=1)
            now_date = now_date + datetime.timedelta(days=1)
            half_begin = half_begin + datetime.timedelta(days=1)
        elif half_flag == 1:
            difference_half = (half_nowDay - half_benjinDay).days
            if difference_half == 0:
                difference_half = 1
            half_rate = now_half - benjin_half
            half_rate = half_rate * 365
            half_rate = half_rate / benjin_half
            half_rate = half_rate / difference_half
            half_ratelist.append(half_rate)
            half_timelist.append(str(now_date)[:10])

        begin_date = begin_date + datetime.timedelta(days=1)
        now_date = now_date + datetime.timedelta(days=1)
        half_begin = half_begin + datetime.timedelta(days=1)
        half_now = half_now + datetime.timedelta(days=1)
        num_index = num_index + 1
        # print(len(num),num_index,num_index+365)
    # print(ratelist)
    return {"timelist": timelist,
            "ratelist": ratelist,
            "half_timelist": half_timelist,
            "half_ratelist": half_ratelist
            }

# num=getNumlist_fund(['163407'],[0.3],'2019-01-01', '2020-01-01')
#
# compute_annualized_yield_fund(['163407'], num, '2019-01-01', '2020-01-01')
