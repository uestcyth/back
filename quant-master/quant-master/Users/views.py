import base64
import datetime
import decimal
import json
from operator import itemgetter

import aiohttp
import asyncio

import pymysql
import requests
# from Crypto.Cipher import AES
from django.core.cache import cache
from django.db.models import Q, Sum
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import redirect

from Product.views import calculate_one_year_increase
from Quantitative.settings import MINI_PROGRAM_ARGS, CRM_URLS, DATABASES
from SMS.views import generate_code, send_sms
from datamodels.models import User, UserRole, Role, Account, Responsible, Channelmanagement, Productofficial, Order, \
    Asset, Accountbank, QuestionnaireAccount, Questionnairetopic, \
    Questionnaireoption, Messageboard, Communicateboard, ChannelmanagementResponsible, Agencyinfo, Responsibleproduct, \
    Networth, Userbehavior


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        super(CustomEncoder, self).default(o)


class CustomEncoder4Time(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d %H:%M:%S')
        super(CustomEncoder4Time, self).default(o)




def CustomResponse(string):
    response = HttpResponse(string)
    response.status_code = 500
    return response


def ResponseCodeMes(msg):
    result = {'code': 400,
              'msg': msg
              }
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


# 同步表，不写回数据库
async def sync_standard(url, data):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data) as response:
            text = await response.text()
            return text


# 同步客户表，未添加日志
async def sync_account(url, data, account_id):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data) as response:
            text = await response.text()
            sync_account_2_mysql(account_id, text)
            return text


# 同步投顾表，未添加日志
async def sync_responsible(url, data, responsible_id):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data) as response:
            text = await response.text()
            sync_responsible_2_mysql(responsible_id, text)
            return text


# 将获取的crmid同步回数据库
def sync_account_2_mysql(account_id, crm_id):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()

    sql = "UPDATE %s.Account SET CRM_ID = %s WHERE ID = %s" % (DATABASES['default']['NAME'], crm_id, account_id)
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
    except:
        # 如果发生错误则回滚
        db.rollback()

    # 关闭数据库连接
    db.close()


# 将获取的crmid同步回数据库
def sync_responsible_2_mysql(responsible_id, crm_id):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()

    sql = "UPDATE %s.Responsible SET CRM_ID = %s WHERE ID = %s" % (DATABASES['default']['NAME'], crm_id, responsible_id)
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
    except:
        # 如果发生错误则回滚
        db.rollback()

    # 关闭数据库连接
    db.close()


# 将获取的crmid同步回数据库
def sync_accountbank_2_mysql(accountbank_id, crm_id):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()

    sql = "UPDATE %s.AccountBank SET CRM_ID = %s WHERE ID = %s" % (DATABASES['default']['NAME'], crm_id, accountbank_id)
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
    except:
        # 如果发生错误则回滚
        db.rollback()

    # 关闭数据库连接
    db.close()


# 加密
def base64_encode(string):
    if string:
        bytes_str = string.encode("utf-8")
        bytes_str = base64.b64encode(bytes_str)  # 被编码的参数必须是二进制数据
        str1 = str(bytes_str)
        str1 = str1[2:-1]
        return str1
    else:
        return None


# 解码
def base64_decode(string):
    if string:
        string = base64.b64decode(string).decode("utf-8")
        return string
    else:
        return None


def test_base64(request):
    data = json.loads(request.body)
    print(data['true'])
    return HttpResponse(base64_decode(data['true']))


# 检查用户角色有效性
def check_user(request):
    data = json.loads(request.body)
    wechat_openid = data['openid']
    try:
        user = User.objects.get(wechat_openid=wechat_openid)
        userrole = UserRole.objects.filter(user_id=user.id)
        roles = []
        for i in userrole:
            if i.role.name not in roles:
                roles.append(i.role.name)
            else:
                i.delete()
        result = {"user": user.id, "role": roles}
        return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
    except User.DoesNotExist:
        return HttpResponse('该用户不在用户表内')


# 停用
def customer_login(request):
    login_data = json.loads(request.body)
    # 姓名，身份证号，身份证类型，微信openid
    Name, CertificateNumber, Paperstype, OpenID, Avatar = login_data['name'], login_data[
        'Certificatenumber'], login_data['paperstype'], login_data['openid'], login_data['avatar']
    # 定义投资者角色
    role_investor = Role.objects.get(id=2)
    # 在用户表User进行匹配（初步根据用户输入的身份证号进行筛选）
    account = Account.objects.filter(certificatenumber=CertificateNumber)
    # 判断客户是否存在（身份证在表里）
    if account:
        # 判断客户其余信息是否正确（成功匹配身份证号，继续匹配身份证类型和姓名）
        if account[0].paperstype == Paperstype and account[0].name == Name:
            # 判断是否绑定微信（通过openid查用户表）
            hasAccountUser = User.objects.filter(wechat_openid=OpenID)
            # 如果用户表有此人微信openid
            if hasAccountUser:
                # 将用户与客户（crm）进行绑定（把user_id添加到客户表account中）
                account.update(user_id=hasAccountUser[0].id)
                # 新增用户权限写入用户-角色表(投资者角色id为2)
                UserRole.objects.create(
                    user=hasAccountUser[0], role=role_investor)
                result = {
                    'accountid': account[0].id, 'userid': hasAccountUser[0].id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
            # 如果用户未绑定微信，则将用户信息（微信openid，微信头像）存到用户（User）表中
            else:
                # 新建用户并将其与的微信进行绑定
                User.objects.create(wechat_openid=OpenID, wechat_avatar=Avatar)
                # 定位到新建的用户
                new_user = User.objects.get(wechat_openid=OpenID)
                # 将用户与crm的投资人进行绑定（把user_id添加到客户表account中
                account.update(user_id=new_user.id)
                # 新增用户权限写入用户-角色表(投资者角色id为2)
                UserRole.objects.create(user=new_user, role=role_investor)
                result = {
                    'accountid': account[0].id, 'userid': new_user.id, 'status': "所有信息匹配，登陆成功"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
        else:
            return HttpResponse('登录失败')
    # 注册
    else:
        return HttpResponse('用户不存在')


# 停用
def customer_login_by_password(request):
    login_data = json.loads(request.body)
    # 姓名，身份证号，身份证类型，微信openid
    CertificateNumber, Password, Paperstype, OpenID, Avatar = login_data['CertificateNumber'], login_data[
        'Password'], login_data['paperstype'], login_data['openid'], login_data['avatar']
    # 定义投资者角色
    role_investor = Role.objects.get(id=2)
    # 在用户表User进行匹配（初步根据用户输入的身份证号进行筛选）
    account = Account.objects.filter(
        Q(certificatenumber=CertificateNumber) & Q(paperstype=Paperstype))
    # 判断客户是否存在（身份证在表里）
    if account:
        # 判断客户其余信息是否正确（成功匹配身份证号，继续匹配身份证类型和姓名）
        if account[0].crm_id[-6:] == Password:
            # 检查重复登录
            if account[0].user_id:
                return HttpResponse('逻辑错误')
            # 判断是否绑定微信（通过openid查用户表）
            hasAccountUser = User.objects.filter(wechat_openid=OpenID)
            # 如果用户表有此人微信openid
            if hasAccountUser:
                # 将用户与客户（crm）进行绑定（把user_id添加到客户表account中）
                account.update(user_id=hasAccountUser[0].id)
                # 新增用户权限写入用户-角色表(投资者角色id为2)
                UserRole.objects.create(
                    user=hasAccountUser[0], role=role_investor)
                result = {
                    'accountid': account[0].id, 'userid': hasAccountUser[0].id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
            # 如果用户未绑定微信，则将用户信息（微信openid，微信头像）存到用户（User）表中
            else:
                # 新建用户并将其与的微信进行绑定
                User.objects.create(wechat_openid=OpenID, wechat_avatar=Avatar)
                # 定位到新建的用户
                new_user = User.objects.get(wechat_openid=OpenID)
                # 将用户与crm的投资人进行绑定（把user_id添加到客户表account中
                account.update(user_id=new_user.id)
                # 新增用户权限写入用户-角色表(投资者角色id为2)
                UserRole.objects.create(user=new_user, role=role_investor)
                result = {
                    'accountid': account[0].id, 'userid': new_user.id, 'status': "所有信息匹配，登陆成功"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
        else:
            return HttpResponse('密码错误')
    # 注册
    else:
        return HttpResponse('用户不存在')


# 停用
def customer_login_by_sms(request):
    login_data = json.loads(request.body)
    # 姓名，身份证号，身份证类型，微信openid
    CertificateNumber, Paperstype, OpenID, Avatar, Num = login_data[
                                                             'CertificateNumber'], login_data['paperstype'], login_data[
                                                             'openid'], login_data['avatar'], login_data['num']
    # 定义投资者角色
    role_investor = Role.objects.get(id=2)
    # 在用户表User进行匹配（初步根据用户输入的身份证号进行筛选）
    account = Account.objects.filter(
        Q(certificatenumber=CertificateNumber) & Q(paperstype=Paperstype))
    # 判断客户是否存在（身份证在表里）
    if account:
        # 判断客户其余信息是否正确（成功匹配身份证号，继续匹配身份证类型和姓名）
        if account[0].num == Num:
            # 检查重复登录
            if account[0].user_id:
                return HttpResponse('逻辑错误')
            # 判断是否绑定微信（通过openid查用户表）
            hasAccountUser = User.objects.filter(wechat_openid=OpenID)
            # 如果用户表有此人微信openid
            if hasAccountUser:
                # 将用户与客户（crm）进行绑定（把user_id添加到客户表account中）
                account.update(user_id=hasAccountUser[0].id)
                # 新增用户权限写入用户-角色表(投资者角色id为2)
                UserRole.objects.create(
                    user=hasAccountUser[0], role=role_investor)
                result = {
                    'accountid': account[0].id, 'userid': hasAccountUser[0].id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
            # 如果用户未绑定微信，则将用户信息（微信openid，微信头像）存到用户（User）表中
            else:
                # 新建用户并将其与的微信进行绑定
                User.objects.create(wechat_openid=OpenID, wechat_avatar=Avatar)
                # 定位到新建的用户
                new_user = User.objects.get(wechat_openid=OpenID)
                # 将用户与crm的投资人进行绑定（把user_id添加到客户表account中
                account.update(user_id=new_user.id)
                # 新增用户权限写入用户-角色表(投资者角色id为2)
                UserRole.objects.create(user=new_user, role=role_investor)
                result = {
                    'accountid': account[0].id, 'userid': new_user.id, 'status': "所有信息匹配，登陆成功"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
        else:
            return HttpResponse('手机号错误')
    # 注册
    else:
        return HttpResponse('用户不存在')


def phone_update(request):
    msg = json.loads(request.body)
    num = msg['num']
    account_id = msg['account_id']
    try:
        customer = Account.objects.get(id=account_id)
    except Account.DoesNotExist:
        return CustomResponse('客户账号查找失败')
    if customer.num == num:
        return HttpResponse('手机号码验证成功')
    else:
        customer.num = num
        customer.save()
        return HttpResponse('已更新手机号码')


# 投顾登录
def responsible_login(request):
    data = json.loads(request.body)
    status = data['status']
    certificatenumber = data['certificatenumber']
    openid = data['openid']
    avatar = data['avatar']
    nickname = data['nickname']
    certificatenumber_base64 = base64_encode(certificatenumber)
    nickname_base64 = base64_encode(nickname)
    import string
    import random
    while True:
        seeds = string.digits
        random_str = random.choices(seeds, k=4)
        recommendcode = "".join(random_str)
        try:
            Responsible.objects.get(recommendcode=recommendcode)
        except Responsible.DoesNotExist:
            try:
                Agencyinfo.objects.get(code=recommendcode)
            except Agencyinfo.DoesNotExist:
                break
    if not data:
        return HttpResponseBadRequest('Request Error')
    # 判断RM是否存在
    try:
        res = Responsible.objects.get(certificatenumber=certificatenumber_base64)
        # 判断短信验证状态
        if status == 'OK':
            try:
                # 判断是否绑定微信
                user = User.objects.get(wechat_openid=openid)
                userrole = UserRole.objects.filter(user=user.id)
                for i in userrole:
                    # 重复绑定(已被登录),返回错误
                    if i.role.id == 1:
                        return HttpResponse('逻辑错误')
                # 绑定角色关系表
                user = User.objects.get(wechat_openid=user.wechat_openid)
                new_user_role = UserRole()
                new_user_role.user = user
                new_user_role.role = Role.objects.get(id=1)
                new_user_role.save()
                # 在Responsible中更新User
                update_res = Responsible.objects.get(certificatenumber=certificatenumber_base64)
                update_res.user_id = user.id
                update_res.recommendcode = recommendcode
                update_res.save()
                result = {'responsibleid': res.id,
                          'userid': update_res.user_id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
            # 未绑定微信
            except User.DoesNotExist:
                # 新建User
                new_user = User()
                new_user.wechat_openid = openid
                new_user.wechat_avatar = avatar
                new_user.wechat_nickname = nickname_base64
                new_user.save()
                # 在Responsible中更新User
                user = User.objects.get(wechat_openid=new_user.wechat_openid)
                update_res = Responsible.objects.get(certificatenumber=certificatenumber_base64)
                update_res.user_id = new_user.id
                update_res.recommendcode = recommendcode
                update_res.save()
                # 绑定角色关系表
                new_user_role = UserRole()
                new_user_role.user = user
                new_user_role.role = Role.objects.get(id=1)
                new_user_role.save()
                result = {'responsibleid': res.id,
                          'userid': update_res.user_id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")

        else:
            return HttpResponse('登录失败')
    except Responsible.DoesNotExist:
        return HttpResponse('RM不存在')


# 投顾注册之前验证机构
def check_agency(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    code = data['code']
    try:
        agency = Agencyinfo.objects.get(code=code)
        result = {
            "name": agency.name
        }
        return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
    except Agencyinfo.DoesNotExist:
        try:
            responsible = Responsible.objects.get(recommendcode=code)
            result = {
                "name": responsible.name
            }
            return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
        except Responsible.DoesNotExist:
            return HttpResponse('未找到邀请机构')


# 注册前应该先进行机构验证/API身份验证/短信验证，返回状态，主动同步
def responsible_register(request):
    from SCCProgram.wsgi import thread_loop_4_user

    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    code = data['code']
    name = data['name']
    certificatenumber = data['certificatenumber']
    phone = data['phone']
    openid = data['openid']
    avatar = data['avatar']
    nickname = data['nickname']
    api_status = data['api_status']
    sms_status = data['sms_status']
    certificatenumber_base64 = base64_encode(certificatenumber)
    phone_base64 = base64_encode(phone)
    nickname_base64 = base64_encode(nickname)
    import string
    import random
    while True:
        seeds = string.digits
        random_str = random.choices(seeds, k=4)
        recommendcode = "".join(random_str)
        try:
            Responsible.objects.get(recommendcode=recommendcode)
        except Responsible.DoesNotExist:
            try:
                Agencyinfo.objects.get(code=recommendcode)
            except Agencyinfo.DoesNotExist:
                break
    if api_status == '匹配成功':
        if sms_status == 'OK':
            try:
                responsible = Responsible.objects.get(certificatenumber=certificatenumber_base64)
                return HttpResponse('账户已存在')
            except Responsible.DoesNotExist:
                try:
                    # 判断是否绑定微信
                    user = User.objects.get(wechat_openid=openid)
                    userrole = UserRole.objects.filter(user=user.id)
                    for i in userrole:
                        # 重复绑定(已被登录),返回错误
                        if i.role.id == 1:
                            return HttpResponse('逻辑错误')
                except User.DoesNotExist:
                    # 未绑定微信,新建User.当前前端逻辑应该不会出现
                    user = User.objects.create(wechat_openid=openid, wechat_avatar=avatar,
                                               wechat_nickname=nickname_base64)

                # 绑定角色关系表
                user_role = UserRole.objects.create(user=user, role=Role.objects.get(id=1))

                new_responsible = Responsible.objects.create(name=name, certificatenumber=certificatenumber_base64,
                                                             phone=phone_base64, user_id=user.id, agencycode=code,
                                                             recommendcode=recommendcode)

                # 异步执行CRM同步
                url = CRM_URLS['RESPONSIBLE']
                crm_data = {
                    'CompanyCode__c': code,
                    'Name': name,
                    'IdCard__c': certificatenumber,
                    'phone__c': phone,
                    'IS_Incumbent__c': 1,
                }
                # 产生coroutine任务sync_order交给子线程theread_loop_4_order执行
                future = asyncio.run_coroutine_threadsafe(
                    sync_responsible(url, json.dumps(crm_data), new_responsible.id), thread_loop_4_user)
                # response = future.result()

                # 未确定组织关系
                return HttpResponse('注册成功')
        else:
            return HttpResponse('短信验证失败')
    else:
        return HttpResponse('身份验证失败')


def channelManager_login(request):
    data = json.loads(request.body)
    status = data['status']
    rmcode = data['rmcode']
    openid = data['openid']
    avatar = data['avatar']
    nickname = data['nickname']
    nickname_base64 = base64_encode(nickname)
    if not data:
        return HttpResponseBadRequest('Request Error')
    # 判断CM是否存在
    try:
        cm = Channelmanagement.objects.get(rmcode=rmcode)
        # 判断短信验证状态
        if status == 'OK':
            try:
                # 判断是否绑定微信
                user = User.objects.get(wechat_openid=openid)
                userrole = UserRole.objects.filter(user=user.id)
                for i in userrole:
                    # 重复绑定,返回错误
                    if i.role.id == 3:
                        return HttpResponse('逻辑错误')
                # 绑定角色关系表
                user = User.objects.get(wechat_openid=user.wechat_openid)
                new_user_role = UserRole()
                new_user_role.user = user
                new_user_role.role = Role.objects.get(id=3)
                new_user_role.save()
                # 在Channelmanagement中更新User
                update_channel = Channelmanagement.objects.get(rmcode=rmcode)
                update_channel.user_id = user.id
                update_channel.save()
                result = {'channelid': cm.id,
                          'userid': update_channel.user_id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")
            # 未绑定微信
            except User.DoesNotExist:
                # 新建User
                new_user = User()
                new_user.wechat_openid = openid
                new_user.wechat_avatar = avatar
                new_user.wechat_nickname = nickname_base64
                new_user.save()
                # 在Responsible中更新User
                user = User.objects.get(wechat_openid=new_user.wechat_openid)
                update_channel = Channelmanagement.objects.get(rmcode=rmcode)
                update_channel.user_id = new_user.id
                update_channel.save()
                # 绑定角色关系表
                new_user_role = UserRole()
                new_user_role.user = user
                new_user_role.role = Role.objects.get(id=3)
                new_user_role.save()
                result = {'channelid': cm.id,
                          'userid': update_channel.user_id, 'status': "200"}
                return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")

        else:
            return HttpResponse('登录失败')
    except Channelmanagement.DoesNotExist:
        return HttpResponse('RM编号不存在')


# 未用
def add_customer(request):
    customermsg = json.loads(request.body)
    msg = Account.objects.filter(certificatenumber=customermsg['certificatenumber'],
                                 paperstype=customermsg['paperstype']).count()
    if msg != 0:
        return HttpResponse("该客户已存在")
    else:
        last_customer = Account.objects.last()
        if last_customer == None:
            customer_id = 0
        else:
            customer_id = last_customer.id + 1
        new_customer = Account()
        new_customer.id = customer_id
        new_customer.save()
        try:
            Account.objects.filter(id=customer_id).update(**customermsg)
            return HttpResponse("添加成功")
            return HttpResponse("添加成功")
        except Account.DoesNotExist:
            return HttpResponse("添加失败")


# 未用
def delete_customer(request):
    deletemsg = json.loads(request.body)
    customer_id = deletemsg['id']
    try:
        customer = Account.objects.get(id=customer_id)
        customer.delete()
        return HttpResponse('删除成功')
    except Account.DoesNotExist:
        return HttpResponse('删除失败')


# 获取客户详情
def detail_customer(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    customer_id = data['account_id']
    customer = Account.objects.filter(id=customer_id).first()
    #个人 0126F0000020pRJQAY  机构 0126F0000020pRFQAY
    if customer.recordtypeid == '0126F0000020pRJQAY':
        type = '个人'
    else:
        type = '机构'
    # 微信授权手机>仅投顾可见手机>CRM手机
    signphone = ""
    wechatphone = ""
    try:
        user = User.objects.get(id=customer.user_id)
        nickname = base64_decode(user.wechat_nickname)
        if user.gender == 1:
            gender = '男'
        elif user.gender == 2:
            gender = '女'
        else:
            gender = '未知'
        avatar = user.wechat_avatar
        country = user.country
        province = user.province
        city = user.city
        wechatphone = user.password
        signphone = user.password
        if signphone == "" and customer.res_phone == "":
            signphone = base64_decode(customer.res_phone)
        elif signphone == "" and customer.num != "":
            signphone = base64_decode(customer.num)
    except User.DoesNotExist:
        nickname = None
        gender = None
        avatar = None
        country = None
        province = None
        city = None
    try:
        remark = base64_decode(customer.remark)
    except Exception:
        remark = customer.remark
    try:
        remarkname = base64_decode(customer.remarkname)
    except Exception:
        remarkname = customer.remarkname
    resphone = ""
    if customer.res_phone != '' and customer.res_phone is not None:
        resphone = base64_decode(customer.res_phone)
    isReamName = "false"
    if (customer.certificatenumber != "") and (customer.name != "") and \
           customer.certificatenumber is not None and  customer.name is not None :
        isReamName = "true"
    results = (
        {'name': customer.name,
         'phone': customer.elephone,
         'recordtypeid': customer.recordtypeid,
         'num': base64_decode(customer.num),
         'wechatphone': wechatphone,
         'certificatenumber': base64_decode(customer.certificatenumber),
         'salutation': customer.salutation,
         'email': customer.email,
         'branch': customer.branch,
         'address': customer.address,
         'risklevel': customer.risklevel,
         'expire': customer.expire,
         'responsible': customer.responsible_id,
         'id': customer.id,
         'user_id': customer.user_id,
         'nickname': nickname,
         'gender': gender,
         'avatar': avatar,
         'country': country,
         'province': province,
         'city': city,
         'remarkname': remarkname,
         'remark': remark,
         'type': type,
         'paperstype': customer.paperstype,
         'signphone': signphone,
         'sex': customer.sex,
         'resphone': resphone,
         'birthday': customer.birthday,
         'frxm': customer.frxm,
         'isReamName': isReamName,
         'account_crmid':customer.crm_id
         }
    )
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 获取游客详情
def detail_visitor(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    try:
        user = User.objects.get(id=user_id)
        password = user.password
        risklevel = user.risklevel
        nickname = base64_decode(user.wechat_nickname)
        if user.gender == 1:
            gender = '男'
        elif user.gender == 2:
            gender = '女'
        else:
            gender = '未知'
        avatar = user.wechat_avatar
        country = user.country
        province = user.province
        city = user.city
    except User.DoesNotExist:
        password = None
        risklevel = None
        nickname = None
        gender = None
        avatar = None
        country = None
        province = None
        city = None
    results = (
        {'phone': password,
         'risklevel': risklevel,
         'nickname': nickname,
         'gender': gender,
         'avatar': avatar,
         'country': country,
         'province': province,
         'city': city,
         }
    )
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 客户详情界面下方理财倾向，暂无认证
def get_customer_tendency(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    account_id = data['account_id']
    customer = Account.objects.get(id=account_id)
    if not customer.risklevel:
        customer.risklevel = "未测评"
    results = ({'qualification': '未认证', 'risklevel': customer.risklevel})
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 客户修改个人信息，主动同步
def update_customer(request):
    from SCCProgram.wsgi import thread_loop_4_user

    data = json.loads(request.body)
    customer_id = data['id']
    num = data['num']
    try:
        info = Account.objects.get(id=customer_id)

        # 异步执行CRM同步
        url = CRM_URLS['ACCOUNT']
        crm_data = {
            'Id': info.crm_id,
            'Num__c': num,
        }
        # 产生coroutine任务sync_account交给子线程theread_loop_4_order执行
        future = asyncio.run_coroutine_threadsafe(sync_standard(url, json.dumps(crm_data)), thread_loop_4_user)
        # response = future.result()

        # 写入数据库
        # info.name = data['name']
        # info.email = data['email']
        info.num = base64_encode(num)
        # info.branch = data['branch']
        info.save()
        return HttpResponse('更新成功')
    except Account.DoesNotExist:
        return HttpResponse('更新失败')


# 微信授权-获取openid/session_key/basic_access_token
def silence_get_openid(request):
    data = json.loads(request.body)
    code = data['code']  # e.g."code":"071Eewma0BvKGB10gGpa073rma0Eewmj"
    if not code:
        return HttpResponseBadRequest('Request Error')
    appid = MINI_PROGRAM_ARGS['APPID']
    secret = MINI_PROGRAM_ARGS['APPSECRET']
    url = f'https://api.weixin.qq.com/sns/jscode2session?appid={appid}&secret={secret}&js_code={code}' \
          f'&grant_type=authorization_code '
    # e.g.{"session_key":"HRf6jzLcgVZhEgKkRWheIA==","openid":"oQPhH481xnyKSYXGakaouFayqBbg"}
    data = requests.get(url)
    access_token = get_basic_access_token()
    if data.json().get('errcode', ''):
        return HttpResponse('Code失效')
    session_key = data.json().get('session_key', '')
    openid = data.json().get('openid', '')
    info = {
        "openid": openid,
        "session_key": session_key,
        "access_token": access_token,  # basic access token
        "expires_in": "7200"
    }
    return HttpResponse(json.dumps(info, ensure_ascii=False), content_type="application/json", charset='utf-8')


# 微信授权-获取user_access_token
def get_user_access_token(request):
    data = json.loads(request.body)
    code = data['code']
    appid = MINI_PROGRAM_ARGS['APPID']
    secret = MINI_PROGRAM_ARGS['APPSECRET']
    url = f'https://api.weixin.qq.com/sns/oauth2/access_token?' \
          f'appid={appid}&secret={secret}&code={code}&grant_type=authorization_code'
    data = requests.get(url)
    if data.json().get('errcode', '48001'):
        return HttpResponse('API 未授权')
    access_token = data.json().get('access_token', '')
    return HttpResponse(access_token)


# 微信授权-获取basic_access_token
def get_basic_access_token():
    appid = MINI_PROGRAM_ARGS['APPID']
    secret = MINI_PROGRAM_ARGS['APPSECRET']
    url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={secret}'
    data = requests.get(url)
    access_token = data.json().get('access_token', '')
    return access_token


# 不可用,不确定REDIRECT_URI
def userAuthorize(request):
    APPID = MINI_PROGRAM_ARGS['APPID']
    REDIRECT_URI = ""  # 授权后重定向的回调链接地址
    SCOPE = "snasapi_userinfo"
    STATE = "1"
    auth_url = f'https://open.weixin.qq.com/connect/oauth2/authorize?appid={APPID}&redirect_uri=' \
               f'{REDIRECT_URI}&response_type=code&scope={SCOPE}&state={STATE}#wechat_redirect '
    return redirect(auth_url)


# 未测试成功
def get_user_info_old(request):
    data = json.loads(request.body)
    openid = data['openid']
    access_token = data['access_token']
    if not openid:
        return HttpResponseBadRequest('Request Error')
    url = f'https://api.weixin.qq.com/sns/userinfo?access_token={access_token}&openid={openid}&lang=zh_CN'
    data = requests.get(url)
    if data.json().get('errcode', ''):
        return HttpResponse('Access Token无效')
    return HttpResponse(data)


# 计算风险评估结果
def risk_eval(request):
    result = 0
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    ops = data['option']
    for op in ops:
        score = op  # 注意:数据库中默认score和option位置一一对应
        result += int(score)

    if 10 <= result < 16:
        type = "安全型"
    elif 16 <= result < 22:
        type = "保守型"
    elif 22 <= result < 28:
        type = "稳健型"
    elif 28 <= result < 34:
        type = "积极型"
    elif 34 <= result <= 41:
        type = "进取型"
    else:
        type = "问卷调查失败"
    return HttpResponse(type)


# 搜索客户
def search_customer(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    responsible_cid = data['responsible_cid']
    name = data['name']
    accounts = []
    logintime = datetime.datetime(1900, 1, 1)
    if not name:
        # 获取潜在
        # accounts = Account.objects.filter(responsible_id=responsible_cid, user_id__isnull=False)
        all_accounts = Account.objects.filter(responsible_id=responsible_cid)
        for acc in all_accounts:
            order = Order.objects.filter(account_cid=acc.crm_id, ostatus__in=valid_status)
            # 搜索存量
            if order:
                accounts.append(acc)
            # 搜索潜在
            else:
                accounts.append(acc)

    else:
        # accounts = Account.objects.filter(name__contains=name, responsible_id=responsible_cid, user_id__isnull=False)
        all_accounts = Account.objects.filter(name__contains=name, responsible_id=responsible_cid)
        for acc in all_accounts:
            order = Order.objects.filter(account_cid=acc.crm_id, ostatus__in=valid_status)
            # 搜索存量
            if order:
                accounts.append(acc)
            # 搜索潜在
            else:
                accounts.append(acc)
    results = []
    for i in accounts:
        account = Account.objects.get(id=i.id)
        assets = Asset.objects.filter(account_cid=account.crm_id)
        user = User.objects.filter(id=account.user_id).first()
        login = Userbehavior.objects.filter(user=user, behaviortype='上线').order_by('-time').first()
        if login:
            logintime = login.time
        position_value = 0
        floating_profit = 0
        interest = 0
        subscription_amount = 0
        for j in assets:
            if j.holdshare is None:
                j.holdshare = 0
            if j.newcleanvalue is None:
                j.newcleanvalue = 0
            if j.floatmoney is None:
                j.floatmoney = 0
            if j.interest is None:
                j.interest = 0
            if j.buyamount is None:
                j.buyamount = 0
            # p = Productofficial.objects.get(crm_id=j.product_cid)
            # if p.recordtype == '二级市场':
            position_value = position_value + j.newasset
            floating_profit = floating_profit + j.floatmoney
            interest = interest + j.interest
            subscription_amount = subscription_amount + j.buyamount
        results.append({'name': i.name,
                        'subscriptionamount': subscription_amount,
                        'position_value': position_value,
                        'id': i.id,
                        'floatingprofit': floating_profit,
                        'interest': interest,
                        'logintime': logintime, })
    results = sorted(results, key=itemgetter('logintime', 'position_value', 'subscriptionamount'), reverse=True)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def summary_responsible(request):
    msg = json.loads(request.body)
    id = msg['id']
    res = Responsible.objects.get(id=id)
    account_res = ResponsibleAccount.objects.filter(responsible_id=res.id)
    account_res_list = list(account_res)
    res_0 = []
    assets = 0
    total_customers = len(account_res_list)
    for i in account_res_list:
        subscription_amount = 0
        position_value = 0
        # result = Account.objects.get(id=i.account_field.id)
        result1 = Asset.objects.filter(account_id=i.account_id)
        pro_cus_list = list(result1)
        floatingprofit = 0
        interest = 0
        for j in pro_cus_list:
            if j.holdshare is None:
                j.holdshare = 0
            if j.newcleanvalue is None:
                j.newcleanvalue = 0
            if j.floatmoney is None:
                j.floatmoney = 0
            if j.interest is None:
                j.interest = 0
            if j.buyamount is None:
                j.buyamount = 0
            position_value = position_value + j.newasset
            floatingprofit = floatingprofit + j.floatmoney
            interest = interest + j.interest
            subscription_amount = subscription_amount + j.buyamount
        assets = assets + position_value + subscription_amount
    res_0.append({'total_customers': total_customers, 'total_assets': assets})
    return HttpResponse(json.dumps(res_0, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def add_responsible(request):
    responsiblemsg = json.loads(request.body)
    msg = Responsible.objects.filter(rmcode=responsiblemsg['rmcode']).count()
    if msg != 0:
        return HttpResponse("该用户已存在")
    else:
        last_responsible = Responsible.objects.last()
        if last_responsible == None:
            responsible_id = 0
        else:
            responsible_id = last_responsible.id + 1
        new_responsible = Responsible()
        new_responsible.id = responsible_id
        new_responsible.save()
        try:
            Responsible.objects.filter(
                id=responsible_id).update(**responsiblemsg)
            return HttpResponse("添加成功")
        except Responsible.DoesNotExist:
            return HttpResponse("添加失败")


# 未用
def delete_responsible(request):
    deletemsg = json.loads(request.body)
    responsible_id = deletemsg['id']
    try:
        responsible = Responsible.objects.get(id=responsible_id)
        responsible.delete()
        return HttpResponse('删除成功')
    except:
        return HttpResponse('删除失败')


# 获取投顾详情
def detail_responsible(request):
    responsiblemsg = json.loads(request.body)
    responsible_id = responsiblemsg['responsible_id']
    responsible = Responsible.objects.filter(id=responsible_id)
    responsible_list = list(responsible)
    responsible = []
    for i in responsible_list:
        isAuthChannel = "false"
        if i.channel and (i.channel in "瑞华财富,昆明瑞华"):
            isAuthChannel = "true"
        responsible.append({'rmcode': i.rmcode, 'department': i.department, 'team': i.team, 'branch': i.branch,
                            'positioni': i.positioni, 'is_incumbent': i.is_incumbent, 'channel': i.channel,
                            'name': i.name, 'user': i.user, 'phone': base64_decode(i.phone), 'id': i.id,
                            'recommendcode': i.recommendcode,'channel': i.channel,'isAuthChannel': isAuthChannel
                            })
    return HttpResponse(json.dumps(responsible, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 投顾修改个人信息，主动同步
def update_responsible(request):
    from SCCProgram.wsgi import thread_loop_4_user

    data = json.loads(request.body)
    responsible_id = data['id']
    num = data['num']
    try:
        info = Responsible.objects.get(id=responsible_id)

        # 异步执行CRM同步
        url = CRM_URLS['RESPONSIBLE']
        crm_data = {
            'Id': info.crm_id,
            'phone__c': num,
        }
        # 产生coroutine任务sync_responsible交给子线程theread_loop_4_order执行
        future = asyncio.run_coroutine_threadsafe(sync_standard(url, json.dumps(crm_data)), thread_loop_4_user)
        # response = future.result()

        # info.name = data['name']
        # info.email = data['email']
        info.phone = base64_encode(data['num'])
        # info.branch = data['branch']
        info.save()
        return HttpResponse('更新成功')
    except Responsible.DoesNotExist:
        return HttpResponse('更新失败')


def detail_channelManager(request):
    channelManagermsg = json.loads(request.body)
    channelManager_id = channelManagermsg['channel_id']
    channelManager = Channelmanagement.objects.filter(id=channelManager_id)
    channelManager_list = list(channelManager)
    channelManager = []
    for i in channelManager_list:
        channelManager.append({'rmcode': i.rmcode, 'department': i.department, 'team': i.team, 'branch': i.branch,
                               'positioni': i.positioni, 'is_incumbent': i.is_incumbent, 'channel': i.channel,
                               'name': i.name, 'phone': i.phone, 'id': i.id
                               })
    return HttpResponse(json.dumps(channelManager, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def update_channelManager(request):
    data = json.loads(request.body)
    channelManager_id = data['id']
    try:
        info = Channelmanagement.objects.get(id=channelManager_id)
        # info.name = data['name']
        # info.email = data['email']
        info.num = data['num']
        # info.branch = data['branch']
        info.save()
        return HttpResponse('更新成功')
    except Channelmanagement.DoesNotExist:
        return HttpResponse('更新失败')


# 未用
def get_my_bank(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    try:
        account = Account.objects.get(id=account_id)
    except Account.DoesNotExist:
        return CustomResponse('账号查找失败')
    name = account.name
    cid = account.crm_id
    bank_res = Accountbank.objects.filter(owneraccount_cid=cid)
    bank_list = list(bank_res)
    bank_res = []
    for i in bank_list:
        bank_res.append({'name': name, 'bankname': i.bankname,
                         'bankno': i.bankno, 'type': i.type, 'bank_id': i.id})
    return HttpResponse(json.dumps(bank_res), content_type="application/json,charset=utf-8")


# 未用
def update_my_bank(request):
    msg = json.loads(request.body)
    bank_id = msg['id']
    Accountbank.objects.filter(id=bank_id).update(**msg)
    return HttpResponse('更新成功')


# 未用
def get_my_responsible(request):
    data = json.loads(request.body)
    id = data['account_id']
    responsible_account = ResponsibleAccount.objects.filter(account_id=id)
    responsible_info_list = []
    for i in responsible_account:
        info = Responsible.objects.get(id=i.responsible_id)
        responsible_info_list.append({'name': info.name, 'phone': info.phone, 'crm_id': info.crm_id})
    return HttpResponse(json.dumps(responsible_info_list), content_type="application/json,charset=utf-8")


# 未用
def add_my_account(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    account_id = data['account_id']
    responsible_id = data['responsible_id']
    try:
        ResponsibleAccount.objects.get(
            responsible_id=responsible_id, account_id=account_id)
        return HttpResponse('您已添加过该客户')
    except ResponsibleAccount.DoesNotExist:
        # 绑定RA表
        new_acc_res = ResponsibleAccount()
        new_acc_res.account_id = account_id
        new_acc_res.responsible_id = responsible_id
        new_acc_res.save()
        # 在客户信息中增加顾问crm_id
        responsible = Responsible.objects.get(id=responsible_id)
        Account.objects.filter(id=account_id).update(
            responsible_id=responsible.crm_id)
        return HttpResponse('添加成功')


# 投顾获取存量/潜在客户列表（包括游客）
def get_my_account(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    responsible_cid = data['responsible_cid']
    # 空cid处理
    if responsible_cid is None or responsible_cid is "":
        return HttpResponse('此用户无有效crm_id')
    accounts = Account.objects.filter(responsible_id=responsible_cid)
    customers = []
    potentials = []
    bound_accounts = []
    visitors = []
    product_list = []
    all_customer_assets = 0
    for acc in accounts:
        try:
            remark = base64_decode(acc.remark)
        except Exception:
            remark = acc.remark
        try:
            remarkname = base64_decode(acc.remarkname)
        except Exception:
            remarkname = acc.remarkname
        buyamount = 0
        position_value = 0
        floatingprofit = 0
        interest = 0
        total_assets = 0
        total_solid_assets = 0
        total_securities_assets = 0
        total_stock_assets = 0
        # 获取用户信息
        user = User.objects.filter(id=acc.user_id).first()
        createtime = datetime.datetime(1900, 1, 1)
        logintime = datetime.datetime(1900, 1, 1)
        if not user:
            avatar = "未绑定"
            nickname = "-"
        else:
            avatar = user.wechat_avatar
            if acc.remarkname != '' and acc.remarkname is not None:
                nickname = acc.remarkname
            elif user.wechat_nickname != '' and user.wechat_nickname is not None:
                nickname = base64_decode(user.wechat_nickname)
            elif acc.name != '' and acc.name is not None:
                nickname = acc.name
            login = Userbehavior.objects.filter(user=user, behaviortype='上线').order_by('-time').first()
            if login:
                logintime = login.time
            if user.createtime:
                createtime = user.createtime
        # 获取存量信息
        order = Order.objects.filter(account_cid=acc.crm_id, ostatus__in=valid_status)
        if order:
            assets = Asset.objects.filter(account_cid=acc.crm_id)
            for a in assets:
                if a.holdshare is None:
                    a.holdshare = 0
                if a.newcleanvalue is None:
                    a.newcleanvalue = 0
                if a.floatmoney is None:
                    a.floatmoney = 0
                if a.interest is None:
                    a.interest = 0
                if a.buyamount is None:
                    a.buyamount = 0
                if a.redeemedearnings is None:
                    a.redeemedearnings = 0
                p = Productofficial.objects.get(crm_id=a.product_cid)
                flag = 0  # 控制append
                if p.recordtype == '类固收':
                    for r in product_list:  # 计算同产品认购金额的总值
                        if p.id == r['id']:
                            r['money'] = r['money'] + a.buyamount
                            flag = 1
                        else:
                            pass
                    if flag == 1:
                        pass
                    else:
                        product_list.append(
                            {'name': p.name, 'abbr': p.productabbreviation, 'recordtype': p.recordtype, 'id': p.id,
                             'crmid': p.crm_id, 'comparison': p.comparison,
                             'comparisonabbreviation': p.comparisonabbreviation,
                             'monthlimit': p.monthlimit, 'origin': p.origin, 'stage': p.stage,
                             'money': a.buyamount})
                    position_value = position_value + a.newasset
                    total_assets = total_assets + a.buyamount
                    total_solid_assets += a.buyamount
                elif p.recordtype == '二级市场':
                    for r in product_list:
                        if p == r:
                            r.money = r.money + a.buyamount
                            flag = 1
                        else:
                            pass
                    if flag == 1:
                        pass
                    else:
                        product_list.append(
                            {'name': p.name, 'abbr': p.productabbreviation, 'recordtype': p.recordtype, 'id': p.id,
                             'crmid': p.crm_id, 'direction': p.direction,
                             'origin': p.origin, 'stage': p.stage, 'renewalperiod': p.renewalperiod,
                             'money': a.buyamount})
                    position_value = position_value + a.newasset
                    total_assets = total_assets + a.newasset
                    total_securities_assets += a.newasset
                elif p.recordtype == '股权':
                    for r in product_list:
                        if p == r:
                            r.money = r.money + a.buyamount
                            flag = 1
                        else:
                            pass
                    if flag == 1:
                        pass
                    else:
                        product_list.append(
                            {'name': p.name, 'abbr': p.productabbreviation, 'recordtype': p.recordtype, 'id': p.id,
                             'crmid': p.crm_id, 'value': p.value,
                             'origin': p.origin, 'stage': p.stage, 'money': a.buyamount})  # 无近一年涨幅
                    position_value = position_value + a.newasset
                    total_assets = total_assets + a.buyamount
                    total_stock_assets += a.buyamount
                buyamount = buyamount + a.buyamount
                # 持仓市值position_value是三类产品的newasset之和
                floatingprofit = floatingprofit + a.floatmoney
                # 已收利息是类固收产品已收利息+已赎回二级的到账收益
                interest = interest + a.interest + a.redeemedearnings
            all_customer_assets += total_assets

            # 总资产
            sumNewAsset = Asset.objects.filter(account_cid=acc.crm_id).aggregate(sum=Sum('newasset'))
            # 持仓收益
            sumFloatmoney = Asset.objects.filter(account_cid=acc.crm_id).aggregate(sum=Sum('floatmoney'))
            inventory_earnings = "-"
            if sumFloatmoney['sum']:
                inventory_earnings = format(sumFloatmoney['sum'], ',')
            total_assets_a = "-"
            if sumNewAsset['sum']:
                total_assets_a = format(sumNewAsset['sum'], ',')
            customers.append({'name': acc.name,
                              'buy_amount': buyamount,
                              'position_value': position_value,
                              'id': acc.id,
                              'floatingprofit': floatingprofit,
                              'total_assets': total_assets,
                              'interest': interest,
                              'avatar': avatar,
                              'remarkname': acc.remarkname,
                              'nickname': nickname,
                              'logintime': logintime,
                              'remark': base64_decode(acc.remark),
                              'inventory_earnings': inventory_earnings,
                              'total_assets_a': total_assets_a,
                              'remarkname': base64_decode(acc.remarkname)})
        else:
            potentials.append({'name': acc.name,
                               'buy_amount': buyamount,
                               'position_value': position_value,
                               'id': acc.id,
                               'floatingprofit': floatingprofit,
                               'total_assets': 0,
                               'interest': interest,
                               'avatar': avatar,
                               'remarkname': acc.remarkname,
                               'nickname': nickname,
                               'createtime': createtime,
                               'logintime': logintime,
                               'remark': base64_decode(acc.remark),
                               'remarkname': base64_decode(acc.remarkname)
                               })
        # # 保存已绑定用户，避免重复出现在存量/潜在表
        # if acc.user_id:
        #     bound_accounts.append(acc.user_id)

    # # 获取游客列表
    # user_potentials = User.objects.filter(responsible_cid=responsible_cid)
    # for u in user_potentials:
    #     if u.id not in bound_accounts:
    #         if not u.password:
    #             password = ""
    #         else:
    #             password = u.password
    #
    #         if u.createtime:
    #             createtime = u.createtime
    #         else:
    #             createtime = datetime.datetime(1900, 1, 1)
    #
    #         visitors.append({'name': "游客" + password,
    #                          'avatar': u.wechat_avatar,
    #                          'nickname': base64_decode(u.wechat_nickname),
    #                          'user_id': u.id,
    #                          'createtime': createtime,
    #                          })

    customers = sorted(customers, key=itemgetter('logintime', 'position_value', 'buy_amount'), reverse=True)
    potentials = sorted(potentials, key=itemgetter('logintime', 'createtime'), reverse=True)
    # visitors = sorted(visitors, key=itemgetter('createtime'), reverse=True)
    results = {'total_customers': len(customers),
               'total_potentials': len(potentials) + len(visitors),
               'all_customer_assets': all_customer_assets,
               'customers': customers,
               'potentials': potentials,
               # 'visitors': visitors
               }
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 获取某角色对应的id
def get_my_id(request):
    data = json.loads(request.body)
    user_id = data['user_id']
    role = data['role']
    try:
        role = Role.objects.get(name=role)
    except Role.DoesNotExist:
        return CustomResponse("role查询失败")
    if role.id == 1:
        try:
            responsible = Responsible.objects.get(user_id=user_id)
            result = {'id': responsible.id, 'crmid': responsible.crm_id}
        except Responsible.DoesNotExist:
            return HttpResponse("未找到角色")
    elif role.id == 2:
        try:
            account = Account.objects.get(user_id=user_id)
            result = {'id': account.id, 'crmid': account.crm_id}
        except Account.DoesNotExist:
            return HttpResponse("未找到角色")
    elif role.id == 3:
        try:
            channel = Channelmanagement.objects.get(user_id=user_id)
            result = {'id': channel.id, 'crmid': channel.crm_id}
        except Channelmanagement.DoesNotExist:
            return HttpResponse("未找到角色")
    else:
        return HttpResponse("未找到角色")
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


# 通过某角色id获取其cid
def id_2_cid(request):
    data = json.loads(request.body)
    id = data['id']
    role_name = data['role']
    try:
        role = Role.objects.get(name=role_name)
    except Role.DoesNotExist:
        return CustomResponse("role查询失败")
    if role.id == 1:
        cid = Responsible.objects.get(id=id).crm_id
    elif role.id == 2:
        cid = Account.objects.get(id=id).crm_id
    elif role.id == 3:
        cid = Channelmanagement.objects.get(id=id).crm_id
    else:
        return HttpResponseBadRequest('身份错误')
    result = {'crm_id': cid}
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


# 通过某角色cid获取其id
def cid_2_id(request):
    data = json.loads(request.body)
    cid = data['crm_id']
    role_name = data['role']
    try:
        role = Role.objects.get(name=role_name)
    except Role.DoesNotExist:
        return CustomResponse("role查询失败")
    if role.id == 1:
        id = Responsible.objects.get(crm_id=cid).id
    elif role.id == 2:
        id = Account.objects.get(crm_id=cid).id
    elif role.id == 3:
        id = Channelmanagement.objects.get(crm_id=cid).id
    else:
        return HttpResponseBadRequest('身份错误')
    result = {'id': id}
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


# 未用
def search_to_add_customer(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    Certificatenumber = data['Certificatenumber']
    paperstype = data['paperstype']
    account = Account.objects.get(
        certificatenumber=base64_decode(Certificatenumber), paperstype=paperstype)
    avatar = User.objects.get(id=account.user_id)
    result = {'name': account.name,
              'avatar': avatar.wechat_avatar, 'account_id': account.id}
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def search_responsible(request):
    msg = json.loads(request.body)
    search_value = msg['search_value']
    result = Responsible.objects.filter(name__contains=search_value)
    customer_list = list(result)
    result = []
    for i in customer_list:
        result.append({'name': i.name, 'id': i.id, 'rmcode': i.rmcode,
                       'department': i.department, 'positioni': i.positioni, 'branch': i.branch, 'channel': i.channel})
    return JsonResponse(result, safe=False)


# 未用
def get_responsible_by_type(request):
    msg = json.loads(request.body)
    id = msg['id']
    type_key = msg['type_key']
    type = msg['type']
    channel_res_info = ChannelmanagementResponsible.objects.filter(
        channelmanagement_id=id)
    responsible_list = list(channel_res_info)
    result = []
    for i in responsible_list:
        responsible = Responsible.objects.get(id=i.responsible_id)
        if type_key == "事业部":
            if responsible.department == type:
                result.append({'name': responsible.name, 'id': responsible.id, 'rmcode': responsible.rmcode,
                               'department': responsible.department, 'positioni': responsible.positioni,
                               'branch': responsible.branch, 'channel': responsible.channel})
        else:
            if responsible.branch == type:
                result.append({'name': responsible.name, 'id': responsible.id, 'rmcode': responsible.rmcode,
                               'department': responsible.department, 'positioni': responsible.positioni,
                               'branch': responsible.branch,
                               'channel': responsible.channel})
    return JsonResponse(result, safe=False)


# 未用
def get_my_responsible_by_channel(request):
    msg = json.loads(request.body)
    id = msg['id']
    res = Channelmanagement.objects.get(id=id)
    responsible_res = ChannelmanagementResponsible.objects.filter(
        channelmanagement_id=res.id)
    responsible_res_list = list(responsible_res)
    res = []
    for i in responsible_res_list:
        responsible_id = i.responsible_id
        responsible_info = Responsible.objects.get(id=responsible_id)
        res.append({'name': responsible_info.name, 'id': responsible_info.id, 'rmcode': responsible_info.rmcode,
                    'department': responsible_info.department, 'positioni': responsible_info.positioni,
                    'branch': responsible_info.branch,
                    'channel': responsible_info.channel})
    return HttpResponse(json.dumps(res), content_type="application/json,charset=utf-8")


# 未用
def save_risk_eval_info(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    account_id = data['account_id']
    ops = data['option']
    type = data['type']
    user = User.objects.filter(id=user_id).first()
    if not user:
        return HttpResponse("没有对应的用户")
    for i, op in enumerate(ops):
        topic = Questionnairetopic.objects.get(number=i + 1)
        option = Questionnaireoption.objects.get(Q(topic=i + 1) & Q(score=op))
        # 如果已有问卷信息，则更新
        if QuestionnaireAccount.objects.filter(topic_id=topic.id):
            QuestionnaireAccount.objects.filter(Q(user_id=user.id) & Q(topic_id=topic.id)).update(
                option_id=option.id)
        else:
            QuestionnaireAccount.objects.create(user_id=user.id, topic_id=topic.id,
                                                option_id=option.id)
    User.objects.filter(id=user_id).update(risklevel=type)
    if account_id != "":
        Account.objects.filter(id=account_id).update(risklevel=type)
    return HttpResponse("风险问卷信息保存成功")


# 获取用户风险等级
def get_risk_eval_info(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    user = User.objects.filter(id=user_id).first()
    if not user:
        return HttpResponse("没有对应的用户")
    return HttpResponse(user.risklevel)


# 获取风险评估信息表
def get_risk_eval(request):
    topics_content = []
    options_content = []
    options_score = []
    topics = Questionnairetopic.objects.all()
    for topic in topics:
        topics_content.append(topic.content)
        options = Questionnaireoption.objects.filter(topic_id=topic.id)
        op_content = []
        op_score = []
        for op in options:
            op_content.append(op.content)
            op_score.append(op.score)
        options_content.append(op_content)
        options_score.append(op_score)
    results = ({
        'topics': topics_content, 'options_content': options_content, 'options_score': options_score,
    })
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def get_new_account(request):
    res = Account.objects.all()  # 新客户定义为没有订单的客户
    res_list = list(res)
    results = []
    k = 0
    for i in res_list:
        k = k + 1
        print(k)
        count = Order.objects.filter(account_cid=i.crm_id).count()
        position_value = 0
        floatingprofit = 0
        interest = 0
        SubscriptionAmount = 0
        if count == 0:
            results.append({'name': i.name, 'subscription_amount': SubscriptionAmount,
                            'position_value': position_value, 'id': i.id, 'floatingprofit': floatingprofit,
                            'interest': interest})
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def change_password_by_responsible(request):
    msg = json.loads(request.body)
    id = msg['id']
    old_password = msg['old_password']
    new_password = msg['new_password']
    res = Responsible.objects.get(id=id)
    user = User.objects.get(id=res.user_id)
    if user.password == old_password:
        user.password = new_password
        user.save()
        return HttpResponse("密码已更改")
    else:
        return HttpResponse("密码错误")


# 未用
def change_password_by_channel(request):
    msg = json.loads(request.body)
    id = msg['id']
    old_password = msg['old_password']
    new_password = msg['new_password']
    res = Channelmanagement.objects.get(id=id)
    user = User.objects.get(id=res.user_0_id)
    if user.password == old_password:
        user.password = new_password
        user.save()
        return HttpResponse("密码已更改")
    else:
        return HttpResponse("密码错误")


# 获取头像
def get_my_avatar(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    id = data['id']
    role_id = data['role_id']
    result = []
    # 投顾
    if role_id == 1:
        try:
            responsible = Responsible.object.get(id=id)
        except Responsible.DoesNotExist:
            return CustomResponse("未查询到")
        try:
            avatar = User.objects.get(id=responsible.user_id).wechat_avatar
        except User.DoesNotExist:
            return CustomResponse("未查询到")
        result = {"name": responsible.name, "avatar": avatar}
    # 投资人
    elif role_id == 2:
        try:
            account = Account.objects.get(id=id)
        except Account.DoesNotExist:
            return CustomResponse("未查询到")
        try:
            avatar = User.objects.get(id=account.user_id).wechat_avatar
        except User.DoesNotExist:
            return CustomResponse("未查询到")
        result = {"name": account.name, "avatar": avatar}
    # 渠道
    elif role_id == 3:
        try:
            channel = Channelmanagement.objects.get(id=id)
        except Channelmanagement.DoesNotExist:
            return CustomResponse("未查询到")
        try:
            avatar = User.objects.get(id=channel.user_id).wechat_avatar
        except User.DoesNotExist:
            return CustomResponse("未查询到")
        result = {"name": channel.name, "avatar": avatar}
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 未用
def delete_my_account(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    account_id = data['account_id']
    responsible_id = data['responsible_id']
    try:
        # 更新RA表
        res_acc = ResponsibleAccount.objects.get(
            responsible_id=responsible_id, account_id=account_id)
        res_acc.delete()
        # 在客户信息中删除顾问crm_id
        Account.objects.filter(id=account_id).update(responsible_id=None)
        return HttpResponse("删除成功！")
    except ResponsibleAccount.DoesNotExist:
        return HttpResponse("删除失败！")


# 未用
def delete_my_responsible(request):
    msg = json.loads(request.body)
    channel_id = msg['channel_id']
    responsible_id = msg['responsible_id']
    try:
        responsible = Responsible.objects.get(id=responsible_id)
        channel = Channelmanagement.objects.get(id=channel_id)
        res = ChannelmanagementResponsible.objects.get(
            responsible_id=responsible_id, channelmanagement_id=channel_id)
        res.delete()
        return HttpResponse("删除成功！")
    except:
        return HttpResponse("删除失败！")


# 未用
def add_my_responsible(request):
    msg = json.loads(request.body)
    channel_id = msg['channel_id']
    responsible_id = msg['responsible_id']
    try:
        res = ChannelmanagementResponsible.objects.get(
            responsible_id=responsible_id, channelmanagement_id=channel_id)
        return HttpResponse('您已添加过该顾问')
    except ChannelmanagementResponsible.DoesNotExist:
        last_res = ChannelmanagementResponsible.objects.last()
        if last_res is None:
            res_id = 0
        else:
            res_id = last_res.id + 1
        new_acc_res = ChannelmanagementResponsible()
        new_acc_res.id = res_id
        new_acc_res.channelmanagement_id = channel_id
        new_acc_res.responsible_id = responsible_id
        new_acc_res.save()
        return HttpResponse('添加成功')

#添加我的银行账号 带 姓名+卡号认证
def add_my_bank_new(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    bankno = data['bankno']
    if bankno == "":
        return ResponseCodeMesData(400, "请输入银行卡号", "")
    bankname = data['bankname']
    if bankname == "":
        return ResponseCodeMesData(400, "请输入正确开户行(仅支持汉字格式)", "")
    try:
        account = Account.objects.get(id=account_id)
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "用户不存在", "")
    try:
        accountbank = Accountbank.objects.get(owneraccount_cid=account.crm_id, bankno=bankno)
        return ResponseCodeMesData(400, "银行账户已存在,请勿重复添加！", "")
    except Accountbank.DoesNotExist:
        #调用三方接口进行实名校验
        try:
            result = bankCardAndNameCheck(account.name, bankno)
            if result["isPass"] != "true":
                return ResponseCodeMesData(400, result["msg"], "")
        except Exception as e:
            print(e)
            return ResponseCodeMesData(400, "三方校验银行卡信息异常", "")
        # 写入数据库
        new_accountbank = Accountbank.objects.create(owneraccount_cid=account.crm_id, bankno=bankno,
                                                     accountname=account.name,
                                                     bankname=bankname)
        # 非异步执行CRM同步
        url = CRM_URLS['ACCOUNTBANK']
        crm_data = {
            'OwnerAccount__c': account.crm_id,
            'Bankname__c': bankname,
            'AccountName__c': account.name,
            'BankNO__c': bankno,
        }
        response = requests.post(url, data=json.dumps(crm_data))
        crm_id = response.text
        sync_accountbank_2_mysql(new_accountbank.id, crm_id)
        return ResponseCodeMesData(200, "银行卡已添加成功", "")

#调用第三方银行卡二三四元素实名认证V2
#正常返回示例
# {
#     "error_code": 0,
#     "reason": "成功",
#     "result": {
#         "respCode": "F",
#         "respMsg": "验证要素格式有误",
#         "detailCode": "12",
#         "bancardInfor": {
#             "bankName": "农业银行",
#             "BankId": 3,
#             "type": "借记卡",
#             "cardname": "金穗通宝卡(银联卡)",
#             "tel": "95599",
#             "Icon": "nongyeyinhang.gif"
#         }
#     }
# }
#错误返回
#{
# 	"error_code": 208301,
# 	"reason": "成功",
# 	"result": {
# 		"respCode": "N",
# 		"respMsg": "发卡行不支持此交易，请联系发卡",
# 		"detailCode": "3",
# 		"bancardInfor": {
# 			"bankName": "农业银行",
# 			"BankId": 3,
# 			"type": "借记卡",
# 			"cardname": "金穗通宝卡(银联卡)",
# 			"tel": "95599",
# 			"Icon": "nongyeyinhang.gif"
# 		}
# 	}
# }
# respCode:应答码 “T”表示有效的，”F”表示无效的，“N”表示无法认证的，”P”表示网络连接超时
# detailCode:细分应答码 0，认证成功，1，信息有误 2，卡状态异常 3，发卡行不支持此交易，请联系发卡行 4,银联不支持该银行 5,无效卡 6，受限制的卡，7，姓名格式有误 8，身份证号码格式有误，9，手机号码格式有误 10，银行卡格式有误，11，密码错误次数超限，12，验证要素格式有误 ，100，其他原因不通过
# detailCode:(0，对应respCode:T) (1, 2,5,6，7,8,9,10,11,12,100,对应respCode:F)(3, 4,对应respCode:N)
# bankName:银行名称，type:卡类型,cardname:卡名称，tel：银行电话，Icon：银行logo
def bankCardAndNameCheck(name,accountNo):
    import urllib
    import urllib.request
    import ssl

    host = 'https://zball.market.alicloudapi.com'
    path = '/v2/bcheck'
    method = 'POST'
    appcode = '64f2bbc330be4133839afce2a62f2eb2'
    querys = ''
    bodys = {}
    url = host + path

    bodys['accountNo'] = accountNo
    bodys['bankPreMobile'] = ''
    bodys['idCardCode'] = ''
    bodys['name'] = name
    post_data = urllib.parse.urlencode(bodys).encode("utf-8")
    request1 = urllib.request.Request(url, post_data)
    request1.add_header('Authorization', 'APPCODE ' + appcode)
    # 根据API的要求，定义相对应的Content - Type
    request1.add_header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    response = urllib.request.urlopen(request1, context=ctx)
    content = response.read().decode('utf-8')
    #content = '{"error_code":0,"reason":"成功","result":{"respCode":"F","respMsg":"认证信息不匹配","detailCode":"1","bancardInfor":{"bankName":"平安银行","BankId":14,"type":"借记卡","cardname":"平安银行IC借记卡","tel":"400-6699-999","Icon":"PingAnYinHang.gif"}}}'
    isPass = "false"
    msg = "通过"
    content = json.loads(content)
    if content and content["result"]:
        resultCode = content["result"]["detailCode"]
        if resultCode == '0':
            isPass = "true"
        msg = content["result"]["respMsg"]
        if msg == "认证信息不匹配":
            msg = "信息不匹配"
    return {"isPass": isPass,"msg": msg}

# 添加客户银行卡，主动同步
def add_my_bank(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    bankno = data['bankno']
    bankname = data['bankname']
    account = Account.objects.get(id=account_id)
    try:
        accountbank = Accountbank.objects.get(owneraccount_cid=account.crm_id, bankno=bankno)
        return HttpResponse("银行账户已存在")
    except Accountbank.DoesNotExist:
        # 写入数据库
        new_accountbank = Accountbank.objects.create(owneraccount_cid=account.crm_id, bankno=bankno,
                                                     accountname=account.name,
                                                     bankname=bankname)
        # 非异步执行CRM同步
        url = CRM_URLS['ACCOUNTBANK']
        crm_data = {
            'OwnerAccount__c': account.crm_id,
            'Bankname__c': bankname,
            'AccountName__c': account.name,
            'BankNO__c': bankno,
        }
        response = requests.post(url, data=json.dumps(crm_data))
        crm_id = response.text
        sync_accountbank_2_mysql(new_accountbank.id, crm_id)
        return HttpResponse("添加成功")


# 获取短信验证码
def get_sms(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    phone = data['phone']
    code = generate_code(6, False)
    cache.set(phone, code, 300)  # 300s有效期
    # print('判断缓存中是否有:', cache.has_key(phone))
    # print('获取Redis验证码:', cache.get(phone))
    result = send_sms(phone, code)
    if result == "OK":
        return HttpResponse(json.dumps({'code': code}, cls=CustomEncoder),
                            content_type="application/json,charset=utf-8")
    else:
        return HttpResponseBadRequest(result)


# 验证短信验证码
def check_sms(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    phone = data['phone']
    code = data['code']
    if phone in cache:
        # print('缓存中是否包含:', cache.has_key(phone))
        # print('取值:', cache.get(phone))
        cache_code = cache.get(phone)
    else:
        return HttpResponse(json.dumps({'status': 'Not cached'}, cls=CustomEncoder),
                            content_type="application/json,charset=utf-8")
    if code == cache_code:
        return HttpResponse(json.dumps({'status': 'OK'}, cls=CustomEncoder),
                            content_type="application/json,charset=utf-8")
    else:
        return HttpResponse(json.dumps({'status': 'False'}, cls=CustomEncoder),
                            content_type="application/json,charset=utf-8")


# 通过rmcode获取手机
def rmcode_2_phone(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    rmcode = data['rmcode']
    role = data['role']

    if role == "advisor":
        responsible = Responsible.objects.filter(rmcode=rmcode).first()
        if responsible:
            return HttpResponse(json.dumps({'phone': base64_decode(responsible.phone)}, cls=CustomEncoder),
                                content_type="application/json,charset=utf-8")

    if role == "manager":
        channel = Channelmanagement.objects.filter(rmcode=rmcode).first()
        if channel:
            return HttpResponse(json.dumps({'phone': channel.phone}, cls=CustomEncoder),
                                content_type="application/json,charset=utf-8")

    return HttpResponse('Phone Not Exist')


# 通过投顾身份证获取手机
def rm_certificatenumber_2_phone(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    certificatenumber = data['certificatenumber']
    certificatenumber_base64 = base64_encode(certificatenumber)
    try:
        responsible = Responsible.objects.get(certificatenumber=certificatenumber_base64)
    except Responsible.DoesNotExist:
        return HttpResponse('Certificate Number Not Exist')
    if responsible.phone:
        return HttpResponse(json.dumps({'phone': base64_decode(responsible.phone)}, cls=CustomEncoder),
                            content_type="application/json,charset=utf-8")
    else:
        return HttpResponse('Phone Not Exist')


# 通过客户身份证获取手机
def certificatenumber_2_phone(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    certificatenumber = data['certificatenumber']
    certificatenumber_base64 = base64_encode(certificatenumber)
    try:
        account = Account.objects.get(certificatenumber=certificatenumber_base64)
    except account.DoesNotExist:
        return HttpResponse('Certificate Number Not Exist')
    if account.num:
        return HttpResponse(json.dumps({'phone': base64_decode(account.num)}, cls=CustomEncoder),
                            content_type="application/json,charset=utf-8")
    else:
        return HttpResponse('Phone Not Exist')


# 添加留言
def add_message(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    cid = data['crm_id']
    content = data['content']
    if content:
        content = base64_encode(content)
    messagetime = data['message_time']
    Messageboard.objects.create(crm_id=cid, content=content, messagetime=messagetime)
    return HttpResponse('OK')


# 获取留言
def get_message(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    cid = data['crm_id']
    result = []
    messages = Messageboard.objects.filter(crm_id=cid)
    for i in messages:
        result.append({
            "message_content": i.content,
            "message_time": i.messagetime,
            "reply_content": i.reply,
            "reply_time": i.replytime
        })
    return HttpResponse(json.dumps(result, cls=CustomEncoder4Time), content_type="application/json,charset=utf-8")


# 通过cid获取openid
def crmid_2_openid(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    cid = data['crm_id']

    account = Account.objects.filter(crm_id=cid).first()
    if account:
        user = User.objects.filter(id=account.user_id).first()
        if user:
            result = {'openid': user.wechat_openid}
            return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")

    responsible = Responsible.objects.filter(crm_id=cid).first()
    if responsible:
        user = User.objects.filter(id=responsible.user_id).first()
        if user:
            result = {'openid': user.wechat_openid}
            return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")

    channel = Channelmanagement.objects.filter(crm_id=cid).first()
    if channel:
        user = User.objects.filter(id=channel.user_id).first()
        if user:
            result = {'openid': user.wechat_openid}
            return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")

    return HttpResponse("查无openid")


# 获取消息
def get_communication(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    cid_1 = data['cid_1']
    cid_2 = data['cid_2']
    results = []
    communication1 = Communicateboard.objects.filter(from_cid=cid_1, to_cid=cid_2)
    for i in communication1:
        results.append({
            "from_cid": cid_1,
            "to_cid": cid_2,
            "content": i.content,
            "time": i.time,
            "role_from": i.role_from,
            "role_to": i.role_to,
        })
    communication2 = Communicateboard.objects.filter(from_cid=cid_2, to_cid=cid_1)
    for i in communication2:
        results.append({
            "from_cid": cid_2,
            "to_cid": cid_1,
            "content": i.content,
            "time": i.time,
            "role_from": i.role_from,
            "role_to": i.role_to,
        })
    results = sorted(results, key=lambda x: x['time'])
    return HttpResponse(json.dumps(results, cls=CustomEncoder4Time), content_type="application/json,charset=utf-8")


# 发送消息
def add_communication(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    from_cid = data['from_cid']
    to_cid = data['to_cid']
    role_from = data['role_from']
    role_to = data['role_to']
    content = data['content']
    time = data['time']
    Communicateboard.objects.create(from_cid=from_cid, to_cid=to_cid, role_from=role_from, role_to=role_to,
                                    content=content, time=time)
    return HttpResponse('添加成功')


# 获取消息列表
def get_communicator_list(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    to_cid = data['to_cid']
    cids = []
    results = []
    communication = Communicateboard.objects.filter(to_cid=to_cid)
    for c in communication:
        if c.from_cid not in cids:
            try:
                account = Account.objects.get(crm_id=c.from_cid)
                name = account.name
            except Account.DoesNotExist:
                try:
                    responsible = Responsible.objects.get(crm_id=c.from_cid)
                    name = responsible.name
                except Responsible.DoesNotExist:
                    try:
                        channel = Channelmanagement.objects.get(crm_id=c.from_cid)
                        name = channel.name
                    except Channelmanagement.DoesNotExist:
                        name = "Unknown"

            results.append({
                "from_cid": c.from_cid, "role_from": c.role_from, "name": name,
            })
            cids.append(c.from_cid)
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 微信=转发通知
def send_subscribe_communication(request):
    access_token = get_basic_access_token()
    url = f'https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token={access_token}'
    response = requests.request('POST', url, data=request.body)
    print(response)
    return HttpResponse(response)


# base64和Crypto install
def get_userphonenumber_by_wechat(request):
    data = json.loads(request.body)
    sessionkey = data['sessionkey']
    encryptedData = data['encryptedData']
    iv = data['iv']
    appid = MINI_PROGRAM_ARGS['APPID']
    secret = MINI_PROGRAM_ARGS['APPSECRET']
    # base64 decode
    sessionKey = base64.b64decode(sessionkey)
    encryptedData = base64.b64decode(encryptedData)
    iv = base64.b64decode(iv)
    cipher = AES.new(sessionKey, AES.MODE_CBC, iv)
    info = str(cipher.decrypt(encryptedData))
    num = info.find("purePhoneNumber")
    if num == -1:
        return HttpResponse("获取手机号失败")
    phoneNumber = info[num + 18:num + 29]
    print(phoneNumber)
    info_appid_index = info.find("appid") + 8
    appid_temp = info[info_appid_index:]
    appid_get = appid[:appid_temp.find("\"")]
    if appid_get != appid:
        return HttpResponse("appid不匹配")
    return HttpResponse(phoneNumber)


def add_user(request):
    data = json.loads(request.body)
    wechat_openid = data['wechat_openid']
    phone_number = str(data['phone_number'])
    wechat_avatar = data['wechat_avatar']
    wechat_nickname = data['nickname']
    responsible_cid = data['responsible_cid']
    nickname_base64 = base64_encode(wechat_nickname)
    user_count = User.objects.filter(wechat_openid=wechat_openid).count()
    if responsible_cid == 'undefined':
        responsible_cid = None

    if user_count > 0:
        return HttpResponse("用户已存在")
    if user_count == 0:
        User.objects.create(wechat_openid=wechat_openid, password=phone_number, wechat_avatar=wechat_avatar,
                            wechat_nickname=nickname_base64,
                            responsible_cid=responsible_cid)
        user = User.objects.get(wechat_openid=wechat_openid)

        user_id = user.id
        new_account = Account.objects.create(
            name=phone_number[0:3] + "****" + phone_number[-4:],
            num=base64_encode(phone_number),
            createddate=datetime.datetime.now(),
            user_id=user_id,
            responsible_id=responsible_cid)

        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return CustomResponse("查询失败")
        try:
            role = Role.objects.get(name="investor")
        except Role.DoesNotExist:
            return CustomResponse("查询失败")
        UserRole.objects.create(user=user, role=role)
        # Account.objects.filter(certificatenumber=certificate_number).update(responsible_id=responsible_cid)
        from SCCProgram.wsgi import thread_loop_4_user

        # CRM同步必须写入Name
        # 异步执行CRM同步
        url = CRM_URLS['ACCOUNT']
        crm_data = {
            'PhoneSecret__c': base64_encode(phone_number),
            'Num__c': phone_number,
            'CertificateNumber__c': None,
            'CreatedDate__c': str(datetime.datetime.now().date()),
            'Name': phone_number[0:3] + "****" + phone_number[-4:],
            'LastName__c': phone_number[0:3] + "****" + phone_number[-4:],
            'PapersType__c': '身份证',
            'Responsible_1__c': responsible_cid,
            'WechatUser__c': user_id,
            'RiskLevel__c': user.risklevel,
        }
        # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
        future = asyncio.run_coroutine_threadsafe(sync_account(url, json.dumps(crm_data), new_account.id),
                                                  thread_loop_4_user)
        # response = future.result()
        result = {
            "user_id": user_id
        }
        return HttpResponse(json.dumps(result, cls=CustomEncoder4Time), content_type="application/json,charset=utf-8")


def check_telephone(request):
    data = json.loads(request.body)
    crm_id = data['crm_id']
    telephone_number = data['telephone_number']
    try:
        account = Account.objects.get(crm_id=crm_id)
        if base64_encode(telephone_number) == account.num:
            return HttpResponse("1")
        else:
            return HttpResponse("0")
    except Account.DoesNotExist:
        return HttpResponse("没有查询到该用户")


def check_bank_card(request):
    data = json.loads(request.body)
    account_cid = data['account_cid']
    bank_card = data['bank_card']
    accountbanks = Accountbank.objects.filter(owneraccount_cid=account_cid)
    for accountbank in accountbanks:
        if accountbank.bankno[-4:] == bank_card:
            return HttpResponse("1")
    return HttpResponse("0")


def check_certificate(request):
    from SCCProgram.wsgi import thread_loop_4_user

    data = json.loads(request.body)
    user_id = str(data['user_id'])
    account_name = data['account_name']
    certificate_number = data['certificate_number']
    certificate_number_base64 = base64_encode(certificate_number)
    phone_number = data['phone_number']
    type = data['type']
    responsible_cid = data['responsible_cid']
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '待打款','打款中']
    if type == '个人':
        typeid = '0126F0000020pRJQAY'
    else:
        typeid = '0126F0000020pRFQAY'

    if responsible_cid == 'undefined':
        responsible_cid = None

    # 检查用户是否是游客
    try:
        account = Account.objects.get(certificatenumber=certificate_number_base64)
        # 检查用户是 否重复绑定
        if account.user_id:
            return HttpResponse("客户已绑定")
        # 未绑定
        elif not account.user_id:
            # 检查用户是否存量
            if Order.objects.filter(account_cid=account.crm_id, ostatus__in=valid_status):
                # 是存量，返回CRMID，之后判断手机号或银行卡后进行合并
                result = {
                    "crm_id": account.crm_id
                }
                return HttpResponse(json.dumps(result, cls=CustomEncoder4Time),
                                    content_type="application/json,charset=utf-8")
            # 潜在客户，直接将新客户与CRM潜在合并
            else:
                try:
                    new_account = Account.objects.get(user_id=user_id, certificatenumber__isnull=True)
                    delete_crm("account", new_account.crm_id)
                    new_account.delete()
                    base64_phone = account.num
                    if base64_phone:
                        base64_phone = base64_encode(phone_number)
                    Account.objects.filter(crm_id=account.crm_id).update(user_id=user_id, num=base64_phone)
                    return HttpResponse("已注册为新客户")
                # 如果没有手机号，按userid查
                except Account.MultipleObjectsReturned:
                    new_account = Account.objects.get(user_id=user_id, certificatenumber__isnull=True)
                    delete_crm("account", new_account.crm_id)
                    new_account.delete()
                    Account.objects.filter(crm_id=account.crm_id).update(user_id=user_id)
                    return HttpResponse("已注册为新客户")


    # 用户以游客身份登录
    except Account.DoesNotExist:
        # 维护add_user新建的account
        new_account = Account.objects.filter(num=base64_encode(phone_number)).first()
        Account.objects.filter(num=base64_encode(phone_number)).update(recordtypeid=typeid,
                                                                       certificatenumber=base64_encode(
                                                                           certificate_number),
                                                                       name=account_name,
                                                                       num=base64_encode(
                                                                           phone_number),
                                                                       paperstype='身份证', )

        # 异步执行CRM同步
        url = CRM_URLS['ACCOUNT']
        crm_data = {
            'PhoneSecret__c': base64_encode(phone_number),
            'Num__c': phone_number,
            'CertificateNumber__c': certificate_number,
            'Name': account_name,
            'LastName__c': account_name,
            'PapersType__c': '身份证',
            'Responsible_1__c': responsible_cid,
            'WechatUser__c': user_id,
            'Id': new_account.crm_id,
        }
        # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
        future = asyncio.run_coroutine_threadsafe(sync_account(url, json.dumps(crm_data), new_account.id),
                                                  thread_loop_4_user)
        # response = future.result()
        return HttpResponse("已注册为新客户")


def check_certificate_old(request):
    from SCCProgram.wsgi import thread_loop_4_user

    data = json.loads(request.body)
    user_id = str(data['user_id'])
    account_name = data['account_name']
    certificate_number = data['certificate_number']
    certificate_number_base64 = base64_encode(certificate_number)
    phone_number = data['phone_number']
    type = data['type']
    responsible_cid = data['responsible_cid']
    valid_status = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '预约成功', '打款中','待打款']
    if type == '个人':
        typeid = '0126F0000020pRJQAY'
    else:
        typeid = '0126F0000020pRFQAY'

    if responsible_cid == 'undefined':
        responsible_cid = None

    # 检查用户是否是游客
    try:
        account = Account.objects.get(certificatenumber=certificate_number_base64)
        # 检查用户是否重复绑定
        if account.user_id:
            return HttpResponse("客户已绑定")
        # 未绑定
        elif not account.user_id:
            # 检查用户是否存量
            if Order.objects.filter(account_cid=account.crm_id, ostatus__in=valid_status):
                result = {
                    "crm_id": account.crm_id
                }
                # 如果存量，将新客户与存量合并
                # Account.objects.filter(certificatenumber=certificate_number_base64).update(user_id=user_id)
                # Account.objects.filter(certificatenumber__isnull=True, num=base64_encode(phone_number)).delete()
                return HttpResponse(json.dumps(result, cls=CustomEncoder4Time),
                                    content_type="application/json,charset=utf-8")
            # 潜在客户，写入add_user新建的account
            else:
                new_account = Account.objects.filter(num=base64_encode(phone_number)).first()
                Account.objects.filter(num=base64_encode(phone_number)).update(recordtypeid=typeid,
                                                                               certificatenumber=base64_encode(
                                                                                   certificate_number),
                                                                               name=account_name,
                                                                               num=base64_encode(
                                                                                   phone_number),
                                                                               paperstype='身份证', )

                # 异步执行CRM同步
                url = CRM_URLS['ACCOUNT']
                crm_data = {
                    'PhoneSecret__c': base64_encode(phone_number),
                    'Num__c': phone_number,
                    'CertificateNumber__c': certificate_number,
                    'Name': account_name,
                    'LastName__c': account_name,
                    'PapersType__c': '身份证',
                    'Responsible_1__c': responsible_cid,
                    'WechatUser__c': user_id,
                }
                # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
                future = asyncio.run_coroutine_threadsafe(sync_account(url, json.dumps(crm_data), new_account.id),
                                                          thread_loop_4_user)
                # response = future.result()
                return HttpResponse("已注册为新客户")
    # 用户以游客身份登录
    except Account.DoesNotExist:
        # 维护add_user新建的account
        new_account = Account.objects.filter(num=base64_encode(phone_number)).first()
        Account.objects.filter(num=base64_encode(phone_number)).update(recordtypeid=typeid,
                                                                       certificatenumber=base64_encode(
                                                                           certificate_number),
                                                                       name=account_name,
                                                                       num=base64_encode(
                                                                           phone_number),
                                                                       paperstype='身份证', )

        # 异步执行CRM同步
        url = CRM_URLS['ACCOUNT']
        crm_data = {
            'PhoneSecret__c': base64_encode(phone_number),
            'Num__c': phone_number,
            'CertificateNumber__c': certificate_number,
            'Name': account_name,
            'LastName__c': account_name,
            'PapersType__c': '身份证',
            'Responsible_1__c': responsible_cid,
            'WechatUser__c': user_id,
        }
        # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
        future = asyncio.run_coroutine_threadsafe(sync_account(url, json.dumps(crm_data), new_account.id),
                                                  thread_loop_4_user)
        # response = future.result()
        return HttpResponse("已注册为新客户")


# 获取用户手机
def get_user_info(request):
    data = json.loads(request.body)
    userid = data['userid']
    try:
        user = User.objects.get(id=userid)
        return HttpResponse(user.password)
    except User.DoesNotExist:
        return HttpResponse("用户信息获取失败")


def check_responsible(request):
    data = json.loads(request.body)
    account_cid = data['crm_id']
    responsible_cid = data['responsible_cid']
    try:
        account = Account.objects.get(crm_id=account_cid)
        if account.responsible_id == responsible_cid:
            pass
        else:
            # user表的修改
            pass
        return HttpResponse(account.responsible_id)
    except Account.DoesNotExist:
        return HttpResponse("0")


def bind_useraccount(request):
    data = json.loads(request.body)
    account_cid = data['crm_id']
    user_id = data['user_id']
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return CustomResponse("查询失败")

    # 合并存量
    # 获取存量的手机号
    account = Account.objects.filter(crm_id=account_cid).first()
    num = account.num
    # 删除add_user的新account
    try:
        old_account = Account.objects.get(user_id=user_id, certificatenumber__isnull=True)
        delete_crm("account", old_account.crm_id)
        old_account.delete()
        Account.objects.filter(crm_id=account_cid).update(user_id=user_id)
        return HttpResponse("1")
    # 如果没有手机号，按userid查
    except Account.MultipleObjectsReturned:
        old_account = Account.objects.get(user_id=user_id, certificatenumber__isnull=True)
        delete_crm("account", old_account.crm_id)
        old_account.delete()
        Account.objects.filter(crm_id=account_cid).update(user_id=user_id)
        return HttpResponse("1")
    # 如果查不到存量！
    except Account.DoesNotExist:
        return CustomResponse("0")


def get_responsible_by_userid(request):
    data = json.loads(request.body)
    user_id = data['user_id']
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return HttpResponse("用户未找到")

    # 优先查Account
    try:
        account = Account.objects.get(user_id=user_id)
        responsible_cid = account.responsible_id
        if not responsible_cid:
            return HttpResponse("未找到投资顾问")
    except Account.DoesNotExist:
        responsible_cid = user.responsible_cid
        if not responsible_cid:
            return HttpResponse("未找到投资顾问")

    try:
        responsible = Responsible.objects.get(crm_id=responsible_cid)
        phone = responsible.phone
        responsible_info = ({'name': responsible.name, 'phone': base64_decode(phone), 'crm_id': responsible.crm_id})
        return HttpResponse(json.dumps(responsible_info), content_type="application/json,charset=utf-8")
    except Responsible.DoesNotExist:
        return HttpResponse("未找到投资顾问")


def update_user_responsible(request):
    from SCCProgram.wsgi import thread_loop_4_user

    data = json.loads(request.body)
    user_id = data['user_id']
    responsible_cid = data['responsible_cid']
    try:
        user = User.objects.get(id=user_id)
        if not user.responsible_cid:
            user.responsible_cid = responsible_cid
            user.save()
        # 对Account表中的responsible也进行更新
        try:
            account = Account.objects.get(user_id=user_id)
            if not account.responsible_id:
                account.responsible_id = responsible_cid
                account.save()
                # 异步执行CRM同步
                url = CRM_URLS['ACCOUNT']
                crm_data = {
                    'Id': account.crm_id,
                    'Responsible_1__c': responsible_cid,
                }
                # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
                future = asyncio.run_coroutine_threadsafe(sync_standard(url, json.dumps(crm_data)), thread_loop_4_user)
                # response = future.result()
        except Account.DoesNotExist:
            pass
        return HttpResponse("1")
    except User.DoesNotExist:
        return HttpResponse("用户不存在")


# 外部API接口-身份认证
def idcard_audit(request):
    data = json.loads(request.body)
    idcard = data['idcard']
    name = data['name']
    if idcard is None:
        return HttpResponseBadRequest('身份证号为必填项')
    if name is None:
        return HttpResponseBadRequest('姓名为必填项')
    if not data:
        return HttpResponseBadRequest('Request Error')
    host = 'https://idcard3.market.alicloudapi.com'
    path = '/idcardAudit'
    appcode = '64f2bbc330be4133839afce2a62f2eb2'
    querys = f'idcard={idcard}&name={name}'
    url = host + path + '?' + querys

    header = {'Authorization': 'APPCODE ' + appcode}
    response = requests.get(url, headers=header)
    status = response.json().get('showapi_res_body', '')
    if status['msg'] == '匹配':
        return HttpResponse("匹配成功")
    else:
        return HttpResponse(status['msg'])


# 外部API接口-企业认证
def check_enterprise(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    host = 'http://qysys.market.alicloudapi.com'
    path = '/communication/personal/9159'
    appcode = '64f2bbc330be4133839afce2a62f2eb2'
    url = host + path

    header = {'Authorization': 'APPCODE ' + appcode}
    response = requests.post(url, headers=header, data=data)
    code = response.json().get('code')
    message = response.json().get('message')
    if code == '1000':
        return HttpResponse("匹配成功")
    else:
        return HttpResponse(message)


# 微信-生成二维码
def get_QRCode(request):
    access_token = get_basic_access_token()
    url = f'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token={access_token}'
    response = requests.request('POST', url, data=request.body)
    return HttpResponse(response)


# 获取名片信息
def get_business_card_info(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    if not data:
        return HttpResponseBadRequest('Request Error')
    products = []
    try:
        responsible = Responsible.objects.get(crm_id=responsible_cid)
        res_products = Responsibleproduct.objects.filter(responsible=responsible)
        for res_product in res_products:
            one_year_increase = 0.0
            if res_product.product.recordtype == "二级市场":
                networth = Networth.objects.filter(product_cid=res_product.product.crm_id).order_by('updatedate')
                # 获取涨幅/收益率
                if networth:
                    one_year_increase = calculate_one_year_increase(networth)
            products.append({
                "product": res_product.product.productabbreviation,
                "product_name": res_product.product.name,
                "product_id": res_product.product.id,
                "product_cid": res_product.product.crm_id,
                "product_type": res_product.product.recordtype,
                "comparison": res_product.product.comparison,
                "comparisonabbreviation": res_product.product.comparisonabbreviation,
                "direction": res_product.product.direction,
                "one_year_increase": one_year_increase,
            })
        results = {
            "name": responsible.name,
            "phone": base64_decode(responsible.phone),
            "photo": responsible.photo,
            "identity": base64_decode(responsible.identity),
            "company": base64_decode(responsible.company),
            "address": base64_decode(responsible.address),
            "companyprofile": base64_decode(responsible.companyprofile),
            "selfprofile": base64_decode(responsible.selfprofile),
            "shareremark": base64_decode(responsible.shareremark),
            "email": responsible.email,
            "products": products,
        }
        return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")
    except Responsible.DoesNotExist:
        return CustomResponse("投顾不存在:" + responsible_cid)


# 更新名片信息
def update_business_card_info(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    phone = data['phone']
    # 暂不支持照片
    identity = data['identity']
    company = data['company']
    address = data['address']
    companyprofile = data['companyprofile']
    selfprofile = data['selfprofile']
    shareremark = data['shareremark']
    email = data['email']
    if not data:
        return HttpResponseBadRequest('Request Error')
    Responsible.objects.filter(crm_id=responsible_cid).update(phone=base64_encode(phone),
                                                              identity=base64_encode(identity),
                                                              company=base64_encode(company),
                                                              address=base64_encode(address),
                                                              companyprofile=base64_encode(companyprofile),
                                                              selfprofile=base64_encode(selfprofile),
                                                              shareremark=base64_encode(shareremark),
                                                              email=email)
    return HttpResponse('名片编辑成功')


# 更新名片产品
def update_business_card_product(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    product_ids = data['product_ids']
    if not data:
        return HttpResponseBadRequest('Request Error')
    try:
        responsible = Responsible.objects.get(crm_id=responsible_cid)
        res_products = Responsibleproduct.objects.filter(responsible=responsible)
        # 删除数据库中多余产品
        for res_p in res_products:
            if res_p.id not in product_ids:
                res_p.delete()
        # 添加新产品
        for product_id in product_ids:
            try:
                p = Productofficial.objects.get(id=product_id)
                if p not in res_products:
                    Responsibleproduct.objects.create(responsible=responsible, product=p)
            except Productofficial.DoesNotExist:
                return CustomResponse("产品不存在:" + product_id)
        return HttpResponse("名片产品添加成功")
    except Responsible.DoesNotExist:
        return CustomResponse("投顾不存在:" + responsible_cid)


# 授权后记录用户额外信息
def save_user_info(request):
    data = json.loads(request.body)
    user_id = data['user_id']
    gender = data['gender']
    country = data['country']
    province = data['province']
    city = data['city']
    today = datetime.datetime.now()
    try:
        user = User.objects.get(id=user_id)
        user.gender = gender
        user.country = country
        user.province = province
        user.city = city
        user.createtime = today
        user.save()
        return HttpResponse('User信息保存成功')
    except User.DoesNotExist:
        return HttpResponse('User不存在')


def ResponseCodeMesData(code, msg, data):
    result = {'code': code,
              'msg': msg,
              "data": data
              }
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


# 投顾端  实名认证
def realNameAuthentication_res(request):
    data = json.loads(request.body)
    name = data['name']
    req_account_id = data['account_id']
    responsible_cid = data['responsible_cid']
    certificatenumber = data['certificatenumber']
    if req_account_id == '':
        return ResponseCodeMesData(400, "用户id不能为空", "")
    if certificatenumber == '':
        return ResponseCodeMesData(400, "身份证为必填项", "")
    if name == '':
        return ResponseCodeMesData(400, "名称为必填项", "")
    if responsible_cid == '':
        return ResponseCodeMesData(400, "投顾ID不能为空", "")
    idtype = "身份证"
    certificatenumber_base64 = base64_encode(certificatenumber)
    # 已存在证件信息
    account_count = Account.objects.filter(certificatenumber=certificatenumber_base64)
    old_account_count = Account.objects.get(id=req_account_id)
    crm_data = {
        'Name': name,
        'LastName__c': name,
        'CertificateNumber__c': certificatenumber,
        'Responsible_1__c': responsible_cid
    }
    reback_id = old_account_count.id
    isDelete = 0
    # account表存在多条数据
    if len(account_count) > 1:
        return ResponseCodeMesData(400, "服务器繁忙,请联系投顾处理！", "")
    elif len(account_count) == 1:  # 证件信息已有投顾关联
        account_sfz = account_count[0]
        userId = old_account_count.user_id
        if not userId:
            userId = account_sfz.user_id
        reback_id = account_sfz.id
        # 检查该证件信息是否有投顾
        if account_sfz.responsible_id:
            # 匹配投顾信息
            res = Responsible.objects.filter(crm_id=account_sfz.responsible_id)
            # 存在一个投顾
            length = len(res)
            if length == 1:
                if responsible_cid == account_sfz.responsible_id:
                    # 投顾是自己 合并两条数据 userid写入存在身份证那一条
                    crm_data.update({"id": account_sfz.crm_id,
                                     'RemarkName__c': base64_encode(old_account_count.remarkname)})
                    Account.objects.filter(id=account_sfz.id).update(name=name, paperstype=idtype,
                                                                     remarkname=old_account_count.remarkname,
                                                                     remark=old_account_count.remark,
                                                                     res_phone=old_account_count.res_phone,
                                                                     birthday=old_account_count.birthday,
                                                                     frxm=old_account_count.frxm,
                                                                     sex=old_account_count.sex,
                                                                     user_id=userId)
                    delete_crm("account", old_account_count.crm_id)
                    old_account_count.delete()
                    isDelete = 1
                    # return ResponseCodeMesData(400, "该客户已存在您的名下，请直接搜索该客户真实姓名进行预约","")
                else:
                    return ResponseCodeMesData(400, "该证件号已绑定投顾", "")
            elif length > 1:  # 一个投顾id匹配到多条投顾数据
                return ResponseCodeMesData(400, "存在多个投顾信息", "")
            else:
                crm_data.update({"id": account_sfz.crm_id,
                                 'RemarkName__c': base64_encode(old_account_count.remarkname)})
                Account.objects.filter(id=account_sfz.id).update(name=name, paperstype=idtype,
                                                                 certificatenumber=certificatenumber_base64,
                                                                 responsible_id=responsible_cid,
                                                                 remarkname=old_account_count.remarkname,
                                                                 remark=old_account_count.remark,
                                                                 res_phone=old_account_count.res_phone,
                                                                 birthday=old_account_count.birthday,
                                                                 frxm=old_account_count.frxm,
                                                                 sex=old_account_count.sex,
                                                                 user_id=userId)
                delete_crm("account", old_account_count.crm_id)
                old_account_count.delete()
                isDelete = 1
        else:
            crm_data.update({"id": account_sfz.crm_id, 'RemarkName__c': base64_encode(old_account_count.remarkname)})
            Account.objects.filter(id=account_sfz.id).update(name=name, paperstype=idtype,
                                                             certificatenumber=certificatenumber_base64,
                                                             responsible_id=responsible_cid,
                                                             remarkname=old_account_count.remarkname,
                                                             remark=old_account_count.remark,
                                                             res_phone=old_account_count.res_phone,
                                                             birthday=old_account_count.birthday,
                                                             frxm=old_account_count.frxm,
                                                             sex=old_account_count.sex,
                                                             user_id=userId)
            delete_crm("account", old_account_count.crm_id)
            old_account_count.delete()
            isDelete = 1
    else:
        crm_data.update({"id": old_account_count.crm_id, 'RemarkName__c': base64_encode(old_account_count.remarkname)})
        Account.objects.filter(id=old_account_count.id).update(name=name, paperstype=idtype,
                                                               certificatenumber=certificatenumber_base64,
                                                               responsible_id=responsible_cid)

    remote_crm(crm_data, reback_id)
    return ResponseCodeMesData(200, "信息完善成功", {"account_id": reback_id, "name": name,"isDelete":isDelete})


def edit_account(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    responsible_cid = data['responsible_cid']
    if responsible_cid == '':
        CustomResponse('投顾ID不能为空')
    remarkname = data['remarkname']
    remark = data['remark']
    name = data['name']
    salutation = data['salutation']
    record_type = data['record_type']
    recordtypeid = '0126F0000020pRJQAY'
    if record_type == '机构':
        recordtypeid = '0126F0000020pRFQAY'
    paperstype = data['paperstype']
    phone_number = str(data['resphone'])
    certificatenumber = data['certificatenumber']
    birthday = data['birthday']
    isReamName = data['isReamName']
    sex = data['sex']
    if sex == '':
        sex = 0
    frxm = data['frxm']
    certificatenumber_base64 = ""
    if certificatenumber != "":
        certificatenumber_base64 = base64_encode(certificatenumber)
    elif certificatenumber == "" and isReamName == "true":
        return ResponseCodeMesData(400, "身份证不能为空", "")
    crm_data = {
        'PhoneSecret__c': base64_encode(phone_number),
        'Name': name,
        'CertificateNumber__c': certificatenumber,
        'PapersType__c': paperstype,
        'LastName__c': name,
        'FirstName__c': "",
        'Responsible_1__c': responsible_cid,
        'Remark__c': base64_encode(remark),
        'recordtypeid': recordtypeid
    }
    # 已存在证件信息
    tg_cus_account = Account.objects.get(id=account_id)
    new_account_id = tg_cus_account.id
    if isReamName == "true":
        crm_data.update({"id": tg_cus_account.crm_id,
                         'RemarkName__c': base64_encode(remarkname)})
        # 覆盖无效投顾 覆盖规则-> 新值替换旧值
        updateAccount(new_account_id, remarkname, remark, name, responsible_cid, salutation, recordtypeid,
                      paperstype, certificatenumber_base64, sex, phone_number, birthday, frxm)
    elif isReamName == "false":
        if certificatenumber:
            account_count = Account.objects.filter(certificatenumber=certificatenumber_base64)
            # account表存在多条数据
            if len(account_count) > 1:
                return ResponseCodeMesData(400, "存在多条客户信息！", "")
            elif len(account_count) == 1:  # 证件信息已有投顾关联
                account_count = account_count[0]
                crm_data.update({"id": account_count.crm_id,
                                 'RemarkName__c': base64_encode(remarkname)})
                userId = tg_cus_account.user_id
                if not userId:
                    userId = account_count.user_id
                new_account_id = account_count.id
                # 检查该证件信息是否有投顾
                account_resId = account_count.responsible_id
                # 判断投顾ID是否为空
                if account_resId:
                    # 匹配投顾信息
                    res = Responsible.objects.filter(crm_id=account_resId)
                    # 存在一个投顾
                    length = len(res)
                    if length == 1:
                        # 投顾是本人
                        if responsible_cid == account_resId:
                            # 投顾是自己 合并两条数据 userid写入存在身份证那一条
                            Account.objects.filter(id=new_account_id).update(remarkname=base64_encode(remarkname),
                                                                             remark=base64_encode(remark),
                                                                             name=name,
                                                                             res_phone=base64_encode(phone_number),
                                                                             responsible_id=responsible_cid,
                                                                             recordtypeid=recordtypeid,
                                                                             paperstype=paperstype,
                                                                             certificatenumber=certificatenumber_base64,
                                                                             sex=sex,
                                                                             birthday=birthday,
                                                                             frxm=frxm,
                                                                             user_id=userId)
                            delete_crm("account", tg_cus_account.crm_id)
                            tg_cus_account.delete()
                        else:
                            return ResponseCodeMesData(400, "该证件号已绑定投顾", "")
                    elif length > 1:  # 一个投顾id匹配到多条投顾数据
                        return ResponseCodeMesData(400, "存在多个投顾信息", "")
                    else:
                        Account.objects.filter(id=new_account_id).update(remarkname=base64_encode(remarkname),
                                                                         remark=base64_encode(remark),
                                                                         name=name,
                                                                         res_phone=base64_encode(phone_number),
                                                                         responsible_id=responsible_cid,
                                                                         recordtypeid=recordtypeid,
                                                                         paperstype=paperstype,
                                                                         certificatenumber=certificatenumber_base64,
                                                                         sex=sex,
                                                                         birthday=birthday,
                                                                         frxm=frxm,
                                                                         user_id=userId)
                        delete_crm("account", tg_cus_account.crm_id)
                        tg_cus_account.delete()

                else:  # 投顾id为空 直接关联
                    Account.objects.filter(id=new_account_id).update(remarkname=base64_encode(remarkname),
                                                                     remark=base64_encode(remark),
                                                                     name=name,
                                                                     res_phone=base64_encode(phone_number),
                                                                     responsible_id=responsible_cid,
                                                                     recordtypeid=recordtypeid,
                                                                     paperstype=paperstype,
                                                                     certificatenumber=certificatenumber_base64,
                                                                     sex=sex,
                                                                     birthday=birthday,
                                                                     frxm=frxm,
                                                                     user_id=userId)
                    delete_crm("account", tg_cus_account.crm_id)
                    tg_cus_account.delete()
            else:  # account表不存在用户数据 新增
                crm_data.update({"id": tg_cus_account.crm_id,
                                 'RemarkName__c': base64_encode(remarkname)})
                updateAccount(tg_cus_account.id, remarkname, remark, name, responsible_cid, salutation, recordtypeid,
                              paperstype, certificatenumber_base64, sex, phone_number, birthday, frxm)
        else:
            crm_data.update({"id": tg_cus_account.crm_id,
                             'RemarkName__c': base64_encode(remarkname)})
            updateAccount(tg_cus_account.id, remarkname, remark, name, responsible_cid, salutation, recordtypeid,
                          paperstype, certificatenumber_base64, sex, phone_number, birthday, frxm)
    remote_crm(crm_data, new_account_id)
    return ResponseCodeMesData(200, "修改成功", {"account_id": new_account_id})


# 远程调用CRM接口更新 CRM Account数据
def remote_crm(crm_data, new_account_id):
    from SCCProgram.wsgi import thread_loop_4_user

    # CRM同步必须写入Name
    # 异步执行CRM同步
    url = CRM_URLS['ACCOUNT']
    # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
    future = asyncio.run_coroutine_threadsafe(sync_account(url, json.dumps(crm_data), new_account_id),
                                              thread_loop_4_user)
    # response = future.result()


# type
def add_account(request):
    data = json.loads(request.body)
    responsible_cid = data['responsible_cid']
    if responsible_cid == '':
        CustomResponse('投顾ID不能为空')
    remarkname = data['remarkname']
    remark = data['remark']
    name = data['name']
    salutation = data['salutation']
    record_type = data['record_type']
    recordtypeid = '0126F0000020pRJQAY'
    if record_type == '机构':
        recordtypeid = '0126F0000020pRFQAY'
    paperstype = data['paperstype']
    phone_number = str(data['phone_number'])
    certificatenumber = data['certificatenumber']
    birthday = data['birthday']
    sex = data['sex']
    if sex == '':
        sex = 0
    frxm = data['frxm']
    certificatenumber_base64 = ""
    if certificatenumber != '':
        certificatenumber_base64 = base64_encode(certificatenumber)
    new_account_id = ""
    if name == '' or name is None:
        name = remarkname
    phoneNumber = ""
    if phone_number:
        phoneNumber = base64_encode(phone_number)
    # remarkname_c =""
    # if remarkname:
    #     remarkname_c = base64_encode(remarkname)
    # remark_c =""
    # if remark:
    #     remark_c = base64_encode(remark)

    crm_data = {
        'PhoneSecret__c': phoneNumber,
        'Name': name,
        'CertificateNumber__c': certificatenumber,
        'PapersType__c': paperstype,
        'LastName__c': name,
        'FirstName__c': "",
        'Responsible_1__c': responsible_cid,
        'RemarkName__c': remarkname,
        'Remark__c': remark,
        'recordtypeid': recordtypeid
    }
    if certificatenumber:
        # 已存在证件信息
        account_count = Account.objects.filter(certificatenumber=certificatenumber_base64)
        # account表存在多条数据
        if len(account_count) > 1:
            return ResponseCodeMesData(400, "服务器繁忙,请联系投顾处理！", "")
        elif len(account_count) == 1:  # 证件信息已有投顾关联
            account_count = account_count[0]
            new_account_id = account_count.id
            # 检查该证件信息是否有投顾
            account_resId = account_count.responsible_id
            # 判断投顾ID是否为空
            crm_data.update({"id": account_count.crm_id})
            if account_resId:
                # 匹配投顾信息
                res = Responsible.objects.filter(crm_id=account_resId)
                # 存在一个投顾
                length = len(res)
                if length == 1:
                    # 投顾是本人
                    if responsible_cid == account_resId:
                        return ResponseCodeMesData(400, "该客户已存在您的名下", "")
                    else:
                        return ResponseCodeMesData(400, "该证件号已绑定投顾", "")
                elif length > 1:  # 一个投顾id匹配到多条投顾数据
                    return ResponseCodeMesData(400, "存在多个投顾信息", "")
                else:
                    # 覆盖无效投顾 覆盖规则-> 新值替换旧值
                    updateAccount(new_account_id, remarkname, remark, name, responsible_cid, salutation, recordtypeid,
                                  paperstype, certificatenumber_base64, sex, phone_number, birthday, frxm)
            else:  # 投顾id为空 直接关联
                updateAccount(new_account_id, remarkname, remark, name, responsible_cid, salutation, recordtypeid,
                              paperstype, certificatenumber_base64, sex, phone_number, birthday, frxm)
        else:  # account表不存在用户数据 新增
            new_account_id = createAccount(remarkname, remark, name, phone_number, responsible_cid, salutation,
                                           recordtypeid, paperstype, certificatenumber_base64, sex, birthday, frxm)
    else:
        new_account_id = createAccount(remarkname, remark, name, phone_number, responsible_cid, salutation,
                                       recordtypeid, paperstype, certificatenumber_base64, sex, birthday, frxm)
    remote_crm(crm_data, new_account_id)
    result = {
        "account_id": new_account_id
    }
    return HttpResponse(json.dumps(result, cls=CustomEncoder4Time), content_type="application/json,charset=utf-8")


# 更新Account
def updateAccount(account_count_id, remarkname, remark, name, responsible_cid, salutation, recordtypeid, paperstype,
                  certificatenumber_base64, sex, phone_number, birthday, frxm):
    Account.objects.filter(id=account_count_id).update(remarkname=base64_encode(remarkname),
                                                       remark=base64_encode(remark),
                                                       name=name,
                                                       res_phone=base64_encode(phone_number),
                                                       responsible_id=responsible_cid,
                                                       recordtypeid=recordtypeid,
                                                       paperstype=paperstype,
                                                       certificatenumber=certificatenumber_base64,
                                                       sex=sex,
                                                       birthday=birthday,
                                                       frxm=frxm)


# 创建Account
def createAccount(remarkname, remark, name, phone_number, responsible_cid, salutation, recordtypeid, paperstype,
                  certificatenumber_base64, sex, birthday, frxm):
    new_account = Account.objects.create(
        remarkname=base64_encode(remarkname),
        remark=base64_encode(remark),
        name=name,
        createddate=datetime.datetime.now(),
        responsible_id=responsible_cid,
        salutation=salutation,
        recordtypeid=recordtypeid,
        paperstype=paperstype,
        certificatenumber=certificatenumber_base64,
        sex=sex,
        res_phone=base64_encode(phone_number),
        birthday=birthday,
        frxm=frxm
    )
    return new_account.id


def update_account(request):
    data = json.loads(request.body)
    account_id = data['account_id']
    remarkname = data['remarkname']
    remark = data['remark']
    name = data['name']
    salutation = data['salutation']
    paperstype = data['paperstype']
    phone_number = str(data['phone_number'])
    certificatenumber = data['certificatenumber']
    branch = data['branch']
    certificatenumber_base64 = base64_encode(certificatenumber)
    try:
        account = Account.objects.get(id=account_id)
    except Account.DoesNotExist:
        return HttpResponse("客户不存在")
    if account.certificatenumber is None:
        account.certificatenumber = certificatenumber_base64
        account.name = name
    account.remarkname = base64_encode(remarkname)
    account.remark = base64_encode(remark)
    account.num = base64_encode(phone_number)
    account.salutation = salutation
    account.paperstype = paperstype
    account.branch = branch
    account.save()

    from SCCProgram.wsgi import thread_loop_4_user

    # CRM同步必须写入Name
    # 异步执行CRM同步
    url = CRM_URLS['ACCOUNT']
    crm_data = {
        'PhoneSecret__c': base64_encode(phone_number),
        'Name': name,
        'FirstName__c': "",
        'CertificateNumber__c': certificatenumber,
        'PapersType__c': paperstype,
        'CreatedDate__c': str(datetime.datetime.now().date()),
        'LastName__c': name,
        'RemarkName__c': base64_encode(remarkname),
        'Remark__c': base64_encode(remark),
        'Id': account.crm_id
    }
    # 产生coroutine任务sync_account交给子线程thread_loop_4_user执行
    future = asyncio.run_coroutine_threadsafe(sync_account(url, json.dumps(crm_data), account.id),
                                              thread_loop_4_user)
    # response = future.result()
    result = {
        "account_id": account.id
    }
    return HttpResponse(json.dumps(result, cls=CustomEncoder4Time), content_type="application/json,charset=utf-8")


# 投顾更换 - 投顾更换
def change_responsible(request):
    data = json.loads(request.body)
    userId = data["user_id"]
    responsible_id = data["responsible_id"]
    if userId == '':
        return ResponseCodeMesData(400, "用户id不能为空", "")
    if responsible_id == '':
        return ResponseCodeMesData(400, "投顾id不能为空", "")
    try:
        #account 中包含老投顾
        account = Account.objects.get(user_id=userId)
        #判断是否实名
        if not account.certificatenumber:
            return ResponseCodeMesData(400, "客户未实名", "")
        #新投顾
        responsible = Responsible.objects.filter(id=responsible_id)
        #确保Code能够找到对应投顾
        if len(responsible) > 1:
            return ResponseCodeMesData(400, "存在多个投顾", "")
        if len(responsible) == 0:
            return ResponseCodeMesData(400, "投顾不存在", "")
        else:
            responsible = responsible[0]
            old_responsible_cid = account.responsible_id
            new_responsible_cid = responsible.crm_id
            #更换投顾
            Account.objects.filter(user_id=userId).update(responsible_id=responsible.crm_id)
            #同步刷新订单归属 根据accountID去找所有归属于该用户的订单 并把dependentsResCrmId 更新为最新所属投顾
            Order.objects.filter(account_cid=account.crm_id).update(dependentsrescrmid=new_responsible_cid)
            #更换投顾 同步至CRM
            crm_data = {
                'Responsible_1__c': responsible.crm_id,
                'Id': account.crm_id
            }
            remote_crm(crm_data, account.crm_id)
            return ResponseCodeMesData(200, "操作成功", {"responsible_id": responsible.id})
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "用户不存在", "")

#投顾查询
def query_responsible_info(request):
    data = json.loads(request.body)
    responsibleCode = data["responsibleCode"]
    if not responsibleCode:
        return ResponseCodeMesData(400, "投顾编码不能为空", "")
    try:
        responsible = Responsible.objects.get(recommendcode=responsibleCode)
        identity = ""
        if responsible.identity:
            identity = base64_decode(responsible.identity)
        result = {
            "responsibleName": responsible.name,
            "responsiblePhone": base64_decode(responsible.phone),
            "responsibleCompany": base64_decode(responsible.company),
            "responsibleEmail": responsible.email,
            "responsibleAddress": base64_decode(responsible.address),
            "responsibleSelfprofile": base64_decode(responsible.selfprofile),
            "responsibleCrmid": responsible.crm_id,
            "identity": identity,
            "responsibleId": responsible.id
        }
        return ResponseCodeMesData(200, "操作成功", result)
    except Responsible.DoesNotExist:
        return ResponseCodeMesData(400, "投顾不存在", "")

#
def get_my_responsible_info(request):
    data = json.loads(request.body)
    userId = data["user_id"]
    if not userId:
        return ResponseCodeMesData(400, "用户id不能为空", "")
    try:
        account = Account.objects.get(user_id=userId)
        try:
            responsible = Responsible.objects.get(crm_id=account.responsible_id)
            identity = ""
            if responsible.identity:
                identity = base64_decode(responsible.identity)
            result = {
                "responsibleName": responsible.name,
                "responsiblePhone": base64_decode(responsible.phone),
                "responsibleCompany": base64_decode(responsible.company),
                "responsibleEmail": responsible.email,
                "responsibleAddress": base64_decode(responsible.address),
                "responsibleSelfprofile":  base64_decode(responsible.selfprofile),
                "responsibleId": responsible.id,
                "identity": identity,
                "userId": userId
            }

            return ResponseCodeMesData(200, "操作成功", result)
        except Responsible.DoesNotExist:
            return ResponseCodeMesData(400, "投顾不存在", "")
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "用户不存在", "")


def fadada_info(request):
    from django.core.cache import cache
    from urllib.parse import unquote
    try:
        data = request.POST.urlencode()
        data_decode = unquote(data[11:])
        data_json = json.loads(data_decode)
        cache.set('lpd', data_json['unionId'], 200)
        return HttpResponse("0")
    except Exception as e:
        cache.set('lpd', e, 200)
        return HttpResponse("0")


def get_redis(request):
    print('sss')
    return HttpResponse(cache.get('lpd'))


def delete_crm(type, cid):
    data = {
        "type": type,
        "id": cid
    }
    url = CRM_URLS['STANDARD'] + 'delete'
    response = requests.post(url, data=json.dumps(data))
    return response
