from django.urls import path

from . import views

app_name = 'Users'
urlpatterns = [

    # 登录注册模块
    path('check_user/', views.check_user, name='check_user'),
    path('get_my_id/', views.get_my_id, name='get_my_id'),

    path('customer_login/', views.customer_login, name='customer_login'),
    path('customer_login_by_password/', views.customer_login_by_password, name='customer_login_by_password'),
    path('customer_login_by_sms/', views.customer_login_by_sms, name='customer_login_by_sms'),
    path('responsible_login/', views.responsible_login, name='responsible_login'),
    path('channelManager_login/', views.channelManager_login, name='channelManager_login'),
    path('phone_update/', views.phone_update, name='phone_update'),
    # 微信验证
    path('silence_get_openid/', views.silence_get_openid, name='silence_get_openid'),
    path('get_user_access_token/', views.get_user_access_token, name='get_user_access_token'),
    path('get_user_info/', views.get_user_info, name='get_user_info'),

    # 风险评估
    path('risk_eval/', views.risk_eval, name='risk_eval'),
    path('save_risk_eval_info/', views.save_risk_eval_info, name='save_risk_eval_info'),
    path('get_risk_eval_info/', views.get_risk_eval_info, name='get_risk_eval_info'),
    path('get_risk_eval/', views.get_risk_eval, name='get_risk_eval'),

    # 各角色的crud
    path('add_customer/', views.add_customer, name='add_customer'),
    path('delete_customer/', views.delete_customer, name='delete_customer'),
    path('detail_customer/', views.detail_customer, name='detail_customer'),
    path('update_customer/', views.update_customer, name='update_customer'),

    path('add_responsible/', views.add_responsible, name='add_responsible'),
    path('delete_responsible/', views.delete_responsible, name='delete_responsible'),
    path('detail_responsible/', views.detail_responsible, name='detail_responsible'),
    path('update_responsible/', views.update_responsible, name='update_responsible'),

    path('detail_channelManager/', views.detail_channelManager, name='detail_channelManager'),
    path('update_channelManager/', views.update_channelManager, name='update_channelManager'),
    # 个人 信息页面
    path('get_my_bank/', views.get_my_bank, name='get_my_bank'),
    path('update_my_bank/', views.update_my_bank, name='update_my_bank'),
    path('change_password_by_responsible/', views.change_password_by_responsible,
         name='change_password_by_responsible'),
    path('change_password_by_channel/', views.change_password_by_channel, name='change_password_by_channel'),
    path('get_my_avatar/', views.get_my_avatar, name='get_my_avatar'),
    path('get_my_responsible/', views.get_my_responsible, name='get_my_responsible'),  # 这是顾客的功能

    # 客户管理（顾问功能）
    path('search_customer/', views.search_customer, name='search_customer'),
    path('add_my_account/', views.add_my_account, name='add_my_account'),
    path('get_my_account/', views.get_my_account, name='get_my_account'),
    path('search_to_add_customer/', views.search_to_add_customer, name='search_to_add_customer'),
    path('get_new_account/', views.get_new_account, name='get_new_account'),
    path('delete_my_account/', views.delete_my_account, name='delete_my_account'),
    path('get_customer_tendency/', views.get_customer_tendency, name='get_customer_tendency'),
    path('detail_visitor/', views.detail_visitor, name='detail_visitor'),
    # 顾问管理（渠道管理员功能）
    path('search_responsible/', views.search_responsible, name='search_responsible'),
    path('get_responsible_by_type/', views.get_responsible_by_type, name='get_responsible_by_type'),  # 按照地区或部门查询我管理的列表
    path('get_my_responsible_by_channel/', views.get_my_responsible_by_channel, name='get_my_responsible_by_channel'),
    # 获取我管理的顾问列表
    path('summary_responsible/', views.summary_responsible, name='summary_responsible'),
    path('delete_my_responsible/', views.delete_my_responsible, name='delete_my_responsible'),
    path('add_my_responsible/', views.add_my_responsible, name='add_my_responsible'),  # 渠道管理员加顾问
    # 未完成功能
    path('add_my_bank/', views.add_my_bank, name='add_my_bank'),
    path('add_my_bank_new/', views.add_my_bank_new, name='add_my_bank_new'),
    # 辅助函数
    path('id_2_cid/', views.id_2_cid, name='id_2_cid'),
    path('cid_2_id/', views.cid_2_id, name='cid_2_id'),
    path('crmid_2_openid/', views.crmid_2_openid, name='crmid_2_openid'),
    # 短信验证
    path('get_sms/', views.get_sms, name='get_sms'),
    path('check_sms/', views.check_sms, name='check_sms'),
    path('rmcode_2_phone/', views.rmcode_2_phone, name='rmcode_2_phone'),
    path('rm_certificatenumber_2_phone/', views.rm_certificatenumber_2_phone, name='rm_certificatenumber_2_phone'),
    path('certificatenumber_2_phone/', views.certificatenumber_2_phone, name='certificatenumber_2_phone'),
    # 留言板
    path('add_message/', views.add_message, name='add_message'),
    path('get_message/', views.get_message, name='get_message'),
    # 通信
    path('get_communication/', views.get_communication, name='get_communication'),
    path('add_communication/', views.add_communication, name='add_communication'),
    path('get_communicator_list/', views.get_communicator_list, name='get_communicator_list'),
    path('send_subscribe_communication/', views.send_subscribe_communication, name='send_subscribe_communication'),
    # 新登录
    path('add_user/', views.add_user, name='add_user'),
    path('check_telephone/', views.check_telephone, name='check_telephone'),
    path('check_bank_card/', views.check_bank_card, name='check_bank_card'),
    path('get_userphonenumber_by_wechat/', views.get_userphonenumber_by_wechat, name='get_userphonenumber_by_wechat'),
    path('check_certificate/', views.check_certificate, name='check_certificate'),
    path('get_user_info/', views.get_user_info, name='get_user_info'),
    path('check_responsible/', views.check_responsible, name='check_responsible'),
    path('bind_useraccount/', views.bind_useraccount, name='bind_useraccount'),
    path('get_responsible_by_userid/', views.get_responsible_by_userid, name='get_responsible_by_userid'),
    path('update_user_responsible/', views.update_user_responsible, name='update_user_responsible'),
    path('idcard_audit/', views.idcard_audit, name='idcard_audit'),
    path('check_enterprise/', views.check_enterprise, name='check_enterprise'),
    path('test_base64/', views.test_base64, name='test_base6get_my_account4'),
    path('check_agency/', views.check_agency, name='check_agency'),
    path('responsible_register/', views.responsible_register, name='responsible_register'),
    path('save_user_info/', views.save_user_info, name='save_user_info'),
    # 名片
    path('get_QRCode/', views.get_QRCode, name='get_QRCode'),
    path('get_business_card_info/', views.get_business_card_info, name='get_business_card_info'),
    path('update_business_card_info/', views.update_business_card_info, name='update_business_card_info'),
    path('update_business_card_product/', views.update_business_card_product, name='update_business_card_product'),
    # 法大大
    path('fadada_info/', views.fadada_info, name='fadada_info'),
    path('get_redis/', views.get_redis, name='get_redis'),
    # 投顾增加客户以及修改客户(修改不需投顾)
    path('add_account/', views.add_account, name='add_account'),
    path('update_account/', views.update_account, name='update_account'),
    path('realNameAuthentication_res/', views.realNameAuthentication_res, name='realNameAuthentication_res'),
    path('edit_account/', views.edit_account, name='edit_account'),
    # 投顾更换 - 我的投顾
    path('get_my_responsible_info/', views.get_my_responsible_info, name='get_my_responsible_info'),
    # 投顾更换 - 投顾查询
    path('query_responsible_info/', views.query_responsible_info, name='query_responsible_info'),
    # 投顾更换 - 投顾更换
    path('change_responsible/', views.change_responsible, name='change_responsible'),
    # # 资产 新接口
    # path('change_responsible/', views.change_responsible, name='change_responsible')
]
