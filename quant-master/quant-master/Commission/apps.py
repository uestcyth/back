from django.apps import AppConfig


class CommissionConfig(AppConfig):
    name = 'Commission'
