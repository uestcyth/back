from django.urls import path

from . import views

app_name = 'Commission'
urlpatterns = [
    path('get_solid_details/', views.get_solid_details, name='get_solid_details'),
    path('get_securities_details/', views.get_securities_details, name='get_securities_details'),
    path('get_list_by_recordtype/', views.get_list_by_recordtype, name='get_list_by_recordtype'),
    path('get_list_by_month/', views.get_list_by_month, name='get_list_by_month'),
    path('get_total_by_recordtype/', views.get_total_by_recordtype, name='get_total_by_recordtype'),
    path('get_total_by_month/', views.get_total_by_month, name='get_total_by_month'),
    path('calculate_all_commission/', views.calculate_all_commission, name='calculate_all_commission'),
    path('get_commission_details_by_year/', views.get_commission_details_by_year, name='get_commission_details_by_year')
]
