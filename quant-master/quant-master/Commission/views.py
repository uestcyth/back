import configparser
import datetime
import decimal
import json

from django.db.models import Q, Sum
from django.http import HttpResponse, HttpResponseBadRequest

from datamodels.models import Order, Productofficial, Account, Responsible, Commissiondetail


# 解决序列化问题
class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        super(CustomEncoder, self).default(o)


def get_solid_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    name = data['name']
    responsible_cid = data['responsible_cid']
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    order = Order.objects.get(Q(name=name) & Q(responsible_cid=responsible_cid))
    if order.ostatus not in valid_status1:
        return HttpResponse("订单数据无效")

    config = configparser.ConfigParser()
    config.read('./solid.ini', encoding='UTF-8')
    if not config:
        return HttpResponseBadRequest('Config Not Found')

    account_id = order.account_cid
    account = Account.objects.filter(crm_id=account_id).first()
    responsible = Responsible.objects.filter(crm_id=responsible_cid).first()
    product_id = order.product_cid
    product = Productofficial.objects.filter(crm_id=product_id).first()
    abbr = product.productabbreviation
    type = config[abbr]['formulaType']
    ratio = float(config[abbr]['commissionRatio'])
    result = 0
    subscription_fee = float(order.subscription_fee)
    if order.maturitydate is None:
        holdday = 0
    else:
        holdday = (order.maturitydate - order.startearningday).days
    if order.money_4 is None:
        money_4 = 0
    else:
        money_4 = float(order.money_4)
    if type == 'SA':
        result = subscription_fee * ratio
    elif type == 'SB':
        result = holdday * money_4 / 365 * ratio
    results = {
        'date': order.date,
        'product_name': product.name,
        'recordtype': product.recordtype,
        'id': order.id,
        'account_name': account.name,
        'money_4': money_4,
        'startearningday': order.startearningday,
        'maturitydate': order.maturitydate,
        'holdday': holdday,
        'commissionformula': result,
        'responsible_name': responsible.name,
    }
    Order.objects.filter(name=name).update(commissionformula=result)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_securities_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    name = data['name']
    responsible_cid = data['responsible_cid']
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    order = Order.objects.get(Q(name=name) & Q(responsible_cid=responsible_cid))
    if order.ostatus not in valid_status1:
        return HttpResponse("订单数据无效")

    config = configparser.ConfigParser()
    config.read('./s&s.ini', encoding='UTF-8')
    if not config:
        return HttpResponseBadRequest('Config Not Found')

    account_id = order.account_cid
    account = Account.objects.filter(crm_id=account_id).first()
    responsible = Responsible.objects.filter(crm_id=responsible_cid).first()
    product_id = order.product_cid
    product = Productofficial.objects.filter(crm_id=product_id).first()
    abbr = product.productabbreviation
    type = config[abbr]['formulaType']
    ratio = float(config[abbr]['commissionRatio'])
    subscription_fee = float(order.subscription_fee)
    if order.maturitydate is None:
        holdday = 0
    else:
        holdday = (order.maturitydate - order.startearningday).days
    if order.money_4 is None:
        money_4 = 0
    else:
        money_4 = float(order.money_4)

    result = calculate(type, subscription_fee, ratio, holdday, money_4)

    # 激励方案
    if config[abbr]['incentive'] == 'true':
        days = config[abbr]['holdday'].split(',')
        days = list(map(float, days))
        ratios = config[abbr]['ratio'].split(',')
        ratios = list(map(float, ratios))
        if holdday <= days[0]:
            ratio = ratios[0]
        elif holdday > days[-1]:
            ratio = ratios[-1]
        else:
            for i in range(len(days) - 1):
                if days[i] < holdday <= days[i + 1]:
                    ratio = ratios[i + 1]
        result = calculate(type, subscription_fee, ratio, holdday, money_4)

    results = {
        'date': order.date,
        'product_name': product.name,
        'recordtype': product.recordtype,
        'id': order.id,
        'account_name': account.name,
        'money_4': money_4,
        'startearningday': order.startearningday,
        'maturitydate': order.maturitydate,
        'holdday': holdday,
        'commissionformula': result,
        'responsible_name': responsible.name,
    }
    Order.objects.filter(name=name).update(commissionformula=result)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 计算佣金
def calculate(type, subscription_fee, ratio, holdday, money_4):
    result = 0
    if type == 'SA':
        result = subscription_fee * ratio
    elif type == 'SB':
        result = holdday * money_4 / 365 * ratio
    return result


def get_list_by_recordtype(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    recordtype = data['recordtype']
    responsible_cid = data['responsible_cid']
    if recordtype == "类固收":
        recordtype = "0126F0000020pRrQAI"
    elif recordtype == "二级市场":
        recordtype = "0126F0000020pRqQAI"
    elif recordtype == "股权":
        recordtype = "0126F0000020pRsQAI"
    else:
        return HttpResponse("不支持此类产品")
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    orders = Order.objects.filter(
        Q(responsible_cid=responsible_cid) & Q(recordtypeid=recordtype) & Q(commissionformula__isnull=False) & Q(
            ostatus__in=valid_status1))
    order_list = list(orders)
    commission_list = []
    total_commission = 0
    for o in order_list:
        product_id = o.product_cid
        product = Productofficial.objects.filter(crm_id=product_id).first()
        if not product:
            continue
        commission_list.append({
            'order_name': o.name,
            'product_name': product.name,
            'product_abbr': product.productabbreviation,
            'commissionformula': o.commissionformula,
            'date': o.date,
        })
        total_commission = total_commission + float(o.commissionformula)
    results = ({
        'total_commission': total_commission, 'commission_list': commission_list
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_list_by_month(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    year = data['year']
    month = data['month']
    responsible_cid = data['responsible_cid']
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    orders = Order.objects.filter(
        Q(responsible_cid=responsible_cid) & Q(date__year=year) & Q(date__month=month) & Q(
            commissionformula__isnull=False) & Q(ostatus__in=valid_status1))
    order_list = list(orders)
    commission_list = []
    total_commission = 0
    for o in order_list:
        product_id = o.product_cid
        product = Productofficial.objects.filter(crm_id=product_id).first()
        if not product:
            continue
        commission_list.append({
            'order_name': o.name,
            'product_name': product.name,
            'product_abbr': product.productabbreviation,
            'commissionformula': o.commissionformula,
            'date': o.date,
            'recordtype': product.recordtype,
        })
        total_commission = total_commission + float(o.commissionformula)
    results = ({
        'total_commission': total_commission, 'commission_list': commission_list
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_total_by_recordtype(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']

    solid_total = 0
    securities_total = 0
    stock_total = 0

    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']

    solid_orders = Order.objects.filter(
        Q(responsible_cid=responsible_cid) & Q(recordtypeid='0126F0000020pRrQAI') & Q(
            commissionformula__isnull=False) & Q(ostatus__in=valid_status1))
    solid_list = list(solid_orders)
    for o in solid_list:
        solid_total = solid_total + float(o.commissionformula)

    securities_orders = Order.objects.filter(
        Q(responsible_cid=responsible_cid) & Q(recordtypeid='0126F0000020pRqQAI') & Q(
            commissionformula__isnull=False) & Q(ostatus__in=valid_status1))
    securities_list = list(securities_orders)
    for o in securities_list:
        securities_total = securities_total + float(o.commissionformula)

    stock_orders = Order.objects.filter(
        Q(responsible_cid=responsible_cid) & Q(recordtypeid='0126F0000020pRsQAI') & Q(
            commissionformula__isnull=False) & Q(ostatus__in=valid_status1))
    stock_orders = list(stock_orders)
    for o in stock_orders:
        stock_total = stock_total + float(o.commissionformula)

    total_commission = solid_total + securities_total + stock_total
    results = ({
        'solid_total': solid_total, 'securities_total': securities_total, 'stock_total': stock_total,
        'total_commission': total_commission,
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_total_by_month(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    start_year = data['start_year']
    end_year = data['end_year']
    months = []
    total_commission = 0
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']

    for i in range(int(start_year), int(end_year) + 1):
        for j in range(12):
            orders = Order.objects.filter(
                Q(responsible_cid=responsible_cid) & Q(date__year=i) & Q(date__month=j + 1) & Q(
                    ostatus__in=valid_status1))
            order_list = list(orders)
            month_total = 0
            for o in order_list:
                month_total = month_total + float(o.commissionformula)
            total_commission = total_commission + month_total
            if month_total != 0 and j + 1 < 10:
                months.append({
                    'time': str(i) + '-0' + str(j + 1),
                    'month_total': month_total,
                })
            if month_total != 0 and j + 1 >= 10:
                months.append({
                    'time': str(i) + '-' + str(j + 1),
                    'month_total': month_total,
                })

    results = ({
        'total_commission': total_commission, 'months_list': months
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


# 同步函数，维护Order表中佣金数据
def calculate_all_commission(request):
    valid_status1 = ['打款审批完成', '资料审核中', '资料审批驳回', '认购成功', '已清算', '全部赎回', '部分赎回']
    orders = Order.objects.filter(ostatus__in=valid_status1)
    # orders = Order.objects.filter(ostatus__in=valid_status1,crm_id='a0N6F00000YMI6jUAH')
    order_list = list(orders)
    config1 = configparser.ConfigParser()
    config1.read('./solid.ini', encoding='UTF-8')
    config2 = configparser.ConfigParser()
    config2.read('./s&s.ini', encoding='UTF-8')
    result = None

    if not config1 or config2:
        HttpResponseBadRequest("没有找到配置文件")

    for i, o in enumerate(order_list):
        name = o.name
        product_id = o.product_cid
        product = Productofficial.objects.filter(crm_id=product_id).first()
        if not product:
            continue
        elif product.recordtype == "类固收":
            abbr = product.productabbreviation
            if abbr not in config1:
                continue
            type = config1[abbr]['formulaType']
            ratio = float(config1[abbr]['commissionRatio'])
            result = 0
            subscription_fee = float(o.subscription_fee)
            if o.maturitydate is None:
                holdday = 0
            elif o.startearningday is None:
                holdday = 0
            else:
                holdday = (o.maturitydate - o.startearningday).days
            if o.money_4 is None:
                money_4 = 0
            else:
                money_4 = float(o.money_4)
            if type == 'SA':
                result = subscription_fee * ratio
            elif type == 'SB':
                result = holdday * money_4 / 365 * ratio

        elif product.recordtype == "二级市场" or "股权" or "FA":
            abbr = product.productabbreviation
            if abbr not in config2:
                continue
            type = config2[abbr]['formulaType']
            ratio = float(config2[abbr]['commissionRatio'])
            subscription_fee = float(o.subscription_fee)
            if o.maturitydate is None:
                holdday = 0
            elif o.startearningday is None:
                holdday = 0
            else:
                holdday = (o.maturitydate - o.startearningday).days
            if o.money_4 is None:
                money_4 = 0
            else:
                money_4 = float(o.money_4)

            result = calculate(type, subscription_fee, ratio, holdday, money_4)

            # 激励方案
            if config2[abbr]['incentive'] == 'true':
                days = config2[abbr]['holdday'].split(',')
                days = list(map(float, days))
                ratios = config2[abbr]['ratio'].split(',')
                ratios = list(map(float, ratios))
                if holdday <= days[0]:
                    ratio = ratios[0]
                elif holdday > days[-1]:
                    ratio = ratios[-1]
                else:
                    for i in range(len(days) - 1):
                        if days[i] < holdday <= days[i + 1]:
                            ratio = ratios[i + 1]
                result = calculate(type, subscription_fee, ratio, holdday, money_4)
        Order.objects.filter(name=name).update(commissionformula=result)
    return HttpResponse("计算所有佣金完成")


# 计算佣金细节，按年给出
def get_commission_details_by_year(request):
    data = json.loads(request.body)
    year = data['year']
    responsible_id = data['responsible_id']
    result = {
        "details": {
            "12": [], "11": [], "10": [], "9": [], "8": [], "7": [],
            "6": [], "5": [], "4": [], "3": [], "2": [], "1": [],
        },
        "sum_all": 0,
        "sum_years": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0},
        "sum_month": {
            "12": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "11": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "10": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "9": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "8": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "7": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "6": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "5": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "4": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "3": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "2": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0},
            "1": {"all": 0, "基础佣金": 0, "团队管理津贴": 0, "区域总津贴": 0, "推荐奖金": 0, "isShow": 0}
        }
    }
    try:
        responsible = Responsible.objects.get(id=responsible_id)
    except Responsible.DoesNotExist:
        return HttpResponseBadRequest('投顾信息不存在')
    # all_commission = Commissiondetail.objects.filter(responsible_cid=responsible.crm_id)
    # # 整体结果
    # for i in all_commission:
    #     if i.type != '区域总津贴':
    #         result['sum_all'] = result['sum_all'] + i.amount
    # 整体结果
    total = Commissiondetail.objects.filter(Q(responsible_cid=responsible.crm_id) & ~Q(type = '区域总津贴')).aggregate(total=Sum("amount"))
    result['sum_all'] = 0
    if total["total"]:
        result['sum_all'] = total["total"]

    details = Commissiondetail.objects.filter( Q(responsible_cid=responsible.crm_id) & Q(date__year=year) & ~Q(type = '区域总津贴'))
    # 按年份查询
    for i in details:
        if i.type != '区域总津贴':
            result['sum_years']['all'] = result['sum_years']['all'] + i.amount
        month = i.date.month
        month_detail = {
            'title': i.title,
            'date': i.date,
            'rgfee': i.rgfee,
            'productname': i.productname,
            'rgmoney': i.rgmoney,
            'rate': i.rate,
            'startearningday': i.startearningday,
            'maturitydate': i.maturitydate,
            'type': i.type,
            'amount': i.amount
        }
        result['details'][str(month)].append(month_detail)
        result['sum_month'][str(month)]['month'] = month
        if  i.type != '区域总津贴':
            result['sum_month'][str(month)]['all'] = result['sum_month'][str(month)]['all'] + i.amount
        if i.type == '基础佣金':
            result['sum_years']['基础佣金'] = result['sum_years']['基础佣金'] + i.amount
            result['sum_month'][str(month)]['基础佣金'] = result['sum_month'][str(month)]['基础佣金'] + i.amount
        elif i.type == '团队管理津贴':
            result['sum_years']['团队管理津贴'] = result['sum_years']['团队管理津贴'] + i.amount
            result['sum_month'][str(month)]['团队管理津贴'] = result['sum_month'][str(month)]['团队管理津贴'] + i.amount
        # elif i.type == '区域总津贴':
        #     result['sum_years']['区域总津贴'] = result['sum_years']['区域总津贴'] + i.amount
        #     result['sum_month'][str(month)]['区域总津贴'] = result['sum_month'][str(month)]['区域总津贴'] + i.amount
        elif i.type == '推荐奖金':
            result['sum_years']['推荐奖金'] = result['sum_years']['推荐奖金'] + i.amount
            result['sum_month'][str(month)]['推荐奖金'] = result['sum_month'][str(month)]['推荐奖金'] + i.amount
    import copy
    result_sum_month = copy.deepcopy(result['sum_month'])
    for i in result['sum_month']:
        if result['sum_month'][str(i)]['all'] == 0 and (len(result['details']) ==0 or  len(result['details'][str(i)]) == 0) :
            result_sum_month.pop(str(i))
    result['sum_month'] = result_sum_month

    result_details = copy.deepcopy(result['details'])
    for i in result['details']:
        if len(result['details'][str(i)]) == 0:
            result_details.pop(str(i))
    result['details'] = result_details



    re = json.dumps(result, sort_keys=False,ensure_ascii=False, cls=CustomEncoder)
    return HttpResponse(re, content_type="application/json",
                        charset='utf-8')



