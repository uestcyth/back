from django.urls import path

from . import views

app_name = 'Product'
urlpatterns = [
    path('search_product/', views.search_product, name='search_product'),
    path('get_securities_details/', views.get_securities_details, name='get_securities_details'),
    path('get_solid_details/', views.get_solid_details, name='get_solid_details'),
    path('get_stock_details/', views.get_stock_details, name='get_stock_details'),
    path('get_product_details/', views.get_product_details, name='get_product_details'),
    path('get_product_value/', views.get_product_value, name='get_product_value'),
    path('recommend_product/', views.recommend_product, name='recommend_product'),
    path('get_product_list/', views.get_product_list, name='get_product_list'),
    path('get_securities_list/', views.get_securities_list, name='get_securities_list'),
    # 推荐组合
    path('recommend_combo/', views.recommend_combo, name='recommend_combo'),
    path('get_type_details/', views.get_type_details, name='get_type_details'),
    path('get_combo_details/', views.get_combo_details, name='get_combo_details'),
    path('get_combo_value/', views.get_combo_value, name='get_combo_value'),
    # 自定义配置
    path('update_combo_proportion/', views.update_combo_proportion, name='update_combo_proportion'),
    path('recommend_custom_combo/', views.recommend_custom_combo, name='recommend_custom_combo'),
    path('get_custom_type_details/', views.get_custom_type_details, name='get_custom_type_details'),
    path('get_custom_combo_details/', views.get_custom_combo_details, name='get_custom_combo_details'),
    path('get_custom_combo_value/', views.get_custom_combo_value, name='get_custom_combo_value'),
    path('add_custom_combo/', views.add_custom_combo, name='add_custom_combo'),
    path('delete_custom_combo/', views.delete_custom_combo, name='delete_custom_combo'),
    path('rename_custom_combo/', views.rename_custom_combo, name='rename_custom_combo'),
    # 关注
    path('favor_product/', views.favor_product, name='favor_product'),
    path('disfavor_product/', views.disfavor_product, name='disfavor_product'),
    path('get_favorite_status/', views.get_favorite_status, name='get_favorite_status'),
    path('get_my_favorite_list/', views.get_my_favorite_list, name='get_my_favorite_list'),
    # 用户行为
    path('record_user_behavior/', views.record_user_behavior, name='record_user_behavior'),
    path('search_behavior_by_user/', views.search_behavior_by_user, name='search_behavior_by_user'),
    path('search_behavior_by_product/', views.search_behavior_by_product, name='search_behavior_by_product'),
    # 产品文档的下载
    path('uploadFile/<str:uid>', views.upload_file),
    path('download/<file_name>', views.download_file),
    path('pdf_download/<str:product_id>', views.pdf_download),
    # 产品风险评估
    path('get_product_risk_eval/', views.get_product_risk_eval, name='get_product_risk_eval'),
    # 私募证券（二级市场） 产品详情
    path('get_second_level_market/', views.get_second_level_market, name='get_second_level_market'),
    # 投顾-客户详情-客户行为
    path('search_behavior_by_user_new/', views.search_behavior_by_user_new, name='search_behavior_by_user_new'),
    # 投顾-客户详情-客户行为-详情
    path('search_behavior_by_user_new_info/', views.search_behavior_by_user_new_info, name='search_behavior_by_user_new_info'),
    # 订单列表
    path('get_orders/', views.get_orders, name='get_orders'),
    # # 订单搜索接口
    # path('get_orders_search /', views.get_orders_search, name='get_orders_search'),
    # 订单详情
    path('get_order_info/', views.get_order_info, name='get_order_info')

]
