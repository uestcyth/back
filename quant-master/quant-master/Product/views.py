import configparser
import datetime
import decimal
import json
import math
from operator import itemgetter

import pymysql
from django.shortcuts import redirect
from dateutil.relativedelta import relativedelta
from django.http import HttpResponse, HttpResponseBadRequest, StreamingHttpResponse
from django.utils import timezone
from django.db.models import Q

from Quantitative.settings import DATABASES
from datamodels.models import Productofficial, Account, Responsible, Networth, Order, Combinationproportion, \
    Combinationinfo, \
    Combinationproduct, Combinationposition, Productbank, Favoriteproduct, User, Userbehavior, Productqatopic, \
    Productqaoption, Signtask


# 解决序列化问题
class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        super(CustomEncoder, self).default(o)


class CustomEncoder4Time(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d %H:%M:%S')
        super(CustomEncoder4Time, self).default(o)


def CustomResponse(string):
    response = HttpResponse(string)
    response.status_code = 500
    return response


def calculate_half_year_increase(networth):
    dates = []
    values = []
    for n in networth:
        dates.append(n.updatedate)
        values.append(n.value)
    newest = networth.last()
    last_half_year_value = 1.0
    last_half_year_date = networth.last().updatedate - relativedelta(months=6)
    for i in range(int((newest.updatedate - last_half_year_date).days)):
        if last_half_year_date - datetime.timedelta(days=i) in dates:
            idx = last_half_year_date - datetime.timedelta(days=i)
            last_half_year_value = values[dates.index(idx)]
            half_year_increase = float(newest.value) / float(last_half_year_value) - 1
            return half_year_increase
    half_year_increase = float(newest.value) / float(last_half_year_value) - 1
    return half_year_increase


def calculate_one_year_increase(networth):
    dates = []
    values = []
    for n in networth:
        dates.append(n.updatedate)
        values.append(n.value)
    newest = networth.last()
    last_year_value = 1.0
    last_year_date = networth.last().updatedate - relativedelta(years=1)
    for i in range(int((newest.updatedate - last_year_date).days)):
        if last_year_date - datetime.timedelta(days=i) in dates:
            idx = last_year_date - datetime.timedelta(days=i)
            last_year_value = values[dates.index(idx)]
            one_year_increase = float(newest.value) / float(last_year_value) - 1
            return one_year_increase
    one_year_increase = float(newest.value) / float(last_year_value) - 1
    return one_year_increase


# 累计净值计算
def calculate_one_year_increase_accumulative(networth):
    dates = []
    values = []
    for n in networth:
        dates.append(n.updatedate)
        values.append(n.cumulativenetworth)
    newest = networth.last()
    last_year_value = 1.0
    last_year_date = networth.last().updatedate - relativedelta(years=1)
    for i in range(int((newest.updatedate - last_year_date).days)):
        if last_year_date - datetime.timedelta(days=i) in dates:
            idx = last_year_date - datetime.timedelta(days=i)
            last_year_value = values[dates.index(idx)]
            one_year_increase = float(newest.cumulativenetworth) / float(last_year_value) - 1
            return one_year_increase
    one_year_increase = float(newest.cumulativenetworth) / float(last_year_value) - 1
    return one_year_increase


def calculate_year_history_profit(networth):
    first_value = 1.0
    year = timezone.now().year
    last_year_last = datetime.date(year - 1, 12, 31)
    year_first = datetime.date(year, 1, 1)
    dates = []
    values = []
    for n in networth:
        # 创建日期/净值数组
        dates.append(n.updatedate)
        values.append(n.value)
        # 查找y-1年最新净值/y年最早净值
        # 前向查找y-1年最新净值
    for i in range(365):
        if last_year_last - datetime.timedelta(days=i) in dates:
            idx_forwards = last_year_last - datetime.timedelta(days=i)
            first_value = (values[dates.index(idx_forwards)])
            # 计算收益
            # print(last_year_last)
            # rint(first_value)
            # print(dates.index(idx_forwards))
            year_profit = float(networth.last().value) / float(first_value) - 1
            history_profit = float(networth.last().value) / float(networth.first().value) - 1
            return year_profit, history_profit
    # 后向查找y年最早精致
    for j in range(365):
        if year_first + datetime.timedelta(days=j) in dates:
            idx_backwards = year_first + datetime.timedelta(days=j)
            first_value = (values[dates.index(idx_backwards)])
            # 计算收益
            # print(year_first)
            # print(first_value)
            year_profit = float(networth.last().value) / float(first_value) - 1
            history_profit = float(networth.last().value) / float(networth.first().value) - 1
            return year_profit, history_profit
    # 如果没有找到
    # print('none')
    year_profit = float(networth.last().value) / float(first_value) - 1
    history_profit = float(networth.last().value) / float(networth.first().value) - 1
    return year_profit, history_profit


def search_product(request):
    data = json.loads(request.body)
    abbr = data['abbr']
    ##增加渠道条件
    if 'userid' in data:
        userid = data['userid']
        user_channel = getChannelByUserId(userid)

    if not abbr:
        products = Productofficial.objects.filter(
            Q(stage='产品推介') & (Q(channel__isnull=True) | Q(channel__contains=user_channel)))
    else:
        products = Productofficial.objects.filter(
            Q(name__contains=abbr) & Q(stage='产品推介') & (Q(channel__isnull=True) | Q(channel__contains=user_channel)))
    results = []
    for p in products:
        year_profit = 0.0
        product_yield = "0.00%"
        # 计算近一年涨幅
        if p.recordtype == '二级市场':
            networth = Networth.objects.filter(product_cid=p.crm_id)
            if networth:
                year_profit, _ = calculate_year_history_profit(networth)
            # 近一年收益率
            # 近一年收益
            networth = Networth.objects.filter(product_cid=p.crm_id).order_by('updatedate')
            try:
                one_year_increase = calculate_one_year_increase_accumulative(networth)
            except Exception as e:
                one_year_increase = 0

            if one_year_increase:
                product_yield = (round(100 * one_year_increase if p.ratesub else 0, 2))
                if product_yield != 0:
                    product_yield = str(product_yield) + "%"
        # 计算用户行为
        favors, disfavors, forwardings, checks = count_behavior(p.id)
        results.append({
            'name': p.name,
            'productabbreviation': p.productabbreviation,
            'crm_id': p.crm_id,
            'id': p.id,
            "one_year_increase": product_yield,
            'recordtype': p.recordtype,
            # 股权
            'direction': p.direction,
            'productperiod': p.productperiod,
            # 证券
            'value': p.value,
            'year_profit': year_profit,
            # 类固收
            'comparison': p.comparison,
            'monthlimit': p.monthlimit,
            'comparisonabbreviation': p.comparisonabbreviation,
            'remainingamountcanbookedf': p.remainingamountcanbookedf,
            'stage': p.stage,
            'net_favors': favors - disfavors,
            'forwardings': forwardings,
            'checks': checks,
            'origin': p.origin,
            'addmin': p.addmin,
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_securities_details(request):
    data = json.loads(request.body)
    abbr = data['abbr']
    products = Productofficial.objects.filter(productabbreviation__contains=abbr)
    results = []
    # 获取数据
    for p in products:
        one_year_increase = 0.0
        half_year_increase = 0.0
        networth = Networth.objects.filter(product_cid=p.crm_id).order_by('updatedate')
        # 获取募集账户
        productbank = Productbank.objects.filter(product_cid=p.crm_id, type='募集账户').first()
        if not productbank:
            bankname = "-"
            bankno = "-"
            accountname = "-"
        else:
            bankname = productbank.bankname
            bankno = productbank.bankno
            accountname = productbank.accountname
        # 获取涨幅/收益率
        if networth:
            one_year_increase = calculate_one_year_increase(networth)
            half_year_increase = calculate_half_year_increase(networth)
            year_profit, history_profit = calculate_year_history_profit(networth)
            results.append({
                # 根据界面样式：
                'crm_id': p.crm_id,
                'id': p.id,
                # 顶部
                'risklevel': p.risklevel,
                'origin': p.origin,
                # 详情
                'productabbreviation': p.productabbreviation,
                'name': p.name,
                'lockday': p.lockday,
                'value': p.value,
                'newest_value': networth.last().value,
                'year_profit': year_profit,
                'adminacus': p.adminacus,
                'adminbcus': p.adminbcus,
                'bankname': bankname,
                'accountname': accountname,
                'bankno': bankno,
                # 'year_first_value': first_value,
                # 'history_value': networth.first().value,
                'history_profit': history_profit,
                'one_year_increase': one_year_increase,
                'half_year_increase': half_year_increase,
            })
        else:
            results.append({
                # 根据界面样式：
                'crm_id': p.crm_id,
                'id': p.id,
                # 顶部
                'risklevel': p.risklevel,
                'origin': p.origin,
                # 详情
                'productabbreviation': p.productabbreviation,
                'name': p.name,
                'lockday': p.lockday,
                'value': p.value,
                'newest_value': 1.0,
                'year_profit': 0.0,
                'adminacus': p.adminacus,
                'adminbcus': p.adminbcus,
                'bankname': bankname,
                'accountname': accountname,
                'bankno': bankno,
                # 'year_first_value': first_value,
                # 'history_value': networth.first().value,
                'history_profit': 0.0,
                'one_year_increase': one_year_increase,
                'half_year_increase': half_year_increase,
            })

    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_solid_details(request):
    data = json.loads(request.body)
    abbr = data['abbr']
    products = Productofficial.objects.filter(productabbreviation__contains=abbr)
    results = []
    for product in products:
        # 获取募集账户
        productbank = Productbank.objects.filter(product_cid=product.crm_id, type='募集账户').first()
        if not productbank:
            bankname = "-"
            bankno = "-"
            accountname = "-"
        else:
            bankname = productbank.bankname
            bankno = productbank.bankno
            accountname = productbank.accountname
        results.append({
            # 根据界面样式：
            'crm_id': product.crm_id,
            'id': product.id,
            # 顶部
            'risklevel': product.risklevel,
            'origin': product.origin,
            # 详情
            'productabbreviation': product.productabbreviation,
            'name': product.name,
            'scale': product.scale,
            'mouthlimit': product.monthlimit,
            'comparison': product.comparison,
            'comparisonabbreviation': product.comparisonabbreviation,
            'adminacus': product.adminacus,
            'adminbcus': product.adminbcus,
            'bankname': bankname,
            'accountname': accountname,
            'bankno': bankno,
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_stock_details(request):
    data = json.loads(request.body)
    abbr = data['abbr']
    products = Productofficial.objects.filter(productabbreviation__contains=abbr)
    results = []
    for product in products:
        # 获取募集账户
        productbank = Productbank.objects.filter(product_cid=product.crm_id, type='募集账户').first()
        if not productbank:
            bankname = "-"
            bankno = "-"
            accountname = "-"
        else:
            bankname = productbank.bankname
            bankno = productbank.bankno
            accountname = productbank.accountname
        results.append({
            # 根据界面样式：
            'crm_id': product.crm_id,
            'id': product.id,
            # 顶部
            'risklevel': product.risklevel,
            'origin': product.origin,
            # 详情
            'productabbreviation': product.productabbreviation,
            'name': product.name,
            'scale': product.scale,
            'productperiod': product.productperiod,
            'direction': product.direction,
            'adminacus': product.adminacus,
            'adminbcus': product.adminbcus,
            'bankname': bankname,
            'accountname': accountname,
            'bankno': bankno,
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_product_details(request):
    data = json.loads(request.body)
    product_id = data['product_id']
    result = []
    try:
        product = Productofficial.objects.get(id=product_id)

        # 获取募集账户
        productbank = Productbank.objects.filter(product_cid=product.crm_id, type='募集账户').first()
        if not productbank:
            bankname = "-"
            bankno = "-"
            accountname = "-"
        else:
            bankname = productbank.bankname
            bankno = productbank.bankno
            accountname = productbank.accountname

        if product.recordtype == "类固收":
            result = ({
                # 根据界面样式：
                'crm_id': product.crm_id,
                'id': product.id,
                # 顶部
                'risklevel': product.risklevel,
                'origin': product.origin,
                # 详情
                'productabbreviation': product.productabbreviation,
                'name': product.name,
                'scale': product.scale,
                'mouthlimit': product.monthlimit,
                'comparison': product.comparison,
                'comparisonabbreviation': product.comparisonabbreviation,
                'adminacus': product.adminacus,
                'adminbcus': product.adminbcus,
                'bankname': bankname,
                'accountname': accountname,
                'bankno': bankno,
                'addmin': product.addmin,
                'file': product.file,
            })
        elif product.recordtype == "二级市场":
            one_year_increase = 0.0
            half_year_increase = 0.0
            networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
            # 获取涨幅/收益率
            if networth:
                one_year_increase = calculate_one_year_increase(networth)
                half_year_increase = calculate_half_year_increase(networth)
                year_profit, history_profit = calculate_year_history_profit(networth)
                result = ({
                    # 根据界面样式：
                    'crm_id': product.crm_id,
                    'id': product.id,
                    # 顶部
                    'risklevel': product.risklevel,
                    'origin': product.origin,
                    # 详情
                    'productabbreviation': product.productabbreviation,
                    'name': product.name,
                    'lockday': product.lockday,
                    'value': product.value,
                    'newest_value': networth.last().value,
                    'year_profit': year_profit,
                    'adminacus': product.adminacus,
                    'adminbcus': product.adminbcus,
                    'bankname': bankname,
                    'accountname': accountname,
                    'bankno': bankno,
                    # 'year_first_value': first_value,
                    # 'history_value': networth.first().value,
                    'history_profit': history_profit,
                    'one_year_increase': one_year_increase,
                    'half_year_increase': half_year_increase,
                    'addmin': product.addmin,
                    'file': product.file,
                })
            else:
                result = ({
                    # 根据界面样式：
                    'crm_id': product.crm_id,
                    'id': product.id,
                    # 顶部
                    'risklevel': product.risklevel,
                    'origin': product.origin,
                    # 详情
                    'productabbreviation': product.productabbreviation,
                    'name': product.name,
                    'lockday': product.lockday,
                    'value': product.value,
                    'newest_value': 1.0,
                    'year_profit': 0.0,
                    'adminacus': product.adminacus,
                    'adminbcus': product.adminbcus,
                    'bankname': bankname,
                    'accountname': accountname,
                    'bankno': bankno,
                    # 'year_first_value': first_value,
                    # 'history_value': networth.first().value,
                    'history_profit': 0.0,
                    'one_year_increase': one_year_increase,
                    'half_year_increase': half_year_increase,
                    'addmin': product.addmin,
                    'file': product.file,
                })
        elif product.recordtype == "股权":
            result = ({
                # 根据界面样式：
                'crm_id': product.crm_id,
                'id': product.id,
                # 顶部
                'risklevel': product.risklevel,
                'origin': product.origin,
                # 详情
                'productabbreviation': product.productabbreviation,
                'name': product.name,
                'scale': product.scale,
                'productperiod': product.productperiod,
                'direction': product.direction,
                'adminacus': product.adminacus,
                'adminbcus': product.adminbcus,
                'bankname': bankname,
                'accountname': accountname,
                'bankno': bankno,
                'addmin': product.addmin,
                'file': product.file,
            })
    except Productofficial.DoesNotExist:
        return CustomResponse("产品不存在:" + product_id)
    return HttpResponse(json.dumps(result, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_product_value(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    product_cid = data['product_cid']
    networth = Networth.objects.filter(product_cid=product_cid).order_by('updatedate')
    if not networth:
        err = {
            'value': [None], 'cumulativenetworth': [None], 'dates': [None],
        }
        # return HttpResponse("没有对应的净值信息")
        return HttpResponse(json.dumps(err, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                            charset='utf-8')
    networth_list = list(networth)
    values = []
    cum_values = []
    dates = []
    for nw in networth_list:
        values.append(nw.value)
        cum_values.append(nw.cumulativenetworth)
        dates.append(nw.updatedate)
    results = {
        'value': values, 'cumulativenetworth': cum_values, 'dates': dates,
    }
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def recommend_product_old(request):
    period = 90
    nums = 20
    year = timezone.now().year
    start_date = timezone.now() - timezone.timedelta(period)
    end_date = timezone.now()
    products = []
    orders = Order.objects.filter(
        date__isnull=False, date__year=year, date__range=(start_date, end_date)).order_by('-date')
    product_list = []
    for o in orders:
        try:
            p = Productofficial.objects.get(crm_id=o.product_cid)
            if p not in product_list:
                year_profit = 0.0
                # 计算近一年涨幅
                if p.recordtype == '二级市场':
                    networth = Networth.objects.filter(product_cid=p.crm_id)
                    if networth:
                        year_profit, _ = calculate_year_history_profit(networth)
                # 计算用户行为
                favors, disfavors, forwardings, checks = count_behavior(p.id)
                products.append({
                    'name': p.name,
                    'productabbreviation': p.productabbreviation,
                    'crm_id': p.crm_id,
                    'id': p.id,
                    'recordtype': p.recordtype,
                    # 股权
                    'direction': p.direction,
                    'productperiod': p.productperiod,
                    # 证券
                    'value': p.value,
                    'year_profit': year_profit,
                    # 类固收
                    'comparison': p.comparison,
                    'monthlimit': p.monthlimit,
                    'comparisonabbreviation': p.comparisonabbreviation,
                    'net_favors': favors - disfavors,
                    'forwardings': forwardings,
                    'checks': checks,
                })
                product_list.append(p)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + o.product_cid)
    results = ({
        "total_nums": len(products), "products": products,
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json", charset='utf-8')


# 手动精选
def recommend_product(request):
    results = []
    products = []
    product_list = ['a0U6F00000loBJfUAM', 'a0U6F00000khBvOUAU', 'a0U6F00000gj9ogUAA']
    for product_cid in product_list:
        try:
            p = Productofficial.objects.get(crm_id=product_cid)
            year_profit = 0.0
            product_yield = "0.00%"
            # 计算近一年涨幅
            if p.recordtype == '二级市场':
                networth = Networth.objects.filter(product_cid=p.crm_id).order_by('updatedate')
                if networth:
                    year_profit, _ = calculate_year_history_profit(networth)
                    # networth = Networth.objects.filter(product_cid=p.crm_id).order_by('updatedate')
                    # one_year_increase = calculate_one_year_increase(networth)
                    one_year_increase = calculate_one_year_increase_accumulative(networth)
                    if one_year_increase:
                        product_yield = (round(100 * one_year_increase if one_year_increase else 0, 2))
                        if product_yield != 0:
                            product_yield = str(product_yield) + "%"
                        else:
                            product_yield = "0.00%"
            # 计算用户行为
            favors, disfavors, forwardings, checks = count_behavior(p.id)

            # 计算已预约
            if p.controlopenmoney and p.remainingamountcanbookedf:
                bookedf = float(p.controlopenmoney) - float(p.remainingamountcanbookedf)
            else:
                bookedf = '无法计算'

            products.append({
                'name': p.name,
                'productabbreviation': p.productabbreviation,
                'crm_id': p.crm_id,
                'id': p.id,
                'recordtype': p.recordtype,
                # 股权
                'direction': p.direction,
                'productperiod': p.productperiod,
                # 证券
                'value': p.value,
                'year_profit': year_profit,
                # 类固收
                'comparison': p.comparison,
                'monthlimit': p.monthlimit,
                'comparisonabbreviation': p.comparisonabbreviation,
                'net_favors': favors - disfavors,
                'forwardings': forwardings,
                'checks': checks,
                'controlopenmoney': p.controlopenmoney,
                'remainingamountcanbookedf': p.remainingamountcanbookedf,
                'bookedf': bookedf,
                'addmin': p.addmin,
                'origin': p.origin,
                'one_year_increase': product_yield,
            })
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + product_cid)

    results = ({
        "total_nums": len(products), "products": products,
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json", charset='utf-8')


def get_product_list(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    recordtype = data['recordtype']
    ##增加渠道条件
    if 'userid' in data:
        userid = data['userid']
        user_channel = getChannelByUserId(userid)

    products = Productofficial.objects.filter(Q(recordtype__contains=recordtype) & Q(stage='产品推介') & (
            Q(channel__isnull=True) | Q(channel__contains=user_channel)))

    product_list = list(products)
    results = []
    for p in product_list:
        year_profit = 0.0
        # 计算近一年涨幅
        if p.recordtype == '二级市场':
            networth = Networth.objects.filter(product_cid=p.crm_id)
            if networth:
                year_profit, _ = calculate_year_history_profit(networth)
        # 计算用户行为
        favors, disfavors, forwardings, checks = count_behavior(p.id)

        # 计算已预约
        if p.controlopenmoney and p.remainingamountcanbookedf:
            bookedf = float(p.controlopenmoney) - float(p.remainingamountcanbookedf)
        else:
            bookedf = '无法计算'

        results.append({
            'name': p.name,
            'productabbreviation': p.productabbreviation,
            'crm_id': p.crm_id,
            'id': p.id,
            # 股权
            'direction': p.direction,
            'productperiod': p.productperiod,
            # 证券
            'value': p.value,
            'year_profit': year_profit,
            # 类固收
            'comparison': p.comparison,
            'monthlimit': p.monthlimit,
            'comparisonabbreviation': p.comparisonabbreviation,
            'stage': p.stage,
            'net_favors': favors - disfavors,
            'forwardings': forwardings,
            'checks': checks,
            'controlopenmoney': p.controlopenmoney,
            'remainingamountcanbookedf': p.remainingamountcanbookedf,
            'bookedf': bookedf,
            'addmin': p.addmin,
            'origin': p.origin,
            'onlinesign': p.onlinesign,
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_securities_list(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    year = data['year']
    ##增加渠道条件
    if 'userid' in data:
        userid = data['userid']
        user_channel = getChannelByUserId(userid)
    products = Productofficial.objects.filter(
        Q(recordtype='二级市场') & Q(stage='产品推介') & (Q(channel__isnull=True) | Q(channel__contains=user_channel)))

    # products = Productofficial.objects.filter(recordtype='二级市场', stage='产品推介',crm_id='a0U6F00000l1r4oUAA')
    results = []
    # 获取数据
    for p in products:
        first_value = 1.0
        product_yield = "0.00%"
        half_year_increase = 0.0
        # 计算用户行为
        favors, disfavors, forwardings, checks = count_behavior(p.id)
        networth = Networth.objects.filter(product_cid=p.crm_id).order_by('updatedate')

        # 计算已预约
        if p.controlopenmoney and p.remainingamountcanbookedf:
            bookedf = float(p.controlopenmoney) - float(p.remainingamountcanbookedf)
        else:
            bookedf = '无法计算'
        if networth:
            one_year_increase = calculate_one_year_increase_accumulative(networth)
            if one_year_increase:
                product_yield = (round(100 * one_year_increase if one_year_increase else 0, 2))
                if product_yield != 0:
                    product_yield = str(product_yield) + "%"
                else:
                    product_yield = "0.00%"
            half_year_increase = calculate_half_year_increase(networth)
            year_profit, history_profit = calculate_year_history_profit(networth)
            results.append({
                'name': p.name,
                'productabbreviation': p.productabbreviation,
                'crm_id': p.crm_id,
                'id': p.id,
                'value': p.value,
                'newest_value': networth.last().value,
                'year_first_value': first_value,
                'year_profit': year_profit,
                'history_value': networth.first().value,
                'history_profit': history_profit,
                'one_year_increase': product_yield,
                'half_year_increase': half_year_increase,
                'net_favors': favors - disfavors,
                'forwardings': forwardings,
                'checks': checks,
                'controlopenmoney': p.controlopenmoney,
                'remainingamountcanbookedf': p.remainingamountcanbookedf,
                'bookedf': bookedf,
                'addmin': p.addmin,
                'origin': p.origin,
            })
        else:
            results.append({
                'id': p.id,
                'productabbreviation': p.productabbreviation,
                'crm_id': p.crm_id,
                'value': p.value,
                'newest_value': p.value,
                'year_first_value': p.value,
                'year_profit': 0.0,
                'history_value': p.value,
                'history_profit': 0.0,
                'one_year_increase': product_yield,
                'half_year_increase': half_year_increase,
                'net_favors': favors - disfavors,
                'forwardings': forwardings,
                'checks': checks,
                'controlopenmoney': p.controlopenmoney,
                'remainingamountcanbookedf': p.remainingamountcanbookedf,
                'bookedf': bookedf,
                'addmin': p.addmin,
                'origin': p.origin,
            })
    # 按关键词排序
    results = sorted(results, key=itemgetter('year_profit', 'history_profit'), reverse=True)

    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def recommend_combo(request):
    config = configparser.ConfigParser()
    config.read('./combo.ini', encoding='UTF-8')
    if not config:
        return HttpResponseBadRequest('Config Not Found')
    results = []
    types = ["股票多头型", "对冲型", "债券型", "流动型"]
    # 进取型
    ratio = config['进取型']['ratio'].split(',')
    for i, r in enumerate(ratio[:]):
        ratio[i] = num_2_percentage(r)
    results.append({"combo": "进取型", "types": types, "ratio": ratio})
    # 稳健型
    ratio = config['稳健型']['ratio'].split(',')
    for i, r in enumerate(ratio[:]):
        ratio[i] = num_2_percentage(r)
    results.append({"combo": "稳健型", "types": types, "ratio": ratio})
    # 保守型
    ratio = config['保守型']['ratio'].split(',')
    for i, r in enumerate(ratio[:]):
        ratio[i] = num_2_percentage(r)
    results.append({"combo": "保守型", "types": types, "ratio": ratio})
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_type_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    type = data['type']

    config = configparser.ConfigParser()
    config.read('./combo.ini', encoding='UTF-8')
    if not config:
        return HttpResponseBadRequest('Config Not Found')

    results = []
    product_abbrs = config[type]['products'].split(',')
    for abbr in product_abbrs:
        try:
            product = Productofficial.objects.get(productabbreviation=abbr)
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + abbr)
        results.append({
            'type': type, 'productname': product.name, 'productabbreviation': product.productabbreviation,
            'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage, 'crm_id': product.crm_id,
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_combo_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    combo = data['combo']

    config = configparser.ConfigParser()
    config.read('./combo.ini', encoding='UTF-8')
    if not config:
        return HttpResponseBadRequest('Config Not Found')

    results = []
    multi = config[combo]['multi'].split(',')
    hedge = config[combo]['hedge'].split(',')
    bond = config[combo]['bond'].split(',')
    flow = config[combo]['flow'].split(',')
    # 股票多头型
    if multi != ['']:
        for abbr in multi:
            try:
                product = Productofficial.objects.get(productabbreviation=abbr)
            except Productofficial.DoesNotExist:
                return CustomResponse('产品不存在:' + abbr)
            if not product:
                continue
            results.append({
                'combo': combo, 'type': "股票多头型", 'productname': product.name,
                'productabbreviation': product.productabbreviation,
                'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage,
                'crm_id': product.crm_id, 'id': product.id,
            })
    # 对冲型
    if hedge != ['']:
        for abbr in hedge:
            try:
                product = Productofficial.objects.get(productabbreviation=abbr)
            except Productofficial.DoesNotExist:
                return CustomResponse('产品不存在:' + abbr)
            if not product:
                continue
            results.append({
                'combo': combo, 'type': "对冲型", 'productname': product.name,
                'productabbreviation': product.productabbreviation,
                'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage,
                'crm_id': product.crm_id, 'id': product.id,
            })
    # 债券型
    if bond != ['']:
        for abbr in bond:
            try:
                product = Productofficial.objects.get(productabbreviation=abbr)
            except Productofficial.DoesNotExist:
                return CustomResponse('产品不存在:' + abbr)
            if not product:
                continue
            results.append({
                'combo': combo, 'type': "债券型", 'productname': product.name,
                'productabbreviation': product.productabbreviation,
                'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage,
                'crm_id': product.crm_id, 'id': product.id,
            })
    # 流动型
    if flow != ['']:
        for abbr in flow:
            try:
                product = Productofficial.objects.get(productabbreviation=abbr)
            except Productofficial.DoesNotExist:
                return CustomResponse('产品不存在:' + abbr)
            if not product:
                continue
            results.append({
                'combo': combo, 'type': "流动型", 'productname': product.name,
                'productabbreviation': product.productabbreviation,
                'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage,
                'crm_id': product.crm_id, 'id': product.id,
            })

    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def num_2_percentage(num):
    return str(float(num) * 100) + "%"


def generate_yearly_networth(year, real_dates, real_values):
    # 生成坐标
    times = []
    values = []
    for i in range(1, 12 + 1):
        times.append(datetime.date(year, i, 1))
    # 获取今年前n月内最近净值作为本月净值
    for i, time in enumerate(times):
        flag = 0
        last_month_time = time - relativedelta(months=i + 1)
        for n in range(int((time - last_month_time).days)):
            if time - datetime.timedelta(days=n) in real_dates:
                idx = time - datetime.timedelta(days=n)
                values.append(real_values[real_dates.index(idx)])
                flag = 1
                break
        # 如果没有前n月数值，默认为1.0
        if flag == 0:
            values.append(1.0)
    return times, values


def get_combo_value(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    combo = data['combo']
    year = data['year']

    config = configparser.ConfigParser()
    config.read('./combo.ini', encoding='UTF-8')
    if not config:
        return HttpResponseBadRequest('Config Not Found')

    ratio = config[combo]['ratio'].split(',')
    multi = config[combo]['multi'].split(',')
    hedge = config[combo]['hedge'].split(',')
    bond = config[combo]['bond'].split(',')
    flow = config[combo]['flow'].split(',')
    dates = []
    values = []
    monthly_return = []

    # 股票多头型
    if multi != ['']:
        for abbr in multi:
            real_dates = []
            real_values = []
            product = Productofficial.objects.filter(productabbreviation=abbr).first()
            if not product:
                return HttpResponseBadRequest("数据库中未找到组合中产品：" + abbr)
            print(product)
            networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
            for nw in networth:
                real_dates.append(nw.updatedate)
                real_values.append(nw.value)
            dates, generated_networth = generate_yearly_networth(year, real_dates, real_values)
            if not values:
                for n in generated_networth:
                    values.append(float(n) * float(ratio[0]) / len(multi))
            else:
                for i, n in enumerate(generated_networth):
                    values[i] += (float(n) * float(ratio[0]) / len(multi))

    # 对冲型
    if hedge != ['']:
        for abbr in hedge:
            real_dates = []
            real_values = []
            product = Productofficial.objects.filter(productabbreviation=abbr).first()
            if not product:
                return HttpResponseBadRequest("数据库中未找到组合中产品：" + abbr)
            networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
            for nw in networth:
                real_dates.append(nw.updatedate)
                real_values.append(nw.value)
            dates, generated_networth = generate_yearly_networth(year, real_dates, real_values)
            if not values:
                for n in generated_networth:
                    values.append(float(n) * float(ratio[1]) / len(hedge))
            else:
                for i, n in enumerate(generated_networth):
                    values[i] += (float(n) * float(ratio[1]) / len(hedge))

    # 债券型
    if bond != ['']:
        for abbr in bond:
            real_dates = []
            real_values = []
            product = Productofficial.objects.filter(productabbreviation=abbr).first()
            if not product:
                return HttpResponseBadRequest("数据库中未找到组合中产品：" + abbr)
            networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
            for nw in networth:
                real_dates.append(nw.updatedate)
                real_values.append(nw.value)
            dates, generated_networth = generate_yearly_networth(year, real_dates, real_values)
            if not values:
                for n in generated_networth:
                    values.append(float(n) * float(ratio[2]) / len(bond))
            else:
                for i, n in enumerate(generated_networth):
                    values[i] += (float(n) * float(ratio[2]) / len(bond))

    # 流动型
    if flow != ['']:
        for abbr in flow:
            real_dates = []
            real_values = []
            product = Productofficial.objects.filter(productabbreviation=abbr).first()
            if not product:
                return HttpResponseBadRequest("数据库中未找到组合中产品：" + abbr)
            networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
            for nw in networth:
                real_dates.append(nw.updatedate)
                real_values.append(nw.value)
            dates, generated_networth = generate_yearly_networth(year, real_dates, real_values)
            if not values:
                for n in generated_networth:
                    values.append(float(n) * float(ratio[3]) / len(flow))
            else:
                for i, n in enumerate(generated_networth):
                    values[i] += (float(n) * float(ratio[3]) / len(flow))

    for i in range(len(dates)):
        if i == 0:
            monthly_return.append(0.0)
        else:
            monthly_return.append(values[i] / values[0] - 1)

    results = ({
        "dates": dates, "values": values, "monthly_return": monthly_return,
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json", charset='utf-8')


def update_combo_proportion(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    combo = data['combo']
    multi = data['multi']
    hedge = data['hedge']
    bond = data['bond']
    flow = data['flow']
    combo_positions = Combinationposition.objects.filter(responsible_cid=responsible_cid)
    for position in combo_positions:
        combo_info = position.combinationinfo
        if combo_info.name == combo:
            combo_id = combo_info.id
            try:
                multi_ = Combinationproportion.objects.get(combination_id=combo_id, producttype='股票多头型')
                multi_.proportion = multi
                multi_.save()
                hedge_ = Combinationproportion.objects.get(combination_id=combo_id, producttype='对冲型')
                hedge_.proportion = hedge
                hedge_.save()
                bond_ = Combinationproportion.objects.get(combination_id=combo_id, producttype='债券型')
                bond_.proportion = bond
                bond_.save()
                flow_ = Combinationproportion.objects.get(combination_id=combo_id, producttype='流动型')
                flow_.proportion = flow
                flow_.save()
                return HttpResponse("更新自定义信息成功")
            except Combinationproportion.DoesNotExist:
                return CustomResponse('组合比例不存在')
        else:
            return HttpResponseBadRequest('要修改的组合不存在')


def recommend_custom_combo(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    results = []
    combo_positions = Combinationposition.objects.filter(responsible_cid=responsible_cid)
    for position in combo_positions:
        combo_info = position.combinationinfo
        types = []
        ratios = []
        proportions = Combinationproportion.objects.filter(combination=combo_info.id)
        for proportion in proportions:
            types.append(proportion.producttype)
            ratios.append(proportion.proportion)
        results.append({
            "combo": combo_info.name, "types": types, "ratio": ratios
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_custom_type_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    type = data['type']
    proportions = Combinationproportion.objects.filter(producttype=type)
    results = []
    products = []
    for proportion in proportions:
        c_products = Combinationproduct.objects.filter(combinationproportion=proportion.id)
        for c_product in c_products:
            try:
                product = Productofficial.objects.get(crm_id=c_product.product_cid)
            except Productofficial.DoesNotExist:
                return CustomResponse('产品不存在:' + c_product.product_cid)
            if product not in products:
                results.append({
                    'type': type, 'productname': product.name, 'productabbreviation': product.productabbreviation,
                    'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage,
                    'crm_id': product.crm_id, 'id': product.id,
                })
                products.append(product)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_custom_combo_details(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    combo = data['combo']
    responsible_cid = data['responsible_cid']
    results = []

    combo_positions = Combinationposition.objects.filter(responsible_cid=responsible_cid)
    for position in combo_positions:
        combo_info = position.combinationinfo
        if combo_info.name == combo:
            proportions = Combinationproportion.objects.filter(combination=combo_info.id)
            products = []
            for proportion in proportions:
                c_products = Combinationproduct.objects.filter(combinationproportion=proportion.id)
                for c_product in c_products:
                    try:
                        product = Productofficial.objects.get(crm_id=c_product.product_cid)
                    except Productofficial.DoesNotExist:
                        return CustomResponse('产品不存在:' + c_product.product_cid)
                    if product not in products:
                        results.append({
                            'combo': combo, 'type': proportion.producttype, 'productname': product.name,
                            'productabbreviation': product.productabbreviation,
                            'recordtype': product.recordtype, 'value': product.value, 'stage': product.stage,
                            'crm_id': product.crm_id, 'id': product.id,
                        })
                        products.append(product)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def get_custom_combo_value(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    combo = data['combo']
    year = data['year']

    dates = []
    values = []
    monthly_return = []

    combo_positions = Combinationposition.objects.filter(responsible_cid=responsible_cid)
    for position in combo_positions:
        combo_info = position.combinationinfo
        if combo_info.name == combo:
            proportions = Combinationproportion.objects.filter(combination=combo_info.id)
            for proportion in proportions:
                c_products = Combinationproduct.objects.filter(combinationproportion=proportion.id)
                for c_product in c_products:
                    real_dates = []
                    real_values = []
                    try:
                        product = Productofficial.objects.get(crm_id=c_product.product_cid)
                    except Productofficial.DoesNotExist:
                        return CustomResponse('产品不存在:' + c_product.product_cid)
                    if not product:
                        return HttpResponseBadRequest("数据库中未找到组合中产品")
                    networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
                    for nw in networth:
                        real_dates.append(nw.updatedate)
                        real_values.append(nw.value)
                    dates, generated_networth = generate_yearly_networth(year, real_dates, real_values)
                    if not values:
                        for n in generated_networth:
                            values.append(float(n) * float(proportion.proportion) / len(c_products))
                    else:
                        for i, n in enumerate(generated_networth):
                            values[i] += (float(n) * float(proportion.proportion) / len(c_products))

    for i in range(len(dates)):
        if i == 0:
            monthly_return.append(0.0)
        else:
            monthly_return.append(values[i] / values[0] - 1)

    results = ({
        "dates": dates, "values": values, "monthly_return": monthly_return,
    })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json", charset='utf-8')


def add_custom_combo(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    combo = data['combo']
    multi = data['multi']
    hedge = data['hedge']
    bond = data['bond']
    flow = data['flow']

    try:
        combo_info = Combinationinfo.objects.create(name=combo, type="自定义", createdate=timezone.now())
        Combinationposition.objects.create(combinationinfo=combo_info, responsible_cid=responsible_cid)
        c_multi = Combinationproportion.objects.create(combination=combo_info, producttype='股票多头型', proportion=multi)
        c_hedge = Combinationproportion.objects.create(combination=combo_info, producttype='对冲型', proportion=hedge)
        c_bond = Combinationproportion.objects.create(combination=combo_info, producttype='债券型', proportion=bond)
        c_flow = Combinationproportion.objects.create(combination=combo_info, producttype='流动型', proportion=flow)

        if multi != 0:
            Combinationproduct.objects.create(combinationproportion=c_multi, product_cid='a0U6F00000khBvOUAU')  # 瑞智一号
            Combinationproduct.objects.create(combinationproportion=c_multi, product_cid='a0U6F00000kJFBfUAO')  # 和裕霸菱A股
            Combinationproduct.objects.create(combinationproportion=c_multi, product_cid='a0U6F00000cg3pHUAQ')  # 紫荆怒放

        if hedge != 0:
            Combinationproduct.objects.create(combinationproportion=c_hedge, product_cid='a0U6F00000gj9ogUAA')  # 瑞盈5号
            Combinationproduct.objects.create(combinationproportion=c_hedge, product_cid='a0U6F00000cg3mmUAA')  # 保银中国价值

        if bond != 0:
            Combinationproduct.objects.create(combinationproportion=c_bond, product_cid='a0U6F00000dSpdfUAC')  # 瑞华霸菱
            Combinationproduct.objects.create(combinationproportion=c_bond, product_cid='a0U6F00000httGhUAI')  # 瑞盈6号

        if flow != 0:
            Combinationproduct.objects.create(combinationproportion=c_flow, product_cid='a0U6F00000dUOvaUAG')  # 瑞盈4号
        return HttpResponse("新增组合成功")
    except:
        return HttpResponse("新增组合失败")


def delete_custom_combo(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    combo = data['combo']

    try:
        combo_positions = Combinationposition.objects.filter(responsible_cid=responsible_cid)
        for position in combo_positions:
            combo_info = position.combinationinfo
            if combo_info.name == combo:
                proportions = Combinationproportion.objects.filter(combination=combo_info)
                for proportion in proportions:
                    products = Combinationproduct.objects.filter(combinationproportion=proportion)
                    for product in products:
                        product.delete()
                    proportion.delete()
                position.delete()
                combo_info.delete()
        return HttpResponse("删除组合成功")
    except:
        return HttpResponse("删除组合失败")


def rename_custom_combo(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    responsible_cid = data['responsible_cid']
    combo = data['combo']
    new_combo = data['new_combo']

    try:
        combo_positions = Combinationposition.objects.filter(responsible_cid=responsible_cid)
        for position in combo_positions:
            combo_info = position.combinationinfo
            if combo_info.name == combo:
                combo_info.name = new_combo
                combo_info.save()
                return HttpResponse("修改组合名成功")
    except Combinationinfo.DoesNotExist:
        return HttpResponseBadRequest('要修改的组合不存在')


def favor_product(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    product_id = data['product_id']
    today = datetime.datetime.now()
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return CustomResponse('用户不存在' + user_id)
    try:
        product = Productofficial.objects.get(id=product_id)
    except Productofficial.DoesNotExist:
        return CustomResponse('产品不存在' + product_id)
    new_favorite = Favoriteproduct.objects.create(user=user, product=product, adddate=today)
    return HttpResponse('关注成功')


def disfavor_product(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    product_id = data['product_id']
    try:
        favorite = Favoriteproduct.objects.get(user=user_id, product=product_id)
    except Favoriteproduct.DoesNotExist:
        return CustomResponse('关注信息不存在')
    favorite.delete()
    return HttpResponse('取消关注成功')


def get_favorite_status(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    product_id = data['product_id']
    try:
        fproduct = Favoriteproduct.objects.get(user=user_id, product=product_id)
        return HttpResponse('已关注')
    except Favoriteproduct.DoesNotExist:
        return HttpResponse('未关注')


def get_my_favorite_list(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    fproducts = Favoriteproduct.objects.filter(user=user_id)
    results = []
    for p in fproducts:
        try:
            p = Productofficial.objects.get(id=p.product_id)
            year_profit = 0.0
            product_yield = "0.00%"
            # 计算近一年涨幅
            if p.recordtype == '二级市场':
                networth = Networth.objects.filter(product_cid=p.crm_id)
                if networth:
                    year_profit, _ = calculate_year_history_profit(networth)
                    one_year_increase = calculate_one_year_increase_accumulative(networth)
                    if one_year_increase:
                        product_yield = (round(100 * one_year_increase if one_year_increase else 0, 2))
                        if product_yield != 0:
                            product_yield = str(product_yield) + "%"
                        else:
                            product_yield = "0.00%"
            # 计算用户行为
            favors, disfavors, forwardings, checks = count_behavior(p.id)
            results.append({
                'name': p.name,
                'productabbreviation': p.productabbreviation,
                'crm_id': p.crm_id,
                'id': p.id,
                'recordtype': p.recordtype,
                # 股权
                'direction': p.direction,
                'productperiod': p.productperiod,
                # 证券
                'value': p.value,
                'year_profit': year_profit,
                "one_year_increase": product_yield,
                # 类固收
                'comparison': p.comparison,
                'monthlimit': p.monthlimit,
                'comparisonabbreviation': p.comparisonabbreviation,
                'stage': p.stage,
                'net_favors': favors - disfavors,
                'forwardings': forwardings,
                'checks': checks,
            })
        except Productofficial.DoesNotExist:
            return CustomResponse('产品不存在:' + p.product_id)
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def record_user_behavior(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    product_id = data['product_id']
    behavior_type = data['behavior_type']
    now = datetime.datetime.now()
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return CustomResponse('用户不存在' + str(user_id))
    product = Productofficial.objects.filter(id=product_id).first()
    new_behavior = Userbehavior.objects.create(user=user, product=product, behaviortype=behavior_type, time=now)
    result = ({
        'status': "OK",
        'time': now,
    })
    return HttpResponse(json.dumps(result, ensure_ascii=False, cls=CustomEncoder4Time),
                        content_type="application/json", charset='utf-8')


# 投顾-客户详情-客户行为-详情
def search_behavior_by_user_new_info(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    account_crmid = data['account_crmid']
    type = data['type']
    param = {
        "关注产品": "关注",
        "查看产品": "查看",
        "转发产品": "转发",
    }

    if account_crmid == "":
        return ResponseCodeMesData(400, "account_crmid不能为空", "")
    if type == "":
        return ResponseCodeMesData(400, "type不能为空", "")
    try:
        account = Account.objects.get(crm_id=account_crmid)
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "用户不存在", "")

    userbehavior = Userbehavior.objects.filter(behaviortype=param[type], user=account.user_id).order_by('-time')
    result = []
    for b in userbehavior:
        product_name = ""
        product_id = ""
        if b.product:
            product_name = b.product.name
            product_id = b.product.id
        result.append({
            "behaviortype": b.behaviortype + "产品",
            "time": str(b.time),
            "product_name": product_name,
            "product_id": product_id,
        })
    return ResponseCodeMesData(200, "请求成功", result)


# 投顾-客户详情-客户行为
def search_behavior_by_user_new(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    account_crmid = data['account_crmid']
    if account_crmid == "":
        return ResponseCodeMesData(400, "account_crmid不能为空", "")
    try:
        account = Account.objects.get(crm_id=account_crmid)
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "用户不存在", "")
    results = {
        "concern": 0,  # 关注
        "view": 0,  # 查看
        "transmit": 0,  # 转发
    }
    sql = "SELECT  be.BehaviorType type , count(*) sum FROM UserBehavior be where be.User_ID = %s  and " \
          "be.BehaviorType in (%s,%s,%s)  GROUP BY be.BehaviorType" % (account.user_id, "'关注'", "'查看'", "'转发'")
    sqlresult = executeSql(sql)
    if sqlresult != "":
        for i in sqlresult:
            if i[0] == "查看":
                results["view"] = i[1]
            if i[0] == "关注":
                results["concern"] = i[1]
            if i[0] == "转发":
                results["transmit"] = i[1]
    return ResponseCodeMesData(200, "请求成功", results)
    # 产品查看 次数
    # results.append({
    #     'product_id': None,
    #     'product_name': None,
    #     'behavior': 0,
    #     'time': 0,
    # })
    # return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
    #                     charset='utf-8')


# 客户行为
def search_behavior_by_user(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    user_id = data['user_id']
    results = []
    behaviors = Userbehavior.objects.filter(user=user_id)
    for b in behaviors:
        try:
            product_id = ""
            product_name = ""
            if b.product:
                product_id = b.product.id
            if b.product:
                product_name = b.product.name
            results.append({
                'product_id': product_id,
                'product_name': product_name,
                'behavior': b.behaviortype,
                'time': b.time,
            })
        except Productofficial.DoesNotExist:
            results.append({
                'product_id': None,
                'product_name': None,
                'behavior': b.behaviortype,
                'time': b.time,
            })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def search_behavior_by_product(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    product_id = data['product_id']
    results = []
    behaviors = Userbehavior.objects.filter(product=product_id)
    for b in behaviors:
        results.append({
            'user_id': b.user.id,
            'behavior': b.behaviortype,
            'time': b.time,
        })
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder), content_type="application/json",
                        charset='utf-8')


def count_behavior(product_id):
    favors, disfavors, forwardings, checks = 0, 0, 0, 0
    behaviors = Userbehavior.objects.filter(product=product_id)
    for b in behaviors:
        if b.behaviortype == '关注':
            favors += 1
        elif b.behaviortype == '取关':
            disfavors += 1
        elif b.behaviortype == '转发':
            forwardings += 1
        elif b.behaviortype == '查看':
            checks += 1
    return favors, disfavors, forwardings, checks


def pdf_download(request, product_id):
    try:
        product = Productofficial.objects.get(id=product_id)
    except Productofficial.DoesNotExist:
        return HttpResponse("0")
    pdf_url = product.file
    if not pdf_url:
        return HttpResponse("0")
    return redirect(pdf_url)


def upload_file(request, uid):
    import os
    if request.method == 'POST':
        myFile = request.FILES.get("myfile", None)
        print(myFile, '++++++++++++++++++++++')
        if not myFile:
            return HttpResponse('no files for upload!')
        destination = open(os.path.join("D:/upload", myFile.name), 'wb+')

        for chunk in myFile.chunks():
            destination.write(chunk)
            print(destination, '----------------------')
        destination.close()
        return HttpResponse("yes")


def download_file(request, file_name):
    import os
    return HttpResponse(os.getcwd())
    file = '/mnt/static/wechat' + file_name

    print(file, '1111111111111111111111')
    result = open(file, 'rb')
    response = StreamingHttpResponse(result)
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachement;filename="{0}"'.format(file_name)
    return response


def get_product_risk_eval(request):
    data = json.loads(request.body)
    product_id = data['product_id']
    account_type = data['account_type']
    topics_content = []
    options_content = []
    options_score = []
    topics = Productqatopic.objects.filter(product_id=product_id, accounttype=account_type)
    for topic in topics:
        topics_content.append(topic.content)
        options = Productqaoption.objects.filter(topic_id=topic.id)
        op_content = []
        op_score = []
        for op in options:
            op_content.append(op.content)
            op_score.append(op.score)
        options_content.append(op_content)
        options_score.append(op_score)
    results = ({
        'topics': topics_content, 'options_content': options_content, 'options_score': options_score,
    })
    return HttpResponse(json.dumps(results, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def getChannelByUserId(userid):
    user_channel = "无指定渠道"
    try:
        responsible = Responsible.objects.get(user_id=userid)
        if not responsible.channel:
            pass
        else:
            user_channel = responsible.channel
    except Responsible.DoesNotExist:
        try:
            account = Account.objects.get(user_id=userid)
            responsible_cid = account.responsible_id
            if not responsible_cid:
                pass
            else:
                responsible = Responsible.objects.get(crm_id=responsible_cid)
                if not responsible.channel:
                    pass
                else:
                    user_channel = responsible.channel
        except Exception:
            pass
    except Exception:
        pass
    return user_channel


# 私募证券（二级市场） 产品详情
def get_second_level_market(request):
    data = json.loads(request.body)
    product_id = data['product_id']
    account_crmid = data['account_crmid']
    if account_crmid == "":
        return ResponseCodeMesData(400, "account_crmid不能为空", "")
    if product_id == "":
        return ResponseCodeMesData(400, "product_id不能为空", "")
    try:
        product = Productofficial.objects.get(id=product_id)
    except Productofficial.DoesNotExist:
        return ResponseCodeMesData(400, "产品不存在", "")
    try:
        account = Account.objects.get(crm_id=account_crmid)
    except Account.DoesNotExist:
        try:
            account = Responsible.objects.get(crm_id=account_crmid)
        except Responsible.DoesNotExist:
            return ResponseCodeMesData(400, "用户不存在", "")
    # 获取是否关注
    isfavorite = "未关注"
    try:
        fproduct = Favoriteproduct.objects.get(user=account.user_id, product=product_id)
        isfavorite = "已关注"
    except Favoriteproduct.DoesNotExist:
        isfavorite = "未关注"
    # 获取募集账户
    productbank = Productbank.objects.filter(product_cid=product.crm_id, type='募集账户').first()
    if not productbank:
        bankname = "-"
        bankno = "-"
        accountname = "-"
    else:
        bankname = productbank.bankname
        bankno = productbank.bankno
        accountname = productbank.accountname
    product_tag = []
    if product.recordtype == "二级市场":
        product_tag.append("私募证券")
    if product.recordtype == "类固收":
        product_tag.append("定融计划")
    if product.recordtype == "股权":
        product_tag.append("私募股权")
    if product.origin:
        product_tag.append(str(round(product.origin / 10000 , 0) )+ "万起购")
    if product.producttag and product.producttag != "":
        strlist = product.producttag.split(',')
        for value in strlist:
            if value not in product_tag:
                product_tag.append(value)

    # product_file =["A",""]
    result = {
        "raise_bankNo": bankno,  # 募集账户
        "raise_bankName": bankname,
        "raise_accountname": accountname,
        "isfavorite": isfavorite,  # 是否收藏
    }

    # 基金规模 产品规模
    product_scale = ""
    if product.scale:
        product_scale = str(format(math.floor(product.scale / 10000 if product.scale else 0), ','))
        if product_scale != 0:
            product_scale = product_scale + "万元"
    # 认购起点
    product_origin = ""
    if product.origin:
        product_origin = str(format(math.floor(product.origin / 10000 if product.origin else 0), ','))
        if product_origin != 0:
            product_origin = product_origin + "万"
    # 认购费
    product_subscription_rates = formatNumber(product.ratesub)
    if product_subscription_rates != "":
        product_subscription_rates = product_subscription_rates + "%"
    # 管理费
    product_manage_rates = formatNumber(product.admina_admincost)
    if product_manage_rates != "":
        product_manage_rates = product_manage_rates + "%/年"
    result.update({
        "id": product.id,
        "product_name": product.name,  # 产品全称
        "product_abbreviation": product.productabbreviation,  # 产品简称
        "onlinesign": product.onlinesign,  # 线上签署
        "product_tag": product_tag,  # 产品标签
    })

    if product.recordtype == "二级市场":
        # 近一年收益
        networth = Networth.objects.filter(product_cid=product.crm_id).order_by('updatedate')
        try:
            one_year_increase = calculate_one_year_increase_accumulative(networth)
        except Exception as e:
            one_year_increase = 0
        # 近一年收益率
        product_yield = "0.00%"
        if one_year_increase:
            product_yield = (round(100 * one_year_increase if one_year_increase else 0, 2))
            if product_yield != 0:
                product_yield = str(product_yield) + "%"
        # 托管+运营服务费
        product_operatee_rates = formatNumber(product.rateoperation)
        if product_operatee_rates != "":
            product_operatee_rates = product_operatee_rates + "%/年"
        result.update({
            "product_yield": product_yield,  # 近一年收益率
            "product_origin": product_origin,  # 认购起点
            "product_netvalue": str(product.value if product.value else ""),  # 单位净值
            "product_manager": product.adminacus,  # 管理人
            "product_lightspot": product.highlights,  # 产品亮点
            "product_file": product.file,
            "bottom_intro": product.underlyingasset,  # 底层介绍
            # "product_ratesub": "佣金费率",  # 佣金费率 延期
            "product_scale": product_scale,  # 基金规模
            "product_strategy": product.tactics,  # 投资策略
            "product_regulation": product.subrule,  # 认购规则
            "product_subscription_rates": product_subscription_rates,  # 认购费
            "product_manage_rates": product_manage_rates,  # 管理费
            "product_operatee_rates": product_operatee_rates,  # 托管+运营服务费
            "product_redeem_rates": product.redemptionrate,  # 赎回费
            "product_redeem_rule": product.opendaydes,  # 赎回规则
            "product_netvalue_date": str(product.valuedate if product.valuedate else ""),  # 单位净值时间
        })
    if product.recordtype == "类固收":
        product_deadline = ""
        if product.monthlimit:
            product_deadline = str(round(product.monthlimit, 0)) + "个月"
        result.update({
            "product_origin": product_origin,  # 认购起点
            "product_deadline": product_deadline,  # 产品期限
            "product_comparison": product.comparison,  # 业绩比较基准
            "product_comparison2": product.comparisonabbreviation,  # 业绩比较基准2
            "product_regulation": product.subrule,  # 认购规则
            "product_mode_distribution": product.distributionmode,  # 分配方式
            "product_bottom": product.underlyingasset,  # 产品底层
            "product_scale": product_scale,  # 产品规模
            "product_publisher": product.publisher,  # 发行人
            "product_manager": product.adminacus,  # 管理人
            "product_listed_institutions": product.listedorg,  # 挂牌机构
            "product_rco": product.riskcontrolsys,  # 风控体系
            "bottom_intro": product.underlyingassetdes,  # 底层介绍
            "product_file": product.file,# 产品文档
            "origin": product.origin,# 产品起投金额
            "addmin": product.addmin,# 产品最小追加
            "product_crmid": product.crm_id,# 产品最小追加
        })
    if product.recordtype == "股权":
        result.update({
            "product_monthlimit": product.productperiod, # 投资期限
            "product_direction": product.direction, # 投资方向
            "product_manager": product.adminacus,  # 管理人
            "product_lightspot": product.highlights,  # 产品亮点
            "product_scale": product.scale, #基金规模
            "product_regulation": product.subrule,  # 认购规则
            "product_subscription_rates": product_subscription_rates,  # 认购费
            "product_manage_rates": product_manage_rates,  # 管理费
            "product_income": product.perforrewardcus,  # 业绩报酬
            "product_exit_strategy": product.qiutway,  # 退出策略
        })
    return resultCodeAndData(200, "查询成功", result)

def resultCodeAndData(code, msg, results):
    results = {'code': code,
               'msg': msg,
               "data": results
               }
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json,charset=utf-8")


# 订单列表
def get_orders(request):
    data = json.loads(request.body)
    crm_id = data['crm_id']
    user_type = data['user_type']
    if crm_id == "":
        return ResponseCodeMesData(400, "投资人或投顾id不能为空", "")
    if user_type in ['1', '2']:
        return ResponseCodeMesData(400, "user_type暂不支持的类型,仅支持1 投资人,2 投顾", "")

    sql = "SELECT pro.`Name` product_name , pro.productabbreviation,o.ostatus,pro.recordtype,pro.onlinesign,o.money_4," \
          " o.bookmoney,o.createdate,o.date,o.startearningday, o.maturitydate,account.`Name` account_name,res.`Name` " \
          " responsible_name,o.defendrate,o.holdday,o.id ,pro.deaddate ,o.createdate,o.num,o.subscription_fee,pro.productperiod,o.nownet,o.confirmdate," \
          " o.taskid  " \
          "  FROM `Order` o" \
          " left join ProductOfficial pro on pro.CRM_ID = o.Product_CID" \
          " left join Responsible res on res.CRM_ID = o.Responsible_CID" \
          " left join Account account on account.CRM_ID = o.Account_CID" \
          " where "
    sqlparams = []
    # 查询条件拼接
    if "product_type" in data and data["product_type"] != "":
        sql += " pro.recordtype = %s and "
        types = data["product_type"]
        if types == "定融计划":
            types = "类固收"
        sqlparams.append(types)
    if "order_date_start" in data and data["order_date_start"] != "":
        sql += " o.date >= STR_TO_DATE(  %s, '%%Y-%%m-%%d' ) and "
        sqlparams.append(data["order_date_start"])
    if "order_date_end" in data and data["order_date_end"] != "":
        sql += " o.date <= STR_TO_DATE(  %s, '%%Y-%%m-%%d' ) and "
        sqlparams.append(data["order_date_end"])
    if "pro_deaddate_start" in data and data["pro_deaddate_start"] != "":
        sql += " o.maturitydate >= STR_TO_DATE(  %s, '%%Y-%%m-%%d' ) and "
        sqlparams.append(data["pro_deaddate_start"])
    if "pro_deaddate_end" in data and data["pro_deaddate_end"] != "":
        sql += " o.maturitydate <= STR_TO_DATE(  %s, '%%Y-%%m-%%d' )  and "
        sqlparams.append(data["pro_deaddate_end"])
    if "query_key" in data and data["query_key"] != "":
        sql += "( instr(pro.`Name`, %s) or  instr(pro.productabbreviation, %s)  or instr(account.`Name`, %s)  ) and "
        sqlparams.append(data["query_key"])
        sqlparams.append(data["query_key"])
        sqlparams.append(data["query_key"])
    if user_type == 1:
        try:
            account = Account.objects.get(crm_id=crm_id)
            sql += " o.Account_CID = '" + account.crm_id + "'"
        except Account.DoesNotExist:
            return ResponseCodeMesData(400, "用户信息不存在", "")
    else:
        try:
            responsible = Responsible.objects.get(crm_id=crm_id)
            sql += "  o.Responsible_CID = '" + responsible.crm_id + "'"
        except Responsible.DoesNotExist:
            return ResponseCodeMesData(400, "投顾信息不存在", "")

    orders = executeSqlParams(sql, sqlparams)
    results = []
    for order in orders:
        product_type = "定融计划"
        if order[3] == "股权":
            product_type = "私募股权"
        elif order[3] == "二级市场":
            product_type = "私募证券"
        isonline_finish = "false"
        if order[4] == 1:
            # 判断订单是否完成电子合同签署 o.taskid
            if order[23] != "" and order[23]:
                fddSignTaskSignTask = Signtask.objects.filter(order=order[15], type=0)
                if len(fddSignTaskSignTask) > 0:
                    isonline_finish = "true"
        order_money_4 = handleNum(order[5])
        order_sub_money = handleNum(order[6])
        order_order_num = handleNum(order[18])
        order_subscription_fee = handleNum(order[19])
        order_defendrate = order[13]
        if order_defendrate:
            order_defendrate = str(order_defendrate) + "%"
        order_holdday = order[14]
        if order_holdday:
            order_holdday = str(order_holdday) + "天"
        results.append({
            "product_name": order[0],
            "product_abbreviation": order[1],  # 产品简称
            "order_ostatus": order[2],  # 订单状态
            "product_type": product_type,  # 产品类型 私募证券
            "isonline": order[4],  # 是否为电子合同 1 是 0 否
            "order_money_4": order_money_4,  # 认购本金
            "order_sub_money": order_sub_money,  # 预约金额
            "order_sub_date": str(order[7]),  # 预约日期
            "order_date": order[8],  # 到账日期
            "order_value_date": order[9],  # 起息日期
            "order_maturitydate": order[10],  # 到期日期
            "order_account_name": order[11],  # 投资人
            "order_responsible": order[12],  # 投资顾问
            "order_defendrate": order_defendrate,  # 业绩比较基准
            "order_holdday": order_holdday,  # 存续天数
            "order_id": order[15],  #
            "pro_deaddate": order[16],  #
            "order_createdate": order[17],  # 创建时间
            "order_order_num": order_order_num,  # 确认份额
            "order_subscription_fee": order_subscription_fee,  # 认购费
            "product_monthlimit": order[20],  # 投资期限
            "order_nownet": handleNum(order[21]),  # 认购净值
            "order_confirmdate": order[22],  # 认购日期
            "isonline_finish": isonline_finish,  # 是否完成电子合同签约
        })
    results = sorted(results, key=itemgetter('order_createdate', 'order_id'), reverse=True)
    results = {'code': 200,
               'msg': "请求成功",
               "data": results
               }
    return HttpResponse(json.dumps(results, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json,charset=utf-8")


# 千分符处理 保留两位小数
def handleNum(num):
    if num:
        num = format(round(num if num else 0, 2), ',')
    return num


# 订单详情
def get_order_info(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    user_type = data['user_type']
    if user_type == "":
        return ResponseCodeMesData(400, "用户类型不能为空", "")
    if user_type in ["1", "2"]:
        return ResponseCodeMesData(400, "用户类型仅支持1,2;1投资人,2投顾", "")
    if order_id == "":
        return ResponseCodeMesData(400, "order_id不能为空", "")
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return ResponseCodeMesData(400, "订单不存在", "")
    try:
        product = Productofficial.objects.get(crm_id=order.product_cid)
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "订单产品信息不存在", "")
    try:
        order_accout = Account.objects.get(crm_id=order.account_cid)
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "订单用户信息不存在", "")
    try:
        order_responsible = Responsible.objects.get(crm_id=order.responsible_cid)
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "订单投顾信息不存在", "")
    isonline_finish = "false"
    if product.onlinesign == 1:
        # 判断订单是否完成电子合同签署
        if order.taskid != "" and order.taskid:
            fddSignTaskSignTask = Signtask.objects.filter(order=order_id, type=0)
            if len(fddSignTaskSignTask) > 0:
                isonline_finish = "true"
    product_type = "定融计划"
    if product.recordtype == "股权":
        product_type = "私募股权"
    elif product.recordtype == "二级市场":
        product_type = "私募证券"
    if order_accout.recordtypeid == '0126F0000020pRJQAY':
        recordtype = '个人'
    else:
        recordtype = '机构'
    # 数据格式处理
    # 认购本金
    order_money_4 = handleNum(order.money_4)
    # 预约金额
    order_sub_money = handleNum(order.bookmoney)
    # 转账金额
    order_buymoney = handleNum(order.buymoney)
    # 确认份额
    order_num = handleNum(order.num)
    # 到账金额
    order_buymoney_end = handleNum(order.buymoney)
    # 认购费
    order_subscription_fee = handleNum(order.subscription_fee)
    # 认购费率
    product_redemptionrate = product.redemptionrate
    # 销售佣金
    order_commissionformula = ""
    # 佣金系数
    order_commissionformula_lv = ""
    if user_type == 2:
        order_commissionformula = handleNum(order.commissionformula)
        order_commissionformula_lv = order.commissionrate
        if order_commissionformula_lv:
            order_commissionformula_lv = str(order_commissionformula_lv) + "%"
    order_final_state = ['打款审批完成', '打款审批中', '资料审核中', '资料审核驳回', '认购成功']
    # 预约成功
    result = {
        "order_id": order_id,
        "product_risklevel": product.risklevel,
        "account_recordtype": order_accout.risklevel,
        "order_account_type": recordtype,
        "account_id": order_accout.id,
        "product_id": product.id,
        "product_origin": product.origin,
        "product_fadd_con_task_id": order.taskid,
        "product_fadd_agree_task_id": order.agreementtaskid,
        "product_addmin": product.addmin,
        "product_name": product.name,  # 产品全称
        "product_abbreviation": product.productabbreviation,  # 产品简称
        "order_ostatus": order.ostatus,  # 订单状态
        "product_type": product_type,  # 产品类型 私募证券
        "order_account_name": order_accout.name,  # 投资人
        "order_responsible": order_responsible.name,  # 投资顾问
        "order_sub_money": order_sub_money,  # 预约金额
        "order_sub_date": str(order.createdate),  # 预约日期
        "order_commissionformula_lv": order_commissionformula_lv,  # 佣金系数
        "order_orderno": order.orderno,  # 订单号
        "isonline": product.onlinesign,  # 是否为电子合同 0 否 1是
        "isonline_finish": isonline_finish,  # 是否完成电子合同签署
        "issignagreement": product.agreementsign,  # 补充协议 0 无 1 有
        "issignagreement_state": order.issignagreement == 1,  # 是否完成补充协议签署
        "is_order_finish_state": order.ostatus in order_final_state,  # 是否完成补充协议签署
    }

    if product.recordtype == "类固收":
        # 预约成功 补充字段
        order_defendrate = order.defendrate
        if order_defendrate:
            order_defendrate = str(order.defendrate) + "%"
        result.update({"order_defendrate": order_defendrate})  # 业绩比较基准
        if order.ostatus == "待打款" or order.ostatus == "打款中":
            # 待打款
            result.update({
                "order_money_4": order_money_4,  # 认购本金
                "order_buymoney": order_buymoney,  # 转账金额
                # "order_subscription_fee_lv": "XXXXXXXXXXX",  # 认购费率
                # "order_subscription_fee": order.subscription_fee,  # 认购费
            })
        if order.ostatus in order_final_state:
            # 存储天数
            order_holdday = order.holdday
            if order_holdday:
                order_holdday = str(order_holdday) + "天"
            # 预期收益（MaturityDate-tartEarningDay）/365*Money_4*DefendRate 、（到期日-起息日）/365*认购本金*业绩比较基准
            order_prospective_earnings = ""
            if order.maturitydate and order.startearningday and order.money_4 and order.defendrate:
                order_prospective_earnings = handleNum(
                    decimal.Decimal((order.maturitydate - order.startearningday).days) /
                    365 * order.money_4 * (order.defendrate / 100))
            # 打款完成
            result.update({
                "order_money_4": order_money_4,  # 认购本金
                "order_date": order.date,  # 到账日期
                "order_value_date": order.startearningday,  # 起息日期
                "order_maturitydate": order.maturitydate,  # 到期日期
                "order_holdday": order_holdday,  # 存续天数
                "order_prospective_earnings": order_prospective_earnings,  # 预期收益
                "order_commissionformula": order_commissionformula,  # 销售佣金
            })
    if product.recordtype == "二级市场":
        # if order.ostatus == "预约成功":
        #     result.update({
        #         # 预约订单
        #         "order_defendrate": order.defendrate,  # 业绩比较基准
        #         # 待打款
        #         "order_buymoney": order.buymoney,  # 转账金额
        #         # 打款完成
        #     })
        if order.ostatus == "待打款" or order.ostatus == "打款中":
            result.update({
                "order_money_4": order_money_4,  # 认购本金
                "order_buymoney": order_buymoney,  # 转账金额
                "order_subscription_fee_lv": product_redemptionrate,  # 认购费率
                "order_subscription_fee": order_subscription_fee,  # 认购费
            })
        if order.ostatus in order_final_state:
            result.update({
                "order_money_4": order_money_4,  # 认购本金
                "order_date": order.date,  # 到账日期
                "order_buymoney_end": order_buymoney_end,  # 到账金额
                "order_subscription_fee_lv": product_redemptionrate,  # 认购费率
                "order_confirmdate": order.confirmdate,  # 认购日期
                "order_nownet": order.nownet,  # 认购净值
                "order_num": order_num,  # 确认份额
                "order_subscription_fee": order_subscription_fee,  # 认购费
                "order_commissionformula": order_commissionformula,  # 销售佣金
            })
    if product.recordtype == "股权":
        # 预约成功 补充字段
        result.update({
            "product_direction": product.direction,  # 投资方向
            "product_monthlimit": product.productperiod,  # 投资期限
        })
        if order.ostatus == "待打款" or order.ostatus == "打款中":
            result.update({
                "order_money_4": order_money_4,  # 认购本金
                "order_buymoney": order_buymoney,  # 转账金额
                "order_subscription_fee_lv": product_redemptionrate,  # 认购费率
                "order_subscription_fee": order.subscription_fee,  # 认购费
            })
        if order.ostatus in order_final_state:
            result.update({
                "order_money_4": order_money_4,  # 认购本金
                "order_date": order.date,  # 到账日期
                "order_buymoney_end": order_buymoney_end,  # 到账金额
                "order_nownet": order.nownet,  # 认购净值
                "order_num": order_num,  # 确认份额
                "order_subscription_fee_lv": product_redemptionrate,  # 认购费率
                "order_subscription_fee": order.subscription_fee,  # 认购费
                "order_commissionformula": order_commissionformula,  # 销售佣金
            })
    resultTable = {
        # "product_name": "产品全称",
        # "product_abbreviation": "产品简称",
        # "order_ostatus": "订单状态",
        # "product_type": "产品类型",
        "order_account_name": "投资人",
        "order_responsible": "投资顾问",
        "order_sub_money": "预约金额",
        "order_sub_date": "预约日期",
        "product_direction": "投资方向",
        "product_monthlimit": "投资期限",
        "order_date": "到账日期",
        "order_value_date": "起息日期",
        "order_maturitydate": "到期日期",
        "order_buymoney_end": "到账金额",
        "order_buymoney": "转账金额",
        "order_confirmdate": "认购日期",
        "order_subscription_fee_lv": "认购费率",
        "order_subscription_fee": "认购费",
        "order_nownet": "认购净值",
        "order_num": "确认份额",
        "order_holdday": "存续天数",
        "order_defendrate": "业绩比较基准",
        "order_prospective_earnings": "预期收益",
        # "order_money_4": "认购本金",
        # "order_commissionformula_lv": "佣金系数",
        # "order_commissionformula": "销售佣金",
        "order_commissionformula_lv": "佣金系数" if (user_type == 2) else "",
        "order_commissionformula": "销售佣金" if (user_type == 2) else "",
        "order_orderno": "订单号",
    }

    resultNew = {
        "result": result,
        "resultTable": resultTable
    }
    return HttpResponse(json.dumps({'code': 200,
                                    'msg': "请求成功",
                                    "data": resultNew
                                    }, ensure_ascii=False, cls=CustomEncoder),
                        content_type="application/json,charset=utf-8")


# 处理1.00 -> 1
def formatNumber(param):
    if param and param != "":
        param = str(param)
        param = param.rstrip('0')
        if param[-1] == ".":
            param = param.rstrip('.')
    else:
        param = ""
    return param


def ResponseCodeMesData(code, msg, data):
    result = {'code': code,
              'msg': msg,
              "data": data
              }
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


def executeSqlParams(sql, Params):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()
    results = ""
    try:
        # 执行sql语句
        cursor.execute(sql, Params)
        results = cursor.fetchall()
        # 提交到数据库执行
        db.commit()
    except Exception as e:
        print(e)
        #     # 如果发生错误则回滚
        db.rollback()
    '''
    sql = "SELECT * FROM scc_5.Order where ID = %s" % order_id
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 获取所有记录列表
        results = cursor.fetchall()
        for row in results:
            print(row)
    except:
        print("Error: unable to fetch data")
    '''
    # 关闭数据库连接
    db.close()
    return results


def executeSql(sql):
    db = pymysql.connect(host=DATABASES['default']['HOST'],
                         database=DATABASES['default']['NAME'],
                         user=DATABASES['default']['USER'],
                         password=DATABASES['default']['PASSWORD'],
                         port=int(DATABASES['default']['PORT']), )
    cursor = db.cursor()
    results = ""
    try:
        # 执行sql语句
        cursor.execute(sql)
        results = cursor.fetchall()
        # 提交到数据库执行
        db.commit()
    except:
        # 如果发生错误则回滚
        db.rollback()
    '''
    sql = "SELECT * FROM scc_5.Order where ID = %s" % order_id
    try:
        # 执行SQL语句
        cursor.execute(sql)
        # 获取所有记录列表
        results = cursor.fetchall()
        for row in results:
            print(row)
    except:
        print("Error: unable to fetch data")
    '''
    # 关闭数据库连接
    db.close()
    return results
