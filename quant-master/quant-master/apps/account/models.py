from django.db import models
from mongoengine import *

# Create your models here.
connect('data', host='47.108.25.113', port=29999, username='test', password='8035test')


class Account(Document):
    _id = ObjectIdField()
    password = StringField()
    phone = IntField(unique=True)
    age = IntField()
    occupation = StringField()
    gender = StringField()
    total_assets = IntField()
    risk_res = StringField()
    risk_score = ListField()
    meta = {'collection': 'Account', 'managed': 'False'}


class Questionnaire(Document):
    topics_content = StringField()
    options_content = ListField()
    options_score = ListField()
    meta = {'collection': 'Questionnaire'}
