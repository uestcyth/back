import json
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render
from .models import *

import datetime
def test(request):
    return HttpResponse('test00003')

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, object):
            return str(o)
        return json.JSONEncoder.default(self, o)


# 注册
def register(request):
    msg = json.loads(request.body)
    password = msg['password']
    phone = msg['phone']
    if Account.objects.filter(phone=phone).count() == 0:
        try:
            account = Account(password=password, phone=phone, age=0, occupation='', gender='', total_assets=0,
                              risk_res='',
                              risk_score=[])
            account.save()
            new_account = Account.objects.get(password=password, phone=phone)
            res = {'code': 0, 'msg': '注册成功！', 'data': {'account_id': new_account._id}}
            return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
        except:
            res = {'code': -1, 'msg': '注册失败！', 'data': None}
            return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
    else:
        res = {'code': -1, 'msg': '该手机号已注册！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")


# 获取账户信息
def get_account_info(request):
    msg = json.loads(request.body)
    _id = msg['account_id']
    try:
        account = Account.objects.get(_id=_id)
        result = {'account_id': _id, 'phone': account.phone, 'age': account.age,
                  'occupation': account.occupation, 'gender': account.gender, 'total_assets': account.total_assets,
                  'risk_res': account.risk_res, 'risk_score': account.risk_score}
        res = {'code': 0, 'msg': '查询成功！', 'data': result}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
    except:
        res = {'code': -1, 'msg': '查询失败！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")


# 资产信息更新
def assets_update(request):
    msg = json.loads(request.body)
    _id = msg['account_id']
    try:
        account = Account.objects.get(_id=_id)
        account.total_assets = msg['total_assets']
        account.save()
        res = {'code': 0, 'msg': '更新成功！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
    except:
        res = {'code': -1, 'msg': '更新失败！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")


# 登录
def login(request):
    msg = json.loads(request.body)
    password = msg['password']
    phone = msg['phone']
    try:
        account = Account.objects.get(phone=phone)
        if account.password == password:
            result = {'account_id': account._id, 'phone': account.phone, 'age': account.age,
                      'occupation': account.occupation, 'gender': account.gender, 'total_assets': account.total_assets,
                      'risk_res': account.risk_res, 'risk_score': account.risk_score}
            res = {'code': 0, 'msg': '登录成功！', 'data': result}
            return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
        else:
            res = {'code': -1, 'msg': '用户名与手机号不匹配，登陆失败！', 'data': None}
            return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
    except:
        res = {'code': -1, 'msg': '该手机号不存在，请注册！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")


# 注册详细信息
def register_image(request):
    msg = json.loads(request.body)
    _id = msg['account_id']
    age = msg['age']
    occupation = msg['occupation']
    gender = msg['gender']
    try:
        account = Account.objects.get(_id=_id)
        account.age = age
        account.occupation = occupation
        account.gender = gender
        account.save()
        res = {'code': 0, 'msg': '存储成功！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")
    except:
        res = {'code': -1, 'msg': '存储失败！', 'data': None}
        return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")


# 风险测评
def risk_eval(request):
    result = 0
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest('Request Error')
    ops = data['option']
    for op in ops:
        score = op
        result += int(score)
    if 10 <= result < 22:
        type = "保守型"
    elif 22 <= result < 35:
        type = "稳健型"
    elif 35 <= result <= 48:
        type = "进取型"
    else:
        type = "问卷调查失败"
    _id = data['account_id']
    account = Account.objects.get(_id=_id)
    account.risk_res = type
    account.risk_score = ops
    account.save()
    res = {'code': 0, 'msg': '', 'data': type}
    return HttpResponse(json.dumps(res, cls=JSONEncoder), content_type="application/json,charset=utf-8")


# 获取风险评估信息表
def get_risk_eval(request):
    topics_content = []
    options_content = []
    options_score = []
    topics = Questionnaire.objects.all()
    for topic in topics:
        topics_content.append(topic.topics_content)
        options_content.append(topic.options_content)
        options_score.append(topic.options_score)
    results = ({
        'topics': topics_content, 'options_content': options_content, 'options_score': options_score
    })
    return HttpResponse(json.dumps(results), content_type="application/json,charset=utf-8")


def create_risk_eval(request):
    Questionnaire.objects.create(topics_content='您的家庭可支配年收入为（折合成人民币）？', options_content=[
        "50 万元以下",
        "50 万至 100 万元",
        "100 万至 500 万元",
        "500 万至 1000 万元",
        "1000 万元以上"
    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    Questionnaire.objects.create(topics_content='在您每年的家庭可支配收入中，可用于金融投资的比例为？', options_content=[
        "小于 10%",
        "10%至 25%",
        "25%至 50%",
        "大于50%"
    ], options_score=[
        1,
        2,
        3,
        4
    ])
    Questionnaire.objects.create(topics_content='您的投资知识可描述为：', options_content=[
        "没有：毫无金融资产交易方面的知识",
        "有一些：会看一些资产交易、投资理财的文章",
        "一般：对金融资产交易及其风险有基本的认识",
        "丰富：系统学习了解过金融资产交易及其风险",
        "精通：从事相关工作，每天都接触并运用"

    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    Questionnaire.objects.create(topics_content='您的投资经验可描述为：', options_content=[
        "除银行储蓄外，基本没有其他投资经验",
        "购买过债券、保险等理财产品",
        "购买过非保本类结构投资产品",
        "参与过股票、基金等产品的交易",
        "参与过权证、期货、期权等产品的交易"

    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    Questionnaire.objects.create(topics_content='您有多少年投资基金、股票、信托、私募证券或金融衍生产品等风险投资品的经验？', options_content=[
        "没有经验",
        "少于 2 年",
        "2 至 5 年",
        "5 至 10 年",
        "10 年以上"

    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    Questionnaire.objects.create(topics_content='您计划的投资期限是多久？', options_content=[
        "1 年以内",
        "1 年至 3 年",
        "3 年至 5 年",
        "5 年至 7 年",
        "7 年以上"

    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    Questionnaire.objects.create(topics_content='您是否有尚未偿清的数额较大的债务，如有，其性质是？', options_content=[
        "有，住房抵押贷款等长期定额债务",
        "有，信用卡欠款，消费信贷等短期债务",
        "有，亲戚朋友借款",
        "无"

    ], options_score=[
        1,
        2,
        3,
        4
    ])
    Questionnaire.objects.create(topics_content='您认为自己能承受的最大投资损失是多少？', options_content=[
        "10%以内",
        "10%-20%",
        "20%-40%",
        "40%-60%",
        "超过 60%"

    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    Questionnaire.objects.create(topics_content='假设有两种投资：投资 A 预期获得 10%的收益，可能承担的损失非常小；投资B预期获得30%的收益，但承担较大亏损。您会怎么支配您的投资：',
                                 options_content=[
                                     "全部投资于收益较小且风险较小的 A",
                                     "同时投资于 A 和 B，但大部分资金投资于 A",
                                     "投资 A 和 B 的资金各占一半",
                                     "同时投资于 A 和 B，但大部分资金投资于 B",
                                     "全部投资于收益较大且风险较大的 B"

                                 ], options_score=[
            1,
            2,
            3,
            4,
            5
        ])
    Questionnaire.objects.create(topics_content='以下哪项描述最符合您的投资态度？', options_content=[
        "厌恶风险，希望本金不损失，并获得稳定回报",
        "保守投资，不希望本金损失，愿意承担一定幅度的收益波动",
        "寻求资金的较高收益和成长性，愿意为此承担有限本金损失",
        "希望赚取高回报，愿意为此承担较大本金损失",
        "对于坚定看好的项目，本金全部亏损也能承受"

    ], options_score=[
        1,
        2,
        3,
        4,
        5
    ])
    return HttpResponse('ok')
