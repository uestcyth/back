from django.urls import path

from . import views

app_name = 'account'
urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('create_risk_eval/', views.create_risk_eval, name='create_risk_eval'),
    path('get_risk_eval/', views.get_risk_eval, name='get_risk_eval'),
    path('risk_eval/', views.risk_eval, name='risk_eval'),
    path('register_image/', views.register_image, name='register_image'),
    path('get_account_info/', views.get_account_info, name='get_account_info'),
    path('assets_update/', views.assets_update, name='assets_update'),
    path('test/', views.test, name='test'),
]
