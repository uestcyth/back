from django.db import models
from mongoengine import *

# Create your models here.
connect('data', host='47.108.25.113', port=29999, username='test', password='8035test')


class fund(Document):
    _id = ObjectIdField()
    display_name = StringField()
    name = StringField()
    start_date = IntField()
    end_date = IntField()
    type = StringField()
    code = StringField()
    meta = {'collection': 'fund', 'managed': 'False'}


class fund_info(Document):
    _id = ObjectIdField()
    id = IntField()
    main_code = StringField()
    name = StringField()
    advisor = StringField()
    trustee = StringField()
    operate_mode_id = IntField()
    operate_mode = StringField()
    underlying_asset_type_id = IntField()
    underlying_asset_type = StringField()
    start_date = IntField()
    end_date = IntField()
    meta = {'collection': 'fund_info', 'managed': 'False'}


class fund_net_value(Document):
    _id = ObjectIdField()
    id = IntField()
    code = StringField()
    day = StringField()
    net_value = FloatField()
    sum_value = FloatField()
    factor = FloatField()
    acc_factor = FloatField()
    refactor_net_value = FloatField()
    meta = {'collection': 'fund_net_value', 'managed': 'False'}


class Historical_configuration(Document):
    _id = ObjectIdField()
    his_id = IntField()
    configuration = ListField()
    time = DateField()
    assets = IntField()
    account_id = StringField()
    meta = {'collection': 'Historical_configuration'}


class Account(Document):
    _id = ObjectIdField()
    password = StringField()
    phone = IntField(unique=True)
    age = IntField()
    occupation = StringField()
    gender = StringField()
    total_assets = IntField()
    risk_res = StringField()
    risk_score = ListField()
    meta = {'collection': 'Account', 'managed': 'False'}


class bond_grade(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'bond_grade', 'managed': 'False'}


class stock_grade(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'stock_grade', 'managed': 'False'}
    


class stock_gradeQH(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'stock_gradeQH', 'managed': 'False'}

class bond_gradeQH(Document):
    _id = ObjectIdField()
    code = StringField()
    grade = FloatField()
    meta = {'collection': 'bond_gradeQH', 'managed': 'False'}