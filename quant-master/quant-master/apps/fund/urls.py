from django.urls import path

from . import views

app_name = 'fund'
urlpatterns = [

    path('get_fund_list/', views.get_fund_list, name='get_fund_list'),
    path('get_fund_detail/', views.get_fund_detail, name='get_fund_detail'),
    path('configure_list/', views.configure_list, name='configure_list'),
    path('get_configure_detail/', views.get_configure_detail, name='get_configure_detail'),
    path('testing_configure/', views.testing_configure, name='testing_configure'),
    path('do_backtest_annualized/', views.do_backtest_annualized, name='do_backtest_annualized'),
    path('get_fund_rec/', views.get_fund_rec, name='get_fund_rec'),


]
