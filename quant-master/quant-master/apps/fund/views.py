import json
from django.http import HttpResponse
from django.shortcuts import render
from .models import *
from Backtest import ComputeAnnualizedYield_fund
import numpy as np
# import time as ti
import datetime


# import threading
# from queue import Queue


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, (datetime.datetime, datetime.date)):
            return o.strftime('%Y-%m-%d')
        if isinstance(o, object):
            return str(o)
        super(CustomEncoder, self).default(o)


# test
def do_backtest_annualized(request):
    msg = json.loads(request.body)
    fund = msg['fund_list']
    i = 600
    num = []
    while (i > 0):
        num.append(fund[1])
        i = i - 1
    num.append(fund[0])
    test = ComputeAnnualizedYield_fund.compute_annualized_yield_fund(fund[0], num, '2019-01-01', '2020-01-01')
    return HttpResponse(json.dumps(test, cls=CustomEncoder), content_type="application/json,charset=utf-8")

#获取推荐基金列表
def get_fund_rec(request):
    msg = json.loads(request.body)
    type = msg['type']
    if type == '债券型':
        funds = bond_grade.objects.all()[:10]
    elif type == '股票型':
        funds = stock_grade.objects.all()[:10]
    result = []
    for j in funds:
        i = fund_info.objects.get(main_code=j.code)
        net_value_dict = fund_net_value.objects.filter(code=j.code).order_by('-day').first()
        if net_value_dict == None:
            pass  # 588090
        else:
            net_value = net_value_dict.net_value
            sum_value = net_value_dict.sum_value
            day = net_value_dict.day
            result.append(
                {'main_code': j.code, 'name': i.name, 'advisor': i.advisor, 'operate_mode': i.operate_mode,
                 'underlying_asset_type': i.underlying_asset_type, 'net_value': net_value, 'sum_value': sum_value,
                 'day': day, 'grade': j.grade})
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")

#获取全部基金列表
def get_fund_list(request):
    msg = json.loads(request.body)
    page = msg['page']
    count = fund_info.objects.filter(end_date=None).count()
    count = int(count / 15) + 1
    first = 15 * (page - 1)
    last = 15 * page
    funds = fund_info.objects.filter(end_date=None)[first:last]
    result = [{'page_count': count}]
    for i in funds:
        net_value_dict = fund_net_value.objects.filter(code=i.main_code).order_by('-day').first()
        if net_value_dict == None:
            pass  # 588090
        else:
            net_value = net_value_dict.net_value
            sum_value = net_value_dict.sum_value
            day = net_value_dict.day
            result.append(
                {'main_code': i.main_code, 'name': i.name, 'advisor': i.advisor, 'operate_mode': i.operate_mode,
                 'underlying_asset_type': i.underlying_asset_type, 'net_value': net_value, 'sum_value': sum_value,
                 'day': day})
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")

#获取某基金详情
def get_fund_detail(request):
    msg = json.loads(request.body)
    main_code = msg['main_code']
    fund = fund_info.objects.get(main_code=main_code)
    net_value_dict = fund_net_value.objects.filter(code=fund.main_code).order_by('-day').first()
    net_value = net_value_dict.net_value
    sum_value = net_value_dict.sum_value
    day = net_value_dict.day

    fund_list = [main_code]
    i = 600
    num = []
    while (i > 0):
        num.append([100])
        i = i - 1
    num.append(fund_list)
    test = ComputeAnnualizedYield_fund.compute_annualized_yield_fund(fund_list, num, '2019-01-01', '2020-01-01')
    result = {'main_code': fund.main_code, 'name': fund.name, 'advisor': fund.advisor, 'trustee': fund.trustee,
              'operate_mode': fund.operate_mode,
              'underlying_asset_type': fund.underlying_asset_type, 'net_value': net_value, 'sum_value': sum_value,
              'day': day, 'annualized_test': test}
    return HttpResponse(json.dumps(result, cls=CustomEncoder), content_type="application/json,charset=utf-8")


def get_smallest_vol(ret_cov_matrix):
    """找出所有资产中最小的波动率；注意是返回σ，要开根号"""
    diags = np.diag(ret_cov_matrix)
    return np.sqrt(np.min(diags))


def max_ret(number, ret_ave_vector, ret_cov_matrix, target_vol):
    """
       number是股票个数；ret_ave_vector是股票的收益率向量，注意是列向量；
       ret_cov_matrix是股票收益率的协方差矩阵；
       target_vol是目标波动率，注意是σ。
    """
    ret_ave_vector = np.mat(ret_ave_vector)
    ret_cov_matrix = np.mat(ret_cov_matrix)
    # print(ret_cov_matrix, ret_ave_vector)
    ret_cov_matrix_I = ret_cov_matrix.I

    e = np.array([1] * number).reshape((-1, 1))
    a = float(np.dot(np.dot(ret_ave_vector.T, ret_cov_matrix_I), ret_ave_vector))
    b = float(np.dot(np.dot(ret_ave_vector.T, ret_cov_matrix_I), e))
    c = float(np.dot(np.dot(e.T, ret_cov_matrix_I), e))
    A = (target_vol * c) ** 2 - c
    B = 2 * b * (1 - c * target_vol ** 2)
    C = (target_vol * b) ** 2 - a
    delta = B ** 2 - 4 * A * C

    lambda21 = 0.5 / A * (- B + np.sqrt(delta))
    lambda22 = 0.5 / A * (- B - np.sqrt(delta))
    lambda11 = (b - c * lambda21) / 2
    lambda12 = (b - c * lambda22) / 2

    weights1 = np.dot(ret_cov_matrix_I, (ret_ave_vector - lambda21 * e)) / (2 * lambda11)
    weights2 = np.dot(ret_cov_matrix_I, (ret_ave_vector - lambda22 * e)) / (2 * lambda12)

    return weights1, weights2

#获取配置基金
def get_fund(list, por, key):
    fund = []
    print(list)
    fund.append(list['type_list'][0])
    for j in range(len(por)):
        if list['type_list'][0][j] == '债券型':
            if key == 0:
                fund_data = bond_grade.objects.all()[:por[j]]
            else:
                fund_data = bond_gradeQH.objects.all()[:por[j]]

        elif list['type_list'][0][j] == '股票型':
            if key == 0:
                fund_data = stock_grade.objects.all()[:por[j]]
            else:
                fund_data = stock_gradeQH.objects.all()[:por[j]]
        else:
            return -1
        arg = []
        fund.append(arg)
        for k in fund_data:
            print(k.code)
            fund[j + 1].append(k.code)

    return fund

#平均
def compute_rate(list, key):
    POR_NUM = 100000
    por = []  # por记录
    print(list)
    for i in range(len(list['type_list'][1])):
        number = list['type_list'][1][i] / 100
        if list['assets'] * number <= POR_NUM:
            por.append(1)
        elif list['assets'] * number > POR_NUM and list['assets'] * number < POR_NUM * 10:
            por.append(int(list['assets'] * number / POR_NUM))
        else:
            por.append(12)
    fund = get_fund(list, por, key)
    for z in range(len(list['type_list'][1])):
        y = 0
        fund_por = []
        while (y < por[z]):
            fund_por.append(list['type_list'][1][z] / por[z])
            y = y + 1
        fund.append(fund_por)
    print(fund)
    return fund

#等差
def compute_rate_arr(list, key):
    POR_NUM = 100000
    por = []  # por记录
    print(list)
    for i in range(len(list['type_list'][1])):
        number = list['type_list'][1][i] / 100
        if list['assets'] * number <= POR_NUM:
            por.append(1)
        elif list['assets'] * number > POR_NUM and list['assets'] * number < POR_NUM * 10:
            por.append(int(list['assets'] * number / POR_NUM))
        else:
            por.append(12)
    print(por)
    rate_p = []
    for z in range(len(por)):
        por_sum = 0
        y = 1
        while y < por[z] + 1:
            print(por[z])
            por_sum = ((y + 1) * y) / 2
            print(por_sum)
            y = y + 1
        rate_p.append(por_sum)
    print(rate_p)
    fund = get_fund(list, por, key)

    for z in range(len(list['type_list'][1])):
        y = 0
        fund_por = []
        while (y < por[z]):
            fund_por.append((list['type_list'][1][z]) * (por[z] - y) / rate_p[z])
            y = y + 1
        fund.append(fund_por)
    return fund

#均值方差模型
def example(list, key, tar):
    POR_NUM = 100000
    por = []
    # print(list)
    for i in range(len(list['type_list'][1])):
        number = list['type_list'][1][i] / 100
        if list['assets'] * number <= POR_NUM:
            por.append(1)
        elif list['assets'] * number > POR_NUM and list['assets'] * number < POR_NUM * 6:
            por.append(int(list['assets'] * number / POR_NUM))
        else:
            por.append(6)
    fund = get_fund(list, por, key)
    # print(fund)
    fund_por = cov(fund, por, tar)
    # for m in range(len(fund_por)):
    #     fund.append(fund_por[m])

    fund1 = []
    fund2 = []
    m = 0
    n = 0

    # print(fund_por)
    while (m < por[0]):
        fund1.append(fund_por[m][0] * 100)

        m = m + 1
    fund.append(fund1)
    while (n < por[1]):
        fund2.append(fund_por[m + n][0] * 100)

        n = n + 1

    fund.append(fund2)
    # print(fund)
    return fund

#计算年化收益率，由其他函数调用
def calculate_average(code, end, period):
    # print(start)
    if end == None:
        # fund_end = fund_net_value.objects.filter(code=i).order_by('-day').first()
        # 和算法统一，使用2019-1-1~2020-1-1
        fund_end = fund_net_value.objects.filter(code=code, day__lt='2020-01-01').order_by('-day').first()

    else:
        fund_end = fund_net_value.objects.filter(code=code, day__lt=end).order_by('-day').first()
    net_value_end = fund_end['net_value']
    end_day = fund_end['day']

    if period == None:
        start_day = end_day.replace(end_day[3], str(int(end_day[3]) - 1), 1)
        # print(end_day)
        fund_start = fund_net_value.objects.filter(code=code, day__gt=start_day).order_by('day').first()
    else:
        end_day = datetime.datetime.strptime(end_day, "%Y-%m-%d")
        # print(end_day)
        start_day = end_day - datetime.timedelta(days=30)
        # print(start_day)
        fund_start = fund_net_value.objects.filter(code=code, day__gt=str(start_day)).order_by('day').first()
    # print(fund_start['day'])
    net_value_start = fund_start['net_value']
    an = (net_value_end - net_value_start) / net_value_start
    return an


def port_info(weights, ret_ave_vector, ret_cov_matrix):
    """计算投资组合的年化收益率，以及年化波动率"""
    port_return = float(np.dot(weights.T, ret_ave_vector))
    port_var = float(np.dot(np.dot(weights.T, ret_cov_matrix), weights))
    port_std = np.sqrt(port_var)
    return port_return, port_std

#计算比例
def cov(fund, por, tar):
    # print(fund)
    an = []

    cov = []
    for i in fund[1]:
        cov1 = []
        an.append(calculate_average(i, None, period=None))
        cov1.append(calculate_average(i, None, period=0) * 12)
        k = 0
        print('testing')
        print(cov1)
        # fund_end = fund_net_value.objects.filter(code=i).order_by('-day').first()
        # 和算法统一，使用2019-1-1~2020-1-1
        fund_end = fund_net_value.objects.filter(code=i, day__lt='2020-01-01').order_by('-day').first()
        day = fund_end['day']
        while k < (por[1] + por[0] - 1):
            day = datetime.datetime.strptime(day, "%Y-%m-%d")
            day = day - datetime.timedelta(days=30)

            day = datetime.datetime.strftime(day, "%Y-%m-%d")
            cov1.append(calculate_average(i, day, period=0) * 12)
            k = k + 1
        cov.append(cov1)
    for j in fund[2]:
        cov1 = []
        an.append(calculate_average(j, None, period=None))
        cov1.append(calculate_average(j, None, period=0) * 12)
        print('testing')
        print(cov1)
        k = 0
        fund_end = fund_net_value.objects.filter(code=j, day__lt='2020-01-01').order_by('-day').first()
        day = fund_end['day']
        while k < (por[1] + por[0] - 1):
            day = datetime.datetime.strptime(day, "%Y-%m-%d")
            day = day - datetime.timedelta(days=30)

            day = datetime.datetime.strftime(day, "%Y-%m-%d")
            cov1.append(calculate_average(j, day, period=0) * 12)
            k = k + 1
        cov.append(cov1)
    print(cov)

    ret_ave1 = np.array(an).reshape((-1, 1))
    ret_cov1 = np.cov(cov)
    ret_cov1 = np.array(ret_cov1)
    for i in range(len(ret_cov1)):
        for j in range(len(ret_cov1[i])):
            # print(ret_cov1[i][j])

            ret_cov1[i][j] = round(ret_cov1[i][j], 3)
            # print(ret_cov1[i][j])
    # print(ret_cov1)
    # print(get_smallest_vol(ret_cov1))
    weights1, weights2 = max_ret(por[0] + por[1], ret_ave1, ret_cov1, tar)
    rate = 0
    for i in weights2:
        if i[0] > 0:
            rate = rate + i[0]
        else:
            i[0] = 0
    for i in weights2:
        if i[0] > 0:
            i[0] = i[0] / rate
        else:
            i[0] = 0
    # print(weights2)
    # print(weights1)
    port_return1, port_std1 = port_info(weights1, ret_ave1, ret_cov1)
    port_return2, port_std2 = port_info(weights2, ret_ave1, ret_cov1)
    print('port_return:')
    print(port_return1, port_return2)
    weights2 = weights2.tolist()

    # print(weights2)
    return weights2

#获取配置列表
def configure_list(request):
    msg = json.loads(request.body)
    account_id = msg['account_id']
    history = Historical_configuration.objects.filter(account_id=account_id)
    res = []
    for i in history:
        res.append({'configure_id': i._id, 'time': i.time, 'assets': i.assets, ' configuration': i.configuration})
    return HttpResponse(json.dumps(res, cls=CustomEncoder), content_type="application/json,charset=utf-8")

#获取配置详情
def get_configure_detail(request):
    msg = json.loads(request.body)
    configure_id = msg['configure_id']
    history = Historical_configuration.objects.get(_id=configure_id)
    fund = []
    por = []
    l = len(history.configuration[1]) + len(history.configuration[2])

    for i in range(len(history.configuration[1])):
        fund.append(history.configuration[1][i]['main_code'])
        por.append(history.configuration[1][i]['rate'])
    for j in range(len(history.configuration[2])):
        fund.append(history.configuration[2][j]['main_code'])
        por.append(history.configuration[1][j]['rate'])
    i = 600
    num = []
    while (i > 0):
        num.append(por)
        i = i - 1
    num.append(fund)
    test = ComputeAnnualizedYield_fund.compute_annualized_yield_fund(fund, num, '2019-01-01', '2020-01-01')
    res = {'configure_id': history._id, 'time': history.time, 'assets': history.assets,
           ' configuration': history.configuration, 'annualized_test': test}
    return HttpResponse(json.dumps(res, cls=CustomEncoder), content_type="application/json,charset=utf-8")


# 配置功能
def testing_configure(request):
    msg = json.loads(request.body)
    _id = msg['account_id']
    try:
        key = msg['key']
    except:
        key = 0
    account = Account.objects.get(_id=_id)
    if account.risk_res == '':
        res = {'code': -1, 'msg': '您还没有填写风险问卷！', 'data': None}
        return HttpResponse(json.dumps(res, cls=CustomEncoder), content_type="application/json,charset=utf-8")
    else:
        risk = account.risk_res
    if account.total_assets == 0:
        res = {'code': -2, 'msg': '您还没有设置资产信息！', 'data': None}
        return HttpResponse(json.dumps(res, cls=CustomEncoder), content_type="application/json,charset=utf-8")
    else:
        assets = account.total_assets
        list0 = ['债券型', '股票型']
        list = []
        list.append(list0)
    if risk == '保守型':
        list.append([80, 20])
        tar = 0.15                      #暂时定为波动率，这个数字是直接取的，没有详细根据
    elif risk == '稳健型':
        list.append([50, 50])
        tar = 0.3
    elif risk == '进取型':
        list.append([20, 80])
        tar = 0.45
    else:
        list = -1
    res0 = {'type_list': list, 'assets': assets}
    try:
        option = msg['option']
        if option == 1:
            fund_list = compute_rate(res0, key)             #对应策略-均分
        elif option == 2:                                   #对应策略-等差
            fund_list = compute_rate_arr(res0, key)

    except:

        fund_list = example(res0, key, tar)                 #对应策略-均值方差模型
        length = len(fund_list[0])
        z = 0
        while z < length:
            sum = 0.0
            for w in range(len(fund_list[length + z + 1])):
                print(fund_list[length + z + 1][w])
                sum = sum + fund_list[length + z + 1][w]
            list[1][z] = sum
            z = z + 1

    print(fund_list)
    data_fund_list = []
    data_fund_list.append(list0)
    fund_detail_list = []
    fund_detail_list.append([fund_list[0]])
    count = len(fund_list[0])
    j = 1
    while (j < count + 1):
        dta_arr = []
        for i in range(len(fund_list[j])):
            fund = fund_info.objects.get(main_code=fund_list[j][i])
            fund_name = fund.name

            net_value_dict = fund_net_value.objects.filter(code=fund_list[j][i]).order_by('-day').first()
            if net_value_dict == None:
                net_value = 0
                start_day = 0  # 588090
            else:
                net_value = net_value_dict.net_value
                sum_value = net_value_dict.sum_value
                start_day = fund_net_value.objects.filter(code=fund_list[j][i]).order_by('day').first().day
            if fund_list[count + j][i] == 0:
                pass
            else:
                # fund_list[j][i]
                dta_arr.append({'name': fund_name, 'main_code': fund_list[j][i], 'start_day': start_day,
                                'net_value': net_value,
                                'rate': fund_list[count + j][i]})
        data_fund_list.append(dta_arr)

        j = j + 1
    time = datetime.datetime.now()
    fund_list = fund_list[:count + 1]
    his_num = Historical_configuration.objects.count()
    if his_num == 0:
        his_id = 1
    else:
        his = Historical_configuration.objects.order_by('-his_id').first()
        his_id = his.his_id + 1
    history = Historical_configuration(configuration=fund_list, time=time, assets=assets, account_id=_id, his_id=his_id)
    history.save()
    configure_id = Historical_configuration.objects.get(his_id=his_id)._id
    result = {'type_list': list, 'fund_list': data_fund_list, 'assets': assets, 'risk_res': risk,
              'configure_id': configure_id}

    res = {'code': 0, 'msg': '配置成功！', 'data': result}
    return HttpResponse(json.dumps(res, cls=CustomEncoder), content_type="application/json,charset=utf-8")
