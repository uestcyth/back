import json
import random
# 
# from aliyunsdkcore.client import AcsClient
# from aliyunsdkcore.request import CommonRequest


def send_sms(phone, code):
    client = AcsClient('LTAIJzcyflVfqnmM', '30AlbuVBEpwXo9obzgBPODfi8lBwwm', 'cn-hangzhou')
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', phone)
    request.add_query_param('SignName', "瑞华控股财富管理服务平台")
    request.add_query_param('TemplateCode', "SMS_174279472")
    request.add_query_param('TemplateParam', {"code": code})

    response = client.do_action_with_exception(request)
    message = json.loads(response)
    # print(str(response, encoding='utf-8'))
    return message['Message']


def generate_code(n=6, alpha=True):
    code = ''
    for i in range(n):
        # 生成随机数字0-9
        num = random.randint(0, 9)
        if alpha:
            # 生成随机大写字符
            upper_alpha = chr(random.randint(65, 90))
            # 生成随机小写字符
            lower_alpha = chr(random.randint(97, 122))
            num = random.choice([num, upper_alpha, lower_alpha])
        code = code + str(num)
    return code
