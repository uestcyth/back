import json
import base64
import os
import zipfile
import datetime

import aiohttp
import asyncio

from django.core.cache import cache
from django.http import HttpResponseBadRequest, HttpResponse
# from fdd_sdk.client.account import AccountClient
# from fdd_sdk.client.client import FddClient
# from fdd_sdk.client.document import DocumentClient
# from fdd_sdk.client.oauth2 import Oauth2Client
# from fdd_sdk.client.sign_task import SignTaskClient
# from fdd_sdk.client.template import TemplateClient
# from fdd_sdk.exception.exceptions import ClientException
# from fdd_sdk.exception.exceptions import ServerException

# from SCCProgram.settings import FDD_ARGS, CRM_URLS
from datamodels.models import Account, Order, \
    Signtask, Contractno, Productofficial, Accountbank


def CustomResponse(string):
    response = HttpResponse(string)
    response.status_code = 500
    return response


def ResponseCodeMesData(code, msg, data):
    result = {'code': code,
              'msg': msg,
              "data": data
              }
    return HttpResponse(json.dumps(result), content_type="application/json,charset=utf-8")


#  解码
def base64_decode(string):
    if string:
        string = base64.b64decode(string).decode("utf-8")
        return string
    else:
        return None


def get_token(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    account_id = data['account_id']

    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    # 获取token
    try:
        result = Oauth2Client.get_token(fdd_client)
        token = result['data']['accessToken']
        # cache.set(account_id, token, 7200)  # 2h有效期
        # print('判断缓存中是否有:', cache.has_key(account_id))
        # print('获取Redis:', cache.get(account_id))
        # print(result)
        # print('token = %s' % result['data']['accessToken'])
        return HttpResponse(token)
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


def get_person_unionid_url(request):
    # 默认请求正式环境
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    token = data['token']
    data = data['data']

    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = AccountClient.get_person_unionid_url(fdd_client, data=data)
        # print(result)
        return HttpResponse(result['data']['nextUrl'])
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


def get_template_detail_by_id(token, data):
    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = TemplateClient.get_template_detail_by_id(fdd_client, data=data)
        # print(result)
        return result['data']['nextUrl']
    except ClientException as e:
        # 客户端初始化异常
        return e
    except ServerException as e:
        # 服务端业务异常
        return e


def create_by_template_id(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    token = data['token']
    pkg_data = data['data']

    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = TemplateClient.create_by_template_id(fdd_client, data=pkg_data)
        # print(result)
        draftId = result['data']['draftId']
        # draftFileId = result['data']['draftFileIds'][0]['draftFileId']
        return HttpResponse(json.dumps(draftId))
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


def agreement_signtasks_create_by_draft_id(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    token = data['token']
    pkg_data = data['data']
    order_id = data['order_id']

    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = SignTaskClient.signtasks_create_by_draft_id(fdd_client, data=pkg_data)
        # print(result)
        taskId = result['data']['taskId']
        # 补充协议签署taskID 写入Oreder表的
        order = Order.objects.filter(id=order_id)
        order.update(agreementtaskid=taskId)

        return HttpResponse(taskId)
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


def signtasks_create_by_draft_id(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    token = data['token']
    pkg_data = data['data']
    order_id = data['order_id']

    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = SignTaskClient.signtasks_create_by_draft_id(fdd_client, data=pkg_data)
        # print(result)
        taskId = result['data']['taskId']
        # signFileId = result['data']['signFileIds'][0]['signFileId']
        # 写入数据库
        order = Order.objects.filter(id=order_id)
        order.update(taskid=taskId)

        """
        # 获取合同编号
        contractno = Contractno.objects.filter(status=0).order_by('no').first()
        order = Order.objects.filter(id=order_id).first()
        Contractno.objects.filter(id=contractno.id).update(status=1, order=order)

        result = {
            "taskId": taskId,
            "contractno": contractno.no
        }
        """
        return HttpResponse(taskId)
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


def get_sign_url(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    token = data['token']
    pkg_data = data['data']

    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = SignTaskClient.get_sign_url(fdd_client, data=pkg_data)
        # print(result)
        signUrl = result['data']['signUrls'][0]['signUrl']
        # miniProgramConfig = result['data']['miniProgramConfig']
        return HttpResponse(signUrl)
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


def fadada_callback(request):
    from urllib.parse import unquote
    from SCCProgram.wsgi import thread_loop_4_order
    # from django.core.cache import cache
    data = request.POST.urlencode()
    data_decode = unquote(data[11:])
    data_json = json.loads(data_decode)
    request_type = request.META.get("HTTP_X_FDD_API_EVENT")
    if request_type == 'sign':
        # 是合同签署的回调
        # cache.set('lpd', "hetong", 200)
        signStatus = data_json['signStatus']
        taskId = data_json['taskId']
        taskStatus = data_json['taskStatus']
        signerId = data_json['signerId']
        unionId = data_json['unionId']
        account_id = ""
        # cache.set('lpd', unionId, 200)
        try:
            account = Account.objects.get(unionid=unionId)
            account_id = account.id
            # cache.set('lpd', '11', 200)
            order = Order.objects.get(taskid=taskId)
            order_id = order.id
            # cache.set('lpd', '22', 200)
            cache.set('lpd', signStatus, 500)
            if signStatus == 2 and signerId == account.unionid:
                # 签署成功
                order.ostatus = '待打款'
                money_4 = json.loads(order.ordercache)['contractInfo']['buyAmount']
                order.money_4 = float(money_4)
                order.save()
                signtasks = Signtask.objects.filter(account_id=account_id, order_id=order_id,type=0)
                if len(signtasks) == 0:
                    Signtask.objects.create(
                        account_id=account_id, order_id=order_id, signstatus=signStatus, party='暂无', type=0)
                # 异步修改CRM
                # 异步执行CRM同步
                url = CRM_URLS['ORDER']
                crm_data = {
                    'Id': order.crm_id,
                    'Ostatus__c': '待打款'
                }
                # 产生coroutine任务sync_order交给子线程thread_loop_4_order执行
                future = asyncio.run_coroutine_threadsafe(sync_order_2_crm_u(url, json.dumps(crm_data)),
                                                          thread_loop_4_order)
        except Exception as e:
            order_agreement_id = ""
            try:
                if signStatus == 2 and signerId == account.unionid:
                    order_agreement = Order.objects.get(agreementtaskid=taskId)
                    order_agreement_id = order_agreement.id
                    signtasks = Signtask.objects.filter(account_id=account_id, order_id=order_agreement_id, type=1)
                    if len(signtasks) == 0:
                        Signtask.objects.create(
                            account_id=account_id, order_id=order_agreement_id, signstatus=signStatus, party='暂无',
                            type=1)
                    Order.objects.filter(id=order_agreement.id).update(issignagreement=1)
            except Exception as e:
                return HttpResponse(e)
            cache.set('lpd', e, 200)
            return HttpResponse("success")
        return HttpResponse("success")
    elif request_type == 'verify':
        # 如果是获取unionId的回调
        account_id = data_json['clientId']
        unionId = data_json['unionId']
        account = Account.objects.get(id=account_id)
        account.unionid = unionId
        account.save()
        # cache.set('lpd', "zhengchang", 200)
        return HttpResponse("success")


# 同步订单表到CRM，未添加日志
async def sync_order_2_crm_u(url, data):
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, data=data) as response:
            text = await response.text()
            return text


def get_unionId(request):
    data = json.loads(request.body)
    try:
        account = Account.objects.get(id=data['account_id'])
        return HttpResponse(account.unionid)
    except Exception as e:
        return HttpResponse("账户无法找到")


def get_redis(request):
    print('sss')
    return HttpResponse(cache.get('lpd'))


def get_by_sign_file_id(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    token = data['token']
    pkg_data = data['data']

    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    fdd_client.set_token(token)
    # 获取个人实名绑定地址
    try:
        result = DocumentClient.get_by_sign_file_id(fdd_client, data=pkg_data)
        # print(result.content)
        # 保存文件
        # save_unzip_pdf(data['data']['taskId'] + '.zip', result.content, data['data']['taskId'])
        # 返回二进制流
        pdf_stream = unzip_stream_pdf(data['data']['taskId'] + '.zip', result.content)

        return HttpResponse(pdf_stream)
    except ClientException as e:
        # 客户端初始化异常
        return HttpResponse(e)
    except ServerException as e:
        # 服务端业务异常
        return HttpResponse(e)


# 保存本地zip和解压的pdf
def save_unzip_pdf(zipname, content, pdfname):
    # 保存文件
    file = open(zipname, 'wb')
    file.write(content)
    file.close()

    # 解压文件
    zip = zipfile.is_zipfile(zipname)
    if zip:
        filez = zipfile.ZipFile(zipname, 'r')
        for file in filez.namelist():
            data = filez.read(file)
            output = open(pdfname + '.pdf', 'wb')
            output.write(data)
            output.close()
            # filez.extract(file)
        filez.close()
        return 0
    else:
        return 1


# 解压zip，返回pdf二进制流，删除本地zip
def unzip_stream_pdf(zipname, content):
    data = ''
    # 保存文件
    file = open(zipname, 'wb')
    file.write(content)
    file.close()

    # 解压文件
    zip = zipfile.is_zipfile(zipname)
    if zip:
        filez = zipfile.ZipFile(zipname, 'r')
        for file in filez.namelist():
            data = filez.read(file)
        filez.close()
    else:
        return '未找到文件'

    # 删除本地zip
    try:
        os.remove(zipname)
        return data
    except FileNotFoundError:
        return '文件删除失败'


# 根据 order_id + account_id 查询 补充协议签署情况
# return 0 未签署 1 已签署 ;;
# 参数 sign_type 默认为1 合同签署 ; 2 补充协议
    #order_id
def get_agreement_signstatus(request):
    from SCCProgram.wsgi import thread_loop_4_order
    data = json.loads(request.body)
    order_id = data['order_id']
    # 默认为1 合同签署 ; 2 补充协议
    sign_type = data['sign_type']
    if sign_type == '':
        sign_type = 1
    if order_id == '':
        return CustomResponse("订单ID不能为空")
    try:
        order = Order.objects.get(id=order_id)
        try:
            account = Account.objects.get(crm_id=order.account_cid)
            unionId = account.unionid
            taskId = order.taskid
            if sign_type == 2 :
                taskId = order.agreementtaskid
            if taskId == '' or taskId is None:
                return HttpResponse(0)
            # 默认请求正式环境
            token = getToken()
            fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
            fdd_client.set_token(token)
            # 获取个人实名绑定地址
            try:
                result = SignTaskClient.get_task_detail_by_task_id(fdd_client, data={
                    "taskId": taskId,
                    "unionId": unionId
                })
                # print(result)
                resultType = 0
                if result["code"] == '100000':
                    list = result["data"]["taskDetails"]
                    for i in list:
                        if i["unionId"] == unionId:
                            signStatus = i["signStatus"]
                            if signStatus == 2:
                                resultType = 1
                                # 默认为1 合同签署 ; 2 补充协议
                                if sign_type == 1:
                                    signtasks = Signtask.objects.filter(account=account.id,order_id=order_id,type =0)
                                    if len(signtasks) == 0:
                                        Signtask.objects.create(
                                            account_id=account.id, order_id=order_id, signstatus=signStatus, party='暂无',
                                            type=0)
                                    order_responsible_cid = order.responsible_cid
                                    #处理投顾更换签单 当回写时 判断订单投顾与当前投顾是否为同一人 若不是则更新为同一人
                                    if order.responsible_cid != account.responsible_id and account.responsible_id:
                                            order.responsible_cid = account.responsible_id
                                            order_responsible_cid = account.responsible_id
                                    # 签署成功
                                    order.ostatus = '待打款'
                                    money_4 = json.loads(order.ordercache)['contractInfo']['buyAmount']
                                    order.money_4 = float(money_4)
                                    order.save()
                                    # 异步执行CRM同步
                                    url = CRM_URLS['ORDER']
                                    crm_data = {
                                        'Id': order.crm_id,
                                        'Ostatus__c': '待打款',
                                        'Responsible__c': order_responsible_cid
                                    }
                                    # 产生coroutine任务sync_order交给子线程thread_loop_4_order执行
                                    future = asyncio.run_coroutine_threadsafe(
                                        sync_order_2_crm_u(url, json.dumps(crm_data)),
                                        thread_loop_4_order)
                                elif sign_type == 2:
                                    order_agreement = Order.objects.get(agreementtaskid=taskId)
                                    order_agreement_id = order_agreement.id
                                    signtasks = Signtask.objects.filter(account_id=account.id,
                                                                        order_id=order_agreement_id, type=1)
                                    if len(signtasks) == 0:
                                        Signtask.objects.create(
                                            account_id=account.id, order_id=order_agreement_id, signstatus=signStatus,
                                            party='暂无',
                                            type=1)
                                    Order.objects.filter(id=order_agreement.id).update(issignagreement=1)
                # 0待他人签 1待我签 2已签署 3已驳回 4已撤销 5已转发 6无需签
                return HttpResponse(resultType)
            except ClientException as e:
                print(e)
                # 客户端初始化异常
                # return HttpResponse(e)
                return HttpResponse(0)
            except ServerException as e:
                print(e)
                return HttpResponse(0)
                # 服务端业务异常
                # return HttpResponse(e)
        except Exception as e:
            print("用户不存在")
            return HttpResponse(0)
    except Exception as e:
        print("用户不存在")
        return HttpResponse(0)



def get_signstatus(request):
    data = json.loads(request.body)
    if not data:
        return HttpResponseBadRequest("Request Error")
    order_id = data['order_id']
    account_id = data['account_id']
    try:
        signtask = Signtask.objects.get(order_id=order_id, account_id=account_id)
        # 0待他人签 1待我签 2已签署 3已驳回 4已撤销 5已转发 6无需签
        status = None
        if signtask.signstatus == 0:
            status = '待他人签'
        elif signtask.signstatus == 1:
            status = '待我签'
        elif signtask.signstatus == 2:
            status = '已签署'
        elif signtask.signstatus == 3:
            status = '已驳回'
        elif signtask.signstatus == 4:
            status = '已撤销'
        elif signtask.signstatus == 5:
            status = '已转发'
        elif signtask.signstatus == 6:
            status = '无需签'
        return HttpResponse(status)
    except Signtask.DoesNotExist:
        return CustomResponse("找不到签署状态")


def get_contractno(request):
    data = json.loads(request.body)
    try:
        if data is None:
            return HttpResponseBadRequest("参数不符合规范")
        type = data['type']
        if type is None:
            return HttpResponseBadRequest("合同编号类型不能为空")
        contractno = Contractno.objects.filter(status=0, type=type).order_by('no').first()
        if contractno is None:
            return HttpResponseBadRequest("未找到该类型的合同编号")
        Contractno.objects.filter(id=contractno.id).update(status=1)
        return HttpResponse(contractno.no)
    except Signtask.DoesNotExist:
        return CustomResponse("参数不符合规范")
# 法大大 退款协议 链接
def fdd_sign_refund_contract(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    env = data['env_type']
    sign_type = "refund"
    contract_data = ""
    if len(env) == 0:
        env = "dev"
    return online_sign_contract(order_id,env,contract_data,sign_type)


# 法大大签署合同链接
#环境说明 online 线上  dev 为测试环境； 默认为测试环境
def fdd_sign_contract(request):
    data = json.loads(request.body)
    order_id = data['order_id']
    env = data['env_type']
    contract_data = data['contract_data']
    if len(env) == 0:
        env = "dev"
    sign_type = "sign"
    return online_sign_contract(order_id,env,contract_data,sign_type)


def online_sign_contract(order_id,env,contract_data,sign_type):
    try:
        order = Order.objects.get(id=order_id)
        try:
            account = Account.objects.get(crm_id=order.account_cid)
            try:
                product = Productofficial.objects.get(crm_id=order.product_cid)
                token = getToken()
                if token is not None:
                    # 设置templateFiles 合同模板所需数据
                    data = productTemplateData(env, account, product, order, contract_data,sign_type)
                    # 默认请求正式环境
                    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
                    fdd_client.set_token(token)
                    # 草稿模板 ID  '1610090094815140108'
                    draftId = createByTemplateId(fdd_client, data["data"])
                    if draftId["state"] != "true":
                        return ResponseCodeMesData(400, "服务器繁忙，请稍后再试！", draftId["data"])
                    draftId = draftId["data"]
                    # 通过模板ID 发起签署任务
                    task_data = {
                        "taskSubject": '签署',
                        "draftId": draftId,
                        "status": 'sent',
                        "sort": 2,
                        "signers": data["singers"]
                    }
                    taskjg = signtasksCreateByDraftId(fdd_client, task_data, order_id)
                    if taskjg["state"] != "true":
                        return ResponseCodeMesData(400, "服务器繁忙，请稍后再试！", taskjg["data"])
                    taskId = taskjg["data"]
                    # 获取签署链接
                    data = {
                        "taskId": taskId,
                        "unionId": account.unionid,
                        "miniProgramSign": 1
                    }
                    signUrl = getSignUrl(fdd_client, data)
                    if signUrl["state"] != "true":
                        return ResponseCodeMesData(400, "服务器繁忙，请稍后再试！", signUrl["data"])
                    # 为订单表增加法大大签署taskid
                    if sign_type == 'sign':
                        Order.objects.filter(id=order_id).update(taskid=taskId)
                    elif sign_type == 'refund':
                        Order.objects.filter(id=order_id).update(agreementtaskid=taskId)
                    return ResponseCodeMesData(200, "请求成功", signUrl["data"])
                return ResponseCodeMesData(400, "Token获取失败")
            except Order.DoesNotExist:
                return ResponseCodeMesData(400, "产品不存在")
        except Productofficial.DoesNotExist:
            return ResponseCodeMesData(400, "用户不存在")
    except Account.DoesNotExist:
        return ResponseCodeMesData(400, "订单不存在")

# 获取签署链接
def getSignUrl(fdd_client, pkg_data):
    # 默认请求正式环境
    # fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    # fdd_client.set_token(token)
    # 获取个人实名绑定地址
    state = "false"
    data = ""
    try:
        result = SignTaskClient.get_sign_url(fdd_client, data=pkg_data)
        # print(result)
        signUrl = result['data']['signUrls'][0]['signUrl']
        # miniProgramConfig = result['data']['miniProgramConfig']
        state = "true"
        data = signUrl
    except ClientException as e:
        # 客户端初始化异常
        data = e.message
    except ServerException as e:
        # 服务端业务异常
        data = e.message
    result = {
        "state": state,
        "data": data
    }
    return result

# 根据草稿id 发起签署任务
def signtasksCreateByDraftId(fdd_client, pkg_data, order_id):
    # 获取个人实名绑定地址
    state = "false"
    data = ""
    try:
        result = SignTaskClient.signtasks_create_by_draft_id(fdd_client, data=pkg_data)
        # print(result)
        taskId = result['data']['taskId']
        # signFileId = result['data']['signFileIds'][0]['signFileId']
        # order = Order.objects.filter(id=order_id)
        # order.update(taskid=taskId)
        state = "true"
        data = taskId
    except ClientException as e:
        # 客户端初始化异常
        data = e.message
    except ServerException as e:
        # 服务端业务异常
        data = e.message
    result = {
        "state": state,
        "data": data
    }
    return result


# 产品设置 合同参数 公用方法
def productTemplateData(env, account, product, order, contract_data,sign_type):
    if env == "online":
        from Fadada.faddSet import FDD_SIGN_TEMPLATE_UNIONID_ONLINE as FDD_SIGN_TEMPLATE_UNIONID
    else:
        from Fadada.faddSet import FDD_SIGN_TEMPLATE_UNIONID_DEV as FDD_SIGN_TEMPLATE_UNIONID
    formFields = ""
    singers = ""
    fddSetting = ""
    documentFileName= product.name
    # 金朗瑞景一号 ------------------------------
    if product.id == 1318:
        fddSetting = FDD_SIGN_TEMPLATE_UNIONID["ruijin1hao"]
        # 签署电子合同
        if sign_type == "sign":
            formFields = ruijin1haoTemplateData(account, product, order, contract_data)
            singers = getRuijin1haoSinger(account.unionid, fddSetting)
    # 融创一期 ------------------------
    elif product.id == 1361 or product.id == 1362:
        # 签署电子合同
        if sign_type == "sign":
            fddSetting = FDD_SIGN_TEMPLATE_UNIONID["rongchuang"]
            formFields = rongchuangTemplateData(account, product, contract_data)
        # 签署 退款协议
        else:
            documentFileName = "补充协议"
            fddSetting = FDD_SIGN_TEMPLATE_UNIONID["rongchuang_tkxy"]
            formFields = rongchuangTemplateData_refund(account)
        singers = getRongchuangSinger(account.unionid, fddSetting)
    # 融创二期 ---------------------------- 3个月1371  4个月1370
    elif product.id == 1371 or product.id == 1370:
        fddSetting = FDD_SIGN_TEMPLATE_UNIONID["rongchuang_2qi"]
        # 签署电子合同
        if sign_type == "sign":
            formFields = rongchuang_2qiTemplateData(account, product, contract_data)
            singers = getrongchuang_2qiSinger(account.unionid, fddSetting)

    # 拼接返回结果 -----------------------------
    result = {
        "data": {
            'templateFiles': [
                {
                    'documentFileName': documentFileName,
                    "formFields": formFields,
                    "templateFileId": fddSetting["templateFileId"]
                }
            ],
            "templateId": fddSetting["templateId"]
        },
        "singers": singers
    }
    return result

# 瑞景一号产品 合同参数配置 如下参数由前端传入
# moneyup，认购金额大写
# moneylow：认购金额小写
# amount 资产总额
# education 学历
# signphone 签约手机号
# work 工作
# age  年龄
# mark  分数
# baoshou  保守型
# wenjiang 稳健型
# pingheng 平衡型
# chengzhang 成长型
# jinqu 进取型
def ruijin1haoTemplateData(account, product, order, reqdata):
    chengluo = '本人承诺以上所填全部信息为本人真是的意思表示，接受评估意见，并了解自己的风险承受类型和适合参与的交易类型、适合投资的金融资产。'
    # 合同编号
    contractono = getContractno(0)
    idcard = base64_decode(account.certificatenumber)
    bank = Accountbank.objects.filter(crm_id=reqdata["bankId"]).first()
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    formFields = {
        "name": account.name,
        "tel": reqdata["signphone"],
        "code": 'G200714001000000',
        "contractono": contractono,
        "person": 'true',
        "amount": reqdata["amount"],
        "qualified": 'true',
        "idtype": '身份证',
        "idnumber": idcard,
        "education": reqdata["education"],
        "work": reqdata["work"],
        "age": reqdata["age"],
        "mark": reqdata["mark"],
        "baoshou": reqdata["baoshou"],
        "wenjiang":  reqdata["wenjiang"],
        "pingheng": reqdata["pingheng"],
        "chengzhang":  reqdata["chengzhang"],
        "jinqu":  reqdata["jinqu"],
        "chengnuo": chengluo,
        "idcard": "true",
        "name2": account.name,
        "tel2": reqdata["signphone"],
        "idnumber2": idcard,
        "address": "",
        "email": "",
        "moneyup":  reqdata["moneyup"],
        "moneylow": reqdata["moneylow"],
        "accountname": account.name,
        "bankname": bank.bankname,
        "bankno": bank.bankno,
        "mark2":  reqdata["mark"],
        "baoshou2": reqdata["baoshou"],
        "wenjiang2": reqdata["wenjiang"],
        "pingheng2": reqdata["pingheng"],
        "chengzhang2": reqdata["chengzhang"],
        "jinqu2": reqdata["jinqu"],
        "chengnuo2": chengluo,
        "year1": year,
        "month1": month,
        "day1": day,
        "year2": year,
        "month2": month,
        "day2": day,
        "year3": year,
        "month3": month,
        "day3": day,
        "year4": year,
        "month4": month,
        "day4": day,
        "year5": year,
        "month5": month,
        "day5": day,
        "year6": year,
        "month6": month,
        "day6": day,
        "year7": year,
        "month7": month,
        "day7": day
    }
    formFields = dict(formFields, **reqdata["wq"])
    return formFields

def rongchuangTemplateData_refund(account):
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    formFields = {
        "month1": month,
        "day1": day,
        "sfz1": base64_decode(account.certificatenumber),
        "day2": "31",
        "month3": month,
        "day3": day,
        "month4": month,
        "day4": day,
        "month5": month,
        "day5": day,
        "month6": month,
        "day6": day
    }
    return formFields

# 融创产品合同签署参数配置
def rongchuangTemplateData(account, product, reqdata):
    prodMonthName = "认购期限2个月"
    if product.id == 1362:
        prodMonthName = "认购期限3个月"
    srqx2s = product.id == 1361
    srqx3s = product.id == 1362
    # 合同编号
    contractono = getContractno(1)
    bank = Accountbank.objects.filter(crm_id=reqdata["bankId"]).first()
    idcard = base64_decode(account.certificatenumber)
    chengluo = '本人承诺以上所填全部信息为本人真是的意思表示，接受评估意见，并了解自己的风险承受类型和适合参与的交易类型、适合投资的金融资产。'
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    formFields = {
        "srqx2": srqx2s,
        "srqx3": srqx3s,
        "zrr": "true",
        "srfmc": account.name,
        "sfz": "true",
        "srfhz": "false",  # 护照
        "srfgatxz": "false",  # 受让人港澳通行证
        "jgz": "false",  # 军官证
        "contractono": contractono,
        "rgjedx": reqdata["moneyup"],  # 认购金额大写
        "rgjexx": reqdata["moneylow"],  # 认购金额小写
        "prodMonth": prodMonthName,
        "yhzhmc": bank.accountname,  # 银行卡账户名称
        "yhzh": bank.bankno,  # 银行账号
        "yhkhh": bank.bankname,  # 银行开户行
        "yhzhmc2": bank.accountname,  # 银行卡账户名称
        "yhzh2": bank.bankno,  # 银行账号
        "yhkhh2": bank.bankname,  # 银行开户行
        "yhzhmc3": bank.accountname,  # 银行卡账户名称
        "yhzh3": bank.bankno,  # 银行账号
        "yhkhh3": bank.bankname,  # 银行开户行
        "amount": reqdata["amount"],
        "person": 'true',
        "qualified": 'true',
        "idtype": '身份证',
        "idnumber": idcard,  # 证件号码，
        "education": reqdata["education"],
        "tzrzjhm": idcard,  # 投资人证件号码
        "tzrphone": reqdata["signphone"],  # 投资人联系电话
        "work": reqdata["work"],
        "age": reqdata["age"],
        "name": account.name,
        "tel": reqdata["signphone"],
        "mark2": reqdata["mark"],
        "mark1": reqdata["mark"],
        "baoshou2": reqdata["baoshou"],
        "wenjiang2": reqdata["wenjiang"],
        "pingheng2": reqdata["pingheng"],
        "chengzhang2": reqdata["chengzhang"],
        "jinqu2": reqdata["jinqu"],
        "baoshou1": reqdata["baoshou"],
        "wenjiang1": reqdata["wenjiang"],
        "pingheng1": reqdata["pingheng"],
        "chengzhang1": reqdata["chengzhang"],
        "jinqu1": reqdata["jinqu"],
        "chengnuo2": chengluo,
        "chengnuo1": chengluo,
        "year1": year,
        "month1": month,
        "day1": day,
        "year2": year,
        "month2": month,
        "day2": day,
        "year3": year,
        "month3": month,
        "day3": day,
        "year4": year,
        "month4": month,
        "day4": day,
        "year5": year,
        "month5": month,
        "day5": day,
        "year6": year,
        "month6": month,
        "day6": day,
        "year7": year,
        "month7": month,
        "day7": day,
        "year8": year,
        "month8": month,
        "day8": day,
        "year9": year,
        "month9": month,
        "day9": day
    }
    formFields = dict(formFields, **reqdata["wq"])
    return formFields

#融创购房尾款资产转让计划二期（3个月） & （4个月)
def rongchuang_2qiTemplateData(account, product, reqdata):
    #3个月1371  4个月1370
    srqx4s = product.id == 1370
    srqx3s = product.id == 1371
    # 合同编号
    contractono = getContractno(2)
    bank = Accountbank.objects.filter(crm_id=reqdata["bankId"]).first()
    idcard = base64_decode(account.certificatenumber)
    chengluo = '本人承诺以上所填全部信息为本人真是的意思表示，接受评估意见，并了解自己的风险承受类型和适合参与的交易类型、适合投资的金融资产。'
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    day = datetime.datetime.now().day
    formFields = {
        "contractono": contractono,
        "yhzhmc": bank.accountname,  # 银行卡账户名称
        "yhzh": bank.bankno,  # 银行账号
        "yhkhh": bank.bankname,  # 银行开户行
        "person": 'true',
        "qualified": 'true',
        "amount": reqdata["amount"],
        "name": account.name,
        "tel": reqdata["signphone"],
        "idtype": '身份证',
        "idnumber": idcard,  # 证件号码，
        "education": reqdata["education"],
        "work": reqdata["work"],
        "age": reqdata["age"],
        "mark1": reqdata["mark"],
        "baoshou1": reqdata["baoshou"],
        "wenjiang1": reqdata["wenjiang"],
        "pingheng1": reqdata["pingheng"],
        "chengzhang1": reqdata["chengzhang"],
        "jinqu1": reqdata["jinqu"],
        "chengnuo1": chengluo,
        "zrr": "true",
        "srfmc": account.name,
        "sfz": "true",
        "srfhz": "false",  # 护照
        "srfgatxz": "false",  # 受让人港澳通行证
        "jgz": "false",  # 军官证
        "tzrzjhm": idcard,  # 投资人证件号码
        "tzrphone": reqdata["signphone"],  # 投资人联系电话
        "rgjedx": reqdata["moneyup"],  # 认购金额大写
        "rgjexx": reqdata["moneylow"],  # 认购金额小写
        "srqx4": srqx4s,
        "srqx3": srqx3s,
        "yhzhmc2": bank.accountname,  # 银行卡账户名称
        "yhzh2": bank.bankno,  # 银行账号
        "yhkhh2": bank.bankname,  # 银行开户行
        "mark2": reqdata["mark"],
        "baoshou2": reqdata["baoshou"],
        "wenjiang2": reqdata["wenjiang"],
        "pingheng2": reqdata["pingheng"],
        "chengzhang2": reqdata["chengzhang"],
        "jinqu2": reqdata["jinqu"],
        "chengnuo2": chengluo,
        "yhzhmc3": bank.accountname,  # 银行卡账户名称
        "yhzh3": bank.bankno,  # 银行账号
        "yhkhh3": bank.bankname,  # 银行开户行
        "year1": year,
        "month1": month,
        "day1": day,
        "month2": month,
        "day2": day,
        "month3": month,
        "day3": day,
        "month4": month,
        "day4": day,
        "month5": month,
        "day5": day,
        "month6": month,
        "day6": day,
        "month7": month,
        "day7": day,
        "month8": month,
        "day8": day,
        "month9": month,
        "day9": day

    }
    formFields = dict(formFields, **reqdata["wq"])
    return formFields

def createByTemplateId(fdd_client, pkg_data):
    # 默认请求正式环境
    # fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    # fdd_client.set_token(token)
    # 获取个人实名绑定地址
    state = "false"
    try:
        result = TemplateClient.create_by_template_id(fdd_client, data=pkg_data)
        # print(result)
        draftId = result['data']['draftId']
        # draftFileId = result['data']['draftFileIds'][0]['draftFileId']
        state = "true"
        data = draftId
    except ClientException as e:
        # 客户端初始化异常
        data = e.message
    except ServerException as e:
        data = e.message
        # 服务端业务异常
    result = {
        "state": state,
        "data": data
    }
    return result

# 转换大写
def moneyToCapital(num):
    #  fuhao = ""
    #  text = num + ""
    # if text.indexOf("-") > -1:
    #     num = text.replace("-", "")
    #     fuhao = "负"
    #
    #  money1 = new Number(num)
    #  monee = Math.round(money1 * 100).toString(10)
    #  leng = monee.length
    #  monval = ""
    # for (let i = 0; i < leng; i++):
    #     monval = monval + this.to_upper(monee.charAt(i)) + this.to_mon(leng - i - 1)
    # return fuhao + this.repace_acc(monval)

    return None


def getContractno(type):
    contractno = Contractno.objects.filter(status=0, type=type).order_by('no').first()
    if contractno is None:
        return HttpResponseBadRequest("未找到该类型的合同编号")
    Contractno.objects.filter(id=contractno.id).update(status=1)
    return contractno.no


def getToken():
    # 默认请求正式环境
    fdd_client = FddClient(FDD_ARGS['APPID'], FDD_ARGS['APPKEY'], request_url=FDD_ARGS['URL'])
    # 获取token
    try:
        result = Oauth2Client.get_token(fdd_client)
        token = result['data']['accessToken']
        return token
    except Exception as e:
        # 客户端初始化异常
        return None


# 瑞景一号电子签单 签署方配置
def getRuijin1haoSinger(unionId, fddConfig):
    singers = [
            {
                "unionId": fddConfig["JIA"],
                "templateRoleName": '甲方（转让方）',
                "signIntendWay": 1,
                "authorizedUnionId": fddConfig["AUTH_JIA"]
            },
            {
                "unionId": fddConfig["YI"],
                "templateRoleName": '乙方（管理人）',
                "signIntendWay": 1,
                "authorizedUnionId": fddConfig["AUTH_YI"]
            },
            {
                "unionId": unionId,
                "templateRoleName": '丙方（受让方）',
                "signIntendWay": 1
            },
            {
                "unionId": fddConfig["DING"],
                "templateRoleName": '丁方（投资顾问）',
                "signIntendWay": 1,
                "authorizedUnionId": fddConfig["AUTH_DING"]
            }
        ]
    return singers


#融创购房尾款资产转让计划二期（3个月） & （4个月)
def getrongchuang_2qiSinger(unionId, fddConfig):
    singers = [
        {
            "unionId": fddConfig["unionIdjia"],
            "templateRoleName": '甲方（转让方）',
            "signIntendWay": 1,
            "authorizedUnionId": fddConfig["authorizedUnionIdjia"]
        },
        {
            "unionId": fddConfig["unionIdyi"],
            "templateRoleName": '乙方（管理人）',
            "signIntendWay": 1,
            "authorizedUnionId": fddConfig["authorizedUnionIdyi"]
        },
        {
            "unionId": unionId,
            "templateRoleName": '丙方（受让方）',
            "signIntendWay": 1
        },
        {
            "unionId": fddConfig["unionIdding"],
            "templateRoleName": '丁方（投资顾问）',
            "signIntendWay": 1,
            "authorizedUnionId": fddConfig["authorizedUnionIdding"]
        },
        {
            'unionId': fddConfig["prod2authorizedUnionIdFR"],
            'templateRoleName': '法人',
            'signIntendWay': 1
        }
    ]
    return singers

# 融创电子签单 签署方配置
def getRongchuangSinger(unionId, fddConfig):
    singers = [
        {
            "unionId": fddConfig["unionIdjia"],
            "templateRoleName": '甲方（转让方）',
            "signIntendWay": 1,
            "authorizedUnionId": fddConfig["authorizedUnionIdjia"]
        },
        {
            "unionId": fddConfig["unionIdyi"],
            "templateRoleName": '乙方（管理人）',
            "signIntendWay": 1,
            "authorizedUnionId": fddConfig["authorizedUnionIdyi"]
        },
        {
            "unionId": unionId,
            "templateRoleName": '丙方（受让方）',
            "signIntendWay": 1
        },
        {
            "unionId": fddConfig["unionIdding"],
            "templateRoleName": '丁方（投资顾问）',
            "signIntendWay": 1,
            "authorizedUnionId": fddConfig["authorizedUnionIdding"]
        },
        {
            'unionId': fddConfig["prod2authorizedUnionIdFR"],
            'templateRoleName': '法人',
            'signIntendWay': 1
        }
    ]
    return singers
