#法大大 环境APPID配置
FDD_SIGN_APPID = {
    "online_appId": "wxa1439f77c6d06a15",
    "dev_appId": "wxa1439f77c6d06a15"
}
#法大大 模板id配置 签署人配置__测试环境
FDD_SIGN_TEMPLATE_UNIONID_DEV = {
    "ruijin1hao": {
        "templateId": "1606368327383149642",
        "templateFileId": "1606368332260130755",
        "JIA": "0150ac229df64ae0b04f861af4f5e23d",
        "AUTH_JIA": "7a38023fad484927bd29f343c8de561b",
        "YI": "0150ac229df64ae0b04f861af4f5e23d",
        "AUTH_YI": "b5ceb429246b44bebf983a8924dad8af",
        "DING": "0150ac229df64ae0b04f861af4f5e23d",
        "AUTH_DING": "b5ceb429246b44bebf983a8924dad8af"
    },
    "rongchuang": {
        "templateId": "1609042234738122369",
        "templateFileId": "1609042241994114639",
        "unionIdjia": "0150ac229df64ae0b04f861af4f5e23d",
        "authorizedUnionIdjia": "7a38023fad484927bd29f343c8de561b",
        "unionIdyi": "0150ac229df64ae0b04f861af4f5e23d",
        "authorizedUnionIdyi": "55c29f81b4f94a59bc6df738f6a82cc2",
        "unionIdding": "0150ac229df64ae0b04f861af4f5e23d",
        "authorizedUnionIdding": "55c29f81b4f94a59bc6df738f6a82cc2",
        "prod2authorizedUnionIdFR": "b3462ce976c34d2b808fb5a67289908e"
    },
    "rongchuang_tkxy": {
        "templateId": "1610608673921141208",
        "templateFileId": "1610608674900134540",
        "unionIdjia": "0150ac229df64ae0b04f861af4f5e23d",
        "authorizedUnionIdjia": "7a38023fad484927bd29f343c8de561b",
        "unionIdyi": "0150ac229df64ae0b04f861af4f5e23d",
        "authorizedUnionIdyi": "55c29f81b4f94a59bc6df738f6a82cc2",
        "unionIdding": "0150ac229df64ae0b04f861af4f5e23d",
        "authorizedUnionIdding": "55c29f81b4f94a59bc6df738f6a82cc2",
        "prod2authorizedUnionIdFR": "b3462ce976c34d2b808fb5a67289908e"
    },
    "rongchuang_2qi": {
        "templateId": "1609069493990173425",
        "templateFileId": "1609069506875111729",
        "unionIdjia": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdjia": "a19af880939a4224b354bd4dec0aa7b5",
        "unionIdyi": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdyi": "8895b9ecc92246fcb4d53a5143b70140",
        "unionIdding": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdding": "8895b9ecc92246fcb4d53a5143b70140",
        "prod2authorizedUnionIdFR": "24e0183ec48e417ba3120332a9e06d25"
    }
}

#法大大 模板id配置 签署人配置__线上环境
FDD_SIGN_TEMPLATE_UNIONID_ONLINE = {
    "ruijin1hao": {
        "templateId": "1606726246526140747",
        "templateFileId": "1606726262291178217",
        "JIA": "6fb2b176f95940bd81f47eb197dc453e",
        "AUTH_JIA": "a19af880939a4224b354bd4dec0aa7b5",
        "YI": "6fb2b176f95940bd81f47eb197dc453e",
        "AUTH_YI": "b958aee0f0374d0fb3dc27f44e1a16da",
        "DING": "6fb2b176f95940bd81f47eb197dc453e",
        "AUTH_DING": "b958aee0f0374d0fb3dc27f44e1a16da"
    },
    "rongchuang": {
        "templateId": "1609069493990173425",
        "templateFileId": "1609069506875111729",
        "unionIdjia": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdjia": "a19af880939a4224b354bd4dec0aa7b5",
        "unionIdyi": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdyi": "8895b9ecc92246fcb4d53a5143b70140",
        "unionIdding": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdding": "8895b9ecc92246fcb4d53a5143b70140",
        "prod2authorizedUnionIdFR": "24e0183ec48e417ba3120332a9e06d25"
    },
    "rongchuang_tkxy": {
        "templateId": "1610694643359147715",
        "templateFileId": "1610694678925121001",
        "unionIdjia": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdjia": "a19af880939a4224b354bd4dec0aa7b5",
        "unionIdyi": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdyi": "8895b9ecc92246fcb4d53a5143b70140",
        "unionIdding": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdding": "8895b9ecc92246fcb4d53a5143b70140",
        "prod2authorizedUnionIdFR": "24e0183ec48e417ba3120332a9e06d25"
    },
    "rongchuang_2qi": {
        "templateId": "1611888851785189399",
        "templateFileId": "1611888869903153033",
        "unionIdjia": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdjia": "a19af880939a4224b354bd4dec0aa7b5",
        "unionIdyi": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdyi": "8895b9ecc92246fcb4d53a5143b70140",
        "unionIdding": "6fb2b176f95940bd81f47eb197dc453e",
        "authorizedUnionIdding": "8895b9ecc92246fcb4d53a5143b70140",
        "prod2authorizedUnionIdFR": "24e0183ec48e417ba3120332a9e06d25"
    }
}