from django.urls import path

from . import views

app_name = 'Fadada'
urlpatterns = [
    path('get_token/', views.get_token, name='get_token'),
    path('create_by_template_id/', views.create_by_template_id, name='create_by_template_id'),
    path('signtasks_create_by_draft_id/', views.signtasks_create_by_draft_id, name='signtasks_create_by_draft_id'),
    path('get_sign_url/', views.get_sign_url, name='get_sign_url'),
    path('fadada_callback/', views.fadada_callback, name='fadada_callback'),
    path('get_unionId/', views.get_unionId, name='get_unionId'),
    path('get_person_unionid_url/', views.get_person_unionid_url, name='get_person_unionid_url'),
    path('get_by_sign_file_id/', views.get_by_sign_file_id, name='get_by_sign_file_id'),
    path('get_signstatus/', views.get_signstatus, name='get_signstatus'),
    path('get_agreement_signstatus/', views.get_agreement_signstatus, name='get_agreement_signstatus'),
    path('get_contractno/', views.get_contractno, name='get_contractno'),
    path('fdd_sign_contract/', views.fdd_sign_contract, name='fdd_sign_contract'),
    path('fdd_sign_refund_contract/', views.fdd_sign_refund_contract, name='fdd_sign_refund_contract'),
    #签署补充协议 接口
    path('agreement_signtasks_create_by_draft_id/', views.agreement_signtasks_create_by_draft_id, name='agreement_signtasks_create_by_draft_id'),
]
