"""Quantitative URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('account/', include('apps.account.urls')),
    path('fund/', include('apps.fund.urls')),
    path('workflow/', include('apps.workflow.urls')),
    path('users/', include('Users.urls')),
    path('product/', include('Product.urls')),
    path('assets/', include('Assets.urls')),
    path('commission/', include('Commission.urls')),
    path('backtest/', include('Backtest.urls')),
    path('channel/', include('Channel.urls')),
    path('SMS/', include('SMS.urls')),
    path('fadada/', include('Fadada.urls')),
    path('algorithm/', include('algorithm.urls')),
]
